<?php

function show_wuss_data_content()
{
	if ( !current_user_can( 'manage_wuss' ) )
	{
		return __('You do not have sufficient permissions to access this page.');
		wp_die('');
	}

	include_once( plugin_dir_path( __DIR__ ).'classes/WussImages.class.php' );

	global $wpdb;
	$output = '<h2>User Data Management</h2>';

	$gid = Postedi("gid");
	$uid = Postedi("uid");
	$sub = Posted('menu_sub');
	if ($sub == '') $sub = "User Data";
	define('MENUSUB', $sub);

	//was this form submitted? Conversely: is there any action to perform?
	if (isset($_POST['wuss_data_action']))
	{
		$table = wuss_prefix."data";
		switch($_POST['wuss_data_action'])
		{
			case "Update":
				$wpdb->update($table, array('fval' => $_POST['fval']),
									  array('uid' => $uid,
										    'gid' => $gid,
										    'cat' => $_POST['cat'],
										    'fid' => $_POST['fid']));
				break;

			case "Delete":
				$image = new WussImages($uid);
				$image->DeleteImage($_POST['fval']);
				$wpdb->delete($table, array('uid' => $uid, 'gid' => $gid, 'cat' => $_POST['cat'], 'fid' => $_POST['fid']));
//				$wpdb->delete($table, array('uid' => $uid, 'gid' => $gid, 'cat' => $_POST['fval']));
				break;

			case "Ban":
				$ban_uid = $uid;
				$ban_gid = $gid;
				$field = $ban_gid."_account_status";
				update_user_meta($ban_uid, $field, 2);
				break;

			case "Suspend for":
				$ban_uid = Postedi("uid");
				$ban_gid = $gid;
				$duration = Postedi("suspend_minutes") *  60;
				$duration += Postedi("suspend_hours") * 3600;
				$duration += Postedi("suspend_days") * 86400;
				update_user_meta($ban_uid, "{$ban_gid}_account_status", 1);
				update_user_meta($ban_uid, "{$ban_gid}_suspension_date", time() + $duration);
				break;

			case "Lift ban / suspension":
				$ban_uid = Postedi("uid");
				$ban_gid = $gid;
				$field = $ban_gid."_account_status";
				update_user_meta($ban_uid, $field, 0);
				break;

			case "Find user":
				$a_user = new WussUsers($gid);
				if ($a_user)
				{
					$username_to_find = Posted("find_user_value");
					foreach($a_user->users as $user)
					{
						if ($user->user_login == $username_to_find) {
							$_POST['uid'] = $_REQUEST['uid'] = $user->ID;
							break;
						}
					}
				}
				break;
		}
	}
	//in case it was updated above
	$uid = Postedi('uid');

	//first determine if there are games
	$games = new WussGames();
	if ($gid == 0 || $gid == '')
	    $gid = $games->GetFirstGameID();

	$games_dropdown = '<form method=post style="width:220px">'
		. '<input type=hidden name="menu_tab" value="' . MENUTAB . '">'
		. '<input type=hidden name="menu_sub" value="' . MENUSUB . '">'
		. $games->GamesDropDownList($gid, true, "gid", "", '','submit')
		. '</form><br>';

	$query = "SELECT ID,user_login,user_email,user_url,user_registered,display_name FROM $wpdb->users ORDER BY user_login";
	$users = $wpdb->get_results($query);
	array_unshift($users, null);

	$users_obj = new WussUsers($gid);

	//prepare the table row so the user is still auto determined
	//keep as a separate string as this entire row might not be shown
	$wuss_users_list = '<table><tr><td>'
	                   . '<form method="POST">'
	                   . $users_obj->DropDownList($gid, $uid, Posted('ufilter'), "GAME DATA")
	                   . $users_obj->FilterDropDown(Posted('ufilter'))
					   . '<input type="hidden" name="menu_sub" value="'. MENUSUB .'"'
	                   . '</form>'
	                   . '</td><td>'
	                   . $users_obj->SearchField($gid)
	                   . '</td></tr></table>';
	if (MENUSUB == 'Shared Data')
		$uid = 0;

	$output .= '<table class="wuss_setting_table wuss_table_striped" style="width:850px">'
	           .  '<tr><td valign="top" class="wuss_settings_description">'
	           .  'Select Game'
	           .  '</td><td valign="top" class="wuss_settings_values">'
	           .  (($games->GameCount() > 0) ? $games_dropdown : '')
	           .  '</td></tr>';

	$output .= '
		<tr><td valign="top" colspan="2" style="border: 1px solid;">
		<form method="post">
		<input type="hidden" name="gid" value="'.$gid.'">
		<input type="hidden" name="menu_tab" value="'.MENUTAB.'">
		<input type="'.(MENUSUB == 'User Data' ? 'button': 'submit').'" name="menu_sub" value="User Data" class="button-'.(MENUSUB == 'User Data' ? 'secondary' : 'primary').'" style="width:120px">
		<input type="'.(MENUSUB == 'Shared Data' ? 'button': 'submit').'" name="menu_sub" value="Shared Data" class="button-'.(MENUSUB == 'Shared Data' ? 'secondary' : 'primary').'" style="width:120px">
		</form> 
		</td></tr>
		';

	$output .= '<tr><td valign="top" class="wuss_settings_description">'
			. 'Select Account Holder'
			. '</td><td valign="top" class="wuss_settings_values">'
			. (($uid > 0) ? $wuss_users_list : 'Shared game settings')
			. '</td></tr>';

	$output .= '<tr><td valign="top" class="wuss_settings_description">'
            .  ($gid > 0 ? 'User data' : 'Shared data')
	        .  '</td><td valign="top" class="wuss_settings_values">';

	$user = null;
	foreach($users as $_user) {
		if ( $_user->ID == $uid )
			$user = $_user;
	}

	$output .= wuss_dataShowUserData($user, $gid)
			.  '</td></tr>';

	$output .=  '</table>';
	return $output;
}

function wuss_dataShowUserData($user, $gid)
{
	global $wpdb;
	$data = null;
	$data_query = "SELECT cat,gid,fid,fval FROM " . wuss_prefix
              . "data WHERE uid='".(null != $user ? $user->ID : 0)."' AND gid='$gid' ORDER BY gid,cat";
	$data       = $wpdb->get_results( $data_query );
	if (!$data)
	{
		$output = 'No data found<br>';
		return $output;
	}
	$last_cat = "nothing";
	$contents_table = "<table class=\"stattable\" >";

	foreach($data as $entry)
	{
		if ($entry->cat != $last_cat)
		{
			$last_cat = $entry->cat;
			$contents_table .= "<tr><td colspan=4 class=stattableheader>".($last_cat == "" ? "Uncategorised" : $last_cat).'</td></tr>';
		}
		$table = "<tr><td width=120>$entry->fid</td>
            <td width=\"*\">
            <form method=post>
            <input type=text  class=\"inputfield\" name=fval value=\"{$entry->fval}\">
            </td>
        
            <td class=\"stattablebuttoncol\">
            <input type=hidden name=fid value=\"{$entry->fid}\">
            <input type=hidden name=cat value=\"{$last_cat}\">
            <input type=hidden name=menu_tab value=\"".MENUTAB."\">
            <input type=hidden name=menu_sub value=\"".MENUSUB."\">
            <input type=hidden name=gid value=\"{$entry->gid}\">
            <input type=hidden name=uid value=\"{$user->ID}\">
            <input type=submit name=\"wuss_data_action\" class=\"button-primary inputbutton\" value=\"Update\">
            </td>
        
            <td class=\"stattablebuttoncol\">
            <input type=submit name=\"wuss_data_action\" class=\"button-primary inputbutton\" value=\"Delete\">
            </form>
        
            </td></tr>";
		$contents_table .= $table;
	}
	$table = "</td></tr></table>";
	$contents_table .= $table;
	$output  = '<div>' . $contents_table . '</div>';
	return $output;
}
