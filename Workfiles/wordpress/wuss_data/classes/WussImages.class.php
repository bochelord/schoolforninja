<?php
class WussImages {

	var $user = null;
	var $uid = 0;

	public function __construct($uid)
	{
		$this->uid = $uid;
		if ($uid == 0) return;

		$this->user = get_user_by('ID', $uid);
	}

	public function StoreImage()
	{
		if (!$this->user && $this->uid > 0)
			return FALSE;

		$name = Posted("imgnm");
		$extension = Posted('imgex');
		$output_file = $this->GetUserUploadFolder()."{$name}.{$extension}";
		$output_file = str_replace(' ','_',$output_file);

		$ifp = fopen( $output_file, "wb" );
		fwrite( $ifp, base64_decode( Posted('imgdt')) );
		fclose( $ifp );

		$response = ($this->uid > 0)
			? $this->wuss_upload_url('baseurl')."/{$this->user->user_login}/{$name}.{$extension}"
			: $this->wuss_upload_url('baseurl')."/{$name}.{$extension}";
		$response = str_replace(' ','_',$response);
		return $response;
	}

	public function DeleteImage($url)
	{
		$dir = wp_upload_dir();
		$path = $dir['basedir'] . substr($url, strlen($this->wuss_upload_url('baseurl')));
		if (file_exists($path))
			wp_delete_file($path);
		else
			return $path;
		return true;
	}

	function wuss_upload_url( $param, $subfolder = '' ) {
		$dir = wp_upload_dir();
		$url = $dir[ $param ];

		if ( ($param === 'url' || $param === 'baseurl') && is_ssl() )
			$url = str_replace( 'http://', 'https://', $url );

		return $url . $subfolder;
	}

	function GetUserUploadFolder()
	{
		$upload_dir = wp_upload_dir('basedir');
		if ($this->uid > 0) {
			$user_dirname = $upload_dir['basedir'] . "/{$this->user->user_login}";
			$user_dirname = str_replace(' ','_',$user_dirname);
			if (!file_exists($user_dirname))
				wp_mkdir_p($user_dirname);
		} else
		{
			$user_dirname = $upload_dir['basedir'];
		}
		return $user_dirname.'/';
	}
}

