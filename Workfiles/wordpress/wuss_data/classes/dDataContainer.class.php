<?php

class dDataContainer {
	var $gid = 0,
		$cat = "",
		$fields;
	var $pro = false;

	public function __construct($gid, $cat)
	{
		$this->gid	= $gid;
		$this->cat	= $cat;
		$this->pro   = Posted('WUDPRO') != '';
	}

	public function AddField($name, $val)
	{
		$this->fields[$name] = $val;
	}
	
	public function ToCML()
	{
		if ($this->pro) {
			if (trim($this->cat) == '')
				$this->cat = 'Global';
			$result = SendNode($this->cat);
		}
		else
			$result = "<_CATEGORY_>category=$this->cat\n";
		foreach($this->fields as $key => $val)
			$result .= SendField($key, $val);
		if ($this->pro)
			$result.= SendNode("/{$this->cat}");
		return $result;
	}
	
	public function commit_fields($uid)
	{
		global $wpdb;
		if (null == $this->fields)
			return;
        $table = wuss_prefix . 'data';
		foreach($this->fields as $key => $value)
		{
			$query  = "INSERT INTO $table (uid, gid, cat, fid, fval) VALUES ('$uid', '$this->gid', '$this->cat', '$key', '$value') "
					. " ON DUPLICATE KEY UPDATE fval = '$value'";

            $wpdb->query($query);
		}
	}
}
