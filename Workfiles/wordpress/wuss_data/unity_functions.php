<?php

	//load this first as this makes sure the WP themes are not processed
	include_once(dirname(__FILE__) . "/../wuss_login/settings.php");

function dataLoadClasses()
{
	wuss_load_class('dDataContainer');
	wuss_load_class('dData');
	wuss_load_class('WussImages');
}

//Fetch only the value of a specific piece of data
function __fetchField($uid)
{
    $data = new dData(Postedi("gid"), $uid, Posted("cat") );
    $data->fetch_field( Posted("fid") );
    $data->ReturnResults();
}

//Fetch all data stored under a specific category for a specific game
function __fetchCategory($uid) {
    $data = new dData(Postedi("gid"), $uid, Posted("cat") );
    $data->fetch_cat();
    $data->ReturnResults();
}

function __fetchCategoryLike($uid) {
		$data = new dData(Postedi("gid"), $uid, Posted("cat") );
		$data->fetch_cat_like();
		$data->ReturnResults();
	}

//Fetch the data of all categories for a specific game
function __fetchAllInfo($uid) {
    $data = new dData(Postedi("gid"), $uid, Posted("cat") );
    $data->fetch_game();
    $data->ReturnResults(true);
}

//Fetch the data of all categories for a specific game
function __fetchGameInfo($uid) {	
	$data = new dData(Postedi("gid"), $uid, Posted("cat") );	
	$data->fetch_game();
	$data->ReturnResults(true);
}

//Fetch all info related to the player but do NOT include data stored
//for games. Does not include Wordpress standard info, only data the developer stored
function __fetchGlobalInfo($uid) {		
	$data = new dData(Postedi("gid"), $uid, Posted("cat") );	
	$data->fetch_global_info();
	$data->ReturnResults(true);
}

//Fetch absolutely everything related to the player:
//1. Data not related to any game as well as
//2. ...all data from all categories
//3. ...for all games
function __fetchEverything($uid) {
	$data = new dData(Postedi("gid"), $uid, Posted("cat") );	
	$data->fetch_all_user_data();
	$data->ReturnResults(true);
}

//Remove a specific piece of data
function __removeField($uid) {
    $data = new dData(Postedi("gid"), $uid, Posted("cat") );
    $data->remove_field( Posted("fid") );
}

//Fetch all data stored under a specific category for a specific game
function __removeCategory($uid) {
    $data = new dData(Postedi("gid"), $uid, Posted("cat") );
    $data->remove_cat();
}

//Remove the data of all categories for a specific game
function __removeGameInfo($uid) {		
	$data = new dData(Postedi("gid"), $uid, Posted("cat") );	
	$data->remove_game();
}

//All data to be saved must be sent over per category.
//Fetch each field and it's value and store it into the database
function __saveData($uid) {
    //remove the operational stuff from the post data leaving behind only the fields we want to save
    $gid = Postedi("gid");
	$cat = Posted ("cat");

    unset($_REQUEST["gid"]);
    unset($_REQUEST["uid"]);
    unset($_REQUEST["cat"]);
    unset($_REQUEST["id"]);
    unset($_REQUEST["wuss"]);
    unset($_REQUEST["action"]);
    unset($_REQUEST["unity"]);

    if (count($_REQUEST) == 0)
    {
		$result = SendNode("DATA") . 
        PrintError("No data to save");
		SendToUnity($result);
        return;
    }

    $data = new dDataContainer($gid, $cat);

	unset($_REQUEST["WUDPRO"]);
    foreach ($_REQUEST as $key => $value)
        $data->AddField( sanitize_text_field( strip_tags($key) ), sanitize_text_field( strip_tags( $value ) ) );

    $data->commit_fields($uid);
    SendToUnity( SendField("success","true") );
}

//This saves images to the uploads folder, sorted under user's login names
//This makes an entry into the WUData table listing the absolute URL to the uploaded image
function __storeImage($uid)
{
	$image = new WussImages($uid);
	$result = $image->StoreImage();

	if (!$result)
		die( SendToUnity( PrintError("Unknown user specified") ) );

	$data = new dDataContainer(Postedi('gid'), Posted('cat'));
	$data->AddField( sanitize_text_field( strip_tags(Posted('imgfd')) ), $result );
	$data->commit_fields($uid);
	SendToUnity( SendField("response",$result) );
}

function _deleteImage($uid) {
	$key = Posted('key');
	$data = new dData(Postedi("gid"), $uid, Posted("cat") );
	$data->fetch_field($key);
	if (count($data->data) == 0 || count($data->data[0]->fields) == 0 || !isset($data->data[0]->fields[$key]))
		die( SendToUnity( PrintError("The required field was not found in the specified category") ) );

	$url = $data->data[0]->fields[$key];
	$images = new WussImages($uid);
	$result = $images->DeleteImage($url);
	if ($result !== TRUE)
		die( SendToUnity( PrintError($result)));

	$data->remove_field($key);
	SendToUnity( SendField("success", "true"));
}

//functions operating on your own data
function dataFetchEverything() { global $current_user; __fetchEverything($current_user->ID); }
function dataFetchField() { global $current_user; __fetchField($current_user->ID); }
function dataFetchCategory() { global $current_user; __fetchCategory($current_user->ID); }
function dataFetchCategoryLike() { global $current_user; __fetchCategoryLike($current_user->ID); }
function dataFetchGameInfo() { global $current_user; __fetchGameInfo($current_user->ID); }	
function dataFetchGlobalInfo() { global $current_user; __fetchGlobalInfo($current_user->ID); }	
function dataRemoveField() { global $current_user; __removeField($current_user->ID);}
function dataRemoveCategory() {	global $current_user; __removeCategory($current_user->ID); }
function dataRemoveGameInfo() { global $current_user; __removeGameInfo($current_user->ID); }
function dataSaveData() { global $current_user; __saveData($current_user->ID); }
function dataStoreImage() {	global $current_user; __storeImage($current_user->ID); }
function dataDeleteImage() {global $current_user; _deleteImage($current_user->ID);}

//functions operating on shared data
function dataFetchSharedField() { __fetchField(0); }
function dataFetchSharedCategory() { __fetchCategory(0); }
function dataFetchSharedCategoryLike() { __fetchCategoryLike(0); }
function dataFetchAllSharedInfo() { __fetchAllInfo(0); }
function dataRemoveSharedField() { __removeField(0);}
function dataRemoveSharedCategory() { __removeCategory(0); }
function dataSaveSharedData() { __saveData(0);}
function dataStoreSharedImage() { __storeImage(0);}
function dataDeleteSharedImage() {_deleteImage(0);}

//functions operating on other player's data
function dataFetchUserField() { __fetchField($_REQUEST['uid']); }
function dataFetchUserCategory() { __fetchCategory($_REQUEST['uid']); }
function dataFetchUserCategoryLike() { __fetchCategoryLike($_REQUEST['uid']); }
function dataFetchUserGameInfo() { __fetchGameInfo($_REQUEST['uid']); }
function dataFetchUserGlobalInfo() { __fetchGlobalInfo($_REQUEST['uid']); }
function dataRemoveUserField() { __removeField($_REQUEST['uid']);}
function dataRemoveUserCategory() { __removeCategory($_REQUEST['uid']); }
function dataSaveUserData() { __saveData($_REQUEST['uid']);}
function dataStoreUserImage() {__storeImage($_REQUEST['uid']);}
function dataDeleteUserImage() {_deleteImage($_REQUEST['uid']);}

// function ninja_fetch_data(){

//     $data = new dData(Postedi("gid"), $uid, Posted("cat") );
//     $data->fetch_cat();

// 	return $data;
// }

// add_shortcode('ninja_wuss_data', 'ninja_fetch_data');
