<?php
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );


// Verwijder admin bar voor ALLE gebruikers behalve administrators
if (!function_exists('df_disable_admin_bar')) {
 
	function df_disable_admin_bar() {
 
		if (!current_user_can('manage_options')) {
 
			// for the admin page
			remove_action('admin_footer', 'wp_admin_bar_render', 1000);
			// for the front-end
			remove_action('wp_footer', 'wp_admin_bar_render', 1000);
 
			// css override for the admin page
			function remove_admin_bar_style_backend() { 
				echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
			}	  
			add_filter('admin_head','remove_admin_bar_style_backend');
 
			// css override for the frontend
			function remove_admin_bar_style_frontend() {
				echo '<style type="text/css" media="screen">
				html { margin-top: 0px !important; }
				* html body { margin-top: 0px !important; }
				</style>';
			}
			add_filter('wp_head','remove_admin_bar_style_frontend', 99);
 
		}
  	}
}
add_action('init','df_disable_admin_bar');
function wpb_add_google_fonts() {
 
wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Nanum+Brush+Script
', false ); 
}
 
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );
add_filter('widget_text', 'do_shortcode');

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'fases',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Fase' ),
                'singular_name' => __( 'Fase' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'fases'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

function ninja_filter_category_fase_1(){

// $args = array(
// 	'show_option_all'    => 'Fase',
// 	'show_option_none'   => '',
// 	'option_none_value'  => '-1',
// 	'orderby'            => 'ID',
// 	'order'              => 'ASC',
// 	'show_count'         => 0,
// 	'hide_empty'         => 0,
// 	'child_of'           => 0,
// 	'exclude'            => '',
// 	'include'            => '',
// 	'echo'               => 1,
// 	'selected'           => 0,
// 	'hierarchical'       => 0,
// 	'name'               => 'cat',
// 	'id'                 => 'select_name',
// 	'class'              => 'postform',
// 	'depth'              => 0,
// 	'tab_index'          => 0,
// 	'taxonomy'           => 'category',
// 	'hide_if_empty'      => false,
// 	'value_field'	     => 'name',
// 	);

// 	wp_dropdown_categories( $args );
	wp_dropdown_categories( array(
	    'hide_empty'   => 0,
	    'name'         => 'select_name',
	    'id'           => 'select_name',
	    'hierarchical' => true,
	) );
}

// wp_dropdown_categories( array(
//     'hide_empty'       => 0,
//     'name'             => 'category_parent',
//     'orderby'          => 'name',
//     'selected'         => $category->parent,
//     'hierarchical'     => true,
//     'show_option_none' => __('None')
// ) );


add_shortcode('filter_category_by_fase_1', 'ninja_filter_category_fase_1');
