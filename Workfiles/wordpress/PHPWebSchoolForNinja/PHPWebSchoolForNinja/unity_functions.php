<?php

//load this first as this makes sure the WP themes are not processed
include_once(dirname(__FILE__) . "/../wuss_login/settings.php");

function customIsSubscribed() {
	$expiry = get_user_meta( get_current_user_id(), Posted('gid').'_sub_expiry_date', true);
	if (trim($expiry) == '') $expiry = 0;
	$subscribed = (intval($expiry) > time());
	SendToUnity(SendField("subscribed", $subscribed ? 'true' : 'false'));
}


function customWriteNinjaPost() {
	$user_id = get_current_user_id();

	$ninjaPost=array(
		'post_type' => 'post',
		'post_title' => '', //Here goes a variable from Unity
		'post_content' => '', //Here goes a variable from Unity

	);

	wp_insert_post($ninjaPost, $wp_error );
}







function customGetNinjaPosts() {

	// global $current_user;
	$user_id = get_current_user_id();

	$args=array(
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'posts_per_page' => -1,
	    'author' => $user_id
	);                       

	$current_user_posts = new WP_Query($args);

//-----------------------------------------------------------
	// global $current_user;
	
	// get_currentuserinfo();                      

	// $args = array(
 //    	'author'        =>  $current_user->ID, // I could also use $user_ID, right?
 //    	'orderby'       =>  'post_date',
 //    	'order'         =>  'ASC' 
 //    );

// get his posts 'ASC'
	// $current_user_posts = get_posts( $args );

	$porPaco = "";
	
	// SendToUnity( PrintError("user id: " . $user_id));
	// SendToUnity ( PrintError("current user post: " . $current_user_posts[0].));
	foreach($current_user_posts->posts as $post)
	{			
		// $porPaco .= $post->post_title;
		$porPaco .= $post->ID;
		$porPaco .= ", ";
	} 
	
	if (empty($porPaco)){
		SendToUnity( PrintError("String is empty..."));
		return;
	}
	//if (have_posts() : the_post()) {
		// SendToUnity(SendField("ninjaPostTitle", the_title()));
	
	SendToUnity(SendField("ninjaPostID", $porPaco));

	//}
	    
	// endwhile; 

	// wp_reset_postdata();

}
//============================================================================================================================================================
function ninjaFetchAvailablePosts()
{
	$query = new WP_Query( array( 'post_type' => 'post', 'post_status' => array('publish','draft')) );
	if (!$query->have_posts())
	{
		SendToUnity( PrintError("No games found") );
		return;
	}

	$games = '';
	foreach($query->posts as $post)
	{			
		$games .= SendNode("Game");
		$games .= SendField("gid", $post->ID);
		$games .= SendField("name", $post->post_title);
		$games .= SendField("state", $post->post_status);
	}
	SendToUnity($games);
}

function ninjaFetchAvailableNinjaPostInfo()
{
	$query = new WP_Query( array( 'post_type' => 'post', 'post_status' => array('publish','draft')) );
	if (!$query->have_posts())
	{
		SendToUnity( PrintError("No posts found") );
		return;
	}

	$games = '';
	foreach($query->posts as $post)
	{			
		$games .= SendNode("Game");
		$games .= SendField("gid", $post->ID);
		$games .= SendField("name", $post->post_title);
		$games .= SendField("state", $post->post_status);
	}
	SendToUnity($games);
}

function ninjaCreateNewPost()
{
	SendToUnity( PrintError("dentro de ninjaCreateNewPost") );
    global $current_user;
    // SendToUnity( PrintError("Dentro de ninjaCreateNewPost, this is something good...");
    if (!is_user_logged_in())
    {
        SendToUnity( PrintError("You are not logged in. Please log in and try again") );
        return false;
    }
    // } else {
    // 	SendToUnity( PrintError("user " . $user_id . " is logged.");
    // }

    if ( !current_user_can( 'manage_wuss', $current_user->ID ) )
    {
        SendToUnity( PrintError("You are not authorised to create posts on this website") );
        $title = wp_strip_all_tags(Posted($_REQUEST['post-title']));
        $email = get_option('admin-email');
        $subject = 'Unauthorised attempt to create a post on your site';
        $message = "$current_user->user_login ($current_user->first_name $current_user->last_name) just tried to create a new post called $title on your site";
        wp_mail($email, $subject, $message);
        return false;
    }

    $overhead = explode(",","gid,wuss,id,unity,action");
    foreach($overhead as $s)
        unset($_REQUEST[$s]);

    //check if post doesn't already exist... Don't want duplicates,do we?
    // $existing = new WP_Query( array( 'post_type' => 'post', 'post_title' => wp_strip_all_tags($_REQUEST['post_title'])) );
    // if ($existing->have_posts())
    // {
	   //  ninjaFetchAvailableNinjaPostInfo();
	   //  return true;
    // }

    $arguments = array();
    foreach ($_REQUEST as $k => $v)
    {
        switch($k) {
            case "post_content":
            case "post_excerpt":
                $arguments[$k] = base64_decode($v);
                break;

            case "post_title":
                $arguments[$k] = wp_strip_all_tags($v);
                break;

            default:
                $arguments[$k] = $v;
                break;
        }
    }
    $arguments['post_type'] = 'post';
    $arguments['post_status'] = 'publish';
    wp_insert_post( $arguments, true );

    ninjaFetchAvailableNinjaPostInfo();
    return true;
}
?>