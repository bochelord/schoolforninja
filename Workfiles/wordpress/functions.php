<?php

include_once(dirname(__FILE__) . "/../wuss_login/settings.php");

function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// Verwijder admin bar voor ALLE gebruikers behalve administrators
if (!function_exists('df_disable_admin_bar')) {
 
	function df_disable_admin_bar() {
 
		if (!current_user_can('manage_options')) {
 
			// for the admin page
			remove_action('admin_footer', 'wp_admin_bar_render', 1000);
			// for the front-end
			remove_action('wp_footer', 'wp_admin_bar_render', 1000);
 
			// css override for the admin page
			function remove_admin_bar_style_backend() { 
				echo '<style>body.admin-bar #wpcontent, body.admin-bar #adminmenu { padding-top: 0px !important; }</style>';
			}	  
			add_filter('admin_head','remove_admin_bar_style_backend');
 
			// css override for the frontend
			function remove_admin_bar_style_frontend() {
				echo '<style type="text/css" media="screen">
				html { margin-top: 0px !important; }
				* html body { margin-top: 0px !important; }
				</style>';
			}
			add_filter('wp_head','remove_admin_bar_style_frontend', 99);
 
		}
  	}
}
add_action('init','df_disable_admin_bar');

function wpb_add_google_fonts() {
 
wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Nanum+Brush+Script
', false ); 
}
 
add_action( 'wp_enqueue_scripts', 'wpb_add_google_fonts' );
add_filter('widget_text', 'do_shortcode');

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'fases',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Fase' ),
                'singular_name' => __( 'Fase' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'fases'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

function ninja_filter_category_fase_1(){

// $args = array(
// 	'show_option_all'    => 'Fase',
// 	'show_option_none'   => '',
// 	'option_none_value'  => '-1',
// 	'orderby'            => 'ID',
// 	'order'              => 'ASC',
// 	'show_count'         => 0,
// 	'hide_empty'         => 0,
// 	'child_of'           => 0,
// 	'exclude'            => '',
// 	'include'            => '',
// 	'echo'               => 1,
// 	'selected'           => 0,
// 	'hierarchical'       => 0,
// 	'name'               => 'cat',
// 	'id'                 => 'select_name',
// 	'class'              => 'postform',
// 	'depth'              => 0,
// 	'tab_index'          => 0,
// 	'taxonomy'           => 'category',
// 	'hide_if_empty'      => false,
// 	'value_field'	     => 'name',
// 	);

// 	wp_dropdown_categories( $args );
	wp_dropdown_categories( array(
	    'hide_empty'   => 0,
	    'name'         => 'select_name',
	    'id'           => 'select_name',
	    'hierarchical' => true,
	) );
}

// wp_dropdown_categories( array(
//     'hide_empty'       => 0,
//     'name'             => 'category_parent',
//     'orderby'          => 'name',
//     'selected'         => $category->parent,
//     'hierarchical'     => true,
//     'show_option_none' => __('None')
// ) );


add_shortcode('filter_category_by_fase_1', 'ninja_filter_category_fase_1');

function ninja_user_display_name(){
	global $current_user;
	get_currentuserinfo();
	// return '<div class="et_pb_module">'.$current_user->display_name.'</div>';
	return $current_user->display_name;
}

add_shortcode('user_display_name', 'ninja_user_display_name');

function ninja_avatar(){
	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	// $query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "user_avatar"';

	// $result = $wpdb->get_var($query);
	// $url = "<img src='".$result."' style='width: 221px;'>";
	
	// return $url;
	return get_avatar($user_id, 225);
}
add_shortcode('ninja_avatar', 'ninja_avatar');

// Gets the ninjaName value on the wuss_data table from the Wordpress Database.
function ninja_fetch_ninjaName(){

	global $wpdb;			// Global WordPressDataBase variable to work with the database.
	global $current_user;	// Var to store the current logged user.

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "wuss_data";	// $wpdb->prefix retuns "wp_" + our wuss_data table from the Unity package.
	$user_id = $current_user->id;				// Current logged user id to use it in the quest

	$query = 'SELECT fval FROM '.$tableName.' WHERE uid = '.$user_id.' AND cat = "ninjaData" AND fid = "ninjaName"';
	//Gets the fval cell from the table 'wp_wuss_data' with cat and fid filters.

	$result = $wpdb->get_var($query);	//Gets the value store in the fval cell

	return $result;	//Returns the value (!Important, don't use echo or DIVI will break the page)
}
add_shortcode('ninjaName_wuss_data', 'ninja_fetch_ninjaName');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function.
function ninja_fetch_ninjaDome(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "wuss_data";
	$user_id = $current_user->id;

	$query = 'SELECT fval FROM '.$tableName.' WHERE uid = '.$current_user->id.' AND cat = "ninjaData" AND fid = "ninjaDome"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('ninjaDome_wuss_data', 'ninja_fetch_ninjaDome');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function.
function ninja_fetch_ninjaLevel(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "wuss_data";
	$user_id = $current_user->id;

	$query = 'SELECT fval FROM '.$tableName.' WHERE uid = '.$current_user->id.' AND cat = "ninjaData" AND fid = "ninjaLevel"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('ninjaLevel_wuss_data', 'ninja_fetch_ninjaLevel');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function.
function ninja_fetch_ninjaPower(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "wuss_data";
	$user_id = $current_user->id;

	$query = 'SELECT fval FROM '.$tableName.' WHERE uid = '.$current_user->id.' AND cat = "ninjaData" AND fid = "ninjaPower"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('ninjaPower_wuss_data', 'ninja_fetch_ninjaPower');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function but this time for Coach role.
function coach_fetch_firstName(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "first_name"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('coach_firstName', 'coach_fetch_firstName');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function but this time for Coach role.
function coach_fetch_lastName(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "last_name"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('coach_lastName', 'coach_fetch_lastName');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function but this time for Coach role.
function coach_fetch_description(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "description"';

	$result = $wpdb->get_var($query);

	return $result;
}
add_shortcode('coach_description', 'coach_fetch_description');

//============================================================================================================================
//Same description as ninja_fetch_ninjaName() function but this time for Coach role.
function coach_fetch_company(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "organisatie"';

	$result = $wpdb->get_var($query);

	return $result;
}

add_shortcode('coach_company', 'coach_fetch_company');

function coach_fetch_location(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "plaats"';

	$result = $wpdb->get_var($query);

	return $result;
}

add_shortcode('coach_location', 'coach_fetch_location');


function coach_fetch_avatarURL(){

	global $wpdb;
	global $current_user;

	get_currentuserinfo();

	$tableName = $wpdb->prefix . "usermeta";
	$user_id = $current_user->id;

	$query = 'SELECT meta_value FROM '.$tableName.' WHERE user_id = '.$current_user->id.' AND meta_key = "user_avatar"';

	$result = $wpdb->get_var($query);
	$url = "<img src='".$result."' style='width: 221px;'>";
	
	return $url;
}

add_shortcode('coach_avatarURL', 'coach_fetch_avatarURL');


function my_llms_lesson_reg( $args ) {
    $args['rewrite']['slug'] = _x( 'les', 'slug', 'my-text-domain' );
    return $args;
}
add_filter( 'lifterlms_register_post_type_lesson', 'my_llms_lesson_reg' );



/**
 * Disable the emoji's
 */
function disable_emojis() {
 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
 remove_action( 'wp_print_styles', 'print_emoji_styles' );
 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );


/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
 if ( is_array( $plugins ) ) {
 return array_diff( $plugins, array( 'wpemoji' ) );
 } else {
 return array();
 }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
 if ( 'dns-prefetch' == $relation_type ) {
 /** This filter is documented in wp-includes/formatting.php */
 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
 }

return $urls;
}

/* STOP Storing IP Address in WordPress Comments */
function wpb_remove_commentsip( $comment_author_ip ) {
return '';
}
add_filter( 'pre_comment_user_ip', 'wpb_remove_commentsip' );
