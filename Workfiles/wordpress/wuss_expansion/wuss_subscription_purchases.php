<?php
add_action( 'wuss_purchase', 'wuss_subscription_purchase' );

function wuss_subscription_purchase($args)
{
	//if the type of this transaction is not "sub" then this transaction was not meant for this plugin so quit
	if( $args['type'] != "sub")
		return;

	$uid = intval($args['uid']);
	$gid = intval($args['gid']);
	$expiry = get_user_meta($uid, "{$gid}_sub_expiry_date", true);
	if (trim($expiry) == '') $expiry = 0;
	$expiry = intval($expiry);

	$period = $args['fields'][0]; //should be: months/weeks/days
	$amount = intval($args['qty']); // qty from the cart
	$amt_to_award = 0;
	switch($period)
	{
		case 'days': $amt_to_award = 86400 * $amount; break;
		case 'weeks': $amt_to_award = 604800 * $amount; break;
		case 'months': $amt_to_award = 2419200 * $amount; break;
	}

	//if a subscription is still active, add the time to it
	//otherwise set a new subscription
	if ( $expiry < time() )
		$expiry = $amt_to_award;
	else
		$expiry += $amt_to_award;

	update_user_meta($uid, "{$gid}_sub_expiry_date", $expiry);
}