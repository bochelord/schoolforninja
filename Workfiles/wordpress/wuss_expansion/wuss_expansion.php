<?php

session_start();

/*
Plugin Name: WUSS Custom Expansions
Plugin URI: http://wuss.mybadstudios.com/
Description: In case you want to extend WUSS assets without fear of your code being overwritten during assets, add your extensions here
Version: 1.0
Network: true
Author: myBad Studios
Author URI: http://www.mybadstudios.com
*/

include_once(dirname(__FILE__) ."/wuss_subscription_purchases.php");

