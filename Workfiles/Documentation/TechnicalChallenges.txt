Challenge -- Anonymity 

Possible combined solutions:

:: registration in secure server - Using SSL (Secure Socket Layer) for HTTP connections and 128bit encription on the possible data saved on client (this can lead to additional challenge on avoiding hacking the client(device) data, that's why we need encryption and design the communication to be as anonymous as possible)

:: API tokenized for anonymity on client/server communication - We use the standard JSON Web Token to minimize the risk on hacks on the encrypted data and allow reuse of loggedin sessions without any human interaction

:: Hardware device for registration and/or activation - We are thinking on using a hardware key solution for a security extra layer when it comes to activating an account on the system

:: AI automated standard solutions are not perfect (profanity filter), and they need to be trained. With some labeling techniques programmed by ourself using approaches like sentiment analysis (categorizing the sentiment of the user) , association rule analysis (infering correlations between data points and user usage), regression analysis (changing an independent variable to check the influence on another); we can speed up the training time for decision making when it comes to analyze the data in an anonimous way.

:: Two factor authentication - Secure Chat - We want to implement a two-factor authentication mechanism for securing the chat functionality. This can lead to another challenge since it adds some burden on the user side, so it has to be designed with usability in mind.


Challenge -- Personalized interventions (involving Data Mining and AI powered analysis)

Possible solutions:

:: Interventions based on Coach input: Coaches can create paths for interventions that are assigned to a category of players

:: AI powered suggestions : Tensorflow machine learning through a high-level API in Python language. We will use Text classification and Regression analysis to categorize the data

:: Interventions based on User input:  the user answers key questions that are processed by an algorithm and presents choices based on validated parameters


Challenge -- Freedom of Choice (structured design of interventions based on Data Analysis from previous runs)

Possible solutions:

:: Intervention labelling and categorizing : By categorizing the interventions we can feed the algorithm with more meaningful information

:: Algorithm to suggest interventions to the player based on previous runs, related to the different data analysis detailed before: Sentiment analysis, association rule analysis and regression analysis

:: Genetic Algorithm to optimize intervention effectiviness


Challenge -- Syncronization for game data on native mobile apps

Possible solutions:

:: Inhouse secure architecture using C# , python and PostgreSQL: Using Unity Engine for a multiplatform native solution (iOS & Android), Python running on Apache Server over Django framework connected to a PostgreSQL (possibly a different future solution we'll use for DataBase)

Challenge -- Realtime Storage / Data flexibility

Possible solutions:

:: GraphQL API implementation: Create an API based on the data query language GraphQL will give more flexibility and realtime access to the database


