﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Rad_Photon_ChannelSelector : MonoBehaviour, IPointerClickHandler
{
    public string Channel;

    public void SetChannel(string channel)
    {
        this.Channel = channel;
        Text t = GetComponentInChildren<Text>();
        t.text = this.Channel;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RadChatPhoton handler = FindObjectOfType<RadChatPhoton>();
        handler.ShowChannel(this.Channel);
    }
}
