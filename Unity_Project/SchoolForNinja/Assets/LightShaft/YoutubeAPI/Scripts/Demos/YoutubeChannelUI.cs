﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class YoutubeChannelUI : MonoBehaviour {

    public Text videoName;
    public string videoId, thumbUrl;
    public Image videoThumb;

    public void LoadChannel()
    {
        GameObject.FindObjectOfType<ChannelSearchDemo>().LoadChannelResult(videoId);
    }

    public void LoadThumbnail()
    {
        StartCoroutine(DownloadThumb());
    }

    IEnumerator DownloadThumb()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(thumbUrl);
        yield return www.SendWebRequest();
        Texture2D thumb = ((DownloadHandlerTexture)www.downloadHandler).texture;
        videoThumb.sprite = Sprite.Create(thumb, new Rect(0, 0, thumb.width, thumb.height), new Vector2(0.5f, 0.5f), 100);
    }

}
