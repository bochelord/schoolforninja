﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WhaleTeleport : MonoBehaviour
{
    public Transform teleportPlayerTo;

    public GameObject whirlAnimation;


    private PathsManagerFase3 pathManager;

    private void Start()
    {
        pathManager = FindObjectOfType<PathsManagerFase3>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            StartCoroutine(FadePlayer(collision.gameObject));
        }
    }


    private IEnumerator FadePlayer(GameObject gameObject)
    {
        whirlAnimation.SetActive(true);
        gameObject.GetComponent<SpriteRenderer>().DOFade(0, .1f);
        yield return new WaitForSeconds(2.1f);
        gameObject.GetComponent<SpriteRenderer>().DOFade(1, .1f);
        whirlAnimation.SetActive(false);
        pathManager.MovePlayerTo(teleportPlayerTo.position);
    }
}
