﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkWhale : MonoBehaviour
{
    public Transform moveTo;

    private bool moved = false;
    public void MoveWhale()
    {
        if (!moved)
        {
            this.transform.position = moveTo.position;
            moved = true;
        }
        
    }
}
