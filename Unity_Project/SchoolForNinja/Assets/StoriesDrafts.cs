﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoriesDrafts : MonoBehaviour
{
    public Dictionary<int, string> storiesData = new Dictionary<int, string>();

    public bool CheckForStory(int _key)
    {
        if (storiesData.ContainsKey(_key))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
