﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LogCard : MonoBehaviour {

    public Image logIcon;
    public TextMeshProUGUI logTitle;
    public TextMeshProUGUI timestampNumber;
    public TextMeshProUGUI timestempMonth;
    public TextMeshProUGUI logDescription;

    public int postId;

    public void CreateCard(Rad_Model.Post_Log _post)
    {
        postId = _post.id;
        System.DateTime _date = System.DateTime.Parse(_post.timestamp);

        if (_post.has_image1)
        {
            logIcon.sprite = BackendManager.Instance.DecodeBase64Texture(_post.image1);
        }
        //else if (_post.has_image2)
        //{
        //    logIcon.sprite = BackendManager.Instance.DecodeBase64Texture(_post.image2);
        //}
        //else if (_post.has_image3)
        //{
        //    logIcon.sprite = BackendManager.Instance.DecodeBase64Texture(_post.image3);
        //}
        else
        {
            logIcon.sprite = BackendManager.Instance.DecodeBase64Texture(_post.story_cover_image);
        }
        logTitle.text = _post.postTitle;
        timestampNumber.text = _date.Day.ToString();
 

        switch (_date.Month)
        {
            case 1:
                timestempMonth.text = "January";
                break;
            case 2:
                timestempMonth.text = "February";
                break;
            case 3:
                timestempMonth.text = "March";
                break;
            case 4:
                timestempMonth.text = "April";
                break;
            case 5:
                timestempMonth.text = "May";
                break;
            case 6:
                timestempMonth.text = "June";
                break;
            case 7:
                timestempMonth.text = "July";
                break;
            case 8:
                timestempMonth.text = "August";
                break;
            case 9:
                timestempMonth.text = "September";
                break;
            case 10:
                timestempMonth.text = "October";
                break;
            case 11:
                timestempMonth.text = "November";
                break;
            case 12:
                timestempMonth.text = "December";
                break;
            default:
                break;
        }
        logDescription.text = _post.postContent1;
    }
}
