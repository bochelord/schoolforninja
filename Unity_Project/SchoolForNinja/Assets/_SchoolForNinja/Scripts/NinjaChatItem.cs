﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class NinjaChatItem : MonoBehaviour {


    public string ninja_name;
    public TextMeshProUGUI ninja_nameText;
    public Image statusIcon;
    public Image channelIcon;

}
