﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuestManagerV2 : MonoBehaviour {

    public Sprite defaultImageToSend;
    public Sprite[] defaultQuestCoverImage;

    private List<Rad_Model.Quest> allQuests = new List<Rad_Model.Quest>();
    private List<Rad_Model.Quest> _thisPhaseQuests = new List<Rad_Model.Quest>();
    private List<Rad_Model.Quest_Page> questPages = new List<Rad_Model.Quest_Page>();
    private List<Rad_Model.Draft> draftsList = new List<Rad_Model.Draft>();
    private List<Rad_Model.Quest_Status> questStatusList = new List<Rad_Model.Quest_Status>();

    private Rad_Model.Quest currentQuest;
    private Rad_Model.Organization currentOrganization;
    private Rad_Model.Quest_Status currentQuestStatus;
    
    private SFN_GuiManager guiManager;
    private PathsManager pathmanager;
    private int questSlot;
    [HideInInspector]
    public int currentQuestId = 0;
    private int questPagesImageAssign = 0;
    private int draftimageAssign = 0;
    private int totalQuestPages;
    private Sprite _draftSprite;
    private Rad_Model.Draft _draftSaved;
    private bool _modifyDraft = false;
    static QuestManagerV2 instance;
    public static QuestManagerV2 Instance { get { return instance; } }

    void  Awake()
    {

        if (!Instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }

    private void Start()
    {
        //subscribe to sceneLoaded event
        SceneManager.sceneLoaded += OnlevelFinishedLoading;
    }


    public void GetQuests()
    {
        BackendManager.Instance.SetLastAction(GetQuests);
        BackendManager.Instance.GetQuests();
    }

    private void GetPhaseQuests()
    {
        BackendManager.Instance.SetLastAction(GetPhaseQuests);
        BackendManager.Instance.GetPhaseQuests(BackendManager.Instance.radModel.currentPhase, pathmanager.GetQuestStatusToLoadMap, pathmanager.OnFail);
    }

    private void GetPhaseQuests(int _phase)
    {
        //find all rad_quests from scene
        Rad_Quest[] _quests = FindObjectsOfType<Rad_Quest>();
        //clear phase quest list
        _thisPhaseQuests.Clear();
        //loop all quests to add only current phase quests
        for (int i = 0; i < allQuests.Count; i++)
        {
            var str = allQuests[i].is_from_phase.Split('/');
            var j = int.Parse(str[str.Length - 2]);
            //phase + 1, because server it's counting starting from 1 and app from 0
            if (j == _phase + 1)
            {
                //please, add
                _thisPhaseQuests.Add(allQuests[i]);
            }
        }

        //then we loop through phase quests to add the correct order set in server
        for (int i = 0; i < _quests.Length; i++)
        {
            for (int j = 0; j < _thisPhaseQuests.Count; j++)
            {
                if (_quests[i].questSlot == _thisPhaseQuests[j].order)
                {
                    _quests[i].questId = _thisPhaseQuests[j].id;
                }
            }
        }

        Debug.Log("phase quests: " + _thisPhaseQuests.Count);
        //load map
        if (pathmanager)
            pathmanager.GetQuestStatusToLoadMap();
    }

    public void SetCurrentQuestStatus(Rad_Model.Quest_Status _Status)
    {
        currentQuestStatus = _Status;
        //if (!_Status.completed)
        //{
        //    QuestActiveNotification(_Status);
        //}
    }

    public void SetQuestStatusList(Rad_Model.Quest_Status[] _array)
    {
        questStatusList.Clear();
        for (int i = 0; i < _array.Length; i++)
        {
            questStatusList.Add(_array[i]);

            //if (!_array[i].completed)
            //{
            //    QuestActiveNotification(_array[i]);
            //}
        }
    }

    public void ModifyQuestStatus(Rad_Model.Quest_Status _Status)
    {
        for (int i = 0; i < questStatusList.Count; i++)
        {
            if(_Status.url == questStatusList[i].url)
            {
                questStatusList[i] = _Status;

            }
        }
    }

    public void AddQuestStatusToList(Rad_Model.Quest_Status _Status)
    {
        questStatusList.Add(_Status);
    }

    #region Parses

    public void ParseOrganizationJSon(JsonData data)
    {
        currentOrganization = new Rad_Model.Organization();

        if (data.Keys.Contains("name"))
        {
            currentOrganization.name = (data["name"].ToString());
        }

        if (data.Keys.Contains("description"))
        {
            currentOrganization.description = (data["description"].ToString());
        }

        if (data.Keys.Contains("website"))
        {
            currentOrganization.website = (data["website"].ToString());
        }

        if (data.Keys.Contains("icon"))
        {
            if (RadSaveManager.spriteListData.CheckForSprite(data["icon"].ToString()))
            {
                SetCurrentOrganizationIconAndAssignToLandingPage(RadSaveManager.spriteListData.spriteData[data["icon"].ToString()]);
            }
            else
            {
                StartCoroutine(BackendManager.Instance.GetOrganizationSprite(data["icon"].ToString()));
            }

        }
    }

    public void ParseQuestPageJSON(JsonData[] data)
    {
        questPages.Clear();
        totalQuestPages = data.Length;
        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Quest_Page quest_page = new Rad_Model.Quest_Page();

            if (data[i].Keys.Contains("id"))
            {
                quest_page.id = int.Parse((data[i]["id"].ToString()));
            }
            if (data[i].Keys.Contains("order"))
            {
                quest_page.order = int.Parse((data[i]["order"].ToString()));
            }
            if (data[i].Keys.Contains("quest_title"))
            {
                quest_page.quest_title = (data[i]["quest_title"].ToString());
            }
            if (data[i].Keys.Contains("has_image"))
            {
                quest_page.has_image = bool.Parse((data[i]["has_image"].ToString()));
            }
            if (data[i].Keys.Contains("has_video"))
            {
                quest_page.has_video = bool.Parse((data[i]["has_video"].ToString()));
            }
            if (data[i].Keys.Contains("title_text"))
            {
                quest_page.title_text = data[i]["title_text"].ToString();
            }
            if (data[i].Keys.Contains("description_text"))
            {
                quest_page.description_text = data[i]["description_text"].ToString();
            }
            if (data[i].Keys.Contains("question_text"))
            {
                quest_page.question_text = data[i]["question_text"].ToString();
            }
            if (data[i].Keys.Contains("is_info_page"))
            {
                quest_page.is_info_page = bool.Parse(data[i]["is_info_page"].ToString());
            }
            if (quest_page.has_image && data[i].Keys.Contains("image"))
            {
                string url = data[i]["image"].ToString();
                if (RadSaveManager.spriteListData.CheckForSprite(url))
                {
                    quest_page.image = RadSaveManager.spriteListData.spriteData[url];
                    questPagesImageAssign++;
                }
                else
                {
                    StartCoroutine(BackendManager.Instance.GetQuestsPageSprite(url, quest_page.id));
                }

            }
            else
            {
                quest_page.image = "no image";
                questPagesImageAssign++;
            }
            questPages.Add(quest_page);
        }
        questPages.Sort((s1, s2) => s1.order.CompareTo(s2.order));


        if (questPagesImageAssign == totalQuestPages && !BackendManager.Instance.isCoach)
        {
            OnGetPagesSuccess();
        }
        else if(questPagesImageAssign == totalQuestPages)
        {
            OnGetPagesSuccessForCoach();
        }
    }

    public List<Rad_Model.Quest_Page> GetQuestPagesList()
    {
        return questPages;
    }


    public void ParseDraftsJSON(JsonData[] data)
    {
        draftimageAssign = 0;
        draftsList.Clear();
        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Draft draft = new Rad_Model.Draft();

            if (data[i].Keys.Contains("url"))
            {
                draft.url = data[i]["url"].ToString();
            }

            if (data[i].Keys.Contains("has_image"))
            {
                draft.has_image = bool.Parse((data[i]["has_image"].ToString()));
            }
            if (data[i].Keys.Contains("description"))
            {
                draft.description = data[i]["description"].ToString();
            }
            if (data[i].Keys.Contains("belongs_to_ninja"))
            {
                draft.belongs_to_ninja = data[i]["belongs_to_ninja"].ToString();
            }
            if (data[i].Keys.Contains("belongs_to_quest"))
            {
                draft.belongs_to_quest = data[i]["belongs_to_quest"].ToString();
            }
            if (draft.has_image && data[i].Keys.Contains("image"))
            {
                if (data[i]["image"].ToString().Length > 3 && !string.IsNullOrEmpty(data[i]["image"].ToString()))
                {
                    string url = data[i]["image"].ToString();
                    draft.image = url;
                    if (RadSaveManager.spriteListData.CheckForSprite(url))
                    {
                        draft.image = RadSaveManager.spriteListData.spriteData[url];
                        draftimageAssign++;
                    }
                    else
                    {
                        StartCoroutine(BackendManager.Instance.GetDraftsSprite(url));
                    }
                }
            }
            else
            {
                draft.image = "no image";
                draftimageAssign++;
            }
            draftsList.Add(draft);
        }

        if (draftimageAssign == draftsList.Count)
        {
            OnGetDraftSuccess();
        }
    }
    public void AssignStringBase64ToDrafts()
    {
        for (int i = 0; i < draftsList.Count; i++)
        {
            if (BackendManager.Instance.GetDraftTempStrings().ContainsKey(draftsList[i].image))
            {
                draftsList[i].image = BackendManager.Instance.GetDraftTempStrings()[draftsList[i].image];
                draftimageAssign++;
            }
        }

        draftsList.Sort((s1, s2) => s1.url.CompareTo(s2.url));

        if (draftimageAssign == draftsList.Count)
        {
            OnGetDraftSuccess();
        }
    }

    private void AssignStringsBase64ToPages()
    {
        for (int i = 0; i < questPages.Count; i++)
        {
            questPages[i].image = BackendManager.Instance.GetQuestPagesTempStrings()[i];
        }
        questPages.Sort((s1, s2) => s1.order.CompareTo(s2.order));
    }
    #endregion



    /// <summary>
    /// when post log is finished and published we send a post request to end the quest in the server
    /// </summary>
    public void EndCurrentQuest(bool firstQuest)
    {
        currentQuestStatus.completed = true;

        string jsonToSend = JsonUtility.ToJson(currentQuestStatus);

        var str = currentQuestStatus.url.Split('/');
        var pk = int.Parse(str[str.Length - 2]);

        BackendManager.Instance.SetLastAction(delegate { EndCurrentQuest(firstQuest); });
        BackendManager.Instance.ModifyQuestStatus(jsonToSend, pk);

        guiManager.ResetPanelQuest();
        guiManager.QuestCompletedPanel(firstQuest);
        guiManager.loadingPage.SetActive(false);

       // Rad_NotificationsManager.Instance.RemoveNotification(pk);
    }



    public void SetQuestPages(Rad_Model.Quest_Page[] _pages)
    {
        questPages.Clear();
        for (int i = 0; i < _pages.Length; i++)
        {
            questPages.Add(_pages[i]);
        }

        questPages.Sort((s1,s2)=>s1.order.CompareTo(s2.order));
    }

    public void SetPhaseQuestList(Rad_Model.Quest[] _quests)
    {
        _thisPhaseQuests.Clear();
        for (int i = 0; i < _quests.Length; i++)
        {
            var str = _quests[i].url.Split('/');
            var pk = int.Parse(str[str.Length - 2]);
            _quests[i].id = pk;
            _thisPhaseQuests.Add(_quests[i]);
        }
    }
    /// <summary>
    /// /Parse and set all quest lists
    /// </summary>
    /// <param name="data"></param>
    public void SetAllQuestsLists(JsonData[] data)
    {
        allQuests.Clear();
        BackendManager.Instance.questImage = 0;

        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Quest quest = new Rad_Model.Quest();

            if (data[i].Keys.Contains("url"))
            {
                quest.url = data[i]["url"].ToString();
                var str = quest.url.Split('/');
                var pk = int.Parse(str[str.Length - 2]);
                quest.id = pk;

                if(quest.id < 20)
                {
                    quest.quest_cover_image = BackendManager.Instance.SpriteToBase64(defaultQuestCoverImage[0]);
                }
                else
                {
                    quest.quest_cover_image = BackendManager.Instance.SpriteToBase64(defaultQuestCoverImage[1]);
                }
            }
            if (data[i].Keys.Contains("has_image"))
            {
                quest.has_image = bool.Parse((data[i]["has_image"].ToString()));
            }
            if (data[i].Keys.Contains("order"))
            {
                quest.order = int.Parse((data[i]["order"].ToString()));
            }
            if (data[i].Keys.Contains("quest_title"))
            {
                quest.quest_title = data[i]["quest_title"].ToString();
            }
            if (data[i].Keys.Contains("quote_text"))
            {
                quest.quote_text = data[i]["quote_text"].ToString();
            }
            if (data[i].Keys.Contains("type"))
            {
                quest.type = data[i]["type"].ToString();
            }
            if (data[i].Keys.Contains("organization"))
            {
                quest.organization = data[i]["organization"].ToString();
            }
            if (data[i].Keys.Contains("is_from_phase"))
            {
                quest.is_from_phase = data[i]["is_from_phase"].ToString();
            }
            if (data[i].Keys.Contains("duration"))
            {
                quest.duration = data[i]["duration"].ToString();
            }
            if (quest.has_image && data[i].Keys.Contains("quest_image") && !string.IsNullOrEmpty(data[i]["quest_image"].ToString()))
            {
                string url = data[i]["quest_image"].ToString();
                quest.quest_image = url;
                if (RadSaveManager.spriteListData.CheckForSprite(url))
                {
                    quest.quest_image = RadSaveManager.spriteListData.spriteData[url];
                    BackendManager.Instance.questImage++;
                }
                else
                {
                    StartCoroutine(BackendManager.Instance.GetQuestsSprite(url,quest.id));
                }
            }
            else
            {
                quest.quest_image = data[i]["quest_image"].ToString();
                BackendManager.Instance.questImage++;
            }

            allQuests.Add(quest);
        }

        if (BackendManager.Instance.questImage == allQuests.Count)
        {
            BackendManager.Instance.IsUserACoach();
        }
    }


    public void SetCurrentQuest()
    {
        BackendManager.Instance.SetLastAction(SetCurrentQuest);
        for (int i = 0; i < _thisPhaseQuests.Count; i++)
        {
            if (currentQuestId == _thisPhaseQuests[i].id)
            {
                currentQuest = _thisPhaseQuests[i];
                BackendManager.Instance.GetQuestStatus(currentQuestId, QuestStatusReceived, QuestStatusFailed);
                return;
            }

        }
    }

    public void CreateQuestStatus()
    {
        BackendManager.Instance.SetLastAction(CreateQuestStatus);
        Rad_Model.Quest_Status _quest = new Rad_Model.Quest_Status();

        _quest.belongs_to_ninja = BackendManager.Instance.radModel.ninja.url;

        if (BackendManager.Instance.UseProduction)
        {
            _quest.belongs_to_quest = BackendManager.Instance.ProductionUrl + BackendManager.Instance.ninjapiURI + BackendManager.Instance.questURI + GetCurrentQuest().id + '/';
        }
        else
        {
            _quest.belongs_to_quest = BackendManager.Instance.DevelopmentUrl + BackendManager.Instance.ninjapiURI + BackendManager.Instance.questURI + GetCurrentQuest().id + '/';
        }

        _quest.quest_page_active = 0;
        _quest.started_on_date = DateTime.Now;
        _quest.completed = false;
        string jsonToSend = JsonUtility.ToJson(_quest);
        BackendManager.Instance.CreateQuestStatus(jsonToSend, SetCurrentQuestStatus, FailCreateQuestStatus);
    }

    private void SetCurrentQuestStatus()
    {
        BackendManager.Instance.SetLastAction(SetCurrentQuestStatus);
        BackendManager.Instance.GetQuestStatus(currentQuest.id);
    }

    private void FailCreateQuestStatus()
    {
        guiManager.ShowNoInternetPanel();
    }
    /// <summary>
    /// QUEST STATUS RECEIVED, FILLING QUEST DATA AND IF THE QUEST WAS COMPLETED, FILL THE LOG
    /// </summary>
    private void QuestStatusReceived()
    {
        questPagesImageAssign = 0;
        BackendManager.Instance.SetLastAction(SetCurrentQuestStatus);
        BackendManager.Instance.GetPages(currentQuest.id, OnGetPagesFail);

    }

    public void AssignQuestImages(string base64img, int questId)
    {
        for (int i = 0; i < allQuests.Count; i++)
        {
            if (allQuests[i].id == questId)
            {
                BackendManager.Instance.questImage++;
                allQuests[i].quest_image = base64img;
                break;
            }
        }

        if(BackendManager.Instance.questImage == allQuests.Count)
        {
            BackendManager.Instance.IsUserACoach();
        }
    }

    public void AssignQuestPagesImages(string base64img, int questPageId)
    {
        for (int i = 0; i < questPages.Count; i++)
        {
            if(questPages[i].id == questPageId)
            {
                questPages[i].image = base64img;
                questPagesImageAssign++;
                break;
            }
        }


        if(questPagesImageAssign == totalQuestPages && !BackendManager.Instance.isCoach)
        {
            OnGetPagesSuccess();
        }
        else 
        {
            OnGetPagesSuccessForCoach();
        }
    }

    private void OnGetPagesSuccessForCoach()
    {
        RadSaveManager.SaveData();
        RadCoachProfileUIManager radCoachProfileUIManager = FindObjectOfType<RadCoachProfileUIManager>();
        if (radCoachProfileUIManager)
        {
            StartCoroutine(radCoachProfileUIManager.GetPagesSucceed());
        }
    }


    private void OnGetPagesSuccess()
    {
        RadSaveManager.SaveData();
        BackendManager.Instance.SetLastAction(OnGetPagesSuccess);
        guiManager.ResetPanelDataIndex();
        guiManager.ResetInputFields();
        for (int i = 0; i < questPages.Count; i++)
        {
            if (!questPages[i].is_info_page)
            {
                if (questPages[i].has_image)
                {
                    Sprite _sprite = BackendManager.Instance.DecodeBase64Texture(questPages[i].image);
                    guiManager.SetQuestPanelDataSprite(questPages[i].question_text ,_sprite, questPages[i].description_text);
                }
                else if (questPages[i].has_video)
                {
                    guiManager.SetQuestPanelDataVideo(questPages[i].question_text, questPages[i].description_text);
                }
                else
                {
                    guiManager.SetQuestPanelDataText(questPages[i].description_text, questPages[i].question_text);
                }
            }
            else
            {
                StartCoroutine(guiManager.SetInformationPageData(questPages[i]));
            }
        }

        guiManager.SetPaginationButtons(questPages.Count); //the last one it's info page
        draftsList.Clear();

        if (currentQuestId == 11 && guiManager)
        {
            guiManager.SetMediaPickerButtons(false);
        }
        else
        {
            guiManager.SetMediaPickerButtons(true);
        }

        if (currentQuestStatus != null)
        {
            if (PostsManager.Instance.CurrentQuestHasNinjaLog(currentQuest.url))
            {
                guiManager.SetAddDraftsToLog(false);
            }
            else
            {
                guiManager.SetAddDraftsToLog(true);
                guiManager.SetCreateLogButton();
            }


            if (currentQuestStatus.has_drafts.Count > 0)
            {
                guiManager.SetPaginationPagesComplete(currentQuestStatus.has_drafts.Count);
                BackendManager.Instance.GetDrafts(currentQuest.id, OnGetDraftFail);

            }
            else
            {
                guiManager.ResetPagesComplete();
                guiManager.ShowQuestPanel(currentQuestStatus.quest_page_active);
            }
        }
        else
        {

            guiManager.ResetStory();
            guiManager.ResetPagesComplete();
            BackendManager.Instance.GetOrganization(currentQuest.organization, OnGetOrganizationFail);
        }
    }

    public void SetCurrentOrganizationIconAndAssignToLandingPage(string base64img)
    {
        currentOrganization.icon = base64img;
        Sprite _sprite = BackendManager.Instance.DecodeBase64Texture(currentOrganization.icon);
        guiManager.ShowLandingPage(_sprite);
    }

    private void OnGetOrganizationFail()
    {
        guiManager.ShowNoInternetPanel();
    }
    private void OnGetPagesFail()
    {
        guiManager.ShowNoInternetPanel();
    }
    /// <summary>
    /// FOR NOW WE ARE DOING THE SAME, DON'T KNOW IF THEY WANT A PANEL SAYING SOMETHING WENT WRONG STANDARD MSG
    /// BECAUSE THE FIRST TIME WE WILL GET THE INFO THERE WON'T BE ANY STATUS 
    /// </summary>
    private void QuestStatusFailed()
    {
        guiManager.ShowNoInternetPanel();
    }

    private void OnGetDraftSuccess()
    {
        if (PostsManager.Instance.CurrentQuestHasNinjaLog(currentQuest.url))
        {
            for (int i = 0; i < PostsManager.Instance.GetPersonalLogsList().Count; i++)
            {
                if(PostsManager.Instance.GetPersonalLogsList()[i].belongs_to_quest == currentQuest.url)
                {
                    StartCoroutine(guiManager.SetPostLogs(PostsManager.Instance.GetPersonalLogsList()[i]));
                    break;
                }
            }
        }
        else
        {
            var str = currentQuest.url.Split('/');
            var pk = int.Parse(str[str.Length - 2]);
            StartCoroutine(guiManager.SetPostLogs(pk));
        }
        guiManager.SetDraftsText(draftsList);
        guiManager.ShowQuestPanel(currentQuestStatus.quest_page_active);


    }

    private void OnGetDraftFail()
    {
        guiManager.ShowNoInternetPanel();
    }
    public void SetDraftLists(Rad_Model.Draft[] _drafts)
    {
        draftsList.Clear();
        for (int i = 0; i < _drafts.Length; i++)
        {
            draftsList.Add(_drafts[i]);
        }
        draftsList.Sort((s1,s2) => s1.url.CompareTo(s2.url));
    }

    public void AddDraftToCurrentQuestStatus(Rad_Model.Draft _draft)
    {
        currentQuestStatus.has_drafts.Add(_draft.url);
        ModifyCurrentQuestStatus();
        
        if(_draft.has_image)
            _draft.image = BackendManager.Instance.SpriteToBase64(_draftSprite);
        draftsList.Add(_draft);
    }

    public void ModifyDraft(Rad_Model.Draft _draft)
    {
        for (int i = 0; i < draftsList.Count; i++)
        {
            if(_draft.url == draftsList[i].url)
            {
                draftsList[i] = _draft;
            }
        }
    }
    public void ModifyCurrentQuestStatus()
    {
        BackendManager.Instance.SetLastAction(ModifyCurrentQuestStatus);
        var jsonToSend = JsonUtility.ToJson(currentQuestStatus);
        var str = currentQuestStatus.url.Split('/');
        var pk = int.Parse(str[str.Length - 2]);
        BackendManager.Instance.ModifyQuestStatus(jsonToSend, pk);
    }
    public void SendLogDraft()
    {
        var jsonToSend = JsonUtility.ToJson(_draftSaved);
        if (_modifyDraft)
        {
            var str = _draftSaved.url.Split('/');
            var pk = int.Parse(str[str.Length - 2]);
            BackendManager.Instance.ModifyDraft(pk, jsonToSend);
        }
        else
        {
            BackendManager.Instance.CreateDraft(jsonToSend);
        }

    }


    public void SendLogDraft(TMPro.TMP_InputField _inputfield)
    {
        Rad_Model.Draft _draft = new Rad_Model.Draft();
        int _index = _inputfield.transform.parent.parent.parent.parent.GetSiblingIndex();

        if(_index < draftsList.Count)
        {
            _draft.url = draftsList[_index].url;
            _modifyDraft = true;
        }
        else
        {
            _modifyDraft = false;
        }

        _draft.description = _inputfield.text;
        _draft.belongs_to_ninja = BackendManager.Instance.radModel.ninja.url;
        _draft.belongs_to_quest = currentQuest.url;
        var _image = _inputfield.transform.parent.Find("Image").GetComponent<Image>();
        if (_image.sprite != null)
        {
            _draft.has_image = true;
            _draft.image = BackendManager.Instance.SpriteToBase64(_image.sprite);
            _draftSprite = _image.sprite;
        }
        else
        {
           // _draft.image = BackendManager.Instance.SpriteToBase64(defaultImageToSend);
            _draft.has_image = false;
        }
        _draftSaved = _draft;
        var jsonToSend = JsonUtility.ToJson(_draft);
        BackendManager.Instance.SetLastAction(SendLogDraft);

        if (_modifyDraft)
        {
            var str = _draft.url.Split('/');
            var pk = int.Parse(str[str.Length - 2]);
            
            BackendManager.Instance.ModifyDraft(pk, jsonToSend);
        }
        else
        {
            BackendManager.Instance.CreateDraft(jsonToSend);
        }
        
    }

    private void OnlevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {

        pathmanager = FindObjectOfType<PathsManager>();
        guiManager = FindObjectOfType<SFN_GuiManager>();
        GetPhaseQuests(BackendManager.Instance.radModel.currentPhase);
        
    }

    public void ResetQuestStatus()
    {
        currentQuestStatus = null;
    }

    public void SetQuestSlot(int id)
    {
        questSlot = id;
    }

    public int GetCurrentQuestSlot()
    {
        return questSlot;
    }

    public void ClearDraftList()
    {
        draftsList.Clear();
    }

    public Rad_Model.Quest GetCurrentQuest()
    {
        return currentQuest;
    }

    public Rad_Model.Quest_Status GetCurrentQuestStatus()
    {
        return currentQuestStatus;
    }

    public List<Rad_Model.Quest_Status> GetQuestStatusList()
    {
        questStatusList.Sort((a, b) => a.belongs_to_quest.CompareTo(b.belongs_to_quest));
        return questStatusList;
    }

    public List<Rad_Model.Draft> GetDraftList()
    {
        return draftsList;
    }
    public string GetCurrentQuestUrl()
    {
        return currentQuest.url;
    }

    public Rad_Model.Organization GetCurrentOrganization()
    {
        return currentOrganization;
    }

    public Rad_Model.Quest FindQuestWithUrl(string url)
    {
        for (int i = 0; i < allQuests.Count; i++)
        {
            if(url == allQuests[i].url)
            {
                return allQuests[i];
            }
        }
        return null;
    }

    public List<Rad_Model.Quest> GetQuestsList()
    {
        return allQuests;
    }

    public string GetQuestName(int id)
    {
        for (int i = 0; i < allQuests.Count; i++)
        {
            if(allQuests[i].id == id)
            {
                return allQuests[i].quest_title;
            }
        }

        return null;
    }


    private void QuestActiveNotification(Rad_Model.Quest_Status _status)
    {
        string[] str = _status.url.Split('/');
        int _id = int.Parse(str[str.Length - 2]);

        str = _status.belongs_to_quest.Split('/');
        int _questId = int.Parse(str[str.Length - 2]);
        string _icon;
        if (_questId < 20)
        {
            _icon = "fase1";
        }
        else
        {
            _icon = "fase2";
        }
        //schedule a reminder every 24h, don't worry, will be cancelled if the quest is completed
        Rad_NotificationsManager.Instance.CreateNotification(_id,
            GetQuestName(_questId) + " wacht op je",
            "Ninja! Hoe gaat het?", _icon,
            System.DateTime.Now.AddHours(24));
    }
}
