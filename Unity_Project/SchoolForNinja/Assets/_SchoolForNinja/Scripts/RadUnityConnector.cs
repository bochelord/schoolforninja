﻿using System.Collections.Generic;
using System;
using UnityEngine;
using LitJson;
using System.Collections;
using UnityEngine.Networking;

public class RadUnityConnector : MonoBehaviour {

    public string webServiceUrl = "";
    public string spreadsheetId = "";
    public string worksheetName = "";
    //public string worksheet2Name = "";
    public string password = "";
    public float maxWaitTime = 10f;


    [Space(5f)]
    public bool debugMode;

    bool updating;
    string currentStatus;
    JsonData[] ssObjects2;
    bool saveToGS;



    #region Connection
    public void Connect()
    {
        if (updating)
            return;

        updating = true;
        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        string connectionString = webServiceUrl + "?ssid=" + spreadsheetId + "&sheet=" + worksheetName + "&pass=" + password + "&action=GetData";
        if (debugMode)
            Debug.Log("Connecting to webservice on " + connectionString);

        UnityWebRequest www = new UnityWebRequest(connectionString);

        float elapsedTime = 0.0f;
        currentStatus = "Stablishing Connection... ";

        while (!www.isDone)
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime >= maxWaitTime)
            {
                currentStatus = "Max wait time reached, connection aborted.";
                Debug.Log(currentStatus);
                updating = false;
                break;
            }

            yield return null;
        }

        if (!www.isDone || !string.IsNullOrEmpty(www.error))
        {
            currentStatus = "Connection error after" + elapsedTime.ToString() + "seconds: " + www.error;
            Debug.LogError(currentStatus);
            updating = false;
            yield break;
        }

        string response = www.downloadHandler.text;
        Debug.Log(elapsedTime + " : " + response);
        currentStatus = "Connection stablished, parsing data...";

        if (response == "\"Incorrect Password.\"")
        {
            currentStatus = "Connection error: Incorrect Password.";
            Debug.LogError(currentStatus);
            updating = false;
            yield break;
        }

        try
        {
            //ssObjects = JsonMapper.ToObject<JsonData[]>(response);
        }
        catch
        {
            currentStatus = "Data error: could not parse retrieved data as json.";
            Debug.LogError(currentStatus);
            updating = false;
            yield break;
        }

        currentStatus = "Data Successfully Retrieved!" + response;
        updating = false;


        //we call the second getdata to 
        //StartCoroutine(GetData2());

        //ManipulateData(ssObjects);
    }

    #endregion
    //public void ManipulateData(JsonData[] ssObjects)
    //{

    //    //TODO TODO : Manipulate here the data (change a SO object or so)
    //    for (int i = 0; i < ssObjects.Length; i++)
    //    {
    //        Building _tempBuilding = new Building();

    //        if (ssObjects[i].Keys.Contains("Name"))
    //        {
    //            _tempBuilding.id = ssObjects[i]["Name"].ToString();
    //            if (string.Equals(ssObjects[i]["Name"].ToString(), "Solar Panels"))
    //            {
    //                _tempBuilding.type = BuildingType.DesertResourceGenerator;
    //                //_tempBuilding.sprite = solarPanel;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Water pumps"))
    //            {
    //                _tempBuilding.type = BuildingType.CoastResourceGenerator;
    //                //_tempBuilding.sprite = hydroPlant;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Mining Rocks"))
    //            {
    //                _tempBuilding.type = BuildingType.MountainResourceGenerator;
    //                //_tempBuilding.sprite = miningRocks;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Housing"))
    //            {
    //                _tempBuilding.type = BuildingType.Housing;
    //                //_tempBuilding.sprite = housing;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Factory"))
    //            {
    //                _tempBuilding.type = BuildingType.Factory;
    //                //_tempBuilding.sprite = factory;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Farm"))
    //            {
    //                _tempBuilding.type = BuildingType.Farm;
    //                // _tempBuilding.sprite = coastFarm;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Entertainment"))
    //            {
    //                _tempBuilding.type = BuildingType.Entertainment;
    //                //_tempBuilding.sprite = entertainment;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Museum"))
    //            {
    //                _tempBuilding.type = BuildingType.Museum;
    //                //_tempBuilding.sprite = museum;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Hospital"))
    //            {
    //                _tempBuilding.type = BuildingType.Hospital;
    //                //_tempBuilding.sprite = hospital;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "Business Center"))
    //            {
    //                _tempBuilding.type = BuildingType.BusinessCenter;
    //                //_tempBuilding.sprite = businessCenter;
    //            }
    //            else if (string.Equals(ssObjects[i]["Name"].ToString(), "School"))
    //            {
    //                _tempBuilding.type = BuildingType.School;
    //                //_tempBuilding.sprite = school;
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Electricity Costs"))
    //        {
    //            if (ssObjects[i]["Electricity Costs"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.electricityCost = (int)(double)ssObjects[i]["Electricity Costs"];
    //            }
    //            else if (ssObjects[i]["Electricity Costs"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.electricityCost = (int)ssObjects[i]["Electricity Costs"];
    //            }
    //            else if (ssObjects[i]["Electricity Costs"].GetJsonType() == JsonType.String)
    //            {
    //                //_tempBuilding.electricityCost = Int32.Parse(ssObjects[i]["Electricity Costs"].ToString());
    //                string tempString = ssObjects[i]["Electricity Costs"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.electricityCost = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.electricityCost = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.electricityCost = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Rocks Costs"))
    //        {
    //            if (ssObjects[i]["Rocks Costs"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.rocksCost = (int)(double)ssObjects[i]["Rocks Costs"];
    //            }
    //            else if (ssObjects[i]["Rocks Costs"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.rocksCost = (int)ssObjects[i]["Rocks Costs"];
    //            }
    //            else if (ssObjects[i]["Rocks Costs"].GetJsonType() == JsonType.String)
    //            {
    //                // _tempBuilding.rocksCost = Int32.Parse(ssObjects[i]["Rocks Costs"].ToString());
    //                string tempString = ssObjects[i]["Rocks Costs"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.rocksCost = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.rocksCost = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.rocksCost = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Water Costs"))
    //        {
    //            if (ssObjects[i]["Water Costs"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.waterCost = (int)(double)ssObjects[i]["Water Costs"];
    //            }
    //            else if (ssObjects[i]["Water Costs"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.waterCost = (int)ssObjects[i]["Water Costs"];
    //            }
    //            else if (ssObjects[i]["Water Costs"].GetJsonType() == JsonType.String)
    //            {
    //                //_tempBuilding.waterCost = Int32.Parse(ssObjects[i]["Water Costs"].ToString());
    //                string tempString = ssObjects[i]["Water Costs"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.waterCost = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.waterCost = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.waterCost = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Electricity Daily"))
    //        {
    //            if (ssObjects[i]["Electricity Daily"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.electricityDaily = (int)(double)ssObjects[i]["Electricity Daily"];
    //            }
    //            else if (ssObjects[i]["Electricity Daily"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.electricityDaily = (int)ssObjects[i]["Electricity Daily"];
    //            }
    //            else if (ssObjects[i]["Electricity Daily"].GetJsonType() == JsonType.String)
    //            {
    //                //_tempBuilding.electricityDaily = Int32.Parse(ssObjects[i]["Electricity Daily"].ToString());
    //                string tempString = ssObjects[i]["Electricity Daily"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.electricityDaily = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.electricityDaily = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.electricityDaily = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Rocks Daily"))
    //        {
    //            if (ssObjects[i]["Rocks Daily"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.rocksDaily = (int)(double)ssObjects[i]["Rocks Daily"];
    //            }
    //            else if (ssObjects[i]["Rocks Daily"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.rocksDaily = (int)ssObjects[i]["Rocks Daily"];
    //            }
    //            else if (ssObjects[i]["Rocks Daily"].GetJsonType() == JsonType.String)
    //            {
    //                //_tempBuilding.rocksDaily = Int32.Parse(ssObjects[i]["Rocks Daily"].ToString());
    //                string tempString = ssObjects[i]["Rocks Daily"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.rocksDaily = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.rocksDaily = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.rocksDaily = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        if (ssObjects[i].Keys.Contains("Water Daily"))
    //        {
    //            if (ssObjects[i]["Water Daily"].GetJsonType() == JsonType.Double)
    //            {
    //                _tempBuilding.waterDaily = (int)(double)ssObjects[i]["Water Daily"];
    //            }
    //            else if (ssObjects[i]["Water Daily"].GetJsonType() == JsonType.Int)
    //            {
    //                _tempBuilding.waterDaily = (int)ssObjects[i]["Water Daily"];
    //            }
    //            else if (ssObjects[i]["Water Daily"].GetJsonType() == JsonType.String)
    //            {
    //                //_tempBuilding.waterDaily = Int32.Parse(ssObjects[i]["Water Daily"].ToString());
    //                string tempString = ssObjects[i]["Water Daily"].ToString();
    //                if (tempString.Contains("+"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.waterDaily = Int32.Parse(tempString);
    //                }
    //                else if (tempString.Contains("-"))
    //                {
    //                    tempString.Remove(0);
    //                    _tempBuilding.waterDaily = -Int32.Parse(tempString);
    //                }
    //                else
    //                {
    //                    _tempBuilding.waterDaily = Int32.Parse(tempString);
    //                }
    //            }
    //        }

    //        buildingsList.buildingsList.Add(_tempBuilding);
    //    }
    //}
}
