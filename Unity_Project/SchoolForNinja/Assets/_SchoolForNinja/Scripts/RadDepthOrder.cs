﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class RadDepthOrder : MonoBehaviour {

    [BoxGroup("School")]
    public bool showSchool;
    [ShowIf("showSchool")]
    [BoxGroup("School")]
    public SpriteRenderer school;
    [ShowIf("showSchool")]
    [BoxGroup("School")]
    [Tooltip("Value where sprite will be over player")]
    public int topValueSchool;
    [ShowIf("showSchool")]
    [BoxGroup("School")]
    [Tooltip("Value where sprite will be under player")]
    public int botValueSchool;
    [ShowIf("showSchool")]
    [BoxGroup("School")]
    [Tooltip("Value where school sorting order will switch")]
    public int switchValueSchool;

    [BoxGroup("FirstVillage")]
    public bool showFirstVillage;
    [ShowIf("showFirstVillage")]
    [BoxGroup("FirstVillage")]
    public SpriteRenderer firstVillage;
    [ShowIf("showFirstVillage")]
    [BoxGroup("FirstVillage")]
    [Tooltip("Value where sprite will be over player")]
    public int topValueFirstVillage;
    [ShowIf("showFirstVillage")]
    [BoxGroup("FirstVillage")]
    [Tooltip("Value where sprite will be under player")]
    public int botValueFirstVillage;
    [ShowIf("showFirstVillage")]
    [BoxGroup("FirstVillage")]
    [Tooltip("Value where first village sorting order will switch")]
    public int switchValueFirstVillage;

    [BoxGroup("Lift")]
    public bool showLift;
    [ShowIf("showLift")]
    [BoxGroup("Lift")]
    public SpriteRenderer lift;
    [ShowIf("showLift")]
    [BoxGroup("Lift")]
    [Tooltip("Value where sprite will be over player")]
    public int topValueLift;
    [ShowIf("showLift")]
    [BoxGroup("Lift")]
    [Tooltip("Value where sprite will be under player")]
    public int botValueLift;
    [ShowIf("showLift")]
    [BoxGroup("Lift")]
    [Tooltip("Value where first village sorting order will switch")]
    public int switchValueLift;


    [BoxGroup("ShoreHouse")]
    public bool showShoreHouse;
    [ShowIf("showShoreHouse")]
    [BoxGroup("ShoreHouse")]
    public SpriteRenderer shoreHouse;
    [ShowIf("showShoreHouse")]
    [BoxGroup("ShoreHouse")]
    [Tooltip("Value where sprite will be over player")]
    public int topValueshoreHouse;
    [ShowIf("showShoreHouse")]
    [BoxGroup("ShoreHouse")]
    [Tooltip("Value where sprite will be under player")]
    public int botValueshoreHouse;
    [ShowIf("showShoreHouse")]
    [BoxGroup("ShoreHouse")]
    [Tooltip("Value where first village sorting order will switch")]
    public int switchValueshoreHouse;
    private SpriteRenderer cachedSpriteRenderer;
    public Transform walkingTransforn;
	// Use this for initialization
	void Start ()
    {
        cachedSpriteRenderer = this.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //cachedSpriteRenderer.sortingOrder = (UnityEngine.Mathf.FloorToInt(walkingTransforn.position.y * 1));
        if(walkingTransforn.position.y > switchValueSchool)
        {
            school.sortingOrder = topValueSchool;
        }
        else
        {
            school.sortingOrder = botValueSchool;
        }

        if (walkingTransforn.position.y > switchValueFirstVillage)
        {
            firstVillage.sortingOrder = topValueFirstVillage;
        }
        else
        {
            firstVillage.sortingOrder = botValueFirstVillage;
        }

        if(walkingTransforn.position.y > switchValueshoreHouse)
        {
            shoreHouse.sortingOrder = topValueshoreHouse;
        }
        else
        {
            shoreHouse.sortingOrder = botValueshoreHouse;
        }
        if (walkingTransforn.position.y > switchValueLift )
        {
            cachedSpriteRenderer.sortingOrder = topValueLift;
        }
        if (walkingTransforn.position.y < switchValueLift)
        {
            cachedSpriteRenderer.sortingOrder = botValueLift;
        }
    }
}
