﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerProfile 
{
    //old legacy 
    public string name = "";
    public float progress = 0.08f; //Progress bar has 25 squares, Slider max value is 1, so 1/25 = 0.04, so 2 squares is 0.08
    public int level = 1;


    public List<int> questsDone = new List<int>();


    public string gameVersion = "";
    public int profileID;

    public string username = "";

    public bool firstTimeInstalled;
    public string language; //in the form: EN , NL, ES, DE, FR, IT, etc...
    //public int level;


    public System.DateTime profileCreatedTimestamp;
    public System.DateTime lastSaveTimestamp;

    public PlayerProfile GeneratePlayerProfile(int id)
    {
        PlayerProfile profile = new PlayerProfile();
    #if !UNITY_EDITOR
        profile.gameVersion = Application.version;
    #else
        profile.gameVersion = "Debugversion";
    #endif


        profile.profileID = id;
        profile.username = "default";
        profile.firstTimeInstalled = true;

        profile.language = "EN";
        profile.profileCreatedTimestamp = System.DateTime.Now;
        profile.lastSaveTimestamp = System.DateTime.Now;



        return profile;
    }
}
