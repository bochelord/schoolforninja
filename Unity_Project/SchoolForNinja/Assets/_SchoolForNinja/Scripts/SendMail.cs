﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;
using DG.Tweening;
using UnityEngine.Networking;   


public class SendMail : MonoBehaviour
{

    //recipient's email id

    public string email_string = "support@schoolforninja.nl";

    //email subject

    public string subject_string = "School For Ninja support";

    //email body

    string body_string = "";

    public RectTransform supportPanel;


    public void ShowSupportPanel()
    {
        MovePanel_In(supportPanel);
    }


    public void ExitSupportPanel()
    {
        MovePanel_Out_Left(supportPanel);
    }

    //for sending mail to single recipient

    public void sendEMailToSingleRecipient()
    {

        string email = email_string;

        string subject = EscapeURLFunction(subject_string);

        string body = EscapeURLFunction(body_string);

        //Open the native default app

        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);

    }



    string EscapeURLFunction(string url)
    {
        return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
    }

    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, 0), 0.25f); //To left position
    }

    #endregion ===============================================================================================

}

