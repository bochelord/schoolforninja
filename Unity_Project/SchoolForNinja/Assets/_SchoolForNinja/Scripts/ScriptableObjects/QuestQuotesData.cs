﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestQuotesData : ScriptableObject {

    [System.Serializable]
    public struct Quote
    {
        public int questQuote;
        public string quoteText;
        public Sprite quoteSprite;
        public string quoteAuthor;
    }

    public List<Quote> quotesList = new List<Quote>();
}
