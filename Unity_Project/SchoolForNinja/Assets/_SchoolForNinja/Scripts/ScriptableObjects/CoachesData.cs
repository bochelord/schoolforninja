﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoachesData : ScriptableObject {

    [System.Serializable]
    public struct Coach
    {
        public string id;
        public Sprite coachPhoto;
        public string name;
        public string jobPosition;
        public string location;
        public string description;
        public string contact;
    }

    public List<Coach> coachesList = new List<Coach>();
}
