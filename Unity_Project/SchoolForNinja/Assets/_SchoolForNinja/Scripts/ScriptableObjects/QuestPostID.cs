﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestPostID : ScriptableObject {

    [System.Serializable]
    public class Quest
    {
        public int phase;
        public int questNumber;
        public int uitlegPostId;
        public int opdrachtPostId;
        public int quotePostId;
        public Sprite quoteSprite;
    }

    public List<Quest> questList = new List<Quest>();
}
