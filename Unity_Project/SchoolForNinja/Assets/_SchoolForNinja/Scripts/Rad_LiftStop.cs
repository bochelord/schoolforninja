﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rad_LiftStop : MonoBehaviour {

    private PathsManagerFase1 _pathsManager;
    public bool goingUp;
    public Rad_LiftStop[] _stops;
    public bool playerchaningPosition;
    private void Awake()
    {
        _pathsManager = FindObjectOfType<PathsManagerFase1>();    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && goingUp && !playerchaningPosition)
        {

            StartCoroutine(PlayerMovementCancel());
            _pathsManager.LiftMovingUp();


            
        }
        else if(collision.tag == "Player" && !playerchaningPosition)
        {

            StartCoroutine(PlayerMovementCancel());
            _pathsManager.LiftMovingDown();

            
        }
    }

    IEnumerator PlayerMovementCancel()
    {
        for (int i = 0; i < _stops.Length; i++)
        {
            _stops[i].playerchaningPosition = true;
        }
        yield return new WaitForSeconds(10);
        for (int i = 0; i < _stops.Length; i++)
        {
            _stops[i].playerchaningPosition = false;
        }

    }
}
