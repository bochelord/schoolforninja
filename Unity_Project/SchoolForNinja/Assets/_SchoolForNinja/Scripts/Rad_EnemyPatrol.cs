﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rad_EnemyPatrol : MonoBehaviour {

    public Transform endPoint;
    public float time;
    private Vector2 _startPosition;


    private void Start()
    {
        _startPosition = this.transform.position;

        InvokeRepeating("StartPatrol", 0, time*2);
    }

    private void StartPatrol()
    {
        this.transform.localRotation = new Quaternion(0, 0, 0, 0);
        this.transform.DOMove(endPoint.position, time).OnComplete(() =>
        {
            this.transform.localRotation = new Quaternion(0, 180, 0, 0);
            this.transform.DOMove(_startPosition, time);
        });
    }
}
