﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteData : MonoBehaviour {

    public Dictionary<string,string> spriteData = new Dictionary<string, string>();

    public bool CheckForSprite(string _key)
    {
        if (spriteData.ContainsKey(_key))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
