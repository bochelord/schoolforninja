﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;

public class PathsManager : MonoBehaviour {

    [BoxGroup("Quest Buttons")] public Rad_Quest[] quest;

    [BoxGroup("Player")] public GameObject player;
    [BoxGroup("Player")] public PlayerMovementController movementController;

    [BoxGroup("Paths")] [Required] public AstarPath pathScan;

    [HideInInspector]public QuestManagerV2 _questManager;
    [HideInInspector]public SFN_GuiManager _guiManager;

    public virtual void Start()
    {
        _guiManager = FindObjectOfType<SFN_GuiManager>();
        _questManager = FindObjectOfType<QuestManagerV2>();

    }


    public virtual void QuestCompleted(int id)
    {
        switch (id)
        {
            case 1:
                player.transform.position = quest[0].transform.position;
                EnableQuestSprite(quest[0].gameObject);
                break;
            case 2:
                player.transform.position = quest[1].transform.position;
                EnableQuestSprite(quest[1].gameObject);
                break;
            case 3:
                player.transform.position = quest[2].transform.position;
                EnableQuestSprite(quest[2].gameObject);
                break;
            case 4:
                player.transform.position = quest[3].transform.position;
                EnableQuestSprite(quest[3].gameObject);
                break;
            case 5:
                player.transform.position = quest[4].transform.position;
                EnableQuestSprite(quest[4].gameObject);
                break;
            case 6:
                player.transform.position = quest[5].transform.position;
                EnableQuestSprite(quest[5].gameObject);
                break;
            case 7:
                player.transform.position = quest[6].transform.position;
                EnableQuestSprite(quest[6].gameObject);
                break;
            case 8:
                EnableQuestSprite(quest[7].gameObject);
                break;
            case 9:
                EnableQuestSprite(quest[8].gameObject);
                break;
        }
    }


    public virtual void EnableQuestSprite(GameObject _questButton)
    {
        //_questButton.GetComponent<BoxCollider2D>().enabled = false;
        _questButton.GetComponent<SpriteRenderer>().DOFade(1, 2);
    }

    public virtual void GetQuestStatusToLoadMap()
    {
        BackendManager.Instance.SetLastAction(GetQuestStatusToLoadMap);
        BackendManager.Instance.GetAllQuestStatus(delegate { StartCoroutine(CheckQuestStatusdAndLoadMap()); }, OnFail);
    }

    public virtual void OnFail()
    {
        _guiManager.ShowNoInternetPanel();
    }

    public virtual IEnumerator CheckQuestStatusdAndLoadMap()
    {
        Dictionary<int, int> _phaseQuest = new Dictionary<int, int>();
        List<int> _slotToUnlock = new List<int>();
        for (int i = 0; i < quest.Length; i++)
        {
            _phaseQuest.Add(quest[i].questId, quest[i].questSlot);
        }


        for (int i = 0; i < _questManager.GetQuestStatusList().Count; i++)
        {
            var str = _questManager.GetQuestStatusList()[i].belongs_to_quest.Split('/');
            var pk = int.Parse(str[str.Length - 2]);

            if (_phaseQuest.ContainsKey(pk) && _questManager.GetQuestStatusList()[i].completed)
            {
                _slotToUnlock.Add(_phaseQuest[pk]);
            }

        }

        _slotToUnlock.Sort((s1, s2) => s1.CompareTo(s2));

        for (int i = 0; i < _slotToUnlock.Count; i++)
        {
            QuestCompleted(_slotToUnlock[i]);
        }
        yield return new WaitForSeconds(1);
        _guiManager.loadingPage.SetActive(false);

    }
}
