﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class LiftAnimationTrigger : MonoBehaviour
{

    [BoxGroup("Camera")][Required][SerializeField] private Camera mainCamera;
    [BoxGroup("Camera")][Required][SerializeField] private Camera secondCamera;
    [BoxGroup("Camera")][Required][SerializeField] private Transform downElevatorPosition;
    [BoxGroup("Camera")][Required][SerializeField] private Transform cameraNormalPosition;
    [BoxGroup("Camera")]public float cameraNormalSize; 
    [BoxGroup("Camera")]public float cameraSecondStepSize;

    [BoxGroup("Quest Canvas")] public Canvas questPanelCanvas;

    [BoxGroup("Rope")] public GameObject rope;
    [BoxGroup("Rope")] public Transform secondPosition;
    private Vector3 _originalPosition;

    private DOTweenAnimation[] animations;
    [Required][SerializeField]private Transform targetForLifterFirstStep;
    [Required][SerializeField]private Transform targetForPlayer;
    private PlayerMovementController _playerMovementController;
    private bool liftEnabled = false;

    private void Start()
    {
        animations = GetComponentsInChildren<DOTweenAnimation>();
        _playerMovementController = FindObjectOfType<PlayerMovementController>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && liftEnabled)
        {
            GameObject player = collision.transform.parent.gameObject;
            StartCoroutine(ElevatorDelay(1.5f, player));
        }
        
    }

    IEnumerator ElevatorDelay(float _time,GameObject player)
    {
        yield return new WaitForSeconds(_time);
        StartCoroutine(CallWithDelay(2, player));
        _playerMovementController.canMove = false;
        _playerMovementController.destinationSetter.GetComponent<Pathfinding.AIPath>().canMove = false;
        for (int i = 0; i < animations.Length; i++)
            animations[i].DOPlay();
        player.transform.DOMoveY(targetForLifterFirstStep.position.y, 10).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            questPanelCanvas.worldCamera = secondCamera;
            _playerMovementController.canMove = true;
            _playerMovementController.destinationSetter.GetComponent<Pathfinding.AIPath>().canMove = true;
            StartCoroutine(RemoveRopeDelay(5));
        });
    }
    IEnumerator CallWithDelay(float _time, GameObject player)
    {
        yield return new WaitForSeconds(_time);
        _originalPosition = rope.transform.position;
        rope.transform.DOMoveY(secondPosition.position.y,4,false);
        mainCamera.gameObject.SetActive(false);
        secondCamera.gameObject.SetActive(true);
    }

    IEnumerator RemoveRopeDelay(float _time)
    {
        yield return new WaitForSeconds(_time);
        rope.transform.position = _originalPosition;
    }
    public void SetLiftEnabled()
    {
        liftEnabled = true;
    }
}
