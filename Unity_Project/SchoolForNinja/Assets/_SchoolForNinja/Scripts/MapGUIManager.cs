﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;

public class MapGUIManager : MonoBehaviour {

    [BoxGroup("Phase Panel")] public RectTransform phasePanel;
    [BoxGroup("Phase Panel")] public TMP_Text phaseTitle;
    [BoxGroup("Phase Panel")] public TMP_Text phaseSubtitle;
    [BoxGroup("Phase Panel")] public TMP_Text phaseDescription;
    [BoxGroup("Phase Panel")] public Button phasePlayButton;

    [BoxGroup("Locks")] public GameObject[] lockImages;

    [BoxGroup("Fase buttons")] public GameObject[] faseButtons;
    [BoxGroup("Fase buttons")] public GameObject[] faseCompletedButtons;

    [BoxGroup("Scenes names")] public string fase1Scene;
    [BoxGroup("Scenes names")] public string fase2Scene;
    [BoxGroup("Scenes names")] public string fase3Scene;
    [BoxGroup("Scenes names")] public string fase4Scene;
    [BoxGroup("Scenes names")] public string fase5Scene;

    private bool[] phaseLocked = new bool[5];

    private void Start()
    {
        BackendManager.Instance.GetPhaseStatusSuccess += OnGetPhaseStatusSuccess;
        BackendManager.Instance.GetPhaseStatus();

    }

    private void OnGetPhaseStatusSuccess(string response)
    {
        var _phasesStatus = BackendManager.Instance.radModel.phaseStatusList;

        for (int i = 0; i < phaseLocked.Length; i++)
        {
            if(i < _phasesStatus.Count && i < 3) //TODO check for new phases
            {
                phaseLocked[i] = false;
                lockImages[i].SetActive(false);
                if (_phasesStatus[i].completed)
                {
                    faseButtons[i].SetActive(false);
                    faseCompletedButtons[i].SetActive(true);
                }
            }
            else
            {
                phaseLocked[i] = true;
            }
        }
    }

    public void CallPhasePanel(int phaseNumber)
    {
        //get phase info based on phaseNumber then move in panel.
        if (!phasePlayButton.IsActive() && phaseNumber == 0)
        {
            phasePlayButton.gameObject.SetActive(true);
        }
        phaseTitle.text = BackendManager.Instance.radModel.phasesList[phaseNumber].theme;
        phaseSubtitle.text = BackendManager.Instance.radModel.phasesList[phaseNumber].name;
        phaseDescription.text = BackendManager.Instance.radModel.phasesList[phaseNumber].info;

        MovePanel_In(phasePanel);

        if (phaseLocked[phaseNumber])
        {
            phasePlayButton.gameObject.SetActive(false);
        }
        else
        {
            phasePlayButton.onClick.AddListener(delegate { GotoFase(phaseNumber); });
        }
    }

    public void ClosePhasePanel()
    {
        MovePanel_Out_Right(phasePanel);
    }


    public void GotoFase(int _phaseNumber)
    {
        if (!BackendManager.Instance.IsLogged())
        {
            Debug.LogError("User NOT LOGGED IN, something is smelly here...");
            return;
        }
        BackendManager.Instance.radModel.currentPhase = _phaseNumber;
        switch (_phaseNumber)
        {
            case 0:
                if(!string.IsNullOrEmpty(fase1Scene))
                    LoadingSceneManager.LoadScene(fase1Scene);
                break;
            case 1:
                if (!string.IsNullOrEmpty(fase2Scene))
                    LoadingSceneManager.LoadScene(fase2Scene);
                break;
            case 2:
                if (!string.IsNullOrEmpty(fase3Scene))
                    LoadingSceneManager.LoadScene(fase3Scene);
                break;
            case 3:
                if (!string.IsNullOrEmpty(fase4Scene))
                    LoadingSceneManager.LoadScene(fase4Scene);
                break;
            case 4:
                if (!string.IsNullOrEmpty(fase5Scene))
                    LoadingSceneManager.LoadScene(fase5Scene);
                break;
        }

      //  Debug.LogError("RADError - next scene is undefined. Please define it in inspector on MapGUIManager");
    }

    private void OnDestroy()
    {
        BackendManager.Instance.GetPhaseStatusSuccess -= OnGetPhaseStatusSuccess;
    }
    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-1300, 0), 0.25f); //To left position
    }

    #endregion ===============================================================================================
}
