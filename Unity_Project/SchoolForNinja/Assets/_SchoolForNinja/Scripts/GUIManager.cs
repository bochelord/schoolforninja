﻿using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class GUIManager : MonoBehaviour {

    [BoxGroup("Map Group")]
    public GameObject mapGroup;

    [BoxGroup("Quest Group")]
    public QuestGroupManager questGroupManager;

    [BoxGroup("Player Profile")]
    public Transform profilePicture;

    [HideInInspector]
    public PopUpText popUpText;

    private float currentProgress;

    private bool isQuestGroupManagerTriggered = false;
    private bool isMapGroupTriggered = false;
    private void Start()
    {
        popUpText = FindObjectOfType<PopUpText>();
        currentProgress = RadSaveManager.profile.progress;
        //currentProgress = 1;
    }

    void Update () {

		if (questGroupManager.isActiveAndEnabled && !isQuestGroupManagerTriggered)
        {
            questGroupManager.AnimateProgressBar();

            float clampedProgress = currentProgress * 25f;
            PopUpText.ShowMoneyPopup("+" + clampedProgress.ToString(), 60f, profilePicture.position, 2f);

            isQuestGroupManagerTriggered = true;
        }

        if (mapGroup.activeInHierarchy && !isMapGroupTriggered)
        {
            DoPunchScale(mapGroup);
            isMapGroupTriggered = true;
        }

	}

    public void DoPunchScale(GameObject item)
    {
        item.transform.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), 1f, 5, 0.5f);
    }
}
