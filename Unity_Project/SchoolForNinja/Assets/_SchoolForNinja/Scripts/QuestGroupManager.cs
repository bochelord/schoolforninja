﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class QuestGroupManager : MonoBehaviour {

    public Slider questGroupSlider;

    //private bool panelActivated = false;

	void Start () {

        if (questGroupSlider != null)
        {
            questGroupSlider.value = RadSaveManager.profile.progress;
        }
        else
        {
            Debug.LogError("questGroupSlider doesn't have a Slider component attached!");
        }
	}

    /// <summary>
    /// Animate the Slider progress bar taking the static 'progress' var value from PlayerProfile.cs
    /// </summary>
    public void AnimateProgressBar()
    {
        DOTween.To(x => questGroupSlider.value = x, 0, RadSaveManager.profile.progress, 1f).OnComplete(()=>
        {
            return;
        });
    }
}
