﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class PlayerMovementController : MonoBehaviour
{
    private enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
    private Direction _direction;

    private Vector2 clickPos, worldClickPos, displacementVector, targetPosition, oldPosition;
    private int currentAnimState;
    private float epsilon = 0.45f; //error in the displacement 
    [SerializeField]
    public float movementSpeed = 1.0f;
    public bool redPlayer = false;
    [Required]
    [SerializeField]
    private Animator animator;
    public Pathfinding.AIDestinationSetter destinationSetter;
    public Transform dummyTarget;
    private Transform player;

    public GameObject prefabTouchUI;
    public bool canMove = true;

    [Header("Animators controllers")]

    [Required]
    [SerializeField]
    private RuntimeAnimatorController animatorController;

    [Required]
    [SerializeField]
    private RuntimeAnimatorController redAnimatorController;
    
    void Start()
    {
        IsRedPlayer = redPlayer;
        oldPosition = Vector2.zero;
        player = destinationSetter.gameObject.transform;
        oldPosition = player.position;
        targetPosition = player.position;
    }

    // Update is called once per frame
    void Update()
    {

        //if position and target position have a difference smaller than epsilon we can work as they are the same position
        if (Mathf.Abs(targetPosition.x - player.transform.position.x) < epsilon && Mathf.Abs(targetPosition.y - player.transform.position.y) < epsilon)
        {
            currentAnimState = animator.GetInteger("MovementState");
            oldPosition = player.position; //to be sure that idle is going to be triggered in SelectAnimation()

            if (currentAnimState >= 0) //if we are not in idle
            {
                if (animator.GetInteger("MovementState") == (int)Direction.Up)
                {
                    //if player stop after moving up we launch up idle
                    animator.SetInteger("MovementState", -1);
                }
                else
                {
                    //if not we launch down idle
                    animator.SetInteger("MovementState", -2);
                }
            }
        }

        if (Input.GetButtonUp("Fire1") && canMove)
        {
            // save ended touch 2d point
            clickPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            worldClickPos = Camera.main.ScreenToWorldPoint(clickPos);
            //Instantiate(prefabTouchUI, worldClickPos, Quaternion.identity);
            //transform.DOKill();//We kill all the tweens that have been fired before to avoid residual bugs (so if you click twice very quickly in 2 different positions the guy gets crazy...)
            //transform.DOMove(worldClickPos, movementSpeed).SetSpeedBased(true).SetEase(Ease.Linear);

            targetPosition = worldClickPos;
            dummyTarget.transform.position = targetPosition;
            destinationSetter.target = dummyTarget;
        }

        SelectAnimation();


    }

    private void SelectAnimation()
    {
        //create vector from the two points
        displacementVector = new Vector2(player.transform.position.x - oldPosition.x, player.transform.position.y - oldPosition.y);
        //normalize the 2d vector
        displacementVector.Normalize();
        if (displacementVector != Vector2.zero)
        {
            if (displacementVector.x < -0.25f || displacementVector.x > 0.25f) //left of right animation
            {
                if (displacementVector.x > 0.1f)
                {
                    _direction = Direction.Right;
                }
                else if (displacementVector.x < -0.1f)
                {
                    _direction = Direction.Left;
                }
                    
            }
            else //up or down animation
            {
                if (displacementVector.y > 0.25f)
                {
                    _direction = Direction.Up;
                }
                else if (displacementVector.y < -0.25f)
                {
                    _direction = Direction.Down;
                }
                    
            }
            animator.SetInteger("MovementState", (int)_direction); //we set the animator to play - states in animator prepared to fit with the enum values
            oldPosition = player.transform.position;
        }
    }
    public bool IsRedPlayer //use to change between players in code 
    {
        set
        {
            if (value == true)
            {
                animator.runtimeAnimatorController = redAnimatorController;
            }
            else
            {
                animator.runtimeAnimatorController = animatorController;
            }
        }
    }

}

