﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerWelkom : MonoBehaviour {

    public GameObject[] textObjectArray;
    private int _index = 1;
    public GameObject nextButton;
    public GameObject previousButton;

    public GameObject speechContainerObject;
    public GameObject questionaireObject;
    public GameObject[] questionsArray;
    private int _indexQuestion = 1;


    #region Button eventcalls
    public void Button_next()
    {

        if (_index < 4)
        {
            _index = _index + 1;
            
            for (int i = 0; i < textObjectArray.Length; i++)
            {
                textObjectArray[i].SetActive(false);
            }

            textObjectArray[_index - 1].SetActive(true);
            //nextButton.SetActive(true);
            

        } else
        {
            //we're in the last one 

            //nextButton.SetActive(false);
            
        }

        CheckArrows();
    }

    public void Button_previous()
    {

        if (_index > 1)
        {
            _index = _index - 1;

            for (int i = 0; i < textObjectArray.Length; i++)
            {
                textObjectArray[i].SetActive(false);
            }

            textObjectArray[_index - 1].SetActive(true);
            
        }
        else
        {
            //we are in the first one and we do NOTHING!
            //actually we switch off the previous button...
            //previousButton.SetActive(false);
            //nextButton.SetActive(true);
        }

        CheckArrows();
    }


    public void Button_Speelnu()
    {
        LoadingSceneManager.LoadScene("FasesMap");
        //ShowQuestionaire();
    }

       


    public void Button_Questionaire()
    {
        ShowQuestionaire();
        speechContainerObject.SetActive(false);
    }


    public void Button_nextQuestionaire()
    {
        _indexQuestion++;

        for (int i = 0; i < questionsArray.Length; i++)
        {
            questionsArray[i].SetActive(false);
        }

        questionsArray[_indexQuestion - 1].SetActive(true);
    }

    #endregion


    private void CheckArrows()
    {
        if (_index == textObjectArray.Length)
        {
            nextButton.SetActive(false);
            previousButton.SetActive(true);
        }
        else if (_index == 1)
        {
            previousButton.SetActive(false);
            nextButton.SetActive(true);
        }
        else
        {
            nextButton.SetActive(true);
            previousButton.SetActive(true);
        }

    }


    private void ShowQuestionaire()
    {
        questionaireObject.SetActive(true);
    }


}
