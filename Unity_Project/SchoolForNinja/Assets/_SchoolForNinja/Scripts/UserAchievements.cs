﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserAchievements {

    public bool[] achievements = new bool[5];

    public UserAchievements InitAchievements()
    {
        UserAchievements userAchievements = new UserAchievements();

        for (int i = 0; i < achievements.Length; i++)
        {
            achievements[i] = false;
            Debug.Log("Achivement " + i + ": " + achievements[i].ToString());
        }

        return userAchievements;
    }

    //public static UserAchievements Create(bool[] array)
    //{
    //    UserAchievements wrapper = new UserAchievements();
    //    wrapper.achievements = array;
    //    return wrapper;
    //}
}
