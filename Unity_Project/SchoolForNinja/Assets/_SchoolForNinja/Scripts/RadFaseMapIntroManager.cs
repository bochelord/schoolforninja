﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;
using Com.LuisPedroFonseca;
using Com.LuisPedroFonseca.ProCamera2D;

public class RadFaseMapIntroManager : MonoBehaviour {


    public SpriteRenderer sunLogoSprite;
    public SpriteRenderer sunburstSprite;
    public SpriteRenderer sunburstGlowSprite;
    
    private IntroSurveyManager _surveyManager;

	void Start () {

        _surveyManager = FindObjectOfType<IntroSurveyManager>();

        //Full screen value: orthosize=593.14 // Y=671

        if (BackendManager.Instance.radModel.ninja.first_time_connected)
        {
            Camera.main.orthographicSize = 480;
            Camera.main.transform.position = new Vector3(1000f, 789f, 0f);

            sunburstSprite.transform.DORotate(new Vector3(0, 0, 359), 0.75f).SetLoops(-1, LoopType.Incremental);

            Camera.main.DOOrthoSize(593.14f, 1f);
            sunburstGlowSprite.DOFade(0, 1.75f);
            sunburstSprite.DOFade(0, 1.75f);

            sunLogoSprite.DOFade(1f, 1.5f).OnComplete(() =>
            {
                sunLogoSprite.transform.DOPunchScale(Vector3.up, 0.5f).OnComplete(() =>
                {
                    Camera.main.DOOrthoSize(593.14f, 1f).OnComplete(() =>
                    {
                        Camera.main.transform.DOMoveX(369f, 1f).OnComplete(() =>
                        {
                            Camera.main.GetComponent<ProCamera2DPanAndZoom>()._panTarget.position = new Vector3(369f, Camera.main.transform.position.y, 0);
                            Camera.main.GetComponent<ProCamera2D>().FollowHorizontal = true;
                            Camera.main.GetComponent<ProCamera2D>().FollowVertical = true;
                            _surveyManager.GetSurveyQuestions();
                        });
                    });
                });
            });
        }
        else
        {

            sunLogoSprite.DOFade(1, 0.01f);
            sunburstSprite.gameObject.SetActive(false);
            sunburstGlowSprite.gameObject.SetActive(false);

            Camera.main.GetComponent<ProCamera2DPanAndZoom>()._panTarget.position = new Vector3(369f, Camera.main.transform.position.y, 0);
            Camera.main.GetComponent<ProCamera2D>().FollowHorizontal = true;
            Camera.main.GetComponent<ProCamera2D>().FollowVertical = true;
        }

        

	}
	
}
