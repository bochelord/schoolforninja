﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;

public class PathsManagerFase1 : PathsManager {


    [Required]
    [SerializeField]
    private AstarPath _astarPath;

    [BoxGroup("Bridge")] public Collider2D bridgeCollider;

    [BoxGroup("Bridge")] public SpriteRenderer brideRenderer;
    [BoxGroup("Bridge")] public SpriteRenderer leverClosed;
    [BoxGroup("Bridge")] public SpriteRenderer leverOpen;

    [BoxGroup("Wall")] public Collider2D wallCollider;
    [BoxGroup("Wall")] public SpriteRenderer wallRenderer;

    [BoxGroup("Blocking Man")] public SpriteRenderer blockingMan;
    [BoxGroup("Blocking Man")] public Collider2D blockingCollider;
    [BoxGroup("Blocking Man")] public Collider2D pathCollider;
    [BoxGroup("Blocking Man")] public Collider2D bmpathToElevatorCollider;

    [BoxGroup("Birds")] public Transform rightTransformAfterQuest;
    [BoxGroup("Birds")] public Transform leftTransformAfterQuest;
    [BoxGroup("Birds")] public GameObject rightBird;
    [BoxGroup("Birds")] public GameObject leftBird;

    [BoxGroup("Lift")] public Collider2D liftBlockingCollider;
    [BoxGroup("Lift")] public Collider2D liftPathCollider;
    [BoxGroup("Lift")] public Transform playerBotPosition;
    [BoxGroup("Lift")] public Transform playerTopPosition;
    [BoxGroup("Lift")] public Collider2D LiftpathToElevatorCollider;
    [BoxGroup("Lift")] public LiftAnimationTrigger liftController;

    [BoxGroup("Portal")] public GameObject portal;
    [BoxGroup("Portal")] public GameObject roadBlock;
    [BoxGroup("Portal")] public GameObject portalCollider;
    [BoxGroup("Portal")] public GameObject firstCollider;

    [BoxGroup("Elevator Collider")] public Collider2D elevatorCollider;

    [BoxGroup("End Phase")] public RadEndPhase endPhase;

    private bool isWallOn = true;


    private bool isElevatorFromLeftBlocked = true;
    private bool blockingManEnabled = true;


    public override void QuestCompleted(int id)
    {
        switch (id)
        {
            case 1:
                AddBridgeAndRescanPath();
                player.transform.position = quest[0].transform.position;
                EnableQuestSprite(quest[0].gameObject);
                break;
            case 2:
                RemoveWallFromPathAndRescanPath();
                player.transform.position = quest[1].transform.position;
                EnableQuestSprite(quest[1].gameObject);
                break;
            case 3:
                RemoveWallFromPathAndRescanPath();
                player.transform.position = quest[2].transform.position;
                EnableQuestSprite(quest[2].gameObject);
                break;
            case 4:
                RemoveBlockingManAndRescanPath();
                player.transform.position = quest[3].transform.position;
                EnableQuestSprite(quest[3].gameObject);
                blockingManEnabled = false;
                break;
            case 5:
                EnableLiftStops();
                player.transform.position = quest[4].transform.position;
                EnableQuestSprite(quest[4].gameObject);
                player.GetComponentInChildren<SpriteRenderer>().sortingOrder = 10;
                isElevatorFromLeftBlocked = false;
                break;
            case 6:
                EnablePathToElevatorFromLift();
                player.transform.position = quest[5].transform.position;
                EnableQuestSprite(quest[5].gameObject);
                liftController.SetLiftEnabled();
                break;
            case 7:
                EnablePathToElevatorFromPath();
                player.transform.position = quest[6].transform.position;
                EnableQuestSprite(quest[6].gameObject);
                liftController.SetLiftEnabled();
                break;
            case 8:
                FadePortal();
                endPhase.EnableEndPhase = true;
                EnableQuestSprite(quest[7].gameObject);
                break;
            case 9:
                FadePortal();
                endPhase.EnableEndPhase = true;
                EnableQuestSprite(quest[8].gameObject);
                break;
        }
    }

    public void AddBridgeAndRescanPath()
    {
        bridgeCollider.gameObject.SetActive(false);
        leverClosed.gameObject.SetActive(false);
        leverOpen.gameObject.SetActive(true);
        brideRenderer.DOFade(1, 2).OnComplete(()=> 
        {
            _astarPath.Scan();
        });
    }


    public void RemoveWallFromPathAndRescanPath()
    {
        isWallOn = false;
        wallCollider.enabled = false;
        wallRenderer.DOFade(0, 2).OnComplete(() =>
         {
             _astarPath.Scan();
         });
    }

    public void RemoveBlockingManAndRescanPath()
    {
        blockingCollider.enabled = false;
        pathCollider.enabled = true;
        blockingMan.DOFade(0, 2).OnComplete(() =>
        {
            _astarPath.Scan();
        });
    }

    public void EnablePathToElevatorFromLift()
    {
        leftBird.transform.position = leftTransformAfterQuest.position;
        leftBird.transform.rotation = leftTransformAfterQuest.rotation;
        leftBird.GetComponent<DOTweenAnimation>().enabled = false;
        blockingCollider.enabled = false;
        elevatorCollider.enabled = false;
        liftBlockingCollider.enabled = false;
        liftPathCollider.enabled = false;
        LiftpathToElevatorCollider.enabled = true;
        _astarPath.Scan();
    }

    public void EnablePathToElevatorFromPath()
    {
        rightBird.transform.position = rightTransformAfterQuest.position;
        rightBird.transform.rotation = rightTransformAfterQuest.rotation;
        rightBird.GetComponent<DOTweenAnimation>().loops = 0;
        elevatorCollider.enabled = false;
        blockingCollider.enabled = false;
        pathCollider.enabled = false;
        bmpathToElevatorCollider.enabled = true;
        player.GetComponentInChildren<SpriteRenderer>().sortingOrder = 10;
        _astarPath.Scan();
    }

    public void LiftMovingUp()
    {
        movementController.canMove = false;
         player.GetComponentInChildren<SpriteRenderer>().DOFade(0, 1).OnComplete(() =>
         {
             player.GetComponentInChildren<SpriteRenderer>().sortingOrder = 10;
             player.transform.position = playerTopPosition.position;
             liftPathCollider.enabled = true;
             liftBlockingCollider.enabled = false;
             blockingCollider.enabled = false;
             if (!isElevatorFromLeftBlocked)
             {
                 LiftpathToElevatorCollider.enabled = true;
             }
             player.GetComponentInChildren<SpriteRenderer>().DOFade(1, 1);
             _astarPath.Scan();
             movementController.canMove = true;
         });
    }

    public void LiftMovingDown()
    {
        movementController.canMove = false;
        player.GetComponentInChildren<SpriteRenderer>().DOFade(0, 1).OnComplete(() =>
        {
            player.GetComponentInChildren<SpriteRenderer>().sortingOrder = 2;
            player.transform.position = playerBotPosition.position;
            if (blockingManEnabled)
            {
                blockingCollider.enabled = true;
            }
            liftPathCollider.enabled = false;
            liftBlockingCollider.enabled = true;
            LiftpathToElevatorCollider.enabled = false;
            player.GetComponentInChildren<SpriteRenderer>().DOFade(1, 1);
            _astarPath.Scan();
            movementController.canMove = true;
        });
    }

    public void EnableLiftStops()
    {
        playerTopPosition.gameObject.SetActive(true);
        playerBotPosition.gameObject.SetActive(true);
    }

    public bool GetIsWallOn()
    {
        return isWallOn;
    }

    public void FadePortal()
    {
        portal.SetActive(true);
        roadBlock.SetActive(false);
        portal.GetComponent<SpriteRenderer>().DOFade(1, 2);
        portalCollider.SetActive(true);
        firstCollider.SetActive(false);
        _astarPath.Scan();

    }

    //public void GetQuestStatusToLoadMap()
    //{
    //    BackendManager.Instance.SetLastAction(GetQuestStatusToLoadMap);
    //    BackendManager.Instance.GetAllQuestStatus(delegate { StartCoroutine(CheckQuestStatusdAndLoadMap()); },OnFail);
    //}

    //public void OnFail()
    //{
    //    //_guiManager.ShowNoInternetPanel();
    //}

    //IEnumerator CheckQuestStatusdAndLoadMap()
    //{
    //    Dictionary<int, int> _phaseQuest = new Dictionary<int, int>();
    //    List<int> _slotToUnlock = new List<int>();
    //    for (int i = 0; i < quest.Length; i++)
    //    {
    //        _phaseQuest.Add(quest[i].questId, quest[i].questSlot);
    //    }


    //    for (int i = 0; i < _questManager.GetQuestStatusList().Count; i++)
    //    {

    //        if (_questManager.GetQuestStatusList()[i].completed)
    //        {
                
    //            var str = _questManager.GetQuestStatusList()[i].belongs_to_quest.Split('/');
    //            var pk = int.Parse(str[str.Length - 2]);

    //            _slotToUnlock.Add(_phaseQuest[pk]);
    //        }

    //    }

    //    _slotToUnlock.Sort((s1, s2) => s1.CompareTo(s2));

    //    for (int i = 0; i < _slotToUnlock.Count; i++)
    //    {
    //        QuestCompleted(_slotToUnlock[i]);
    //    }
    //    yield return new WaitForSeconds(1);
    //    _guiManager.loadingPage.SetActive(false);

    //}
}
