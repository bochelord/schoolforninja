﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sfn_languageswitcher : MonoBehaviour {


    public bool localizationDutch = false;

	// Use this for initialization
	void Start () {
		
        if (localizationDutch)
        {
            I2.Loc.LocalizationManager.CurrentLanguage = "Dutch";
        }
        else
        {
            I2.Loc.LocalizationManager.CurrentLanguage = "English";
        }


	}

}
