﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class Rad_MoveObject : MonoBehaviour {

    public bool moveOtherObject = false;
    [ShowIf("moveOtherObject")] public GameObject objectToMove;

    public bool childAnimation = false;
    [ShowIf("childAnimation")] public string triggerName;
    public bool movePlayer = false;
    public bool canMoveBack = false;
    [Space(10)]
    public Transform treeAnimationPosition;
    public float timeToGoal;

    private GameObject playerGameobject;
    private PlayerMovementController _playerMovementController;

    private bool moveBackActive = false;
    private bool _inOriginPosition = true;
    private bool _animationActive = false;
    private Vector3 _originPosition;
    private void Start()
    {
        playerGameobject = GameObject.FindGameObjectWithTag("Player").transform.parent.gameObject;
        _originPosition = this.transform.position;
        _playerMovementController = FindObjectOfType<PlayerMovementController>();
    }
    /// <summary>
    /// called by animation events
    /// </summary>
    public void MoveTree()
    {
        if (!_animationActive)
        {
            _animationActive = true;

            GameObject _objectToMove = null;
            Vector3 _destination;


            if (moveOtherObject) _objectToMove = objectToMove;
            else _objectToMove = this.gameObject;

            if (_inOriginPosition) _destination = treeAnimationPosition.position;
            else _destination = _originPosition;

            if (playerGameobject && movePlayer)
            {
                _playerMovementController.canMove = false;
                playerGameobject.GetComponentInParent<Pathfinding.Seeker>().enabled = false;
                playerGameobject.GetComponentInParent<Pathfinding.AIPath>().enabled = false;
                playerGameobject.GetComponentInParent<Pathfinding.AIDestinationSetter>().enabled = false;
                playerGameobject.transform.position = _objectToMove.transform.position;


                playerGameobject.transform.DOMove(_destination, timeToGoal).OnComplete(() =>
                {
                    playerGameobject.GetComponentInParent<Pathfinding.Seeker>().enabled = true;
                    playerGameobject.GetComponentInParent<Pathfinding.AIPath>().enabled = true;
                    playerGameobject.GetComponentInParent<Pathfinding.AIDestinationSetter>().enabled = true;
                    _playerMovementController.dummyTarget.position = this.transform.position;
                    _playerMovementController.canMove = true;
                });
            }



            _objectToMove.transform.DOMove(_destination, timeToGoal).OnComplete(()=>
            {
                moveBackActive = !moveBackActive;
                _inOriginPosition = !_inOriginPosition;
                _animationActive = false;
            });    
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && canMoveBack && moveBackActive)
        {
            MoveTree();
        }
    }

    public void ActivateChildAnimation()
    {
        if(childAnimation)GetComponentInChildren<Animator>().SetTrigger(triggerName);
    }
}
