﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingSceneManager : MonoBehaviour
{
    [Header("Binding")]
    public static string LoadingScreenSceneName = "Loading";

    [Header("GameObjects")]
    public Text LoadingText;
    public List<GameObject> LoadingIconList = new List<GameObject>();
    //public Image LoadingImageEN;
    //public Image LoadingImageAR;
    //public CanvasGroup LoadingProgressBar;
    //public CanvasGroup LoadingAnimation;
    //public CanvasGroup LoadingCompleteAnimation;


    [Header("Time")]
    public float StartFadeDuration = 0.2f;
    public float ProgressBarSpeed = 2f;
    public float ExitFadeDuration = 0.2f;
    public float LoadCompleteDelay = 0.5f;


    protected AsyncOperation _asyncOperation;
    protected static string _sceneToLoad = "";
    protected float _fadeDuration = 0.5f;
    protected float _fillTarget = 0f;

    private string _loadingText;

    /// <summary>
    /// Call this static method to load a scene from anywhere
    /// </summary>
    /// <param name="sceneToLoad">Level name.</param>
    public static void LoadScene(string sceneToLoad)
    {
        _sceneToLoad = sceneToLoad;
        Application.backgroundLoadingPriority = ThreadPriority.High;
        if (LoadingScreenSceneName != null)
        {
            SceneManager.LoadScene(LoadingScreenSceneName);
        }
    }

    /// <summary>
    /// On Start(), we start loading the new level asynchronously
    /// </summary>
    protected virtual void Start()
    {

        //setup the loading icon randomly between the existing ones
        //int i =Random.Range(0, LoadingIconList.Count);
        //if(LoadingIconList[i].gameObject)
        //    LoadingIconList[i].gameObject.SetActive(true);
        
        if (_sceneToLoad != "")
        {
            StartCoroutine(LoadAsynchronously());
        }
    }

    
    /// <summary>
    /// Loads the scene to load asynchronously.
    /// </summary>
    protected virtual IEnumerator LoadAsynchronously()
    {
        // we setup our various visual elements
        //LoadingSetup();

        // we start loading the scene
        _asyncOperation = SceneManager.LoadSceneAsync(_sceneToLoad, LoadSceneMode.Single);
        _asyncOperation.allowSceneActivation = false;

        // while the scene loads, we assign its progress to a target that we'll use to fill the progress bar smoothly
        while (_asyncOperation.progress < 0.9f)
        {
            _fillTarget = _asyncOperation.progress;
            yield return null;
        }
        // when the load is close to the end (it'll never reach it), we set it to 100%
        _fillTarget = 1f;

        //Debug.Log("filltarget esta lleno");

        // we wait for the bar to be visually filled to continue
        //while (LoadingProgressBar.GetComponent<Image>().fillAmount != _fillTarget)
        //{
        //    yield return null;
        //}

        // the load is now complete, we replace the bar with the complete animation
        //LoadingComplete();
        yield return new WaitForSeconds(LoadCompleteDelay);

        // we fade to black
        //EduoLoadingGUIManager.Instance.FaderOn(true, ExitFadeDuration);
        //yield return new WaitForSeconds(ExitFadeDuration);

        // we switch to the new scene
        _asyncOperation.allowSceneActivation = true;
    }
}
