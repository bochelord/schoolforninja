﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rad_UIManagerTests : MonoBehaviour {

    public GameObject panelWritePost;
    public GameObject panelGetPostByID;
    
    //Method called from the UI button "Open Post Panel" - Scene -> ExpansionDemo.Unity
    public void OpenPostPanel()
    {
        if (!panelWritePost.activeInHierarchy)
        {
            panelWritePost.SetActive(true);
        }
    }

    //Method called from the UI button "Close Post Panel" - Scene -> ExpansionDemo.Unity
    public void CloserPostPanel()
    {
        if (panelWritePost.activeInHierarchy)
        {
            panelWritePost.SetActive(false);
        }
    }

    public void OpenGetPostByIDPanel()
    {
        if (!panelGetPostByID.activeInHierarchy)
        {
            panelGetPostByID.SetActive(true);
        }
    }

    public void CloseGetPostByIdPanel()
    {
        if (panelGetPostByID.activeInHierarchy)
        {
            panelGetPostByID.SetActive(false);
        }
    }
}
