﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class IntroCamera : MonoBehaviour {

    Camera camarita;

    void Start()
    {
        camarita = this.GetComponent<Camera>();
    }

    public void Zoom(float orthosize, float delay)
    {
        camarita.DOOrthoSize(orthosize, delay);
    }

	public void ZoomMe()
    {
        camarita.DOOrthoSize(475f, 2f).SetEase(Ease.InOutBack);
    }


    public void ZoomMeOut()
    {
        camarita.DOOrthoSize(650f, 1f);
    }
}
