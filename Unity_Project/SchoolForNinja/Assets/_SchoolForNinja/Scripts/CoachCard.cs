﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoachCard : MonoBehaviour {

    public Image coachPhoto;
    public Text coachName;
    public Text coachJob;
    public Text coachDescription;
}
