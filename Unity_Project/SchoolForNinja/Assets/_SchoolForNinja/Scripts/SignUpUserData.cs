﻿using UnityEngine;
using UnityEngine.UI;

public class SignUpUserData : MonoBehaviour {

    public string username;
    public string email;
    public string password;
    public string repassword;
    public bool userAgree;

    public void CreateNewSignupUserData()
    {
        username = "";
        email = "";
        password = "";
        repassword = "";
        userAgree = false;
    }
}
