﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FishController : MonoBehaviour {

	
	void Start () {


        Startfish();


	}
	
    private void Startfish()
    {
        this.transform.DOLocalMoveX(300f, UnityEngine.Random.Range(40f, 60f)).SetSpeedBased().SetEase(Ease.Linear).OnComplete(() => ResetFish());

        
    }

    private void ResetFish()
    {

        this.transform.localPosition = new Vector2(-344f, this.transform.localPosition.y);
        Startfish();

    }
	
}
