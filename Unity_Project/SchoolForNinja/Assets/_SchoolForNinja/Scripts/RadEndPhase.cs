﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadEndPhase : MonoBehaviour {


    [HideInInspector]
    public bool EnableEndPhase = false;

    private SFN_GuiManager _guiManager;

    private void Start()
    {
        _guiManager = FindObjectOfType<SFN_GuiManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && EnableEndPhase)
        {
            _guiManager.ShowFinishedPhaseScreen();
        }
    }
}
