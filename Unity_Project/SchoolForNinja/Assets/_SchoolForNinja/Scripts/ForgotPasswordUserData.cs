﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ForgotPasswordUserData : MonoBehaviour {

    public string from_email;
    public string email;

    public void CreateNewForgotPasswordUserData()
    {
        from_email = "";
        email = "";
    }
}
