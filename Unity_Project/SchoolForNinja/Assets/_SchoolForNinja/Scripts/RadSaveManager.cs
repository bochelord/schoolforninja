﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadSaveManager : MonoBehaviour {

    public static bool debugmode = false;

    public static string playerProfileFilename = "SchoolforNinja.rad";
    public static PlayerProfile profile = new PlayerProfile().GeneratePlayerProfile(0);

    public static string loginUserDataFileName = "sfn_model.rad";
    public static LoginUserData loginUserData = new LoginUserData();

    public static string spriteDataFileName = "sfn_sprites.rad";
    public static SpriteData spriteListData = new SpriteData();

    public static string achievementsFileName = "sfn_achievements.rad";
    public static UserAchievements userAchievements = new UserAchievements();

    public static string storiesDraftsFileName = "sfn_stories.rad";
    public static StoriesDrafts storiesList = new StoriesDrafts();

    public static void SaveData()
    {
        ES2.Save(profile, playerProfileFilename + "?tag=profile-" + profile.profileID);
        ES2.Save(loginUserData, loginUserDataFileName + "?tag=loginUserData");
        ES2.Save(spriteListData, spriteDataFileName + "?tag=spriteListData");
        ES2.Save(userAchievements, achievementsFileName + "?tag=achievements");
        ES2.Save(storiesList, storiesDraftsFileName + "?tag=stories");
        Debug.Log("== SAVING PROFILE ==");
    }

    public static void LoadData()
    {
        if (ES2.Exists(playerProfileFilename + "?tag=profile-" + profile.profileID))
        {
            profile = ES2.Load<PlayerProfile>(playerProfileFilename + "?tag=profile-" + profile.profileID);
        }
        else
        {
            profile = new PlayerProfile().GeneratePlayerProfile(0);
        }

        if (ES2.Exists(achievementsFileName + "?tag=achievements"))
        {
            userAchievements = ES2.Load<UserAchievements>(achievementsFileName + "?tag=achievements");
        }
        else
        {
            userAchievements = new UserAchievements().InitAchievements();
        }

        if (ES2.Exists(storiesDraftsFileName + "?tag=stories"))
        {
            storiesList = ES2.Load<StoriesDrafts>(storiesDraftsFileName + "?tag=stories");
        }
        else
        {
            storiesList = new StoriesDrafts();
        }
    }


    public static void LoadLoginUserData()
    {
        if (ES2.Exists(loginUserDataFileName + "?tag=loginUserData"))
        {
            loginUserData = ES2.Load<LoginUserData>(loginUserDataFileName + "?tag=loginUserData");
        }
        else
        {
            loginUserData = new LoginUserData();
            loginUserData.CreateNewLoginUserData("","");
            Debug.Log("Login User Data has not been found, new empty login user data created. username: '', password: ''");
        }
    }

    public static void CreateLoginUserData(string username, string password)
    {
        if (!ES2.Exists(loginUserDataFileName + "?tag=loginUserData"))
        {
            loginUserData = new LoginUserData();
            loginUserData.CreateNewLoginUserData(username, password);
            Debug.Log("Login User Data has not been found, new empty login user data created. username: '', password: ''");
        }
    }

    public static void LoadSpriteListData()
    {
        if(ES2.Exists(spriteDataFileName + "?tag=spriteListData"))
        {
            spriteListData = ES2.Load<SpriteData>(spriteDataFileName + "?tag=spriteListData");
        }
        else
        {
            spriteListData = new SpriteData();
            SaveSpriteListData();
        }
    }

    public static void SaveSpriteListData()
    {
        ES2.Save(spriteListData, spriteDataFileName + "?tag=spriteListData");
    }

    public static void DeleteLoginUserData()
    {
        if (ES2.Exists(loginUserDataFileName + "?tag=loginUserData"))
        {
            ES2.Delete(loginUserDataFileName);
        }
    }

    public static void OnApplicationQuit()
    {
        SaveData();
    }

}
