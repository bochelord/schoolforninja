﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadNamePick : MonoBehaviour {

    private const string UserNamePlayerPref = "NamePickUserName";

    public RadChatPhoton radChatPhoton;

    public InputField idInput;

    public void Start()
    {
        this.radChatPhoton = FindObjectOfType<RadChatPhoton>();


        string prefsName = PlayerPrefs.GetString(RadNamePick.UserNamePlayerPref);
        if (!string.IsNullOrEmpty(prefsName))
        {
            this.idInput.text = prefsName;
        }
    }


    // new UI will fire "EndEdit" event also when loosing focus. So check "enter" key and only then StartChat.
    public void EndEditOnEnter()
    {
        if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        {
            this.StartChat();
        }
    }

    public void StartChat()
    {
        RadChatPhoton chatNewComponent = FindObjectOfType<RadChatPhoton>();
        chatNewComponent.UserName = this.idInput.text.Trim();
        chatNewComponent.Connect();
        enabled = false;

        PlayerPrefs.SetString(RadNamePick.UserNamePlayerPref, chatNewComponent.UserName);
    }





}
