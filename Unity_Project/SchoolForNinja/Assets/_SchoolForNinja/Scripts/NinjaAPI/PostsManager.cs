﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
using UnityEngine.SceneManagement;

public class PostsManager : MonoBehaviour {


    public Sprite defaultImageToSend;
    public Sprite defaultStoryImageToSend;
    private List<Rad_Model.Post_Log> personalLogsList = new List<Rad_Model.Post_Log>();
    private List<Rad_Model.Post_Log> logsList = new List<Rad_Model.Post_Log>();
    private List<Rad_Model.Post_Log> questStoriesList = new List<Rad_Model.Post_Log>();
    private Dictionary<int, List<Rad_Model.Post_Log>> allQuestsLogsByQuestID = new Dictionary<int, List<Rad_Model.Post_Log>>();
    private Dictionary<int, List<Rad_Model.Post_Log>> allQuestsLogsByNinjaID = new Dictionary<int, List<Rad_Model.Post_Log>>();
    private Rad_Model.Post_Log currentLog = new Rad_Model.Post_Log();
    private Rad_Model.Post_Log _postToSend;
    private bool _firstQuest;
    static PostsManager instance;
    public static PostsManager Instance { get { return instance; } }

    void Start()
    {
        if (!Instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }

    #region Parse
    /// <summary>
    /// we need manual Parse because the images
    /// </summary>
    /// <param name="data">json received</param>
    /// <param name="login">if it's called in login or not</param>
    public void ParseAllPOSTJSON(JsonData[] data, bool login)
    {
        bool addtoList = true;
        
        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Post_Log post = new Rad_Model.Post_Log();
            int _questId = 0;
            int _ninjaId = 0;
            if (data[i].Keys.Contains("url"))
            {
                post.url = data[i]["url"].ToString();
                var str = post.url.Split('/');
                post.id = int.Parse(str[str.Length - 2]);
            }
            if (data[i].Keys.Contains("postTitle"))
            {
                post.postTitle = data[i]["postTitle"].ToString();
            }
            if (data[i].Keys.Contains("postContent1"))
            {
                post.postContent1 = data[i]["postContent1"].ToString();
            }
            if (data[i].Keys.Contains("belongs_to_ninja"))
            {
                post.belongs_to_ninja = (data[i]["belongs_to_ninja"].ToString());
                var str = post.belongs_to_ninja.Split('/');
                _ninjaId = int.Parse(str[str.Length - 2]);
            }
            if (data[i].Keys.Contains("belongs_to_quest"))
            {
                post.belongs_to_quest = (data[i]["belongs_to_quest"].ToString());
                var str = post.belongs_to_quest.Split('/');
                _questId = int.Parse(str[str.Length - 2]);
            }
            if (data[i].Keys.Contains("timestamp"))
            {
                post.timestamp = data[i]["timestamp"].ToString();
            }
            if (data[i].Keys.Contains("privacy"))
            {
                post.privacy = data[i]["privacy"].ToString();
            }
            if (data[i].Keys.Contains("has_image1"))
            {
                post.has_image1 = bool.Parse(data[i]["has_image1"].ToString());
            }
            if (data[i].Keys.Contains("story_cover_image"))
            {
                post.story_cover_image = BackendManager.Instance.SpriteToBase64(defaultStoryImageToSend);
            }

            if (post.has_image1 && data[i].Keys.Contains("image1"))
            {
                string url = data[i]["image1"].ToString();
                post.image1 = url;
                if (RadSaveManager.spriteListData.CheckForSprite(url))
                {
                    BackendManager.Instance.storiesSprites++;
                    post.image1 = RadSaveManager.spriteListData.spriteData[url];
                }
                else
                {
                    StartCoroutine(BackendManager.Instance.GetPostsSprite(url));
                }
            }
            else if(data[i].Keys.Contains("image1"))
            {
                post.image1 = "no image";
                BackendManager.Instance.storiesSprites++;
            }

            //if the post it's from the user
            if(post.belongs_to_ninja == BackendManager.Instance.radModel.ninja.url)
            {
                //check if we already have it
                for (int j = 0; j < personalLogsList.Count; j++)
                {
                    if(personalLogsList[j].belongs_to_quest == post.belongs_to_quest)
                    {
                        addtoList = false;
                    }
                }
                //add it if not
                if(addtoList)
                    personalLogsList.Add(post);
            }
            //reset value
            addtoList = true;

            questStoriesList.Add(post);
            //check if we have a list with questId key
            if (allQuestsLogsByQuestID.ContainsKey(_questId) )
            {
                //and check if we already have it in the list
                for (int j = 0; j < allQuestsLogsByQuestID[_questId].Count; j++)
                {
                    if(allQuestsLogsByQuestID[_questId][j].belongs_to_ninja == post.belongs_to_ninja)
                    {
                        addtoList = false;
                        break;
                    }
                }
                //add it if not
                if (addtoList)
                {
                    allQuestsLogsByQuestID[_questId].Add(post);
                }
                
            }
            //in case that the quest Id still don't have his own list
            else
            {
                List<Rad_Model.Post_Log> _newList = new List<Rad_Model.Post_Log>();
                _newList.Add(post);
                allQuestsLogsByQuestID.Add(_questId, _newList);
            }
            //reset value
            addtoList = true;

            //same for ninjas Id lists
            if (allQuestsLogsByNinjaID.ContainsKey(_ninjaId))
            {
                for (int j = 0; j < allQuestsLogsByNinjaID[_ninjaId].Count; j++)
                {
                    if (allQuestsLogsByNinjaID[_ninjaId][j].belongs_to_quest == post.belongs_to_quest)
                    {
                        addtoList = false;
                        break;
                    }
                }
                if (addtoList)
                {
                    allQuestsLogsByNinjaID[_ninjaId].Add(post);
                }
            }
            else
            {
                List<Rad_Model.Post_Log> _newList = new List<Rad_Model.Post_Log>();
                _newList.Add(post);
                allQuestsLogsByNinjaID.Add(_ninjaId, _newList);
            }

            addtoList = true;
        }

        questStoriesList.Sort((a, b) => b.id.CompareTo(a.id));
        //if we are ready to go
        if (BackendManager.Instance.storiesSprites == PostsManager.Instance.GetQuestStoriesList().Count)
        {
            BackendManager.Instance.ActivateEvent(login);
        }
        else
        {
            StartCoroutine(GetStoriesSpritesSuccess(login));
        }
    }

    /// <summary>
    /// If we arrive to this, we need to check every X time for the sprites, so we can continue
    /// </summary>
    /// <param name="login">if it's in login event or not</param>
    /// <returns></returns>
    private IEnumerator GetStoriesSpritesSuccess(bool login)
    {
        yield return new WaitForSeconds(0.5f);
        if (BackendManager.Instance.storiesSprites == PostsManager.Instance.GetQuestStoriesList().Count)
        {
            BackendManager.Instance.ActivateEvent(login);
            StopAllCoroutines();
        }
        else
        {
            StartCoroutine(GetStoriesSpritesSuccess(login));
        }
        
    }

    public void ParsePostAndAddToList(JsonData data)
    {
        var _questId = 0;

        if (data.Keys.Contains("url"))
        {
            currentLog.url = data["url"].ToString();
            var str = currentLog.url.Split('/');
            currentLog.id = int.Parse(str[str.Length - 2]);
        }
        if (data.Keys.Contains("timestamp"))
        {
            currentLog.timestamp = data["timestamp"].ToString();
        }

        if (allQuestsLogsByQuestID.ContainsKey(_questId))
        {
            allQuestsLogsByQuestID[_questId].Add(currentLog);
        }
        else
        {
            List<Rad_Model.Post_Log> _newList = new List<Rad_Model.Post_Log>();
            _newList.Add(currentLog);
            allQuestsLogsByQuestID.Add(_questId, _newList);
        }

        personalLogsList.Add(currentLog);

        var RadProfileUiManager = FindObjectOfType<RadProfileUIManager>();
        if (RadProfileUiManager)
        {
            RadProfileUiManager.PersonalLogsShow();
        }
    }

    #endregion

    public void ClearAllPostsLists(bool _personalAlso)
    {
        if (_personalAlso)
        {
            personalLogsList.Clear();
        }
            
        allQuestsLogsByQuestID.Clear();
        allQuestsLogsByNinjaID.Clear();
    }

    public void CreatePostLog(string _postTitle, string _postContent1, string _postContent2, string _postContent3,
                               string _belongsToNinjaID, int _belongsToQuestID,string _privacy, Sprite image1, 
                               Sprite image2, Sprite image3, bool firstQuest)
    {

        currentLog = new Rad_Model.Post_Log();

        currentLog.postTitle = _postTitle;

        currentLog.postContent1 = _postContent1;
        currentLog.story_cover_image = BackendManager.Instance.SpriteToBase64(defaultStoryImageToSend);

        if (image1 != null)
        {
            currentLog.image1 = BackendManager.Instance.SpriteToBase64(image1);
            currentLog.has_image1 = true;
        }
        else
        {
            //var _sprite = BackendManager.Instance.SpriteToBase64(defaultImageToSend);
            //currentLog.image1 = _sprite;
            currentLog.has_image1 = false;
        }

        currentLog.belongs_to_ninja = _belongsToNinjaID;

        if (BackendManager.Instance.UseProduction)
        {
            currentLog.belongs_to_quest = BackendManager.Instance.ProductionUrl + BackendManager.Instance.ninjapiURI + BackendManager.Instance.questURI + _belongsToQuestID + "/";
        }
        else
        {
            currentLog.belongs_to_quest = BackendManager.Instance.DevelopmentUrl + BackendManager.Instance.ninjapiURI + BackendManager.Instance.questURI + _belongsToQuestID + "/";
        }



        if(_privacy.Contains("Alleen ik"))
        {
            currentLog.privacy = "only_me";
        }
        else if (_privacy.Contains("Mijn sensei"))
        {
            currentLog.privacy = "only_coach";
        }
        else if (_privacy.Contains("Andere Ninja en Sensei"))
        {
            currentLog.privacy = "everyone";
        }
        else if (_privacy.Contains("Iedereen, ook mijn werkgever"))
        {
            currentLog.privacy = "everyone_outside";
        }


        _postToSend = currentLog;

        string jsonToSend = JsonUtility.ToJson(currentLog);

        _firstQuest = firstQuest;

        BackendManager.Instance.SetLastAction(CreatePostLog);

        if (firstQuest)
        {
            ModifyNinjaFirstQuest();
        }
        else
        {
            BackendManager.Instance.CreateStory(jsonToSend, delegate { QuestManagerV2.Instance.EndCurrentQuest(firstQuest); }, OnFailedSendinPost);
        }
        

    }

    public void CreatePostLog()
    {
        string jsonToSend = JsonUtility.ToJson(_postToSend);

        if (_firstQuest)
        {
            ModifyNinjaFirstQuest();
        }
        else
        {
            BackendManager.Instance.CreateStory(jsonToSend, delegate { QuestManagerV2.Instance.EndCurrentQuest(_firstQuest); }, OnFailedSendinPost);
        }
    }

    public void ModifyNinjaFirstQuest()
    {
        BackendManager.Instance.SetLastAction(ModifyNinjaFirstQuest);

        BackendManager.Instance.CheckNinjaName(QuestManagerV2.Instance.GetDraftList()[QuestManagerV2.Instance.GetDraftList().Count - 1].description, OnGetNinjaNameSuccess,
            OnNinjaNameUsed, OnGetNinjaNameFail);

    }
    
    private void OnGetNinjaNameFail()
    {
        if (FindObjectOfType<SFN_GuiManager>())
        {
            FindObjectOfType<SFN_GuiManager>().ShowNoInternetPanel();
        }
    }

    private void OnNinjaNameUsed()
    {
        if (FindObjectOfType<SFN_GuiManager>())
        {
            FindObjectOfType<SFN_GuiManager>().ShowNameUsedWarning();
        }
    }

    private void OnGetNinjaNameSuccess()
    {
        var ninja = BackendManager.Instance.radModel.ninja;
        ninja.ninja_name = QuestManagerV2.Instance.GetDraftList()[QuestManagerV2.Instance.GetDraftList().Count - 1].description;
        var ninjajson = JsonUtility.ToJson(ninja);
        BackendManager.Instance.ModifyNinja(ninjajson, delegate { QuestManagerV2.Instance.EndCurrentQuest(true); }, OnFailedSendinPost);
    }

    private void OnFailedSendinPost()
    {
        if (FindObjectOfType<SFN_GuiManager>())
        {
            FindObjectOfType<SFN_GuiManager>().ShowNoInternetPanel();
        }

    }

    public void ModifyPostLog(string _postTitle, string _postContent1, string _postContent2, string _postContent3,
                                string _belongsToNinjaID, int _belongsToQuestID,string _privacy, 
                                    Sprite image1, Sprite image2, Sprite image3, bool updateQuestStatus)
    {

        Rad_Model.Post_Log _post = new Rad_Model.Post_Log();

        _post.postTitle = _postTitle;

        _post.postContent1 = _postContent1;
        _post.story_cover_image = BackendManager.Instance.SpriteToBase64(defaultStoryImageToSend); 
        if (image1 != null)
        {
            _post.image1 = BackendManager.Instance.SpriteToBase64(image1);
            _post.has_image1 = true;
        }
        else
        {
           // _post.image1 = BackendManager.Instance.SpriteToBase64(defaultImageToSend);
            _post.has_image1 = false;
        }

        _post.belongs_to_ninja = _belongsToNinjaID;

        _post.belongs_to_quest = GetCurrentLog().belongs_to_quest;


        if (_privacy.Contains("Alleen ik"))
        {
            _post.privacy = "only_me";
        }
        else if (_privacy.Contains("Mijn sensei"))
        {
            _post.privacy = "only_coach";
        }
        else if (_privacy.Contains("Andere Ninja en Sensei"))
        {
            _post.privacy = "everyone";
        }
        else if (_privacy.Contains("Iedereen, ook mijn werkgever"))
        {
            _post.privacy = "everyone_outside";
        }

        if (GetCurrentLog().privacy != _post.privacy)
        {
            SendPrivacySettingsChangedAction(_post.privacy);
        }
        _post.id = GetCurrentLog().id;
        _post.timestamp = GetCurrentLog().timestamp;
        _postToSend = _post;
        string jsonToSend = JsonUtility.ToJson(_post);
        BackendManager.Instance.SetLastAction(ModifyPostLog);
        if (updateQuestStatus)
        {
            BackendManager.Instance.ModifyStory(_post.id, jsonToSend, delegate { QuestManagerV2.Instance.EndCurrentQuest(false); }, OnFailedSendinPost);

        }
        else
        {
            BackendManager.Instance.ModifyStory(_post.id, jsonToSend);
        }

        if (FindObjectOfType<RadProfileUIManager>())
        {
            FindObjectOfType<RadProfileUIManager>().UpdateLogCard(_post);
        }

        for (int i = 0; i < personalLogsList.Count; i++)
        {
            if (_post.id == personalLogsList[i].id)
            {
                _post.timestamp = personalLogsList[i].timestamp;
                personalLogsList[i] = _post;
                break;
            }
        }
    }

    public void ModifyPostLog(string _postTitle, string _postContent1, string _privacy)
    {

        Rad_Model.Post_Log _post = new Rad_Model.Post_Log();

        _post = currentLog;
        _post.postTitle = _postTitle;
        _post.postContent1 = _postContent1;
        _post.story_cover_image = BackendManager.Instance.SpriteToBase64(defaultStoryImageToSend);
        if (_privacy.Contains("Alleen ik"))
        {
            _post.privacy = "only_me";
        }
        else if (_privacy.Contains("Mijn sensei"))
        {
            _post.privacy = "only_coach";
        }
        else if (_privacy.Contains("Andere Ninja en Sensei"))
        {
            _post.privacy = "everyone";
        }
        else if (_privacy.Contains("Iedereen, ook mijn werkgever"))
        {
            _post.privacy = "everyone_outside";
        }

        if (GetCurrentLog().privacy != _post.privacy)
        {
            SendPrivacySettingsChangedAction(_post.privacy);
        }

        if (_post.image1 == null)
        {
           // _post.image1 = BackendManager.Instance.SpriteToBase64(defaultImageToSend);
            _post.has_image1 = false;
        }

        _postToSend = _post;
        var _id = GetCurrentLog().id;
        string jsonToSend = JsonUtility.ToJson(_post);
        BackendManager.Instance.SetLastAction(ModifyPostLog);
        BackendManager.Instance.ModifyStory(_id, jsonToSend);
    }

    private void SendPrivacySettingsChangedAction(string _privacy)
    {
        Rad_Model.Action _action = new Rad_Model.Action();
        _action.action_type = "logs";
        _action.actor_object_id = BackendManager.Instance.radModel.ninja.pk;
        _action.description = "User has changed privacy settings, from " + GetCurrentLog().privacy + " to " + _privacy + " for story id: " + GetCurrentLog().id;
        _action.verb = "Modify privacy settings";

        string json = JsonUtility.ToJson(_action);
        BackendManager.Instance.CreateAction(json);
    }

    public void ModifyPostLog()
    {
        var _id = GetCurrentLog().id;
        string jsonToSend = JsonUtility.ToJson(_postToSend);
        BackendManager.Instance.ModifyStory(_id, jsonToSend, delegate { QuestManagerV2.Instance.EndCurrentQuest(false); }, OnFailedSendinPost);
    }

    private void OnModifyPostSucceess()
    {
        if (FindObjectOfType<SFN_GuiManager>())
        {
            var _guiManager = FindObjectOfType<SFN_GuiManager>();
            _guiManager.ResetPanelQuest();
            _guiManager.loadingPage.SetActive(false);
        }
    }

    public bool CurrentQuestHasNinjaLog(string _questUrl)
    {
       // Debug.Log("CURRENT QUEST URL: " + _questUrl);
        for (int i = 0; i < personalLogsList.Count; i++)
        {
          //  Debug.Log("BELONGS TO QUEST URL: " + personalLogsList[i].belongs_to_quest);
            if (_questUrl == personalLogsList[i].belongs_to_quest && personalLogsList[i].belongs_to_ninja == BackendManager.Instance.radModel.ninja.url)
            {
                currentLog = personalLogsList[i];
                return true;
            }
        }

        return false;
    }

    public void SetLogListByQuestId(int questId)
    {
        if (allQuestsLogsByQuestID.ContainsKey(questId))
        {
            logsList = allQuestsLogsByQuestID[questId];
        }
    }

    public void SetLogListByNinjaId(int ninjaId)
    {
        if (allQuestsLogsByNinjaID.ContainsKey(ninjaId))
        {
            logsList = allQuestsLogsByNinjaID[ninjaId];
        }
    }

    public Rad_Model.Post_Log GetCurrentLog()
    {
        return currentLog;
    }

    public void SetCurrentPost(Rad_Model.Post_Log _log)
    {
        currentLog = _log;
    }
    public List<Rad_Model.Post_Log> GetCurrentLogsList()
    {
        return logsList;
    }

    public List<Rad_Model.Post_Log> GetPersonalLogsList()
    {
        return personalLogsList;
    }

    public Dictionary<int, List<Rad_Model.Post_Log>> GetAllQuestsLogsByQuestID()
    {
        return allQuestsLogsByQuestID;
    }

    public Dictionary<int, List<Rad_Model.Post_Log>> GetAllQuestsLogsByNinjaID()
    {
        return allQuestsLogsByNinjaID;
    }

    public List<Rad_Model.Post_Log> GetQuestStoriesList()
    {
        return questStoriesList;
    }
}
