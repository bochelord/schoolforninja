﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using NaughtyAttributes;
using System.Linq;
using Proyecto26;

//in Order to make a try again button for server requests, I need to save who was the last manager
//making the request, right now those 3 are the only who are making server requests
public partial class BackendManager
{
    /// <summary>
    /// Events for callback on server responses
    /// </summary>
    public event Action<string> LoginFailure;
    public event Action<string> LoginSuccess;
    public event Action<string> RegistrationSuccess;
    public event Action<string> RegistrationFailure;
    public event Action<string> ForgotPasswordSuccess;
    public event Action<string> ForgotPasswordFailure;

    public event Action<string> GetStoriesSuccess;

    public event Action<string> GetCoachSuccess;
    public event Action<string> GetCoachFailure;

    public event Action<string> GetHistorySuccess;
    public event Action<string> GetHistoryFailure;

    public event Action<string> GetPhaseStatusSuccess;
    public event Action<string> GetPhaseStatusFailure;
    /// <summary>
    /// URLS
    /// ninjapiURI -> http://127.0.0.1:8000/ninjapi/
    /// ninjasURI -> 
    /// </summary>
    /// 
    [BoxGroup("URLs")] public string ninjapiURI;
    [BoxGroup("URLs")] public string ninjasURI;
    [BoxGroup("URLs")] public string questURI;
    [BoxGroup("URLs")] public string loginURI;
    [BoxGroup("URLs")] public string refreshUrl;
    [BoxGroup("URLs")] public string logoutURI;
    [BoxGroup("URLs")] public string userURI;
    [BoxGroup("URLs")] public string postURI;
    [BoxGroup("URLs")] public string coachURI;
    [BoxGroup("URLs")] public string questStatusURI;
    [BoxGroup("URLs")] public string signUpURI;
    [BoxGroup("URLs")] public string resetPasswordURI;
    [BoxGroup("URLs")] public string draftsURI;
    [BoxGroup("URLs")] public string pagesURI;
    [BoxGroup("URLs")] public string surveyQuestionsURI;
    [BoxGroup("URLs")] public string surveyAnswersURI;
    [BoxGroup("URLs")] public string phasesURI;
    [BoxGroup("URLs")] public string actionsURI;
    [BoxGroup("URLs")] public string chatHistoryURI;
    [BoxGroup("URLs")] public string phaseStatusURI;
    [BoxGroup("URLs")] public string serverStatusURI;

    [BoxGroup("Model")] public Rad_Model radModel = new Rad_Model();
    //The Token that the server will send once the user login
    [Tooltip("Time needed to expire the user token")]
    [BoxGroup("Token")] public float tokenExpiration;


    [BoxGroup("Logo")] public Sprite schoolForNinjaLogo;

    private JsonData[] jsonData;
    private JsonData[] ninjson;

    private bool isLogged = false;
    public bool isCoach = false;

    static BackendManager instance;
    public static BackendManager Instance { get { return instance; } }


    private string[] questSpritesTemp = new string[6];

    private Dictionary<string, string> draftsSpritesTemp = new Dictionary<string, string>();
    private Dictionary<string,string> postSpritesTemp = new Dictionary<string,string>();
    private Dictionary<int, string> questCoversList = new Dictionary<int, string>();
    private Dictionary<int, string> ninjaListAvatar = new Dictionary<int, string>();

    private Action _lastAction;

    [HideInInspector]
    public int spritesCalled = 0;
    private int avatarNinjaAssigned = 0;
    [HideInInspector]
    public int questImage = 0;
    private bool callPostsOnce = true;
    public int storiesSprites = 0;

    private void Start()
    {
        
        if (!Instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

    }
    /// <summary>
    /// Method to prepare the user data, transform that data to JSON and starts the Login coroutine call to server.
    /// </summary>
    /// <param name="APILoginURL">The Login API url</param>
    /// <param name="username">Ninja username, coming from input field on scene</param>
    /// <param name="password">Ninja password, coming from input field on scene</param>
    public void TokenLogin(string username, string password)
    {

        string realLoginURL = "";

        LoginUserData loginUserData = new LoginUserData();
        loginUserData.username = username;
        loginUserData.password = password;

        string jsonToSend = JsonUtility.ToJson(loginUserData);

        if (UseProduction)
        {
            realLoginURL = ProductionUrl + loginURI;
        }
        else
        {
            realLoginURL = DevelopmentUrl + loginURI;
        }

        // StartCoroutine(CallLogin(realLoginURL, jsonToSend));
        CallLogin(realLoginURL, jsonToSend);
    }



    /// <summary>
    /// Method to Login on Server
    /// Creates UnityWebRequest with all the needed data and attributes, HandlerRaw, HandlerBuffer, UTF8Encoding
    /// Handles request messages, print info in console
    /// Captures the csrf_token value
    /// Tips -> https://stackoverflow.com/questions/50313537/unity-web-request-django-csrf-token-missing-or-incorrect
    /// </summary>
    /// <param name="url">Login API url</param>
    /// <param name="logindataJsonString">JSON data to send to server</param>
    /// <returns></returns>
    public void CallLogin(string url, string logindataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(logindataJsonString);
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Timeout = 10,
            UploadHandler = new UploadHandlerRaw(jsonToSend), 
            ContentType = "application/json",
        }).Then(response => {

            radModel.djangoUser = new Rad_Model.DjangoUser();
            radModel.djangoUser = JsonUtility.FromJson<Rad_Model.DjangoUser>(response.Request.downloadHandler.text);
            isLogged = true;
            //RefreshToken();
            //StartCoroutine(GetNinjaForUser());
            StartCoroutine(AutoRefreshToken(3000));
            GetPhases();
        }).Catch(err => {
            LoginFailure("Login Failure");
        });
    }
    /// <summary>
    /// Convert 'App sign up form data' to json and call coroutine to start server call.
    /// </summary>
    /// <param name="email"></param>
    /// <param name="password"></param>
    /// <param name="repassword"></param>
    /// <param name="userAgree"></param>
    public void SignUp(string email, string password, string repassword, bool userAgree)
    {
        SignUpUserData signupUserData = new SignUpUserData();
        signupUserData.username = email;
        signupUserData.email = email;
        signupUserData.password = password;
        signupUserData.repassword = repassword;
        signupUserData.userAgree = userAgree;

        string jsonToSend = JsonUtility.ToJson(signupUserData);

        CallServer_SignUp(jsonToSend);
    }

    private void CallServer_SignUp(string signUpDataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(signUpDataJsonString);

        string realSignUpURL = "";

        if (UseProduction)
        {
            realSignUpURL = ProductionUrl + signUpURI;
        }
        else
        {
            realSignUpURL = DevelopmentUrl + signUpURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = realSignUpURL,
            Method = "POST",
            Timeout = 10,
            UploadHandler = new UploadHandlerRaw(jsonToSend),
            ContentType = "application/json",
        }).Then(response => 
        {
            RegistrationSuccess(response.Request.downloadHandler.text);
            
        }).Catch(err => {
            RegistrationFailure(err.Message);
        });
    }

    /// <summary>
    /// Logout user from Server
    /// </summary>
    public void Logout()
    {
        CallServer_Logout();
    }

    public void CallServer_Logout()
    {
        RestClient.Post(new RequestHelper { Uri = logoutURI }).Then(response => 
        {
            Debug.Log("All OK");
            
        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Send a refresh token request, once finished will call this coroutine again
    /// </summary>
    /// <param name="token">token to refresh</param>
    /// <returns></returns>
    private IEnumerator AutoRefreshToken(float _time)
    {
        yield return new WaitForSeconds(_time);
        //Create request with login url and POST method
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + refreshUrl;
        }
        else
        {
            url = DevelopmentUrl + refreshUrl;
        }

        Rad_Model.Token _token = new Rad_Model.Token();
        _token.token = radModel.djangoUser.token;
        var json = JsonUtility.ToJson(_token);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);


        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            var tokenjson = JsonMapper.ToObject<Rad_Model.Token>(response.Request.downloadHandler.text);
            radModel.djangoUser.token = tokenjson.token;
            Debug.Log(radModel.djangoUser.token);

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

        StartCoroutine(AutoRefreshToken(_time));
    }


    private void RefreshToken()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + refreshUrl;
        }
        else
        {
            url = DevelopmentUrl + refreshUrl;
        }

        Rad_Model.Token _token = new Rad_Model.Token();
        _token.token = radModel.djangoUser.token;
        var json = JsonUtility.ToJson(_token);
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(json);


        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            var tokenjson = JsonMapper.ToObject<Rad_Model.Token>(response.Request.downloadHandler.text);
            radModel.djangoUser.token = tokenjson.token;
            Debug.Log(radModel.djangoUser.token);

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });


    }


    public bool IsLogged()
    {
        return isLogged;
    }

    /// <summary>
    /// API Call to get the current user Token.
    /// </summary>
    /// <returns></returns>
    public string GetCurrentUserToken()
    {
        return radModel.djangoUser.token;
    }

    public void ChangeUserName()
    {
        int userPK = radModel.djangoUser.user.pk; //needed pk to be added at the end of the uri
        Rad_Model.Ninja ninja = new Rad_Model.Ninja();

        ninja.ninja_name = "ninja name";
        ninja.avatar = null;
        ninja.gender = "m";
        ninja.domain = "domain";
        ninja.power = "power";
        ninja.profilePrivacy = "everyone";

        string jsonString = JsonUtility.ToJson(ninja); //we transform the user model class to json
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jsonString);

        string realSignUpURL = "";

        if (UseProduction)
        {
            realSignUpURL = ProductionUrl + ninjapiURI + userURI + userPK + "/";
        }
        else
        {
            realSignUpURL = DevelopmentUrl + ninjapiURI + userURI + userPK + "/";
        }

        RestClient.Request(new RequestHelper
        {
            Uri = realSignUpURL,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", radModel.djangoUser.token }
            },
            Timeout = 10,
            UploadHandler = new UploadHandlerRaw(jsonToSend),
            ContentType = "application/json",

        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                Debug.Log(response.Request.error);
            }
            else
            {
                Debug.Log("The User should have changed, check it out!");
            }
        });
    }

    /// <summary>
    /// Creates a RestClient request to get specific organization data from server
    /// </summary>
    /// <param name="url">organization url</param>
    /// <param name="OnFail">event triggered if the requests fails</param>
    public void GetOrganization(string url, Action OnFail)
    {
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            EnableDebug = true,
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
                OnFail();
            }
            else
            {
                var json = JsonMapper.ToObject<JsonData>(response.Request.downloadHandler.text);

                QuestManagerV2.Instance.ParseOrganizationJSon(json);
            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
            OnFail();
        });
    }

    public void ChangeOrganization()
    {
        StartCoroutine(CallServer_ChangeOrganization());
    }

    IEnumerator CallServer_ChangeOrganization()
    {
        //Get the Loged user PK to work with lager
        //int userPK = radModel.djangoUser.user.pk; //needed pk to be added at the end of the uri
        //Now we are gonna get 
        //string userNewName = "Paco Powers"; //new user name to be changed on the server

        //radModel.djangoUser.user.username = userNewName; //update the model with the new data as we have ToJson
        //string ninja_name = "ninja name";
        //string gender = "m";
        //string domain = "domain";
        //string power = "power";
        //string profilePrivacy = "profile privacy";

        Rad_Model.Ninja ninja = new Rad_Model.Ninja();

        ninja.ninja_name = "ninja name";
        ninja.avatar = null;
        ninja.gender = "m";
        ninja.domain = "domain";
        ninja.power = "power";
        ninja.profilePrivacy = "everyone";

        string jsonString = JsonUtility.ToJson(ninja); //we transform the user model class to json

        var request = new UnityWebRequest(ninjapiURI + userURI + /*userPK*/ "/", "PUT");

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jsonString);

        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();

        request.SetRequestHeader("Content-Type", "application/json");
        request.SetRequestHeader("Authorization", "JWT " + radModel.djangoUser.token);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log("The User should have changed, check it out!");
        }
    }


    /// <summary>
    /// It gets the default basic root view for DefaultRouter
    /// {
    /// "users": "http://127.0.0.1:8000/ninjapi/users/",
    /// "ninjas": "http://127.0.0.1:8000/ninjapi/ninjas/",
    /// "coaches": "http://127.0.0.1:8000/ninjapi/coaches/",
    /// "organizations": "http://127.0.0.1:8000/ninjapi/organizations/",
    /// "quests": "http://127.0.0.1:8000/ninjapi/quests/",
    /// "quest_statuses": "http://127.0.0.1:8000/ninjapi/quest_statuses/",
    /// "post_logs": "http://127.0.0.1:8000/ninjapi/post_logs/"
    /// }
    /// </summary>
    public void GetNinjapiRoot()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI;
        }

        var ninjapi = FindObjectOfType<Rad_NinjapiInterface>().ninjasUIPanel;
        if (ninjapi)
        {
            ninjapi.SetActive(true);
        }
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            EnableDebug = true,
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
            }
            else
            {
                Debug.Log(response.Request.downloadHandler.text);

            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to active server to get all ninjas data
    /// </summary>
    public void GetNinjas()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + ninjasURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + ninjasURI;
        }

        var ninjapi = FindObjectOfType<Rad_NinjapiInterface>().ninjasUIPanel;
        if (ninjapi)
        {
            ninjapi.SetActive(true);
        }
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            EnableDebug = true,
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
            }
            else
            {
                ninjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

                ParseNINJSON(ninjson);
            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive all ninjas assigned to the active coach
    /// </summary>
    public void GetNinjasForCoach()
    {
        string url;
        //var str = radModel.userCoach.url.Split('/');
        //var pk = int.Parse(str[str.Length - 2]);
        var pk = radModel.djangoUser.user.pk;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + ninjasURI + "?has_coaches=" + pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + ninjasURI + "?has_coaches=" + pk;
        }


        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
            }
            else
            {
                if (response.Request.downloadHandler.text.Length > 3)
                {
                    ninjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);
                    ParseCoachNinjaListNINJSON(ninjson);
                }

                AssignPersonalLogSprites();

            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClietn request to check if the given name is used in the DB
    /// </summary>
    /// <param name="_name">name to check</param>
    /// <param name="OnSuccess">Event triggered if the request is succed</param>
    /// <param name="OnNameUsed">Event triggered if the name is already in use</param>
    /// <param name="OnFail">Event triggered if the request fails</param>
    public void CheckNinjaName(string _name, Action OnSuccess,Action OnNameUsed, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + ninjasURI + "?ninja_name=" + _name;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + ninjasURI + "?ninja_name=" + _name;
        }


        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            if(response.Request.downloadHandler.text.Length > 3)
            {
                OnNameUsed();
            }
            else
            {
                OnSuccess();
            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
            OnFail();
        });
    }

    /// <summary>
    /// Creates a RestClient request to modify user ninja in DB
    /// </summary>
    /// <param name="ninjadataJsonString">ninja json</param>
    /// <param name="OnSuccess">Event triggered if the requests succed</param>
    /// <param name="OnFail">Event triggered if the request fails</param>
    public void ModifyNinja(string ninjadataJsonString, Action OnSuccess, Action OnFail)
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + ninjasURI + radModel.ninja.pk + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + ninjasURI + radModel.ninja.pk + '/';
        }

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(ninjadataJsonString);

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend),
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
                OnFail();
            }
            else
            {
                RadProfileUIManager radProfileUIManager = FindObjectOfType<RadProfileUIManager>();
                if (radProfileUIManager)
                {
                    radProfileUIManager.RefreshNinjaAvatar();
                    radProfileUIManager.RefreshNinjaName();
                }

                OnSuccess();
            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
            if (err.Message.Contains("Unknown"))
            {
                OnFail();
            }
        });
    }
    /// <summary>
    /// Creates a RestClient request to modify user ninja in DB
    /// </summary>
    /// <param name="ninjadataJsonString">ninja json</param>
    public void ModifyNinja(string ninjadataJsonString)
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + ninjasURI + radModel.ninja.pk + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + ninjasURI + radModel.ninja.pk + '/';
        }

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(ninjadataJsonString);

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend),
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //Check internet panel
            }
            else
            {
                RadProfileUIManager radProfileUIManager = FindObjectOfType<RadProfileUIManager>();
                if (radProfileUIManager)
                {
                    radProfileUIManager.RefreshNinjaAvatar();
                    radProfileUIManager.RefreshNinjaName();
                }

            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });

    }

    /// <summary>
    /// Creates RestClient request to get ninja data related to user loged
    /// Calls GetQuests() after request succeed, used only in login
    /// </summary>
    private void GetNinja()
    {
        var pk = radModel.djangoUser.user.pk;

        string realGetNinjaURL = "";

        if (UseProduction)
        {
            realGetNinjaURL = ProductionUrl + ninjapiURI + ninjasURI + "?user=" + pk ;
        }
        else
        {
            realGetNinjaURL = DevelopmentUrl + ninjapiURI + ninjasURI + "?user=" + pk ;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = realGetNinjaURL,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            EnableDebug = true,
        }).Then(response =>
        {
            ninjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);
            ParseNINJSON(ninjson);
            GetQuests();

            
        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }
    
    /// <summary>
    /// Creates a RestClient request to receive all coaches data from active server
    /// if surveyManager is active, creates a list with all coaches there
    /// </summary>
    public void GetCoaches()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + coachURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + coachURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            EnableDebug = true,
        }).Then(response =>
        {
            Debug.Log("Get coaches success");
            var coachjson = JsonMapper.ToObject<Rad_Model.Coach[]>(response.Request.downloadHandler.text);

            var surveyManager = FindObjectOfType<IntroSurveyManager>();
            if (surveyManager)
            {
                surveyManager.SetCoachList(coachjson);
            }
            
        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to check if the user logged is a coach
    /// If it is continues with GetNinjasForCoach()
    /// If it's not continues with GetPersonalStories()
    /// Used only in login
    /// </summary>
    public void IsUserACoach()
    {
        var pk = radModel.djangoUser.user.pk;
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + coachURI + "?user=" + pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + coachURI + "?user=" + pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            if (response.Request.downloadHandler.text.Length < 3)
            {
                GetPersonalStories(GetQuestsStatuses);
            }
            else
            {
                var coaches = JsonMapper.ToObject<Rad_Model.Coach[]>(response.Request.downloadHandler.text);
                radModel.userCoach = coaches[0];
                if (RadSaveManager.spriteListData.CheckForSprite(radModel.userCoach.coach_avatar))
                {
                    radModel.userCoach.coach_avatar = RadSaveManager.spriteListData.spriteData[radModel.userCoach.coach_avatar];
                }
                else
                {
                    StartCoroutine(GetUserCoachAvatar(radModel.userCoach.coach_avatar));
                }

                isCoach = true;
                GetNinjasForCoach();
            }
            
        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }


    /// <summary>
    /// Creates a RestClient request to modify a specific coach data in server,
    /// </summary>
    /// <param name="coachjson">coach to modify</param>
    /// <param name="_coach">coach id</param>
    /// <param name="OnSuccess">Event triggered if the request succeed</param>
    /// <param name="OnFail">Event triggered if the event fails</param>
    public void ModifyCoach(string coachjson,int _coach,Action OnSuccess, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + coachURI + _coach + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + coachURI + _coach + '/';
        }

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(coachjson);

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend),
        }).Then(response =>
        {
            OnSuccess();

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
            OnFail();
        });
    }

    /// <summary>
    /// Creates a RestClient request to modify UserCoach information
    /// </summary>
    /// <param name="coachjson"></param>
    public void ModifyUserCoach(string coachjson)
    {
        string url;

        var str = radModel.userCoach.url.Split('/');
        var _coach = int.Parse(str[str.Length - 2]);

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + coachURI + _coach + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + coachURI + _coach + '/';
        }

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(coachjson);

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend),
        }).Then(response =>
        {

            Debug.Log("Modify OK");
            

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });


    }

    /// <summary>
    /// Creates a RestClient request to receive the coach for active user
    /// if the RadProfileUIManager is active, sets the youtuber url
    /// </summary>
    public void GetMyCoach()
    {
        RestClient.Request(new RequestHelper
        {
            Uri = radModel.ninja.has_coaches[0],
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            radModel.myCoach = JsonUtility.FromJson<Rad_Model.Coach>(response.Request.downloadHandler.text);

            if (FindObjectOfType<RadProfileUIManager>())
            {
                FindObjectOfType<RadProfileUIManager>().coachVidedo.youtubeUrl = radModel.myCoach.youtubeurl;
            }

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive your coach name
    /// </summary>
    /// <param name="coachURL">coach to get name</param>
    public void GetMyCoachName(string coachURL)
    {
        RestClient.Request(new RequestHelper
        {
            Uri = coachURL,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            radModel.myCoach = JsonUtility.FromJson<Rad_Model.Coach>(response.Request.downloadHandler.text);
            ParseMyCoachName(radModel.myCoach, response.Request.downloadHandler.text);

        }).Catch(err => {
            Debug.Log("Error: " + err.Message);
            GetCoachFailure(err.Message);
        });

    }

    private void ParseMyCoachName(Rad_Model.Coach data, string response)
    {
        radModel.myCoach = new Rad_Model.Coach();
        radModel.myCoach.url = data.url.ToString();
        radModel.myCoach.coach_name = data.coach_name.ToString();
        radModel.myCoach.youtubeurl = data.youtubeurl;

        if (RadSaveManager.spriteListData.CheckForSprite(data.coach_avatar.ToString()))
        {
            radModel.myCoach.coach_avatar = RadSaveManager.spriteListData.spriteData[data.coach_avatar.ToString()];
            GetCoachSuccess(response);
        }
        else
        {
            StartCoroutine(GetMyCoachAvatar(data.coach_avatar.ToString(), response));
        }
        
    }


    #region Quests Requests

    /// <summary>
    /// Creates a RestClient request to receive all phases data from server,
    /// continues with GetNinja()
    /// called only in login
    /// </summary>
    public void GetPhases()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + phasesURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + phasesURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var phasesjson = JsonMapper.ToObject<Rad_Model.Phase[]>(response.Request.downloadHandler.text);

            for (int i = 0; i < phasesjson.Length; i++)
            {
                radModel.phasesList.Add(phasesjson[i]);
            }

            radModel.phasesList.Sort((a, b) => a.url.CompareTo(b.url));
            GetNinja();

        })
        .Catch(Err => {
            Debug.Log("Error Message: " + Err.Message);

            if(Err.Message.Contains("No credentials provided"))
            {
                RefreshToken();
            }
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive all quests for a given phase
    /// </summary>
    /// <param name="_phase">phase you want to receive the quests</param>
    /// <param name="OnSuccess">Event triggered if the request succeed</param>
    /// <param name="OnFail">Event triggered if the event fails</param>
    public void GetPhaseQuests(int _phase, Action OnSuccess, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questURI + "?is_from_phase=" + _phase;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questURI + "?is_from_phase=" + _phase;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var questjson = JsonMapper.ToObject<Rad_Model.Quest[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.SetPhaseQuestList(questjson);
            }

            OnSuccess();

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);

            OnFail();
            
        });


    }

    /// <summary>
    /// Creates a RestClient request to receive all quests from server
    /// called only in login
    /// </summary>
    public void GetQuests()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var questjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);
            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.SetAllQuestsLists(questjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    #endregion

    #region Quest Statuses requests
    /// <summary>
    /// Creates a RestClient request to receive if exists the quest status for active player, If exists
    /// sets the currentQuestStatus in QuestManagerV2 to recevied one
    /// </summary>
    /// <param name="questID">quest Id you want the quest status</param>
    public void GetQuestStatus(int questID)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questID;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questID;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null && postjson.Length > 0)
            {
                QuestManagerV2.Instance.SetCurrentQuestStatus(postjson[0]);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive all quests status for active user, called in login
    /// continues with AssignPersonalLogSprites()
    /// </summary>
    private void GetQuestsStatuses()
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var questStatusjson = JsonMapper.ToObject<Rad_Model.Quest_Status[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.SetQuestStatusList(questStatusjson);
            }
            AssignPersonalLogSprites();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates RestClient request to receive all quest status for active user
    /// </summary>
    /// <param name="OnSuccess">even triggered when request succeed</param>
    /// <param name="OnFail">event triggered when request fails</param>
    public void GetAllQuestStatus(Action OnSuccess, Action OnFail)
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.SetQuestStatusList(postjson);
            }

            OnSuccess();
            
        }).Catch(Err => {

            OnFail();
            
        });
    }

    /// <summary>
    /// Creates RestClient request to recieve questStatus for given quest ID
    /// </summary>
    /// <param name="questID">quest to check quest status</param>
    /// <param name="onSuccess">event triggered when request succeed</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void GetQuestStatus(int questID, Action onSuccess, Action onFail)
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questID;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questID;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null && postjson.Length > 0)
            {
                QuestManagerV2.Instance.SetCurrentQuestStatus(postjson[0]);
            }
            else
            {
                QuestManagerV2.Instance.ResetQuestStatus();
            }
            onSuccess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);

            onFail();
            
        });
    }

    /// <summary>
    /// Creates RestClient request to create a quest Status
    /// </summary>
    /// <param name="queststatusdataJsonString">quest status json string to send</param>
    public void CreateQuestStatus(string queststatusdataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(queststatusdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status>(response.Request.downloadHandler.text);
            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.AddQuestStatusToList(postjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates RestClient request to create a quest Status
    /// </summary>
    /// <param name="queststatusdataJsonString">quest status json string</param>
    /// <param name="onSuccess">Event triggered when request succeed</param>
    /// <param name="onFail">Event triggered when request fails</param>
    public void CreateQuestStatus(string queststatusdataJsonString, Action onSuccess, Action onFail)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(queststatusdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status>(response.Request.downloadHandler.text);
            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.AddQuestStatusToList(postjson);
            }

            onSuccess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);

            onFail();
            
        });
    }

    /// <summary>
    /// Creates RestClient request to modify a quest status
    /// </summary>
    /// <param name="queststatusJson">quest status json</param>
    /// <param name="queststatusID">quest status ID</param>
    public void ModifyQuestStatus(string queststatusJson, int queststatusID)
    {

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(queststatusJson);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + questStatusURI + queststatusID + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + questStatusURI + queststatusID + '/';
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<Rad_Model.Quest_Status>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.ModifyQuestStatus(postjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    #endregion

    #region Pages requests
    /// <summary>
    /// Creates RestClient request to receive all pages for given quest
    /// </summary>
    /// <param name="questId">quest ID</param>
    /// <param name="onSucceess">event triggered when request succeed</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void GetPages(int questId, Action onSucceess, Action onFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + pagesURI + "?belongs_to_quest=" + questId;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + pagesURI + "?belongs_to_quest=" + questId;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var questjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.ParseQuestPageJSON(questjson);
            }
            onSucceess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            onFail();
        });
    }

    /// <summary>
    /// Creates RestClient request to receive all pages for given quest
    /// </summary>
    /// <param name="questId">questID</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void GetPages(int questId, Action onFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + pagesURI + "?belongs_to_quest=" + questId;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + pagesURI + "?belongs_to_quest=" + questId;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var questjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.ParseQuestPageJSON(questjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);

            onFail();
            
        });
    }

    #endregion

    #region Drafts Requests
    /// <summary>
    /// Creates a RestClient request to check and receive if the quest opened have drafts written by the user
    /// </summary>
    /// <param name="questId">questID </param>
    /// <param name="onFail">event triggered when request fails</param>
    public void GetDrafts(int questId, Action onFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + draftsURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questId;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + draftsURI + "?belongs_to_ninja=" + radModel.ninja.pk + "&belongs_to_quest=" + questId;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var draftjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null)
            {
                QuestManagerV2.Instance.ParseDraftsJSON(draftjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            onFail();
        });
    }

    /// <summary>
    /// Creates a RestClient request to create a draft for active quest
    /// </summary>
    /// <param name="draftdataJsonString">draft json</param>
    public void CreateDraft(string draftdataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(draftdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + draftsURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + draftsURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {

            //TODO link received draft to current quest status
            var draftjson = JsonMapper.ToObject<Rad_Model.Draft>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null && draftjson != null)
            {
                QuestManagerV2.Instance.AddDraftToCurrentQuestStatus(draftjson);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to modify a draft
    /// </summary>
    /// <param name="_draftId">draft ID</param>
    /// <param name="draftdataJsonString">draft json</param>
    /// <param name="onSucceess">event triggered when request succeed</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void ModifyDraft(int _draftId, string draftdataJsonString, Action onSucceess, Action onFail)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(draftdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + draftsURI + _draftId + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + draftsURI + _draftId + '/';
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {

            var draftjson = JsonMapper.ToObject<Rad_Model.Draft>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null && draftjson != null)
            {
                QuestManagerV2.Instance.ModifyDraft(draftjson);
            }

            onSucceess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            onFail();
        });
    }

    /// <summary>
    /// Creates a RestClient request to modify a draft 
    /// </summary>
    /// <param name="_draftId">draft ID</param>
    /// <param name="draftdataJsonString">draft json</param>
    public void ModifyDraft(int _draftId, string draftdataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(draftdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + draftsURI + _draftId + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + draftsURI + _draftId + '/';
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {

            var draftjson = JsonMapper.ToObject<Rad_Model.Draft>(response.Request.downloadHandler.text);

            if (QuestManagerV2.Instance != null && draftjson != null)
            {
                QuestManagerV2.Instance.ModifyDraft(draftjson);
            }

            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    #endregion

    #region Posts Requests
    /// <summary>
    /// Get posts with privacy everyone and everyone_outside
    /// </summary>
    /// <param name="_isRefresh">/param>
    /// <returns></returns>
    public void GetStories(bool _isRefresh)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=everyone";
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=everyone";
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);
            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson, true);
            }
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=everyone_outside";
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=everyone_outside";
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);
            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson,false);
            }

            GetStoriesForCoaches();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates RestCLient request to receive all stories with privacy everyone and everyone_outside 
    /// for given quest
    /// </summary>
    /// <param name="_questId">quest ID you want to receive the stories</param>
    public void GetStoriesForQuest(int _questId)
    {
        storiesSprites = 0;
        string url;
        PostsManager.Instance.GetQuestStoriesList().Clear();

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=everyone_outside&&belongs_to_quest=" + _questId;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=everyone_outside&&belongs_to_quest=" + _questId;
        }
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson, false);
            }



        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=everyone&&belongs_to_quest=" + _questId;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=everyone&&belongs_to_quest=" + _questId;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson, false);
            }
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });





    }

    /// <summary>
    /// Creates a RestClient request to receive all the stories for the active user
    /// </summary>
    /// <param name="onSuccess">event triggered when request succeed</param>
    public void GetPersonalStories(Action onSuccess)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson,true);
            }
            onSuccess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive all the stories with privacy only_coach
    /// </summary>
    /// <param name="OnSuccess">event triggered when request succeed</param>
    public void GetPostsForCoaches(Action OnSuccess)
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=only_coach";
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=only_coach";
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson,true);
            }

            OnSuccess();
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }
    
    /// <summary>
    /// Creates a RestClient request to receive all the stories with privacy only_coach
    /// </summary>
    public void GetStoriesForCoaches()
    {
        string url;
        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + "?privacy=only_coach";
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + "?privacy=only_coach";
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            var postjson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParseAllPOSTJSON(postjson,true);
            }

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

    }

    /// <summary>
    /// Creates a RestClient request to create a story
    /// </summary>
    /// <param name="postdataJsonString">story json</param>
    /// <param name="onSuccess">event triggered when request succeed</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void CreateStory(string postdataJsonString, Action onSuccess, Action onFail)
    {

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(postdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            var postjson = JsonMapper.ToObject<JsonData>(response.Request.downloadHandler.text);

            if (PostsManager.Instance != null)
            {
                PostsManager.Instance.ParsePostAndAddToList(postjson);
            }

            onSuccess();

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);

            onFail();
            
        });


    }

    /// <summary>
    /// Creates a RestClient request to modify a story
    /// </summary>
    /// <param name="_storytId">story Id to modify</param>
    /// <param name="storydataJsonString">story json</param>
    public void ModifyStory(int _storytId, string storydataJsonString)
    {

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(storydataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + _storytId + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + _storytId + '/';
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
             Debug.Log(response.Request.downloadHandler.text);
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

    }

    /// <summary>
    /// Creates a RestClient request to modofy a story
    /// </summary>
    /// <param name="_postId">story Id to modiify</param>
    /// <param name="storydataJsonString">story json</param>
    /// <param name="onSuccess">event triggered when request succeed</param>
    /// <param name="onFail">event triggered when request fails</param>
    public void ModifyStory(int _postId, string storydataJsonString, Action onSuccess, Action onFail)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(storydataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + postURI + _postId + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + postURI + _postId + '/';
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            onSuccess();
            Debug.Log(response.Request.downloadHandler.text);
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            onFail();
        });



    }

    #endregion
    #region Survey
    /// <summary>
    /// Creates a RestClient request to receive all survey questions from the server
    /// </summary>
    /// <param name="OnSuccess">event triggered when request succeed</param>
    /// <param name="OnFail">event triggered when request fails</param>
    public void GetSurveyQuestions(Action OnSuccess, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + surveyQuestionsURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + surveyQuestionsURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            Debug.Log("Survey Questions");
            var surveyjson = JsonMapper.ToObject<Rad_Model.SurveyQuestion[]>(response.Request.downloadHandler.text);

            var surveyManager = FindObjectOfType<IntroSurveyManager>();

            if (surveyManager)
            {
                surveyManager.SetSurveyQuestionList(surveyjson);
            }

            OnSuccess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            OnFail();
        });
    }

    /// <summary>
    /// Creates a RestClient request to receive all survey questions from the server
    /// </summary>
    /// <param name="OnSuccess">event triggered when request succeed</param>
    /// <param name="OnFail">event triggered when request fails</param>
    public void GetSurveyAnswers(Action OnSuccess, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + surveyAnswersURI +"?belongs_to_ninja=" + radModel.ninja.pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + surveyAnswersURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            IntroSurveyManager surveyManager = FindObjectOfType<IntroSurveyManager>();
            if (surveyManager && response.Request.downloadHandler.text.Length > 3)
            {
                surveyManager.finalSurvey = true;
            }

            OnSuccess();

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            OnFail();
        });
    }


    /// <summary>
    /// Creates a RestClient request to send surveyAnsers
    /// </summary>
    /// <param name="answersDataJsonString">survey answers json</param>
    /// <param name="OnSuccess">event triggered when request succeed</param>
    /// <param name="OnFail">event triggered when request fails</param>
    public void SendSurveyAnswers(string answersDataJsonString, Action OnSuccess, Action OnFail)
    {

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(answersDataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + surveyAnswersURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + surveyAnswersURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            Debug.Log(response.Request.downloadHandler.text);
            OnSuccess();
            
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            OnFail();
        });
    }


    #endregion

    #region Actions
    /// <summary>
    /// Creates a RestClient request to create an Action in active server
    /// </summary>
    /// <param name="actiondataJsonString">action json</param>
    public void CreateAction(string actiondataJsonString)
    {

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(actiondataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + actionsURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + actionsURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            Debug.Log(response.Request.downloadHandler.text);
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

    }
    #endregion

    #region ChatHistory
    /// <summary>
    /// retrieve the last 100 messages for a given channel;
    /// </summary>
    /// <param name="channel">the channel name you want the messages from</param>
    /// <returns></returns>
    public void GetChatHistory(string _channel)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + chatHistoryURI + "?channel=" + _channel;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + chatHistoryURI + "?channel=" + _channel;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                GetHistoryFailure(response.Request.downloadHandler.text);
            }
            else
            {
                var chatjson = JsonUtility.FromJson<Rad_Model.ChatHistoryPage>(response.Request.downloadHandler.text);

                ParseChatHistory(chatjson, _channel);
            }
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });
    }

    /// <summary>
    /// Creates a RestClient request to send Message written in any channel
    /// </summary>
    /// <param name="chatdataJsonString">Chat history json</param>
    public void CreateChatHistory(string chatdataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(chatdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + chatHistoryURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + chatHistoryURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            Debug.Log(response.Request.downloadHandler.text);
        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
        });

    }

    #endregion

    #region PhaseStatus
    /// <summary>
    /// Creates a RestClient request to receive all phase status for actve user and launches GetPhaseStatusSuccess event
    /// </summary>
    public void GetPhaseStatus()
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + phaseStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + phaseStatusURI + "?belongs_to_ninja=" + radModel.ninja.pk;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {
            if (response.Request.isNetworkError)
            {
                //NO INTERNET PANEL
                Debug.Log(response.Request.downloadHandler.text);
            }
            else
            {
                Debug.Log(response.Request.downloadHandler.text);
                var phasejson = JsonMapper.ToObject<JsonData[]>(response.Request.downloadHandler.text);

                ParsePhaseStatus(phasejson);

                GetPhaseStatusSuccess("Success");

            }
        }).Catch(Err =>
         {
             Debug.Log(Err.Message);
             GetPhaseStatusFailure("Failure");
         });
    }

    /// <summary>
    /// Creates a RestClient request to create a phase Status for active user
    /// </summary>
    /// <param name="fase"></param>
    public void CreatePhaseStatus(int fase)
    {

        Rad_Model.Phase_Status _newFaseStatus = new Rad_Model.Phase_Status();

        _newFaseStatus.belongs_to_ninja = radModel.ninja.url;
        _newFaseStatus.belongs_to_phase = radModel.phasesList[fase].url;
        _newFaseStatus.completed = false;

        string phasestatusdataJsonString = JsonUtility.ToJson(_newFaseStatus);

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(phasestatusdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + phaseStatusURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + phaseStatusURI;
        }
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            Debug.Log(response.Request.downloadHandler.text);

        }).Catch(Err => 
        {
            Debug.Log("Error: " + Err.Message);
        });

    }

    /// <summary>
    /// Creates a RestClient request to modify an active phase status
    /// </summary>
    /// <param name="_phasestatustId">phase status Id to modify</param>
    /// <param name="phasestatusdataJsonString">phase status json</param>
    public void ModifyPhaseStatus(int _phasestatustId, string phasestatusdataJsonString)
    {
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(phasestatusdataJsonString);
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + phaseStatusURI + _phasestatustId + '/';
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + phaseStatusURI + _phasestatustId + '/';
        }
        Debug.Log("calling: " + url);
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "PUT",
            Headers = new Dictionary<string, string>
            {
                {"Authorization", "JWT " + radModel.djangoUser.token }
            },
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend)
        }).Then(response =>
        {
            Debug.Log(response.Request.downloadHandler.text);
        }).Catch(Err => 
        {
            Debug.Log("Error: " + Err.Message);
        });



    }

    #endregion

    #region Server Status
    /// <summary>
    /// Creates a RestClient request to check Server Status
    /// </summary>
    /// <param name="OnSuccess">event triggered when request succeed</param>
    /// <param name="OnFail">eventr triggered when request fails</param>
    public void GetServerStatus(Action OnSuccess, Action OnFail)
    {
        string url;

        if (UseProduction)
        {
            url = ProductionUrl + ninjapiURI + serverStatusURI;
        }
        else
        {
            url = DevelopmentUrl + ninjapiURI + serverStatusURI;
        }

        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "GET",
            Timeout = 10,
            ContentType = "application/json",
        }).Then(response =>
        {

            var statusjson = JsonMapper.ToObject<Rad_Model.ServerStatus[]>(response.Request.downloadHandler.text);
            radModel.serverStatus = statusjson[0];
            OnSuccess();

        }).Catch(Err => {
            Debug.Log("Error: " + Err.Message);
            OnFail();
        });
    }

    #endregion

    private void ParsePhaseStatus(JsonData[] data)
    {
        radModel.phaseStatusList.Clear();
        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Phase_Status _status = new Rad_Model.Phase_Status();
            if (data[i].Keys.Contains("url"))
            {
                _status.url = data[i]["url"].ToString();
            }
            if (data[i].Keys.Contains("belongs_to_ninja"))
            {
                _status.belongs_to_ninja = data[i]["belongs_to_ninja"].ToString();
            }
            if (data[i].Keys.Contains("belongs_to_phase"))
            {
                _status.belongs_to_phase = data[i]["belongs_to_phase"].ToString();
            }
            if (data[i].Keys.Contains("completed"))
            {
                _status.completed = bool.Parse(data[i]["completed"].ToString());
            }
            if (data[i].Keys.Contains("started_on_date"))
            {
                _status.started_on_date = data[i]["started_on_date"].ToString();
            }

            radModel.phaseStatusList.Add(_status);
        }

        Debug.Log("PHASE STATUS LIST COUNT: " + radModel.phaseStatusList.Count);
        radModel.phaseStatusList.Sort((a, b) => a.url.CompareTo(b.url));
    }

    private void ParseNINJSON(JsonData[] data)
    {
        radModel.ninja = new Rad_Model.Ninja();

        for (int i = 0; i < data.Length; i++)
        {

            if (data[i].Keys.Contains("url"))
            {
                radModel.ninja.url = data[i]["url"].ToString();
                var str = radModel.ninja.url.Split('/');
                radModel.ninja.pk = int.Parse(str[str.Length - 2]);
            }

            if (data[i].Keys.Contains("ninja_name"))
            {
                radModel.ninja.ninja_name = data[i]["ninja_name"].ToString();
            }
            if (data[i].Keys.Contains("avatar"))
            {
                string url = data[i]["avatar"].ToString();
                if (RadSaveManager.spriteListData.CheckForSprite(url))
                {
                   radModel.ninja.avatar =  RadSaveManager.spriteListData.spriteData[url];
                }
                else
                {
                    StartCoroutine(GetNinjaSprite(url));
                }
            }

            if (data[i].Keys.Contains("gender"))
            {
                radModel.ninja.gender = data[i]["gender"].ToString();
            }

            if (data[i].Keys.Contains("level"))
            {
                radModel.ninja.level = (int)data[i]["level"];
            }

            if (data[i].Keys.Contains("domain"))
            {
                radModel.ninja.domain = data[i]["domain"].ToString();
            }

            if (data[i].Keys.Contains("power"))
            {
                radModel.ninja.power = data[i]["power"].ToString();
            }

            if (data[i].Keys.Contains("user"))
            {
                radModel.ninja.user = data[i]["user"].ToString();
            }
            if (data[i].Keys.Contains("first_time_connected"))
            {
                radModel.ninja.first_time_connected = bool.Parse(data[i]["first_time_connected"].ToString());
            }

            if (data[i].Keys.Contains("has_coaches"))
            {
                
                if (data[i]["has_coaches"].IsArray)
                {
                    radModel.ninja.has_coaches.Clear();
                    radModel.ninja.has_coaches.Add(data[i]["has_coaches"][0].ToString());
                }

            }
        }

    }

    /// <summary>
    /// Parse all the ninjas coach have
    /// </summary>
    /// <param name="data"></param>
    private void ParseCoachNinjaListNINJSON(JsonData[] data)
    {
        avatarNinjaAssigned = 0;
        radModel.userCoachNinjas = new List<Rad_Model.Ninja>();

        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.Ninja _newNinja = new Rad_Model.Ninja();
            if (data[i].Keys.Contains("url"))
            {
                _newNinja.url = data[i]["url"].ToString();
                var str = _newNinja.url.Split('/');
                _newNinja.pk = int.Parse(str[str.Length - 2]);
            }

            if (data[i].Keys.Contains("ninja_name"))
            {
                _newNinja.ninja_name = data[i]["ninja_name"].ToString();
            }
            if (data[i].Keys.Contains("avatar"))
            {
                string url = data[i]["avatar"].ToString();
                if (RadSaveManager.spriteListData.CheckForSprite(url))
                {
                    _newNinja.avatar = RadSaveManager.spriteListData.spriteData[url];
                    avatarNinjaAssigned++;
                }
                else
                {
                    StartCoroutine(GetNinjaSpriteInDictionary(url, _newNinja.pk));
                }
                
            }

            if (data[i].Keys.Contains("gender"))
            {
                _newNinja.gender = data[i]["gender"].ToString();
            }

            if (data[i].Keys.Contains("level"))
            {
                _newNinja.level = (int)data[i]["level"];
            }

            if (data[i].Keys.Contains("domain"))
            {
                _newNinja.domain = data[i]["domain"].ToString();
            }

            if (data[i].Keys.Contains("power"))
            {
                _newNinja.power = data[i]["power"].ToString();
            }

            if (data[i].Keys.Contains("user"))
            {
                _newNinja.user = data[i]["user"].ToString();
            }
            if (data[i].Keys.Contains("first_time_connected"))
            {
                _newNinja.first_time_connected = bool.Parse(data[i]["first_time_connected"].ToString());
            }

            if (data[i].Keys.Contains("has_coaches"))
            {
                if (data[i]["has_coaches"].IsArray)
                {
                    _newNinja.has_coaches.Clear();
                    _newNinja.has_coaches.Add(data[i]["has_coaches"][0].ToString());
                }
            }

            radModel.userCoachNinjas.Add(_newNinja);
        }

        if (avatarNinjaAssigned == radModel.userCoachNinjas.Count)
        {
            AssignNinjaListAvatars();
        }
    }

    private void AssignNinjaListAvatars()
    {
        for (int i = 0; i < radModel.userCoachNinjas.Count; i++)
        {
            if (ninjaListAvatar.ContainsKey(radModel.userCoachNinjas[i].pk))
            {
                radModel.userCoachNinjas[i].avatar = ninjaListAvatar[radModel.userCoachNinjas[i].pk];
            }
        }
    }

    /// <summary>
    /// https://stackoverflow.com/questions/44733841/how-to-make-texture2d-readable-via-script
    /// </summary>
    /// <param name="sprite"></param>
    /// <returns></returns>
    public string SpriteToBase64(Sprite sprite)
    {
        
        byte[] spriteData = sprite.texture.EncodeToJPG();
        string base64Sprite = Convert.ToBase64String(spriteData);

        return base64Sprite;
    }


    /// <summary>
    /// get sprite from server and store it in base64
    /// </summary>
    /// <param name="url">sprite url</param>
    /// <param name="base64Img">where to store the base64 string</param>
    /// <returns></returns>
    public IEnumerator GetQuestsPageSprite(string url, int pageId)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        
        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        if (!RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
        }

        QuestManagerV2.Instance.AssignQuestPagesImages(base64Img, pageId);
    }

    /// <summary>
    /// get sprite from server and store it in base64
    /// </summary>
    /// <param name="url">sprite url</param>
    /// <param name="base64Img">where to store the base64 string</param>
    /// <returns></returns>
    public IEnumerator GetQuestsSprite(string url, int questId)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        if (!RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
        }

        QuestManagerV2.Instance.AssignQuestImages(base64Img, questId);
    }

    /// <summary>
    /// get sprite from server and store it in base64
    /// </summary>
    /// <param name="url">sprite url</param>
    /// <param name="base64Img">where to store the base64 string</param>
    /// <returns></returns>
    public IEnumerator GetDraftsSprite(string url)
    {

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        if (!draftsSpritesTemp.ContainsKey(url))
        {
            draftsSpritesTemp.Add(url, base64Img);
        }

        if (!RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
            RadSaveManager.SaveData();
        }

       QuestManagerV2.Instance.AssignStringBase64ToDrafts();

    }

    public IEnumerator GetQuestCoverSprite(string url, int questId)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        questImage++;
        questCoversList.Add(questId, base64Img);

        if (!RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
        }

        if (questCoversList.Count == QuestManagerV2.Instance.GetQuestsList().Count)
        {
            AssignQuestImagesListToBase64();
        }
    }

    public void AssignQuestImagesListToBase64()
    {
        for (int i = 0; i < QuestManagerV2.Instance.GetQuestsList().Count; i++)
        {
            if (questCoversList.ContainsKey(QuestManagerV2.Instance.GetQuestsList()[i].id))
            {
                QuestManagerV2.Instance.GetQuestsList()[i].quest_cover_image = questCoversList[QuestManagerV2.Instance.GetQuestsList()[i].id];
            }
        }
        if (callPostsOnce)
        {

            callPostsOnce = false;
        }
        
    }

    public IEnumerator GetPostsSprite(string url)
    {
        if (!postSpritesTemp.ContainsKey(url))
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            while (!www.isDone) yield return null;

            var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
                new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
                ((DownloadHandlerTexture)www.downloadHandler).texture.height),
                new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

            postSpritesTemp.Add(url, base64Img);
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);

            storiesSprites++;
        }

    }

    public void ActivateEvent(bool login)
    {
        if(!login)
            GetStoriesSuccess("Success");
    }

    public void AssignPersonalLogSprites()
    {
        var loglist = PostsManager.Instance.GetPersonalLogsList();
        for (int i = 0; i < loglist.Count; i++)
        {
            if (postSpritesTemp.ContainsKey(loglist[i].image1))
            {
                loglist[i].image1 = postSpritesTemp[loglist[i].image1];
            }

        }
        LoginSuccess("Login Success");
        RadSaveManager.SaveData();
    }

    public IEnumerator GetOrganizationSprite(string url)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        while (!www.isDone) yield return null;

        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        if (RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
        }
        QuestManagerV2.Instance.SetCurrentOrganizationIconAndAssignToLandingPage(base64Img);


    }
    public IEnumerator GetCoachAvatar(string url, string coachUrl)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        while (!www.isDone) yield return null;

        string base64Img = null;

        if (!www.downloadHandler.text.Contains("Not Found"))
        {
            base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
                        new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
                        ((DownloadHandlerTexture)www.downloadHandler).texture.height),
                        new Vector2(0, 0), 100.0f).texture.EncodeToPNG());
        }

        var surveyManager = FindObjectOfType<IntroSurveyManager>();

        if (surveyManager)
        {
            surveyManager.AsignCoachesAvatars(coachUrl, base64Img);
        }
    }

    public IEnumerator GetUserCoachAvatar(string url)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        while (!www.isDone) yield return null;

        if (!www.downloadHandler.text.Contains("Not Found"))
        {
            var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
                            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
                            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
                            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

            radModel.userCoach.coach_avatar = base64Img;

            if (!RadSaveManager.spriteListData.CheckForSprite(url))
            {
                RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
            }
        }
    }

    public IEnumerator GetMyCoachAvatar(string url, string response)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        while (!www.isDone) yield return null;

        if (!www.downloadHandler.text.Contains("Not Found"))
        {
            var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
                            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
                                ((DownloadHandlerTexture)www.downloadHandler).texture.height),
                                    new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

            radModel.myCoach.coach_avatar = base64Img;

            if (!RadSaveManager.spriteListData.CheckForSprite(url))
            {
                RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
            }
        }



        GetCoachSuccess(response);
    }

    private void ParseChatHistory(Rad_Model.ChatHistoryPage data, string channel)
    {
        //we clean the history retrieved before
        radModel.chatHistoryList.Clear();

        for (int i = 0; i < data.results.Count; i++)
        {
            radModel.chatHistoryList.Add(data.results[i]);
        }

        radModel.chatHistoryList.Reverse();

        GetHistorySuccess(channel);

    }


    public Dictionary<string,string> GetDraftTempStrings()
    {
        return draftsSpritesTemp;
    }
    public string[] GetQuestPagesTempStrings()
    {
        return questSpritesTemp;
    }
    /// <summary>
    /// get sprite from server and store it in base64
    /// </summary>
    /// <param name="url">sprite url</param>
    /// <param name="base64Img">where to store the base64 string</param>
    /// <returns></returns>
    public IEnumerator GetNinjaSprite(string url)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        while (!www.isDone) yield return null;
        


        if (!((DownloadHandlerTexture)www.downloadHandler).text.Contains("Not Found"))
        {
            var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
                             new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
                                ((DownloadHandlerTexture)www.downloadHandler).texture.height),
                                    new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

            if (!RadSaveManager.spriteListData.CheckForSprite(url))
            {
                RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
            }
            radModel.ninja.avatar = base64Img;
        }

    }

    public IEnumerator GetNinjaSpriteInDictionary(string url, int ninjaId)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        while (!www.isDone) yield return null;

        var base64Img = System.Convert.ToBase64String(Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture,
            new Rect(0, 0, ((DownloadHandlerTexture)www.downloadHandler).texture.width,
            ((DownloadHandlerTexture)www.downloadHandler).texture.height),
            new Vector2(0, 0), 100.0f).texture.EncodeToPNG());

        ninjaListAvatar.Add(ninjaId, base64Img);
        avatarNinjaAssigned++;
        if (!RadSaveManager.spriteListData.CheckForSprite(url))
        {
            RadSaveManager.spriteListData.spriteData.Add(url, base64Img);
        }


        if (avatarNinjaAssigned == radModel.userCoachNinjas.Count)
        {
            AssignNinjaListAvatars();
        }
    }

    public void SetLastAction(Action _action)
    {
        _lastAction = _action;
    }

    public void CallLastAction()
    {
        _lastAction();
    }

    /// <summary>
    /// Decoding base64 to sprite
    /// </summary>
    /// <param name="base64Img">base64 string</param>
    /// <param name="_sprite"> sprite to store</param>
    public void DecodeBase64Texture(string base64Img, Sprite _sprite)
    {
        if (!string.IsNullOrEmpty(base64Img))
        {
            byte[] Bytes = System.Convert.FromBase64String(base64Img);
            Texture2D tex = new Texture2D(500, 700);
            tex.LoadImage(Bytes);
            Rect rect = new Rect(0, 0, tex.width, tex.height);
            _sprite = Sprite.Create(tex, rect, new Vector2(), 100f);
        }

    }

    public Sprite DecodeBase64Texture(string base64Img)
    {
        if (!string.IsNullOrEmpty(base64Img) && base64Img.Length > 10)
        {
            byte[] Bytes = System.Convert.FromBase64String(base64Img);
            Texture2D tex = new Texture2D(500, 700);
            tex.LoadImage(Bytes);
            Rect rect = new Rect(0, 0, tex.width, tex.height);
            var sprite = Sprite.Create(tex, rect, new Vector2(), 100f);
            return sprite;
        }
        return null;
    }

    public bool IsCoach()
    {
        return isCoach;
    }

    #region Reset Password

    public void ResetPassword(string userEmail, string fromEmail)
    {
        ForgotPasswordUserData forgotPasswordUserData = new ForgotPasswordUserData();
        forgotPasswordUserData.email = userEmail;
        forgotPasswordUserData.from_email = fromEmail;

        string jsonToSend = JsonUtility.ToJson(forgotPasswordUserData);

        string resetPasswordURL = "";

        if (UseProduction)
        {
            resetPasswordURL = ProductionUrl + resetPasswordURI;
        }
        else
        {
            resetPasswordURL = DevelopmentUrl + resetPasswordURI;
        }

        CallServer_ResetPassword(resetPasswordURL, jsonToSend);
    }

    /// <summary>
    /// https://docs.djangoproject.com/en/1.11/topics/auth/passwords/
    /// https://github.com/django/django/blob/1.11.2/django/contrib/auth/forms.py#L225
    /// https://stackoverflow.com/questions/52276097/how-do-i-send-email-to-user-to-reset-password-without-explicitly-filling-passwor
    /// https://wsvincent.com/django-user-authentication-tutorial-password-reset/
    /// </summary>
    /// <param name="userEmail"></param>
    /// <returns></returns>
    public void CallServer_ResetPassword(string url, string forgotPasswordJsonString)
    {
        byte[] jsonToSend = System.Text.Encoding.UTF8.GetBytes(forgotPasswordJsonString);
        RestClient.Request(new RequestHelper
        {
            Uri = url,
            Method = "POST",
            //Headers = new Dictionary<string, string>
            //{
            //    {"Authorization", "JWT " + radModel.djangoUser.token }
            //},
            Timeout = 10,
            ContentType = "application/json",
            UploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend),
            ChunkedTransfer = false,
        }).Then(response =>{

            if (response.Request.isNetworkError)
            {
                ForgotPasswordFailure(response.Request.downloadHandler.text);
            }
            else
            {
                ForgotPasswordSuccess(response.Request.downloadHandler.text);
            }

        }).Catch(Err => {
                Debug.Log("Error: " + Err.Message);
        });
    }

    #endregion
}
