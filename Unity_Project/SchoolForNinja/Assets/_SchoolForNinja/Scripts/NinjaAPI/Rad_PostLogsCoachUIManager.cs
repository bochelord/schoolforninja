﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class Rad_PostLogsCoachUIManager : MonoBehaviour {


    [BoxGroup("ListingContainers")] public GameObject logsListingContainer;
    [BoxGroup("ListingContainers")] public GameObject singlePostContainer;

    [BoxGroup("SinglePostContainer")] public Image[] postImages;
    [BoxGroup("SinglePostContainer")] public TMPro.TextMeshProUGUI[] postLogs;

    [BoxGroup("Prefab")] public GameObject postButtonPrefab;

    public RadCoachProfileUIManager radProfileUIManager;

    private RectTransform logsListingRect;
    private RectTransform singlePostRect;



    void Start()
    {
        logsListingRect = logsListingContainer.GetComponent<RectTransform>();
        singlePostRect = singlePostContainer.GetComponent<RectTransform>();
    }

    private void OnGetAllQuestStatuSuccess()
    {
        //TODO review to see if this method is needed
    }

    private void OnGetAllQuestStatuFail()
    {
        //TODO review to see if this method is needed
    }




    #region Buttons



    public void Button_ShowLogsForQuest(int _questId,string _questTitle)
    {
        logsListingContainer.transform.Find("Header").Find("questTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _questTitle;
        if (PostsManager.Instance.GetAllQuestsLogsByQuestID().ContainsKey(_questId))
        {
            List<Rad_Model.Post_Log> _posts = PostsManager.Instance.GetAllQuestsLogsByQuestID()[_questId];
            PostsManager.Instance.SetLogListByQuestId(_questId);
            _posts.Sort((a, b) => b.timestamp.CompareTo(a.timestamp));
            for (int i = 0; i < _posts.Count; i++)
            {
                AssignButtonPostCalls(i, _posts[i]);
            }
        }
    }

    public void Button_ShowLogsForNinja(int _ninjaID, string _ninjaName)
    {
        Transform parent = logsListingContainer.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        for (int i = 0; i < parent.childCount; i++)
        {
            parent.GetChild(i).gameObject.SetActive(false);
        }
        logsListingContainer.transform.Find("Header").Find("Title").GetComponent<TMPro.TextMeshProUGUI>().text = _ninjaName;
        if (PostsManager.Instance.GetAllQuestsLogsByNinjaID().ContainsKey(_ninjaID))
        {
            List<Rad_Model.Post_Log> _posts = PostsManager.Instance.GetAllQuestsLogsByNinjaID()[_ninjaID];
            _posts.Sort((a, b) => b.timestamp.CompareTo(a.timestamp));
            PostsManager.Instance.SetLogListByNinjaId(_ninjaID);

            for (int i = 0; i < _posts.Count; i++)
            {
                AssignButtonPostCalls(i,_posts[i]);
            }
        }
    }

    private void AssignButtonPostCalls(int index,Rad_Model.Post_Log _post)
    {
        var _parent = logsListingContainer.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        GameObject _button = null;
        if (_parent.childCount > index)
        {
            _button = _parent.GetChild(index).gameObject;
            _button.gameObject.SetActive(true);
        }
        else
        {
            _button = Instantiate(postButtonPrefab, _parent);
        }

        if (_post.has_image1)
        {
            _button.GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_post.image1);
        }
        else
        {
            _button.GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_post.story_cover_image);
        }
        _button.GetComponent<Button>().onClick.RemoveAllListeners();
        _button.GetComponent<Button>().onClick.AddListener(delegate { SetPostContent(_post.id); });
        _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _post.postTitle;

        _button.GetComponent<Button>().onClick.AddListener(Button_ShowSinglePost);
    }


    private void SetPostContent(int postId)
    {
        Rad_Model.Post_Log _log = new Rad_Model.Post_Log();
        for (int i = 0; i < PostsManager.Instance.GetCurrentLogsList().Count; i++)
        {
            if (PostsManager.Instance.GetCurrentLogsList()[i].id == postId)
            {
                _log = PostsManager.Instance.GetCurrentLogsList()[i];
                break;
            }
        }
        singlePostContainer.transform.Find("Header").Find("postTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _log.postTitle;

        if (_log.has_image1)
        {
            postImages[0].sprite = BackendManager.Instance.DecodeBase64Texture(_log.image1);
            postImages[0].preserveAspect = true;
        }
        else
        {
            postImages[0].sprite = QuestManagerV2.Instance.defaultQuestCoverImage[0];
            postImages[0].preserveAspect = true;
        }


        if (!string.IsNullOrEmpty(_log.postContent1))
        {
            postLogs[0].text = _log.postContent1;
        }



    }

    




    public void Button_ShowLogsListing()
    {
        logsListingContainer.SetActive(true);
        MovePanel_In(logsListingRect);
        radProfileUIManager.onLogListing = true;
    }


    public void Button_ShowSinglePost()
    {
        singlePostContainer.SetActive(true);
        MovePanel_In(singlePostRect);
        radProfileUIManager.onLogListing = true;
    }

    public void Button_BacktoPostLogs()
    {
        MovePanel_Out_Right(logsListingRect);
        radProfileUIManager.onLogListing = false;
    }


    public void Button_BacktoListing()
    {
        MovePanel_Out_Right(singlePostRect);
    }

    #endregion
    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(0, panel.transform.localPosition.y), 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, panel.transform.localPosition.y), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, panel.transform.localPosition.y), 0.25f); //To left position
    }

    #endregion ===============================================================================================

}
