﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Rad_Model {

    //public LoginUserData loginUserData;
    public DjangoUser djangoUser;
    public User user;
    public Coach userCoach; //if user is a coach
    public Quest quest;
    public Ninja ninja;
    public Coach myCoach;  //user coach
    public List<Phase> phasesList = new List<Phase>();
    public List<Ninja> userCoachNinjas = new List<Ninja>();
    public List<Quest> questsList = new List<Quest>();
    public List<ChatHistory> chatHistoryList = new List<ChatHistory>();
    public List<Phase_Status> phaseStatusList = new List<Phase_Status>();
    public int currentPhase;
    public ServerStatus serverStatus = new ServerStatus();
    /// <summary>
    /// Helper class to create later the JSON string with these 2 variables, username & password.
    /// </summary>
    //[Serializable]
    //public class LoginUserData
    //{
    //    public string username;
    //    public string password;
    //}

    [Serializable]
    public class DjangoUser
    {
        public string token;
        public User user = new User();
    }

    [Serializable]
    public struct User
    {
        public int pk;
        public string username;
        public string email;
        public string first_name;
        public string last_name;
    }

    [Serializable]
    public struct Phase
    {
        public string url;
        public string name;
        public string theme;
        public string info;
    }

    [Serializable]
    public class Quest
    {
        public string url;
        public int order;
        public int id;
        public string quest_title;
        public bool has_image;
        public string quest_image;
        public string quote_text;
        public string type;
        public string quest_cover_image;
        public DateTime creation_date;
        public string duration;
        public string is_from_phase;
        public string organization;
    }

    [Serializable]
    public class Quest_Page
    {
        public int order;
        public int id;
        public string quest_title;
        public bool has_image;
        public bool has_video;
        public string title_text;
        public string description_text;
        public string question_text;
        public string image;
        public bool is_info_page;
    }


    [Serializable]
    public class Quest_Status
    {
        public string url;
        public string belongs_to_ninja;
        public string belongs_to_quest;
        public int quest_page_active;
        public DateTime started_on_date;
        //public string completed_on_date;
        public bool completed;
        public List<string> has_drafts = new List<string>();
    }

    [Serializable]
    public class Ninja
    {
        public string url;
        public int pk;
        public string user;
        public string ninja_name;
        public string avatar;
        public string gender;
        public int level;
        public string domain;
        public string power;
        public string profilePrivacy;
        public bool first_time_connected;
        public List<string> has_coaches = new List<string>();

    }

    [Serializable]
    public struct Organization
    {
        public string name;
        public string icon;
        public string description;
        public string website;
    }


    [Serializable]
    public class Post_Log
    {
        //public int id;
        public int id;
        public string url;
        public string postTitle;
        public string postContent1;
        public bool has_image1;
        public string story_cover_image;
        public string image1;
        public string belongs_to_ninja;
        public string belongs_to_quest;
        public string timestamp;
        public string privacy;
    }

    [Serializable]
    public class Draft
    {
        public string url;
        public bool has_image;
        public string image;
        public string description;
        public string belongs_to_ninja;
        public string belongs_to_quest;
    }

    public class SurveyQuestion
    {
        public string url;
        public bool end_block;
        public int order_in_survey;
        public string question_title;
        public string question_text;
        public List<string> question_answers = new List<string>();
    }

    public class SurveyAnswers
    {
        public string belongs_to_ninja;
        public List<int> answers = new List<int>();
    }

    [Serializable]
    public class Coach
    {
        public string url;
        public string coach_name;
        public string coach_avatar;
        public string youtubeurl;
        public string about_me;
        public string gender;
        public string is_in_organization;
        public string[] has_ninjas;
    }


    [Serializable]
    public struct Action
    {
        public string url;
        public string verb;
        public int actor_object_id;
        public int target_object_id;
        public DateTime timestamp;
        public string description;
        public string action_type;
    }

    [Serializable]
    public struct ChatHistory
    {
        public string url;
        public string channel;
        public string sender;
        public string content;
        public DateTime timestamp;
    }

    [Serializable]
    public class ChatHistoryPage
    {
        public string count;
        public string next;
        public string previous;
        public List<Rad_Model.ChatHistory> results;
    }

    [Serializable]
    public class Phase_Status
    {
        public string url;
        public string belongs_to_ninja;
        public string belongs_to_phase;
        public string started_on_date;
        public bool completed;
    }

    [Serializable]
    public class ServerStatus
    {
        public string url;
        public string server_name;
        public string user_message;
        public bool maintenance;
    }

    [Serializable]
    public class Token
    {
        public string token;
    }
    //[Serializable]
    //public class MyCoach
    //{
    //    public string url;
    //    public string coach_avatar;
    //    public string coach_name;
    //    public string about_me;
    //    public string gender;
    //    public string is_in_organization;
    //    public string[] has_ninjas;
    //}
}
