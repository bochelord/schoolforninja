﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;
using Com.LuisPedroFonseca.ProCamera2D;

public class PathsManagerFase2 : PathsManager {

    public enum TeleportSpots { CaveEntrance, CaveInside,CaveInsideExit ,  CaveExit }

    [BoxGroup("Camera")] public Transform playerCavePosition;
    [BoxGroup("Camera")] public Transform playerCaveExitPosition;
    [BoxGroup("Camera")] public Transform playerCaveInsideExitPosition;
    [BoxGroup("Camera")] public Transform playerCaveEntrancePosition;

    [BoxGroup("Outside Camera Stats")] public float topBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float botBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float leftBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float rightBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float cameraSizeOutside;

    [BoxGroup("Cave Camera Stats")] public float topBoundaryCave;
    [BoxGroup("Cave Camera Stats")] public float botBoundaryCave;
    [BoxGroup("Cave Camera Stats")] public float leftBoundaryCave;
    [BoxGroup("Cave Camera Stats")] public float rightBoundaryCave;
    [BoxGroup("Cave Camera Stats")] public float cameraSizeCave;

    [BoxGroup("Colliders")] public GameObject colliderOutside1;
    [BoxGroup("Colliders")] public GameObject colliderOutside2;
    [BoxGroup("Colliders")] public GameObject colliderOutside3;
    [BoxGroup("Colliders")] public GameObject colliderOutside4;
    [BoxGroup("Colliders")] public GameObject colliderOutside5;
    [BoxGroup("Colliders")] public GameObject colliderOutside6;
    [BoxGroup("Colliders")] public GameObject colliderOutside7;
    [BoxGroup("Colliders")] public GameObject colliderOutside8;
    [BoxGroup("Colliders")] public GameObject colliderCave1;
    [BoxGroup("Colliders")] public GameObject colliderCave2;

    [BoxGroup("Sand Bridge")] public Animator sandBridge;
    [BoxGroup("Falling Rock")] public Animator fallingRock;
    [BoxGroup("Falling Tree")] public Animator fallingTree;
    [BoxGroup("Salamander")] public Animator salamander;
    [BoxGroup("RiverLeaf")] public Rad_MoveObject riverLeaf;
    [BoxGroup("Portal")] public SpriteRenderer portal;
    [BoxGroup("EndPhase")] public RadEndPhase endPhase;
    [BoxGroup("CaveLadder")] public GameObject caveLadder;
    [BoxGroup("CaveLadder")] public Transform caveLadderEndPosition;
    [BoxGroup("Fire")] public GameObject fire;

    private bool rockActive = true;
    private bool treeDown = false;
    private bool salamanderPunch = false;
    private bool sandBridgeActive = false;
    private bool treeActive = false;
    public override void Start()
    {
        base.Start();
    }

    public override void QuestCompleted(int id)
    {
        Debug.Log(id);
        switch (id)
        {
            case 1:
                if (!sandBridgeActive)
                {
                    SetOutsideCameraSettings();
                    player.transform.position = quest[0].transform.position;
                    EnableQuestSprite(quest[0].gameObject);
                    sandBridge.gameObject.SetActive(true);
                    sandBridge.SetTrigger("SandBridge");
                    colliderOutside1.SetActive(false);
                    colliderOutside2.SetActive(true);
                    pathScan.Scan();
                    sandBridgeActive = true;
                }
                
                break;
            case 2:
                SetOutsideCameraSettings();
                player.transform.position = quest[1].transform.position;
                EnableQuestSprite(quest[1].gameObject);
                if (rockActive)
                {
                    fallingRock.SetTrigger("Fall");
                    colliderOutside2.SetActive(false);
                    colliderOutside3.SetActive(true);
                    pathScan.Scan();
                    rockActive = false;
                }
                
                break;
            case 3:
                SetOutsideCameraSettings();
                player.transform.position = quest[2].transform.position;
                EnableQuestSprite(quest[2].gameObject);
                if (rockActive)
                {
                    fallingRock.SetTrigger("Fall");
                    colliderOutside2.SetActive(false);
                    colliderOutside3.SetActive(true);
                    pathScan.Scan();
                    rockActive = false;
                }
                break;
            case 4:
                SetCaveCameraSettings();
                player.transform.position = quest[3].transform.position;
                EnableQuestSprite(quest[3].gameObject);
                caveLadder.transform.position = caveLadderEndPosition.position;
                caveLadder.transform.rotation = caveLadderEndPosition.rotation;
                colliderCave1.SetActive(false);
                colliderCave2.SetActive(true);
                pathScan.Scan();
                break;
            case 5:
                SetCaveCameraSettings();
                player.transform.position = quest[4].transform.position;
                EnableQuestSprite(quest[4].gameObject);
                caveLadder.transform.position = caveLadderEndPosition.position;
                caveLadder.transform.rotation = caveLadderEndPosition.rotation;
                colliderCave1.SetActive(false);
                colliderCave2.SetActive(true);
                pathScan.Scan();
                break;
            case 6:
                if (!treeActive)
                {
                    SetOutsideCameraSettings();
                    player.transform.position = quest[5].transform.position;
                    EnableQuestSprite(quest[5].gameObject);

                    fallingTree.SetTrigger("Fall");
                    colliderOutside3.SetActive(false);
                    colliderOutside4.SetActive(false);
                    colliderOutside5.SetActive(true);
                    treeDown = true;
                    if (salamanderPunch)
                    {
                        colliderOutside6.SetActive(false);
                    }
                    pathScan.Scan();

                    treeActive = true;
                }
                
                break;
            case 7:
                SetOutsideCameraSettings();
                player.transform.position = quest[6].transform.position;
                EnableQuestSprite(quest[6].gameObject);

                salamander.SetTrigger("Punch");
                riverLeaf.MoveTree();
                salamanderPunch = true;
                if (!treeDown)
                {
                    colliderOutside3.SetActive(false);
                    colliderOutside4.SetActive(false);
                    colliderOutside6.SetActive(true);
                    salamanderPunch = true;
                }
                pathScan.Scan();
                break;
            case 8:
                SetOutsideCameraSettings();
                player.transform.position = quest[7].transform.position;
                EnableQuestSprite(quest[7].gameObject);
                fire.SetActive(false);
                colliderOutside5.SetActive(false);
                if (salamanderPunch)
                {
                    colliderOutside6.SetActive(false);
                    colliderOutside7.SetActive(true);
                }
                if (treeDown)
                {
                    colliderOutside5.SetActive(false);
                    colliderOutside8.SetActive(true);
                }

                if(treeDown && salamanderPunch)
                {
                    colliderOutside7.SetActive(false);
                }
                FadeObject(portal, 1, 1);
                pathScan.Scan();
                endPhase.EnableEndPhase = true;
                break;
            case 9:
                EnableQuestSprite(quest[8].gameObject);
                break;
        }
    }

    /// <summary>
    /// you need an explanation? come on
    /// </summary>
    /// <param name="_renderer"></param>
    /// <param name="_time"></param>
    /// <param name="_value"></param>
    private void FadeObject(SpriteRenderer _renderer, float _time, float _value)
    {
        _renderer.DOFade(1, 1);
    }

    public void TeleportPlayerInsideCaveStart()
    {
        player.transform.position = playerCavePosition.position;
        player.GetComponent<Pathfinding.AIDestinationSetter>().target.position = player.transform.position;
        SetCaveCameraSettings();
    }

    public void TeleportPlayerInsideCaveExit()
    {
        player.transform.position = playerCaveInsideExitPosition.position;
        player.GetComponent<Pathfinding.AIDestinationSetter>().target.position = player.transform.position;
        SetCaveCameraSettings();
    }

    public void TeleportPlayerToCaveExit()
    {
        colliderOutside3.SetActive(false);
        colliderOutside5.SetActive(true);
        if (salamanderPunch)
        {
            colliderOutside6.SetActive(false);
            colliderOutside7.SetActive(true);
        }
        if (treeDown)
        {
            colliderOutside5.SetActive(false);
            colliderOutside8.SetActive(true);
        }

        if (treeDown && salamanderPunch)
        {
            colliderOutside7.SetActive(false);
        }
        pathScan.Scan();
        player.transform.position = playerCaveExitPosition.position;
        player.GetComponent<Pathfinding.AIDestinationSetter>().target.position = player.transform.position;
        SetOutsideCameraSettings();
    }

    public void TeleportPlayerToCaveEntrance()
    {
        colliderOutside8.SetActive(false);
        colliderOutside7.SetActive(false);
        colliderOutside6.SetActive(false);
        colliderOutside5.SetActive(false);
        colliderOutside4.SetActive(false);
        colliderOutside3.SetActive(true);
        pathScan.Scan();
        player.transform.position = playerCaveEntrancePosition.position;
        player.GetComponent<Pathfinding.AIDestinationSetter>().target.position = player.transform.position;
        SetOutsideCameraSettings();
    }

    private void SetCaveCameraSettings()
    {
        Camera.main.orthographicSize = cameraSizeCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().TopBoundary = topBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().RightBoundary = rightBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().LeftBoundary = leftBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().BottomBoundary = botBoundaryCave;
    }

    private void SetOutsideCameraSettings()
    {
        Camera.main.orthographicSize = cameraSizeOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().TopBoundary = topBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().RightBoundary = rightBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().LeftBoundary = leftBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().BottomBoundary = botBoundaryOutside;
    }
}
