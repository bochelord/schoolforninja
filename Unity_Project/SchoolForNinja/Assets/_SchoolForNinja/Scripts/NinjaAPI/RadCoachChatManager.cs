﻿using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.Chat;
using NaughtyAttributes;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine;





public class RadCoachChatManager : MonoBehaviour, IChatClientListener
{


    public List<string> ChannelsToJoinOnConnect;
    public ChatClient chatClient;
    public int HistoryPhotonChatLengthToFetch;
    public int HistoryNinjapiChatLengthToFetch;
    public RectTransform ChatPanel;

    private BackendManager backendManager;
    private bool _appIDpresent;
    private bool _connected;
    private string usernameChat;
    private string selectedChannelName;

    private Dictionary<string, NinjaChatItem> ninjasForPrivate = new Dictionary<string, NinjaChatItem>();

    private List<GameObject> chatBoxesList = new List<GameObject>();


    public GameObject chatBoxPrefab;
    public GameObject chatboxesContainer;

    public GameObject onlineIconPrefab;
    public TMPro.TMP_InputField InputFieldChat;

    [BoxGroup("Status Links")] public TMPro.TextMeshProUGUI chatStatusText;
    [BoxGroup("Status Links")] public Button connectButton;


    [BoxGroup("OpenChat Links")] public RectTransform openChatRect;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI openChannelName;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI openChannelInfo;
    [BoxGroup("OpenChat Links")] public Image channelImage;
    [BoxGroup("OpenChat Links")] public GameObject openChannelOutputParent;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI selectedChannelText;

    private List<string> ninjaChannels = new List<string>();
    private int _numberOfPrivateChannels = 0;
    IEnumerator Start()
    {


        yield return new WaitForSeconds(0.5f);

        DontDestroyOnLoad(gameObject);

        backendManager = FindObjectOfType<BackendManager>();

        CheckChannelsToSubscribeandNinjastoAdd();

        BackendManager.Instance.GetHistorySuccess += OnGetHistorySuccess;
        BackendManager.Instance.GetHistoryFailure += OnGetHistoryFailure;
    }

    void Update()
    {
        if (this.chatClient != null)
        {
            this.chatClient.Service(); // make sure to call this regularly! it limits effort internally, so calling often is ok!
        }

        if (!_connected)
        {
            chatStatusText.text = "Not connected";
            connectButton.gameObject.SetActive(true);
        } else
        {
            connectButton.gameObject.SetActive(false);
            chatStatusText.text = "Connected as " + usernameChat;
        }



    }

    public void InitializeChat()
    {
        if (ChatSettings.Instance.AppId == "" | ChatSettings.Instance.AppId == null)
        {
            _appIDpresent = false;
        } else
        {
            _appIDpresent = true;
        }


        if (!_appIDpresent)
        {
            Debug.LogError("You need to set the chat app ID in the PhotonServerSettings file in order to continue.");
            //return;
        } else
        {
            //Connect to the Photon Chat Servers
            this.Connect();



        }
    }


    private void CheckChannelsToSubscribeandNinjastoAdd()
    {

        for (int i = 0; i < backendManager.radModel.userCoachNinjas.Count; i++)
        {
            int ninjaID = backendManager.radModel.userCoachNinjas[i].pk;
            string ninjaChannel = backendManager.radModel.ninja.pk.ToString() + ninjaID.ToString();
            ninjaChannels.Add(ninjaChannel);
            ChannelsToJoinOnConnect.Add(ninjaChannel);
        }
        //We add now all the Quest channels

        string[] questsNamesArray = new string[9] { "Jouw Ninja Naam", "Hoe gaat het?", "Ninja Nu", "Nulmeting", "In jouw ogen", "De teken-check" , "Vertragen" , "Het spelende kind" , "Stoppen of doorrijden?" };


        for (int i = 0; i < 9; i++)
        {
            ChannelsToJoinOnConnect.Add("Quest " + questsNamesArray[i]);
        }


        //And finally we add the main global channel

        ChannelsToJoinOnConnect.Add("Groepschat met alle Ninja en Sensei");

        InitializeChat();

    }

    /// <summary>
    /// Connects to the PhotonChatService
    /// </summary>
    public void Connect()
    {
        usernameChat = backendManager.radModel.userCoach.coach_name;//TODO RADICAL we need to figure out how to make this unique since Photon servers need a unique username. Maybe with a filter not allowing people to use the same ninja name...

        this.chatClient = new ChatClient(this);
        this.chatClient.ChatRegion = "EU";
        this.chatClient.Connect(ChatSettings.Instance.AppId, "0.9", new ExitGames.Client.Photon.Chat.AuthenticationValues(usernameChat));

        Debug.Log("Connection created with: AppId:" + ChatSettings.Instance.AppId + ", AppVersion: 0.9, UserName:" + usernameChat);
    }


    /// <summary>
    /// Button for when you're not connected to the chat. Sometimes we lose chat connection...
    /// It should drop you to the overview if you're on a chat already...
    /// </summary>
    public void Button_Connect()
    {
        if (openChatRect.gameObject.activeInHierarchy)
        {
            Button_BackToChatOverview();
        }

        Connect();
    }




    public void Button_BackToChatOverview()
    {
        MovePanel_Out_Right(openChatRect);
        //UpdateBubblesChat();
    }

    private void CreateChatBoxSeparator()
    {
        GameObject chatbox = Instantiate(chatBoxPrefab, chatboxesContainer.transform);
        chatbox.GetComponent<RectTransform>().position = Vector3.zero;
        chatbox.transform.localScale = Vector3.one;
        UIChatPanelController chatBoxPanelCtr = chatbox.GetComponent<UIChatPanelController>();

        chatBoxPanelCtr.chatIcon.gameObject.SetActive(false);
        chatBoxPanelCtr.chatIcon.transform.parent.gameObject.SetActive(false);
        Destroy(chatBoxPanelCtr.chatIcon.transform.parent.gameObject.GetComponent<Button>());
        chatBoxPanelCtr.chatLastMessage.text = "";
        chatBoxPanelCtr.chatLastMessageTimeStamp.text = "";
        chatBoxPanelCtr.chatTitle.text = "";
        chatBoxPanelCtr.GetComponent<Image>().color = new Color32(6, 94, 52, 255);
        chatBoxesList.Add(chatbox);
        
    }

    private void CreateChatBoxforChannel(string channel, bool privatechannel)
    {
        GameObject chatbox = Instantiate(chatBoxPrefab, chatboxesContainer.transform);
        chatbox.GetComponent<RectTransform>().position = Vector3.zero;
        chatbox.transform.localScale = Vector3.one;
        UIChatPanelController chatBoxPanelCtr = chatbox.GetComponent<UIChatPanelController>();

        if (privatechannel)
        {

            chatBoxPanelCtr.chatTitle.text = backendManager.radModel.userCoach.coach_name + ":" + channel;

            chatbox.GetComponent<Button>().onClick.AddListener(() => { Button_OpenChat(backendManager.radModel.userCoach.coach_name + ":" + channel); });

        }
        else
        {
           // Debug.Log("CHANNEL:" + channel);
            bool questChannel = true;
            for (int i = 0; i < ninjaChannels.Count; i++)
            {
                if (channel.Contains(ninjaChannels[i]))
                {
                    questChannel = false;

                    string coachCount = backendManager.radModel.ninja.pk.ToString();
                    int ninjaId = int.Parse(channel.Substring(coachCount.Length));

                   // Debug.Log(">>>>>>>>>>>>>NINJA ID TO CHECK>>>>" + ninjaId);
                    for (int j = 0; j < backendManager.radModel.userCoachNinjas.Count; j++)
                    {
                       // Debug.Log(">>>>>>>>>>>>>NINJA ID TO CHECK IN LIST>>>>" + backendManager.radModel.userCoachNinjas[j].pk);
                        if (ninjaId == backendManager.radModel.userCoachNinjas[j].pk)
                        {
                            chatBoxPanelCtr.chatTitle.text = backendManager.radModel.userCoachNinjas[j].ninja_name;
                            chatBoxPanelCtr.chatIcon.sprite = backendManager.DecodeBase64Texture(backendManager.radModel.userCoachNinjas[j].avatar);

                            chatbox.GetComponent<Button>().onClick.AddListener(() => { Button_OpenChat(channel); });

                            break;
                        }
                    }

                    break;
                }
            }

            if (questChannel)
            {
                chatBoxPanelCtr.chatTitle.text = channel;
                if (channel.Contains("Groepschat met alle Ninja en Sensei"))
                {
                    chatBoxPanelCtr.chatIcon.sprite = backendManager.schoolForNinjaLogo;
                }
                else
                {
                    chatBoxPanelCtr.chatIcon.sprite = QuestManagerV2.Instance.defaultQuestCoverImage[0];
                }

                chatbox.GetComponent<Button>().onClick.AddListener(() => { Button_OpenChat(channel); });
            }


        }

        chatBoxesList.Add(chatbox);
    }

    
    public void ShowChannel(string channelName)
    {
        if (string.IsNullOrEmpty(channelName))
        {
            return;
        }


        bool privateChannel;

        if (channelName.Contains(":"))
        {
            string[] splitNames = channelName.Split(new char[] { ':' });

            if (!System.String.IsNullOrEmpty(splitNames[1]))
            {
                privateChannel = ninjasForPrivate.ContainsKey(splitNames[1]);
            } else
            {
                privateChannel = false;
            }
        }
        else
        {
            privateChannel = false;
        }
        

        

        

        //print("ShowChannel method with channel: " + channelName);

        if ( privateChannel )
        {
            //It's a private chat

            //channelName = backendManager.radModel.userCoach.coach_name + ":" + channelName;
            //print("Name reconverted to force PRIVATE CHAAAT! " + channelName);
        }
        else
        {
            //It's public channel
            ChatChannel channel = null;
            bool found = this.chatClient.TryGetChannel(channelName, out channel);
            if (!found)
            {
                Debug.Log("ShowChannel failed to find channel: " + channelName);
                return;
            }

        }

        int result;
        if (int.TryParse(channelName,out result))
        {

            Debug.Log("MIERDA TODO es private CHANELLLLLL");
            openChannelName.text = GetNameforChannelBasedonNinjaPJK(channelName);
        }
        else
        {
            openChannelName.text = channelName;
        }

        

        openChannelInfo.text = GetDescriptionForChannel(channelName);


        GetHistoryforChannel(channelName);



    }

    private void GetHistoryforChannel(string channelName)
    {
        backendManager.GetChatHistory(channelName);
        //TurnOnPanelHistory();
    }


    public void OnEnterSend()
    {
        if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        {
            SendChatMessage(this.InputFieldChat.text);
            this.InputFieldChat.text = "";
        }
    }

    public void OnClickSend()
    {
        if (this.InputFieldChat.text != null)

        {
            Debug.Log("OnClickSend: " + this.InputFieldChat.text);
            SendChatMessage(this.InputFieldChat.text);
            this.InputFieldChat.text = "";
        } else
        {
            Debug.Log("InputFieldChat is empty");
        }
    }


    public void Button_OpenChat(string channelName)
    {
        this.selectedChannelName = channelName;
        //we fill in the data for each channel on the fly
        ShowChannel(channelName);

        CreateBubblesChat(channelName);

        //We show the openchat panel
        openChatRect.gameObject.SetActive(true);
        MovePanel_In(openChatRect);
    }

    private string GetDescriptionForChannel(string channelName)
    {
        string desc = "";

        if (channelName.Equals("Groepschat met alle Ninja en Sensei"))
        {
            desc = "Groepschat met alle Ninja en Sensei";
        } else if(channelName.Contains("Quest"))
        {
            desc = channelName + " Chat";
        }
        else
        {
            string[] splitted = channelName.Split(new char[] { ':' });
            if (splitted.Length > 2)
            {
                desc = "Private chat with " + splitted[1];
            }

        }

        return desc;
    }

    private string GetNameforChannelBasedonNinjaPJK(string strName)
    {
        string ret = "";
        string coachCount = backendManager.radModel.ninja.pk.ToString();
        int ninjaId = int.Parse(strName.Substring(coachCount.Length));

        for (int j = 0; j < backendManager.radModel.userCoachNinjas.Count; j++)
        {
            // Debug.Log(">>>>>>>>>>>>>NINJA ID TO CHECK IN LIST>>>>" + backendManager.radModel.userCoachNinjas[j].pk);
            if (ninjaId == backendManager.radModel.userCoachNinjas[j].pk)
            {
                ret = backendManager.radModel.userCoachNinjas[j].ninja_name;
                break;
            }
        }

        return ret;
    }


    private void CreateBubblesChat(string channelName)
    {

        //this.selectedChannelText.text = "";

        ChatChannel channel = null;
        bool found = this.chatClient.TryGetChannel(channelName, out channel);

        if (!found)
        {
            Debug.Log("CreateBubblesChat failed to find channel: " + channelName);
            return;
        }


        //PrettifyBubblesChat(channel.ToStringMessages());

        PrettifyBubblesChatSingle(channel.ToStringMessages());

    }

    private void CreateBubblesChatforPrivate(string channelName, object message, string sender)
    {
        if (message != null)
        {
            PrettifyBubblesChatSingle(message.ToString());
        }



    }



    private void PrettifyBubblesChatSingle(string messages)
    {
        if (System.String.IsNullOrEmpty(messages))
        {
            return;
        }

        if (messages.Contains("\n"))
        {
            string[] splitted = messages.Split(new char[] { '\n' });

            for (int i = 0; i < splitted.Length - 1; i++)//We left out the last element since we get an empty line..
            {
                print("linea: " + splitted[i]);

                splitted[i] = RecolorRealignChatText(splitted[i]);


                if (i == splitted.Length - 2)
                {
                    this.selectedChannelText.text += '\n' + splitted[i];
                }


            }
        } else
        {
            messages = RecolorRealignChatText(messages);

            this.selectedChannelText.text += '\n' + messages;
        }

    }

    private void PrettifyBubblesChat(string messages, bool doRecolor)
    {

        //print("Mensajes " + messages);
        this.selectedChannelText.text = "";

        if (System.String.IsNullOrEmpty(messages))
        {
            return;
        }

        if (messages.Contains("\n"))
        {
            string[] splitted = messages.Split(new char[] { '\n' });

            for (int i = 0; i < splitted.Length - 1; i++)//We left out the last element since we get an empty line..
            {
                print("linea: " + splitted[i]);

                if (doRecolor)
                {
                    splitted[i] = RecolorRealignChatText(splitted[i]);
                }



                this.selectedChannelText.text += '\n' + splitted[i];
            }
        } else
        {
            messages = RecolorRealignChatText(messages);

            this.selectedChannelText.text += '\n' + messages;
        }


    }

    private string RecolorRealignChatText(string message)
    {
        string[] recolorsplit = message.Split(new char[] { ':' });

        recolorsplit[0] = "<color=#EF6866><size=75%>" + recolorsplit[0] + "</size></color>";

        if (recolorsplit.Length > 1)
        {
            if (recolorsplit[0].Contains(backendManager.radModel.userCoach.coach_name))
            {
                return "<align=\"right\">" + recolorsplit[1] + "</align>";
            } else
            {
                if (recolorsplit.Length > 1)
                {
                    return recolorsplit[0] + recolorsplit[1];
                } else
                {
                    return recolorsplit[0];
                }

            }
        } else
        {
            return recolorsplit[0];
        }



    }

    private void SendChatMessage(string inputLine)
    {
        if (string.IsNullOrEmpty(inputLine))
        {
            Debug.Log("inputline is empty");
            return;
        }

        this.chatClient.PublishMessage(this.selectedChannelName, inputLine);
        //History on Ninjapi
        Rad_Model.ChatHistory histo = new Rad_Model.ChatHistory();

        print("URL sender: " + backendManager.radModel.ninja.url);
        histo.sender = backendManager.radModel.ninja.url;
        histo.channel = this.selectedChannelName;
        histo.content = backendManager.radModel.userCoach.coach_name + ":" + inputLine;

        var jsonToSend = JsonUtility.ToJson(histo);

        //print(jsonToSend);

        backendManager.CreateChatHistory(jsonToSend);
    }



    #region Photon Chat Interface members

    /// <summary>
    /// New status of another user (you get updates for users set in your friends list).
    /// </summary>
    /// <param name="user">Name of the user.</param>
    /// <param name="status">New status of that user.</param>
    /// <param name="gotMessage">True if the status contains a message you should cache locally. False: This status update does not include a
    /// message (keep any you have).</param>
    /// <param name="message">Message that user set.</param>
    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {

        Debug.LogWarning("status: " + string.Format("{0} is {1}. Msg:{2}", user, status, message));

        foreach(var ninja in ninjasForPrivate)
        {
            if (ninja.Key.Equals(user))
            {
                print("Status Update from one of your ninjas:" + user);

                string _status = "Unknown";

                switch (status)
                {
                    case 1:
                        _status = "Invisible";
                        break;
                    case 2:
                        _status = "Online";
                        break;
                    case 3:
                        _status = "Away";
                        break;
                    case 4:
                        _status = "Do not disturb";
                        break;
                    case 5:
                        _status = "Looking For Game/Group";
                        break;
                    case 6:
                        _status = "Playing";
                        break;
                    default:
                        _status = "Offline";
                        break;
                }

                if (_status.Equals("Offline") || _status.Equals("Invisible"))
                {
                    ninja.Value.statusIcon.color = new Color32(199, 197, 197, 255);//Gray color
                } else if (_status.Equals("Away") || _status.Equals("Do not disturb"))
                {
                    ninja.Value.statusIcon.color = new Color32(247, 135, 133, 255);// Red Color
                } else if (_status.Equals("Online"))
                {
                    ninja.Value.statusIcon.color = new Color32(134, 255, 11, 255);//Green Color
                } else
                {
                    ninja.Value.statusIcon.color = new Color32(0, 0, 0, 255);//Black Color
                }



            }
        }


       


    }

    public void OnUnsubscribed(string[] channels)
    {
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        int s;
        for (int i = 0; i < channels.Length; i++)
        {
            this.chatClient.PublishMessage(channels[i], "<color=#379A7F>joined</color>");
            this.CreateChatBoxforChannel(channels[i], false);

            if (int.TryParse(channels[i], out s))
            {
                _numberOfPrivateChannels++;
            }

            if (_numberOfPrivateChannels == backendManager.radModel.userCoachNinjas.Count)
            {
                this.CreateChatBoxSeparator();
                _numberOfPrivateChannels++;
            }
        }

        Debug.Log("OnSubscribed: " + string.Join(", ", channels));

        ShowChannel(channels[0]);
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        print("Privatemessage received from channel: " + channelName + " from " + sender + " message " + message);
        GameObject currentChatBox = null;

        foreach (var ninja in ninjasForPrivate)
        {
            if (ninja.Key.Equals(sender))
            {
                //we know it's a private message from this channel -> coach:sender
                for (int i = 0; i < chatBoxesList.Count; i++)
                {
                    if (chatBoxesList[i].GetComponent<UIChatPanelController>().chatTitle.text.Contains(sender))
                    {
                        currentChatBox = chatBoxesList[i];
                    }
                }

                currentChatBox.GetComponent<UIChatPanelController>().chatLastMessage.text = sender + ":" + message.ToString();

                currentChatBox.GetComponent<UIChatPanelController>().chatLastMessageTimeStamp.text = System.DateTime.Now.ToShortTimeString();
            }
        }


        CreateBubblesChatforPrivate(channelName, message, sender);

        //ShowChannel(channelName);

    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        GameObject currentChatBox = null;

        string _parseChannel = " ";
        string coachCount = backendManager.radModel.ninja.pk.ToString();
        int ninjaId;
        if (int.TryParse(channelName.Substring(coachCount.Length),out ninjaId))
        {
            ninjaId = int.Parse(channelName.Substring(coachCount.Length));
            for (int j = 0; j < backendManager.radModel.userCoachNinjas.Count; j++)
            {
                if (ninjaId == backendManager.radModel.userCoachNinjas[j].pk)
                {
                    _parseChannel = backendManager.radModel.userCoachNinjas[j].ninja_name;
                    break;
                }
            }
        }

        for (int i = 0; i < messages.Length; i++)
        {
            for (int j = 0; j < chatBoxesList.Count; j++)
            {
                if (chatBoxesList[j].GetComponent<UIChatPanelController>().chatTitle.text == channelName ||
                    chatBoxesList[j].GetComponent<UIChatPanelController>().chatTitle.text == _parseChannel)
                {
                    Debug.Log("chat boxes name: " + chatBoxesList[j].GetComponent<UIChatPanelController>().chatTitle.text);
                    currentChatBox = chatBoxesList[j];

                }
            }
            currentChatBox.GetComponent<UIChatPanelController>().chatLastMessage.text = senders[0] + ":" + messages[i].ToString();
            currentChatBox.GetComponent<UIChatPanelController>().chatLastMessageTimeStamp.text = System.DateTime.Now.ToShortTimeString();

            if (selectedChannelName == channelName)
            {
                CreateBubblesChat(channelName);
            }
        }
    }


    public void OnChatStateChange(ChatState state)
    {
    }

    public void OnConnected()
    {


        _connected = true;

        if (this.ChannelsToJoinOnConnect != null && this.ChannelsToJoinOnConnect.Count > 0)
        {
            //this.chatClient.Subscribe(this.ChannelsToJoinOnConnect.ToArray(), this.HistoryPhotonChatLengthToFetch);
            this.chatClient.Subscribe(this.ChannelsToJoinOnConnect.ToArray());
        }

        Debug.Log("Connected as:" + usernameChat);

        this.ChatPanel.gameObject.SetActive(true);


        if (ninjasForPrivate!=null && ninjasForPrivate.Count > 0)
        {
            List<string> listninjas = new List<string>();

            foreach (var ninja in ninjasForPrivate)
            {
                listninjas.Add(ninja.Value.ninja_name);

                string[] channelnameCombined = new string[1];

                channelnameCombined[0] = backendManager.radModel.userCoach.coach_name + ":" + ninja.Value.ninja_name;

                this.chatClient.Subscribe(channelnameCombined,0);
            }
            this.chatClient.AddFriends(listninjas.ToArray());

        }

        this.chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).
    }

    public void OnDisconnected()
    {
        _connected = false;
        Debug.Log("Disconnected");

        for (int i = 0; i < chatBoxesList.Count; i++)
        {
            chatBoxesList[i].gameObject.SetActive(false);
        }
    }

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
    {
        if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
        {
            UnityEngine.Debug.LogError(message);
        } else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
        {
            UnityEngine.Debug.LogWarning(message);
        } else
        {
            UnityEngine.Debug.Log(message);
        }
    }

    #endregion


    #region event callbacks
    void OnGetHistorySuccess(string channelName)
    {
        print("GetHistorysuccess for channel " + channelName);
        string history = null;

        for (int i = 0; i < backendManager.radModel.chatHistoryList.Count; i++)
        {
            if (backendManager.radModel.chatHistoryList[i].channel.Equals(channelName))
            {
                history += backendManager.radModel.chatHistoryList[i].content + "\n";
            }
        }
        PrettifyBubblesChat(history, true);
    }

    void OnGetHistoryFailure(string response)
    {
        Debug.LogError(response);
    }


    #endregion




    #region dotween UI animations
    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(0, -174f), 0.25f); //To center position
    }
    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(1080, -174f), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-1300, 0), 0.25f); //To left position
    }

    #endregion
}
