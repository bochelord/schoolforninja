﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using NaughtyAttributes;
using DG.Tweening;

public class IntroSurveyManager : MonoBehaviour {

    [BoxGroup("Pagination")] public Slider[] paginationSlider;
    [BoxGroup("Pagination")] public Image[] circlePagination;
    [BoxGroup("Pagination")] public GameObject[] circleChecks;
    [BoxGroup("Pagination")] public Color circleFillColor;

    [BoxGroup("Intro Panel")] public RectTransform[] introPanelArray;
    [BoxGroup("Intro Panel")] public RectTransform[] introPanelArrayFinalSurvey;
    [BoxGroup("Intro Panel")] public Slider introPaginationSlider;
    [BoxGroup("Intro Panel")] public Image introPaginationCircle;
    private int introPanelArraySize;
    private int introPanelArrayCurrentPanel;

    [BoxGroup("Question Panel")]public RectTransform questionPanelOne;
    [BoxGroup("Question Panel")]public RectTransform questionPanelTwo;
    [BoxGroup("Question Panel")]public Sprite answerPressed;
    [BoxGroup("Question Panel")]public Sprite answerNull;

    [BoxGroup("Sensei Panel")] public RectTransform senseiPanel;
    [BoxGroup("Sensei Panel")] public RectTransform senseiVideoPanel;
    [BoxGroup("Sensei Panel")] public Image senseiAvatar;
    [BoxGroup("Sensei Panel")] public TMPro.TextMeshProUGUI senseiName;
    [BoxGroup("Sensei Panel")] public GameObject videoRenderTexture;
    [BoxGroup("Sensei Panel")] public YoutubePlayer youtubePlayer;

    [BoxGroup("Ninja Avatar Panel")] public RectTransform ninjaAvatarPanel;
    [BoxGroup("Ninja Avatar Panel")] public RectTransform ninjaAvatarMask;
    [BoxGroup("Ninja Avatar Panel")] public float firstNinjaAvatarHeight;
    [BoxGroup("Ninja Avatar Panel")] public float secondNinjaAvatarHeight;
    [BoxGroup("Ninja Avatar Panel")] public float thirdNinjaAvatarHeight;
    [BoxGroup("Ninja Avatar Panel")] public GameObject starsAnimationObject;
    [BoxGroup("Ninja Avatar Panel")] public GameObject firstQuoteText;
    [BoxGroup("Ninja Avatar Panel")] public GameObject firstQuoteAuthor;
    [BoxGroup("Ninja Avatar Panel")] public GameObject secondQuoteText;
    [BoxGroup("Ninja Avatar Panel")] public GameObject secondQuoteAuthor;
    [BoxGroup("Ninja Avatar Panel")] public GameObject ninjaCongratsText;

    [BoxGroup("FinalSurvey Congrats")] public RectTransform ninjaFinalCongratsPanel;
    [BoxGroup("FinalSurvey Congrats")] public string subscribeURL;
    private int ninjaAvatarDisplayedTimes = 0;

    [BoxGroup("No Internet Panek")] public GameObject noInternetPanel;

    [BoxGroup("Loading Panel")] public GameObject loadingPanel;

    [BoxGroup("Next Scene")] public string nextSceneToLoad;

    private List<Rad_Model.SurveyQuestion> _survey = new List<Rad_Model.SurveyQuestion>();
    private List<int> _answers = new List<int>();
    private List<Rad_Model.Coach> _coaches = new List<Rad_Model.Coach>();
    private Rad_Model.Coach _currentCoach = new Rad_Model.Coach();
    private bool[] _questionAnswered;
    private int questionIndex = 0;
    private int currentAnswer = 0;
    private bool panelOneUsed = true;

    private int _firstblockEnd = 0;
    private int _secondBlockEnd = 0;
    private int _thirdBlockEnd = 0;
    private bool _answered = false;
    private bool _avatarDisplayedOnce;
    private bool _avatarDisplayedTwice;
    private bool _avatarDisplayedThrice;

    private int coachAvatarAssigned = 0;
    [HideInInspector]
    public bool videoSenseiOn = false;
    [HideInInspector]
    public bool finalSurvey = false;


    private void Start()
    {
        BackendManager.Instance.GetSurveyAnswers(GetSurveyQuestions, OnGetSurveyQuestionsFail);
        //Initial Intro Panel
        introPanelArrayCurrentPanel = 0;
    }

    public void GetSurveyQuestions()
    {
        BackendManager.Instance.SetLastAction(GetSurveyQuestions);
        BackendManager.Instance.GetSurveyQuestions(OnGetSurveyQuestionsSuccees,OnGetSurveyQuestionsFail);
    }

    private void OnGetSurveyQuestionsSuccees()
    {
        introPaginationSlider.transform.parent.gameObject.SetActive(true);
        if (!finalSurvey)
        {
            MovePanel_In(introPanelArray[introPanelArrayCurrentPanel]);
            introPanelArraySize = introPanelArray.Length;
        }
        else
        {
            MovePanel_In(introPanelArrayFinalSurvey[introPanelArrayCurrentPanel]);
            introPanelArraySize = introPanelArrayFinalSurvey.Length;

        }
    }

    private void OnGetSurveyQuestionsFail()
    {
        
    }

    public void ShowNinjaAvatar()
    {
        MovePanel_In(ninjaAvatarPanel);
        Image maskImage = ninjaAvatarMask.GetComponent<Image>();
        if (ninjaAvatarDisplayedTimes == 1)
        {
            circlePagination[0].color = circleFillColor;
            DOTween.To(() => maskImage.fillAmount, x => maskImage.fillAmount = x, 0.221f, 1f);
            circleChecks[0].SetActive(true);
        }
        else if (ninjaAvatarDisplayedTimes == 2)
        {
            circlePagination[1].color = circleFillColor;
            DOTween.To(() => maskImage.fillAmount, x => maskImage.fillAmount = x, 0.507f, 1f);
            circleChecks[1].SetActive(true);
            firstQuoteText.SetActive(false);
            firstQuoteAuthor.SetActive(false);
            secondQuoteAuthor.SetActive(true);
            secondQuoteText.SetActive(true);
        }
        else
        {
            circlePagination[2].color = circleFillColor;
            DOTween.To(() => maskImage.fillAmount, x => maskImage.fillAmount = x, 1f, 1f);
            circleChecks[2].SetActive(true);
            starsAnimationObject.SetActive(true);
            secondQuoteAuthor.SetActive(false);
            secondQuoteText.SetActive(false);
            ninjaCongratsText.SetActive(true);
        }
    }

    public void Button_NextIntroPanel()
    {
        RectTransform[] _panel;

        if (!finalSurvey)
        {
            _panel = introPanelArray;
        }
        else
        {
            _panel = introPanelArrayFinalSurvey;
        }
        MovePanel_Out_Left(_panel[introPanelArrayCurrentPanel]);

        if (introPanelArrayCurrentPanel < introPanelArraySize - 1)
        {
            introPanelArrayCurrentPanel++;
            MovePanel_In(_panel[introPanelArrayCurrentPanel]);
            introPaginationSlider.value = (float)introPanelArrayCurrentPanel / (float)(introPanelArraySize - 1);
            if(introPanelArrayCurrentPanel == introPanelArraySize - 1)
            {
                introPaginationCircle.color = circleFillColor;
            }
        }
        else
        {
            introPanelArrayCurrentPanel = 0; //Reset introPanelArrayCurrentPanel just in case a new ninja login the app or didn't complete the questionnaire.
            paginationSlider[0].transform.parent.gameObject.SetActive(true);
            introPaginationSlider.transform.parent.gameObject.SetActive(false);
            ShowSurveyQuestion();
        }
        
    }


    public void Button_PreviousIntroPanel()
    {

        RectTransform[] _panel;

        if (!finalSurvey)
        {
            _panel = introPanelArray;
        }
        else
        {
            _panel = introPanelArrayFinalSurvey;
        }
        MovePanel_Out_Right(_panel[introPanelArrayCurrentPanel]);

        if (introPanelArrayCurrentPanel > 0)
        {
            introPanelArrayCurrentPanel--;
            MovePanel_In(_panel[introPanelArrayCurrentPanel]);
            introPaginationSlider.value = (float)introPanelArrayCurrentPanel / (float)(introPanelArraySize - 1);
            if (introPanelArrayCurrentPanel == introPanelArraySize - 1)
            {
                introPaginationCircle.color = circleFillColor;
            }
        }
    }
    public void HideNinjaAvatarPanel()
    {
        MovePanel_Out_Right(ninjaAvatarPanel);
        ShowSurveyQuestion();
    }


    public void ShowSurveyQuestion()
    {
        if(questionIndex >= _survey.Count)
        {
            SendSurveyAnswers();
        }
        else
        {
            RectTransform _panel;
            if (panelOneUsed)
            {
                _panel = questionPanelOne;
                MovePanel_In(_panel);
                MovePanel_Out_Left(questionPanelTwo);
            }
            else
            {
                _panel = questionPanelTwo;
                MovePanel_In(_panel);
                MovePanel_Out_Left(questionPanelOne);
            }

            _panel.Find("QuestionText").GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _survey[questionIndex].question_text;
            if (!_answered)
            {
                _panel.Find("NextButton").GetComponent<Button>().interactable = false;
            }
            var answersParent = _panel.Find("Answers");
            for (int i = 0; i < answersParent.childCount; i++)
            {
                if (i < _survey[questionIndex].question_answers.Count)
                {
                    var _child = answersParent.GetChild(i);
                    _child.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _survey[questionIndex].question_answers[i];
                    _child.GetComponentInChildren<Image>().sprite = answerNull;
                    _child.gameObject.SetActive(true);

                }
                else
                {
                    answersParent.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        MovePanel_Out_Right(introPanelArray[introPanelArray.Length - 1]);
    }

    private void SendSurveyAnswers()
    {
        Rad_Model.SurveyAnswers _answersToSend = new Rad_Model.SurveyAnswers();
        for (int i = 0; i < _answers.Count; i++)
        {
            _answersToSend.answers.Add(_answers[i]);
        }
        _answersToSend.belongs_to_ninja = BackendManager.Instance.radModel.ninja.url;

        var jsonToSend = JsonUtility.ToJson(_answersToSend);
        BackendManager.Instance.SendSurveyAnswers(jsonToSend, OnSendAnswersSuccess, OnSendAnswersFail);
    }

    private void FakeAssignCoach()
    {
        MovePanel_Out_Right(questionPanelOne);
        MovePanel_Out_Right(questionPanelTwo);
        paginationSlider[0].transform.parent.gameObject.SetActive(false);
        loadingPanel.SetActive(true);
    }
    private void OnSendAnswersSuccess()
    {
        if (!finalSurvey)
        {
            FakeAssignCoach();
            BackendManager.Instance.GetCoaches();
        }
        else
        {
            //TODO COngrats panel and see you soon
            MovePanel_In(ninjaFinalCongratsPanel);
        }

    }

    public void Button_SubscribeButton()
    {
        Application.OpenURL(subscribeURL);
    }

    private void OnSendAnswersFail()
    {

    }


    public void BUTTON_Answers(int _value)
    {
        RectTransform _panel;
        if(panelOneUsed)
        {
            _panel = questionPanelOne;

        }
        else
        {
            _panel = questionPanelTwo;

        }

        if (!_answered)
        {
            _panel.Find("NextButton").GetComponent<Button>().interactable = true;
            _answered = true;
        }
        
        
        currentAnswer = _value;
        var answersParent = _panel.Find("Answers");
        for (int i = 0; i < answersParent.childCount; i++)
        {   
            if(i == _value)
            {
                answersParent.GetChild(i).GetComponent<Image>().sprite = answerPressed;
            }
            else
            {
                answersParent.GetChild(i).GetComponent<Image>().sprite = answerNull;
            }
        }
    }

    public void BUTTON_NextQuestion()
    {
        if(panelOneUsed)
        {
            panelOneUsed = false;
        }
        else
        {
            panelOneUsed = true;
        }

        if(questionIndex <= _questionAnswered.Length)
        {
            _questionAnswered[questionIndex] = true;
        }

        if(questionIndex < _answers.Count)
        {
            _answers[questionIndex] = currentAnswer;
        }
        else
        {
            _answers.Add(currentAnswer);
        }
        _answered = false;
        questionIndex++;

        UpdatePaginationSlider();

        if (((questionIndex == _firstblockEnd + 1 && !_avatarDisplayedOnce) || 
            (questionIndex == _secondBlockEnd + 1 && !_avatarDisplayedTwice) || 
            (questionIndex == _thirdBlockEnd + 1 && !_avatarDisplayedThrice)) && !finalSurvey)
        {
            if (!_avatarDisplayedOnce)
            {
                _avatarDisplayedOnce = true;
            }
            else if (!_avatarDisplayedTwice)
            {
                _avatarDisplayedTwice = true;
            }
            else if (!_avatarDisplayedThrice)
            {
                _avatarDisplayedThrice = true;
            }

            ninjaAvatarDisplayedTimes++;
            ShowNinjaAvatar();
        }
        else
        {
            ShowSurveyQuestion();
        }
        
    }

    public void UpdatePaginationSlider()
    {

        if(questionIndex < _firstblockEnd)
        {
            paginationSlider[0].value = (float)questionIndex / (float)_firstblockEnd;
        }
        else if (questionIndex < _secondBlockEnd)
        {
            paginationSlider[0].value = 1;
            paginationSlider[1].value = (float)questionIndex / (float)_secondBlockEnd;
        }
        else if (questionIndex < _thirdBlockEnd)
        {
            paginationSlider[1].value = 1;
            paginationSlider[2].value = (float)questionIndex / (float)_thirdBlockEnd;
        }
        else
        {
            paginationSlider[2].value = 1;
        }

    }
    public void ParseSurveyQuestions(JsonData[] data)
    {
        for (int i = 0; i < data.Length; i++)
        {
            Rad_Model.SurveyQuestion _question = new Rad_Model.SurveyQuestion();

            if (data[i].Keys.Contains("order_in_survey"))
            {
                _question.order_in_survey = int.Parse(data[i]["order_in_survey"].ToString());
            }
            if (data[i].Keys.Contains("question_title"))
            {
                _question.question_title = data[i]["order_in_survey"].ToString();
            }
            if (data[i].Keys.Contains("question_text"))
            {
                _question.question_text = data[i]["question_text"].ToString();
            }
        }
    }

    public void BUTTON_PreviousQuestionAnswered()
    {
        if(questionIndex > 0)
        {
            questionIndex--;

            UpdatePaginationSlider();
            ShowSurveyQuestion();

        }
    }

    public void BUTTON_NextQuestionAnswered()
    {
        if(questionIndex < _survey.Count - 1 && _questionAnswered[questionIndex+1])
        {
            questionIndex++;
            UpdatePaginationSlider();
            ShowSurveyQuestion();
        }
    }

    public void SetSurveyQuestionList(Rad_Model.SurveyQuestion[] _questions)
    {
        for (int i = 0; i < _questions.Length; i++)
        {
            _survey.Add(_questions[i]);
        }

        _survey.Sort((s1, s2) => s1.order_in_survey.CompareTo(s2.order_in_survey));

        _questionAnswered = new bool[_survey.Count];
        for (int i = 0; i < _survey.Count; i++)
        {
            if (_survey[i].end_block)
            {
                if(_firstblockEnd == 0)
                {
                    _firstblockEnd = i;
                }
                else if(_secondBlockEnd == 0)
                {
                    _secondBlockEnd = i;
                }
                else
                {
                    _thirdBlockEnd = i;
                    break;
                }
            }
        }
    }
    /// <summary>
    /// Triggered by UI button - Moves out the Sensei video panel and loads next scene.
    /// </summary>
    public void Button_MoveSenseiVideoPanel()
    {


        //MovePanel_Out_Right(senseiVideoPanel);
        LoadingSceneManager.LoadScene(nextSceneToLoad);
    }
    /// <summary>
    /// Triggered by UI button - Moves out the Assigned Sensei panel and moves in the Sensei Video panel
    /// </summary>
    public void BUTTON_MoveSenseiPanel()
    {
        if (!videoRenderTexture.activeInHierarchy)
        {
            videoRenderTexture.SetActive(true);
        }
        MovePanel_Out_Right(senseiPanel);
        MovePanel_In(senseiVideoPanel);
    }


    public void SetCoachList(Rad_Model.Coach[] _data)
    {
        Debug.Log("Set coach list");
        for (int i = 0; i < _data.Length; i++)
        {
            _coaches.Add(_data[i]);
            StartCoroutine(BackendManager.Instance.GetCoachAvatar(_data[i].coach_avatar, _data[i].url));
        }

    }
    public void AsignCoachesAvatars(string url, string base64Img)
    {
        for (int i = 0; i < _coaches.Count; i++)
        {
            if (_coaches[i].url == url)
            {
                _coaches[i].coach_avatar = base64Img;
                coachAvatarAssigned++;
                break;
            }
        }

        if (coachAvatarAssigned == _coaches.Count)
        {
            Rad_Model.Coach coachToSend = null;

            for (int i = 0; i < _coaches.Count; i++)
            {

                if (!_coaches[i].coach_name.Contains("Esra") && (coachToSend == null || coachToSend.has_ninjas.Length > _coaches[i].has_ninjas.Length))
                {
                    coachToSend = _coaches[i];
                }
            }

            _currentCoach = coachToSend;


            BackendManager.Instance.CreatePhaseStatus(0);
            if (coachToSend != null)
            {
                StartCoroutine(SetAndAsignCoach(coachToSend));
            }
            else
            {
                //WHO THE FUCK ARE YOU panel on
            }


        }
        
        
    }

    //TODO waiting for THE algorithm
    private IEnumerator SetAndAsignCoach(Rad_Model.Coach _coach)
    {
        Sprite _sprite = BackendManager.Instance.DecodeBase64Texture(_coach.coach_avatar);
        senseiAvatar.sprite = _sprite;
        senseiName.text = _coach.coach_name;
        youtubePlayer.youtubeUrl = _coach.youtubeurl;

        MovePanel_In(senseiPanel);


        BackendManager.Instance.radModel.ninja.has_coaches.Clear();
        BackendManager.Instance.radModel.ninja.has_coaches.Add( _coach.url);
        BackendManager.Instance.radModel.ninja.first_time_connected = false;
        //BackendManager.Instance.radModel.ninja.avatar = BackendManager.Instance.SpriteToBase64(_sprite);

        var ninjaToModify = JsonUtility.ToJson(BackendManager.Instance.radModel.ninja);

        BackendManager.Instance.ModifyNinja(ninjaToModify, ModifyCoachWithninja, OnNinjaModyfyFail);

        yield return new WaitForSeconds(3); //JUST TO MAKE THE FAKE NOT SO FAKE, WAITING FOR ALGORITHM
        loadingPanel.SetActive(false);
    }

    private void OnCoachModifySuccess()
    {
        MovePanel_Out_Right(questionPanelOne);
        MovePanel_Out_Right(questionPanelTwo);
        paginationSlider[0].transform.parent.gameObject.SetActive(false);
    }

    private void ModifyCoachWithninja()
    {
        List<string> _list = new List<string>();
        for (int i = 0; i < _currentCoach.has_ninjas.Length; i++)
        {
            _list.Add(_currentCoach.has_ninjas[i]);
        }
        _list.Add(BackendManager.Instance.radModel.ninja.url);
        _currentCoach.has_ninjas = _list.ToArray();
        var str = _currentCoach.url.Split('/');
        var pk = int.Parse(str[str.Length - 2]);
        var json = JsonUtility.ToJson(_currentCoach);

        BackendManager.Instance.ModifyCoach(json, pk, OnCoachModifySuccess, OnModifyCoachFail);
    }

    private void OnModifyCoachFail()
    {

    }

    private void OnNinjaModyfyFail()
    {

    }

    public List<Rad_Model.Coach> GetCoachesList()
    {
        return _coaches;
    }
    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, 0), 0.25f); //To left position
    }

    #endregion ===============================================================================================
}
