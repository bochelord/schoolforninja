﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class SFN_GuiManager : MonoBehaviour {

    [BoxGroup("QuestPanels")] public GameObject questPanelGodFather;
    [BoxGroup("QuestPanels")] public RectTransform questLandingPage;
    [BoxGroup("QuestPanels")] public RectTransform questInfoPage;
    [BoxGroup("QuestPanels")] public RectTransform questCompleted;
    [BoxGroup("QuestPanels")] public RectTransform[] questPanels;

    [BoxGroup("Start Quest")] public GameObject startQuestPanel;

    [BoxGroup("No Internet Panel")] public GameObject noInternetPanel;

    [BoxGroup("Quest Completed panel")] public GameObject firstQuestCompletedText;
    [BoxGroup("Quest Completed panel")] public GameObject questCompletedText;

    [BoxGroup("Log Page")] public RectTransform questLogPage;
    [BoxGroup("Log Page")] public TMP_InputField logTitleInputField;
    [BoxGroup("Log Page")] public Transform logContentScrollView;
    [BoxGroup("Log Page")] public TMP_Dropdown privacyDropDown;
    [BoxGroup("Log Page")] public GameObject createLogButton;
    [BoxGroup("Log Page")] public GameObject modifyLogButton;
    [BoxGroup("Log Page")] public TextMeshProUGUI logTitleQuest;
    [BoxGroup("Log Page")] public TMP_InputField[] logTexts;
    [BoxGroup("Log Page")] public Image[] logImages;
    [BoxGroup("Log Page")] public GameObject[] cameraButtons;
    [BoxGroup("Log Page")] public GameObject titleAlert;
    [BoxGroup("Log Page")] public GameObject logAlert;

    [BoxGroup("Information Page")] public TextMeshProUGUI infoTitleText;
    [BoxGroup("Information Page")] public TextMeshProUGUI infoDescriptionText;
    [BoxGroup("Information Page")] public Image infoImage;

    [BoxGroup("Image Zoom")] public RectTransform imageZoomPanel;
    [BoxGroup("Image Zoom")] public Image imageToZoom;

    [BoxGroup("Media Picker")] public GameObject[] mediaPickerButton;
    [BoxGroup("Media Picker")] public Image[] draftsImages;

    [BoxGroup("Finished phase screen")] public RectTransform finishedScreen;

    [BoxGroup("Loading Page")] public GameObject loadingPage;

    [BoxGroup("Support Button")] public GameObject supportButton;

    [Required]
    [BoxGroup("Pagination Component")] public Rad_UIPagination questPagination; 

    [BoxGroup("Managers")][Required][SerializeField]private PlayerMovementController _playerController;
    [BoxGroup("Managers")] [Required] [SerializeField] private PathsManager _pathManager;
    [BoxGroup("Managers")] public RadProfileUIManager _radProfileUIManager;

    private int currentQuestPanelPage = 0;
    private int maxQuestPanelPages = 0;
    private bool infoPanelActive = false; // we need to know when info page is active,since the pagination close is the same for all of them
    private bool addDraftsToLog = true;
    private int setPanelDataIndex = 0;
    private bool _goToSurvey = false;
    [HideInInspector] public bool videoOn = false;
    private void Start()
    {
        _pathManager = FindObjectOfType<PathsManager>();

    }

    #region MediaPicker
    public void Button_RemoveImage(int _image)
    {
        draftsImages[_image].sprite = null;
        mediaPickerButton[_image].SetActive(true);
        draftsImages[_image].gameObject.SetActive(false);
    }

    public void BUTTON_MediaPicker(int _image)
    {
        NativeGallery.GetImageFromGallery((path) =>
        {
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, -1, false);
                if (texture == null)
                {
                    return;
                }

                Sprite spriteTemp = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                draftsImages[_image].sprite = spriteTemp;
                draftsImages[_image].gameObject.SetActive(true);
                draftsImages[_image].preserveAspect = true;
                mediaPickerButton[_image].SetActive(false);
                
            }
        }, "Select a image", "image/jpeg",512);
    }

    public void Button_RemoveImageLog(int _image)
    {
        logImages[_image].gameObject.SetActive(false);
        cameraButtons[_image].SetActive(true);
    }

    public void BUTTON_LogMediaPicker(int _image)
    {
        NativeGallery.GetImageFromGallery((path) =>
        {
            if (path != null)
            {
                Texture2D texture = NativeGallery.LoadImageAtPath(path, -1, false);
                if (texture == null)return;

                Sprite spriteTemp = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                logImages[_image].sprite = spriteTemp;
                logImages[_image].gameObject.SetActive(true);
                logImages[_image].preserveAspect = true;
                cameraButtons[_image].SetActive(false);
            }
        }, "Select an image", "image/jpeg", 512);
    }
    #endregion


    public void BUTTON_SetCurrentQuest(int id)
    {
        supportButton.SetActive(false);
        loadingPage.SetActive(true);
        QuestManagerV2.Instance.currentQuestId = id;
        QuestManagerV2.Instance.SetCurrentQuest();
        _playerController.canMove = false;
    }

    public void ResetInputFields()
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            var content = questPanels[i].transform.Find("DraftScrollView").GetComponent<ScrollRect>().content.transform;
            content.Find("InputField").GetComponent<TMP_InputField>().text = "";
            if (draftsImages[i])
            {
                draftsImages[i].sprite = null;
                draftsImages[i].gameObject.SetActive(false);
                
            }
            if (mediaPickerButton[i])
            {
                mediaPickerButton[i].gameObject.SetActive(true);
            }
        }
    }
    public void ResetPanelDataIndex()
    {
        setPanelDataIndex = 0;
    }

    /// <summary>
    /// set all the information required to the Quest Panel and turn it on
    /// </summary>
    /// <param name="_panelTitle"></param>
    /// <param name="_panelDescription"></param>
    /// <param name="panel">panel number</param>
    public void SetQuestPanelDataText(string _panelDescription, string _panelQuestion)
    {
        var _desc = questPanels[setPanelDataIndex].transform.Find("panelDescription").GetComponent<TextMeshProUGUI>();
        _desc.text = _panelDescription;
        _desc.gameObject.SetActive(true);
        questPanels[setPanelDataIndex].transform.Find("panelQuestion").GetComponent<TextMeshProUGUI>().text = _panelQuestion;
        questPanels[setPanelDataIndex].transform.Find("questImage").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.Find("imageDescription").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.GetChild(questPanels[setPanelDataIndex].transform.childCount - 1).gameObject.SetActive(false);
        setPanelDataIndex++;
        
    }

    public void Button_SendLogDraft(TMPro.TMP_InputField _inputfield)
    {
        QuestManagerV2.Instance.SendLogDraft(_inputfield);
    }

    /// <summary>
    /// set quest data for sprites
    /// </summary>
    /// <param name="panel"></param>
    /// <param name="_panelTitle"></param>
    /// <param name="_panelSPrite"></param>
    public void SetQuestPanelDataSprite(string _panelQuestion, Sprite _panelSPrite, string _panelDescription)
    {
        questPanels[setPanelDataIndex].transform.Find("panelQuestion").GetComponent<TextMeshProUGUI>().text = _panelQuestion;
        var _image = questPanels[setPanelDataIndex].transform.Find("questImage").GetComponent<Image>();
        _image.sprite =_panelSPrite;
        _image.preserveAspect = true;
        _image.gameObject.SetActive(true);
        var _imageDescription = questPanels[setPanelDataIndex].transform.Find("imageDescription").GetComponent<TextMeshProUGUI>();
        _imageDescription.text = _panelDescription;
        _imageDescription.gameObject.SetActive(true);
        questPanels[setPanelDataIndex].transform.Find("panelDescription").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.GetChild(questPanels[setPanelDataIndex].transform.childCount - 1).gameObject.SetActive(false);
        setPanelDataIndex++;
    }

    public void SetQuestPanelDataVideo(string _panelQuestion, string _videoUrl)
    {
        questPanels[setPanelDataIndex].transform.Find("panelQuestion").GetComponent<TextMeshProUGUI>().text = _panelQuestion;
        questPanels[setPanelDataIndex].transform.Find("questImage").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.Find("imageDescription").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.Find("panelDescription").gameObject.SetActive(false);
        questPanels[setPanelDataIndex].transform.GetChild(questPanels[setPanelDataIndex].transform.childCount - 1).gameObject.SetActive(true);
        questPanels[setPanelDataIndex].transform.GetChild(questPanels[setPanelDataIndex].transform.childCount - 1).GetComponentInChildren<YoutubePlayer>().youtubeUrl = _videoUrl;
        setPanelDataIndex++;
    }

    public IEnumerator SetInformationPageData(Rad_Model.Quest_Page _page)
    {
        infoTitleText.text = _page.title_text;
        infoDescriptionText.text = _page.description_text;
        infoDescriptionText.gameObject.SetActive(true);
        if (_page.has_image)
        {
            Sprite spriteTemp = BackendManager.Instance.DecodeBase64Texture(_page.image);

            infoImage.sprite = spriteTemp;
            infoImage.gameObject.SetActive(true);
        }
        else
        {
            infoImage.gameObject.SetActive(false);
        }

        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(questInfoPage);
    }

    public void SetDraftsText(List<Rad_Model.Draft> _draftsList)
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            var content = questPanels[i].transform.Find("DraftScrollView").GetComponent<ScrollRect>().content.transform;
            if (i < _draftsList.Count)
            {
                //var q = questPanels[i].transform.Find("ModifyDraftInputField").gameObject;

                var q = content.Find("InputField").gameObject;
                q.GetComponent<TMP_InputField>().text = _draftsList[i].description;
                var _image = content.Find("Image").GetComponent<Image>();
                if (_draftsList[i].has_image)
                {
                    _image.gameObject.SetActive(true);
                    Sprite spriteTemp = BackendManager.Instance.DecodeBase64Texture(_draftsList[i].image);
                    _image.sprite = spriteTemp;
                    mediaPickerButton[i].gameObject.SetActive(false);
                }
                else
                {
                    _image.gameObject.SetActive(false);
                    if(QuestManagerV2.Instance.currentQuestId != 11)
                    {
                        mediaPickerButton[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        mediaPickerButton[i].gameObject.SetActive(false);
                    }
                }
            }
        }

        UnlockNextPageButtons(_draftsList.Count);
    }

    public void SetMediaPickerButtons(bool _value)
    {
        for (int i = 0; i < mediaPickerButton.Length; i++)
        {
            mediaPickerButton[i].gameObject.SetActive(_value);
        }
    }

    public IEnumerator SetPostLogs(int _questId)
    {

        if (RadSaveManager.storiesList.storiesData.ContainsKey(_questId))
        {
            logTexts[0].text = RadSaveManager.storiesList.storiesData[_questId];
        }
        else
        {
            logTexts[0].text = " ";
        }

        logImages[0].sprite = null;
        logImages[0].gameObject.SetActive(false);
        cameraButtons[0].gameObject.SetActive(true);

        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(questLogPage);
        //logDescriptionInputField.text += "\n <align=\"center\"><size=400%><sprite=0>";
    }

    public IEnumerator SetPostLogs(Rad_Model.Post_Log _log)
    {

        logTitleInputField.text = _log.postTitle;

        if (_log.has_image1)
        {
            logImages[0].sprite = BackendManager.Instance.DecodeBase64Texture(_log.image1);
            logImages[0].gameObject.SetActive(true);
            logImages[0].preserveAspect = true;
            cameraButtons[0].SetActive(false);
        }
        else
        {
            logImages[0].sprite = null;
            logImages[0].gameObject.SetActive(false);
            cameraButtons[0].SetActive(true);
        }

        logTexts[0].text = _log.postContent1;

        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(questLogPage);
    }


    public void SetCreateLogButton()
    {
        createLogButton.SetActive(true);
    }

    public void SetModifyLogButton()
    {
        modifyLogButton.SetActive(true);
    }


    //TODO COMBINE BOTH AND GET LAST SEEN PANEL OR SHOW LANDING PAGE
    public void ShowLandingPage(Sprite _organizationIcon)
    {
        Rad_Model.Quest currentQuest = QuestManagerV2.Instance.GetCurrentQuest();

        questPanelGodFather.SetActive(true);
        questLandingPage.transform.Find("QuestTitle").GetComponent<TextMeshProUGUI>().text = currentQuest.quest_title;
        questLandingPage.transform.Find("QuestQuote").GetComponent<TextMeshProUGUI>().text = currentQuest.quote_text;
        questLandingPage.transform.Find("QuestDuration").GetComponent<TextMeshProUGUI>().text = currentQuest.duration;
        questLandingPage.transform.Find("QuestType").GetComponent<TextMeshProUGUI>().text = currentQuest.type;
        if (currentQuest.has_image)
        {
            questLandingPage.transform.Find("Logo").GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(currentQuest.quest_image);
        }
        else
        {
            questLandingPage.transform.Find("Logo").GetComponent<Image>().sprite = _organizationIcon;
        }

        
        loadingPage.SetActive(false);
        MovePanel_In(questLandingPage);
        LockNextPageButoons();
    }
    public void ShowQuestPanel(int _panel)
    {
        questPanelGodFather.SetActive(true);
        questPagination.gameObject.SetActive(true);
        BUTTON_GoToPage(_panel);
        loadingPage.SetActive(false);
    }

    public void UnlockNextPageButton()
    {
        StartCoroutine(UnlockNextPageButtonDelay());
    }

    IEnumerator UnlockNextPageButtonDelay()
    {
        yield return new WaitForSeconds(0.1f);
        questPanels[currentQuestPanelPage].transform.Find("NextPageButton").GetComponent<Button>().interactable = true;
    }

    private void LockNextPageButoons()
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            questPanels[i].transform.Find("NextPageButton").GetComponent<Button>().interactable = false;
        }
    }
    private void UnlockNextPageButtons(int j)
    {
        for (int i = 0; i < j; i++)
        {
            questPanels[i].transform.Find("NextPageButton").GetComponent<Button>().interactable = true;
        }
    }

    public void SetPaginationButtons(int _amount)
    {
        maxQuestPanelPages = _amount;
        questPagination.gameObject.SetActive(true);
        if(QuestManagerV2.Instance.currentQuestId == 11)
        {
            questPagination.SetCurrentPaginationButtons(_amount-1);
        }
        else
        {
            questPagination.SetCurrentPaginationButtons(_amount);
        }
        
    }

    public void CreateLogAndSendIt()
    {
        var ninja = BackendManager.Instance.radModel.ninja;
        if (PostsManager.Instance != null && QuestManagerV2.Instance != null)
        {
            string[] _posts = new string[3];
            Sprite[] _sprites = new Sprite[3];

            string _content = logContentScrollView.transform.Find("Log1").GetComponent<TMP_InputField>().text;
            _posts[0] = _content;
            Sprite _contentSprite = logContentScrollView.transform.Find("Image1").GetComponent<Image>().sprite;
            _sprites[0] = _contentSprite;

            if (QuestManagerV2.Instance.GetCurrentQuestSlot() == 1 && BackendManager.Instance.radModel.currentPhase == 0)
            {
                PostsManager.Instance.CreatePostLog(logTitleInputField.text, _posts[0], _posts[1], _posts[2],ninja.url, QuestManagerV2.Instance.GetCurrentQuest().id,
                                                        privacyDropDown.options[privacyDropDown.value].text, _sprites[0], _sprites[1], _sprites[2],true);
            }
            else
            {
                PostsManager.Instance.CreatePostLog(logTitleInputField.text, _posts[0], _posts[1], _posts[2], ninja.url, QuestManagerV2.Instance.GetCurrentQuest().id,
                                        privacyDropDown.options[privacyDropDown.value].text, _sprites[0], _sprites[1], _sprites[2],false);
            }
            loadingPage.SetActive(true);
            //questPanels[currentQuestPanelPage].SetActive(false);
            MovePanel_Out_Right(questPanels[currentQuestPanelPage]);
        }

    }

    public void ValidateLog()
    {

        if(string.IsNullOrEmpty(logTitleInputField.text))
        {
            StartCoroutine(ShowLogAlert(titleAlert));
            return;
        }

        bool _logEmpty = true;

        for (int i = 0; i < 3; i++)
        {
            var _text = logContentScrollView.transform.Find("Log" + (i + 1)).GetComponent<TMP_InputField>();
            if (!string.IsNullOrEmpty(_text.text))
            {
                _logEmpty = false;
                break;
            }
        }

        if (_logEmpty)
        {
            StartCoroutine(ShowLogAlert(logAlert));
            return;
        }

        if (!PostsManager.Instance.CurrentQuestHasNinjaLog(QuestManagerV2.Instance.GetCurrentQuestUrl()))
        {
            CreateLogAndSendIt();
        }
        else
        {
            ModifyLogAndSendIt();
        }
    }

    IEnumerator ShowLogAlert(GameObject _gameObject)
    {
        _gameObject.SetActive(true);
        yield return new WaitForSeconds(5);
        _gameObject.SetActive(false);
    }

    public void ModifyLogAndSendIt()
    {
        var ninja = BackendManager.Instance.radModel.ninja.url;

        if (PostsManager.Instance != null && QuestManagerV2.Instance != null)
        {
            string[] _posts = new string[3];
            Sprite[] _sprites = new Sprite[3];
            string _content = logContentScrollView.transform.Find("Log1").GetComponent<TMP_InputField>().text;
            _posts[0] = _content;
            Sprite _contentSprite = logContentScrollView.transform.Find("Image1").GetComponent<Image>().sprite;
            _sprites[0] = _contentSprite;

            PostsManager.Instance.ModifyPostLog(logTitleInputField.text, _posts[0], _posts[1], _posts[2],ninja, QuestManagerV2.Instance.GetCurrentQuest().id,
                                        privacyDropDown.options[privacyDropDown.value].text, _sprites[0], _sprites[1], _sprites[2],true);
            loadingPage.SetActive(true);
            //questPanels[currentQuestPanelPage].SetActive(false);
            MovePanel_Out_Right(questPanels[currentQuestPanelPage]);
        }
    }

    public void Button_BackToEditLog()
    {
        if (QuestManagerV2.Instance.currentQuestId == 11)
        {
            MovePanel_In(questPanels[currentQuestPanelPage]);
        }
        else
        {
            MovePanel_In(questLogPage);
        }

        MovePanel_Out_Right(questCompleted);
        questPagination.gameObject.SetActive(true);
        _playerController.canMove = false;
    }

    public void StartQuest()
    {
        QuestManagerV2.Instance.CreateQuestStatus();
        MovePanel_Out_Left(questLandingPage);
        MovePanel_In(questPanels[0]);
        questPagination.gameObject.SetActive(true);
        questPagination.CurrentStep(0);
        SetCreateLogButton();
        currentQuestPanelPage = 0;
        QuestManagerV2.Instance.ClearDraftList();
        logTitleInputField.text = "";
    }


    public void ShowNameUsedWarning()
    {
        loadingPage.SetActive(false);
        StartCoroutine(NinjaNameWarning());
    }

    private IEnumerator NinjaNameWarning()
    {
        Debug.Log("currentQuestPanel: " + currentQuestPanelPage);
        questPanels[currentQuestPanelPage].Find("NinjaNameWarning_disabled").gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        questPanels[currentQuestPanelPage].Find("NinjaNameWarning_disabled").gameObject.SetActive(false);
    }

    public void QuestPanelNextPage()
    {
        currentQuestPanelPage++;
        questPagination.CurrentStep(currentQuestPanelPage);
        if (questPanels[currentQuestPanelPage-1].transform.GetChild(questPanels[currentQuestPanelPage-1].transform.childCount - 1).gameObject.activeInHierarchy)
        {
            questPanels[currentQuestPanelPage-1].transform.GetChild(questPanels[currentQuestPanelPage].transform.childCount - 1).GetComponentInChildren<YoutubePlayer>().Stop();
        }
        if (currentQuestPanelPage == maxQuestPanelPages - 1  && QuestManagerV2.Instance.currentQuestId != 11) //TODO hardcoded
        {
            MovePanel_Out_Left(questPanels[currentQuestPanelPage - 1]);
            MovePanel_In(questLogPage);
            if (addDraftsToLog)
            {
                FillLogTextWithDrafts(QuestManagerV2.Instance.currentQuestId);
                //logTexts[0].text = "";
            }
        }
        else if(currentQuestPanelPage == maxQuestPanelPages - 1 && QuestManagerV2.Instance.currentQuestId == 11) //TODO hardcoded
        {
            currentQuestPanelPage--;
            PostsManager.Instance.ModifyNinjaFirstQuest();
            loadingPage.SetActive(true);
        }
        else
        {
            MovePanel_Out_Left(questPanels[currentQuestPanelPage - 1]);
            MovePanel_In(questPanels[currentQuestPanelPage]);
        }
        QuestManagerV2.Instance.GetCurrentQuestStatus().quest_page_active = currentQuestPanelPage;
        QuestManagerV2.Instance.ModifyCurrentQuestStatus();
    }

    private void FillLogTextWithDrafts(int _questId)
    {
        logImages[0].sprite = null;
        logImages[0].gameObject.SetActive(false);
        cameraButtons[0].gameObject.SetActive(true);

        if (RadSaveManager.storiesList.storiesData.ContainsKey(_questId))
        {
            logTexts[0].text = RadSaveManager.storiesList.storiesData[_questId];
        }
        else
        {
            logTexts[0].text = " ";
        }
    }

    IEnumerator PlayerControllerCanMove()
    {
        yield return new WaitForSeconds(1f);
        _playerController.canMove = true;
    }

    public void BUTTON_QuestCompleted()
    {
        _pathManager.QuestCompleted(QuestManagerV2.Instance.GetCurrentQuestSlot());
        MovePanel_Out_Right(questCompleted);
        questPagination.gameObject.SetActive(false);
        supportButton.SetActive(true);
        StartCoroutine(PlayerControllerCanMove());
    }

    public void BUTTON_CloseQuestLandingPage()
    {
        MovePanel_Out_Right(questLandingPage);
        questPagination.gameObject.SetActive(false);
        StartCoroutine(PlayerControllerCanMove());
        supportButton.SetActive(true);
    }

    public void BUTTON_CLoseQuestPanel()
    {
        if (!infoPanelActive)
        {
            //questPanels[currentQuestPanelPage].SetActive(false);
            MovePanel_Out_Right(questPanels[currentQuestPanelPage]);
            questPagination.gameObject.SetActive(false);
            //questLogPage.SetActive(false);
            MovePanel_Out_Right(questLogPage);
            StartCoroutine(PlayerControllerCanMove());
            supportButton.SetActive(true);

            if (questPanels[currentQuestPanelPage].Find("RenderTexture_disabled"))
            {
                questPanels[currentQuestPanelPage].Find("RenderTexture_disabled").GetComponentInChildren<YoutubePlayer>().Stop();
            }
        }
        else
        {
            //questInfoPage.SetActive(false);
            MovePanel_Out_Right(questInfoPage);
            infoPanelActive = false;
            questPagination.CurrentStep(currentQuestPanelPage, false);


        }
    }

    public void BUTTON_GoToInfoPage()
    {
        infoPanelActive = true;
        //questInfoPage.SetActive(true);
        MovePanel_In(questInfoPage);
        questPagination.CurrentStep(currentQuestPanelPage, true);
    }


    public void BUTTON_GoToPage(int j)
    {


        if(currentQuestPanelPage == maxQuestPanelPages - 1)
        {
            Button_SaveStoryDraft();
        }

        currentQuestPanelPage = j;

        

        for (int i = 0; i < questPanels.Length; i++)
        {
            if (i != j)
            {
                MovePanel_Out_Right(questPanels[i]);
                if (questPanels[i].Find("RenderTexture_disabled"))
                {
                    questPanels[i].Find("RenderTexture_disabled").GetComponentInChildren<YoutubePlayer>().Stop();
                }
            }
        }

        if (j == maxQuestPanelPages - 1)
        {
            //questLogPage.SetActive(true);
            MovePanel_In(questLogPage);
            for (int i = 0; i < questPanels.Length; i++)
            {
                MovePanel_Out_Right(questPanels[i]);
            }
            if (addDraftsToLog)
            {
                FillLogTextWithDrafts(QuestManagerV2.Instance.currentQuestId);
                //logTexts[0].text = "";
            }
        }
        else
        {
            //questLogPage.SetActive(false);
            MovePanel_Out_Right(questLogPage);
            for (int i = 0; i < questPanels.Length; i++)
            {
                if (i == j)
                {
                    //questPanels[i].SetActive(true);
                    MovePanel_In(questPanels[i]);
                }
                else
                {
                    //questPanels[i].SetActive(false);
                    MovePanel_Out_Right(questPanels[i]);
                }

            }

        }
        questPagination.CurrentStep(j);
        //questInfoPage.SetActive(false);
        MovePanel_Out_Right(questInfoPage);
        QuestManagerV2.Instance.GetCurrentQuestStatus().quest_page_active = currentQuestPanelPage;
        QuestManagerV2.Instance.ModifyCurrentQuestStatus();
    }

    public void BUTTON_OpenImageZoomPanel(Image _image)
    {
        imageToZoom.sprite = _image.sprite;
        imageZoomPanel.gameObject.SetActive(true);
        MovePanel_In(imageZoomPanel);
    }

    public void BUTTON_CloseImageZoomPanel()
    {
        MovePanel_Out_Left(imageZoomPanel, delegate { imageZoomPanel.gameObject.SetActive(false); });

    }


    public void ShowFinishedPhaseScreen()
    {
        if (!finishedScreen.gameObject.activeInHierarchy)
        {
            finishedScreen.gameObject.SetActive(true);
        }

        Rad_Model.Phase_Status faseStatus = new Rad_Model.Phase_Status();

        faseStatus = BackendManager.Instance.radModel.phaseStatusList[BackendManager.Instance.radModel.currentPhase];
        if (!faseStatus.completed)
        {
            BackendManager.Instance.CreatePhaseStatus(BackendManager.Instance.radModel.currentPhase + 1);
            faseStatus.completed = true;
            _goToSurvey = true;
        }

        string[] str = faseStatus.url.Split('/');
        int pk = int.Parse(str[str.Length - 2]);

        string jsonTosend = JsonUtility.ToJson(faseStatus);

        BackendManager.Instance.ModifyPhaseStatus(pk, jsonTosend);

        MovePanel_In(finishedScreen);
        supportButton.SetActive(false);
    }

    public void BUTTON_GoToPhaseMap(string _sceneName)
    {
        LoadingSceneManager.LoadScene(_sceneName);
    }
    public void Button_CheckPhaseCompleteAndLoadNextMap()
    {
        if (SceneManager.GetActiveScene().name.Contains("Fase3") && _goToSurvey)
        {
            LoadingSceneManager.LoadScene("SurveyScene");
        }
        else
        {
            LoadingSceneManager.LoadScene("PhasesMap");
        }
    }
   

    public void SetPaginationPagesComplete(int _page)
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            if (i <= _page)
            {
                questPagination.SetPageCompleted(i,true);
            }
            else
            {
                questPagination.SetPageCompleted(i, false);
            }
        }
    }

    public void Button_SaveStoryDraft()
    {
        if (RadSaveManager.storiesList.storiesData.ContainsKey(QuestManagerV2.Instance.currentQuestId))
        {
            RadSaveManager.storiesList.storiesData[QuestManagerV2.Instance.currentQuestId] = logTexts[0].text;
        }
        else
        {
            RadSaveManager.storiesList.storiesData.Add(QuestManagerV2.Instance.currentQuestId, logTexts[0].text);
        }

        RadSaveManager.SaveData();
    }

    public void ResetPagesComplete()
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            questPagination.SetPageCompleted(i, false);
        }

    }

    public void ResetStory()
    {
        logTexts[0].text = " ";
        logTexts[0].text = "";
        logImages[0].gameObject.SetActive(false);
        cameraButtons[0].SetActive(true);
    }
    public void ResetPanelQuest()
    {
        for (int i = 0; i < questPanels.Length; i++)
        {
            //questPanels[i].SetActive(false);
            MovePanel_Out_Right(questPanels[i]);
        }
        questPagination.CurrentStep(1);
        //questLogPage.SetActive(false);
        MovePanel_Out_Right(questLogPage);
        questPagination.gameObject.SetActive(false);
    }

    public void QuestCompletedPanel(bool firstQuest)
    {
        if (firstQuest)
        {
            firstQuestCompletedText.SetActive(true);
            questCompletedText.SetActive(false);
        }
        else
        {
            firstQuestCompletedText.SetActive(false);
            questCompletedText.SetActive(true);
        }
        MovePanel_In(questCompleted);
    }
    public void SetAddDraftsToLog(bool _value)
    {
        addDraftsToLog = _value;
    }

    public void ShowNoInternetPanel()
    {
        loadingPage.SetActive(false);
        noInternetPanel.SetActive(true);
    }

    /// <summary>
    /// called from quest triggers
    /// </summary>
    /// <param name="_questNumber"></param>
    /// <param name="_phase"></param>
    public void RequestQuestAndSetWaitPanelOn(int _questNumber)
    {

        startQuestPanel.SetActive(false);
        BUTTON_SetCurrentQuest(_questNumber);
    }

    public void ShowStartQuestButton(int _questId, Vector2 _position)
    {
        startQuestPanel.transform.position = _position;
        startQuestPanel.SetActive(true);
        startQuestPanel.GetComponent<Button>().onClick.RemoveAllListeners();
        startQuestPanel.GetComponent<Button>().onClick.AddListener(delegate { RequestQuestAndSetWaitPanelOn(_questId); });
    }

    public void CloseAskQuestPanel()
    {
        startQuestPanel.SetActive(false);
        StartCoroutine(PlayerControllerCanMove());
    }
    #region DoTween UI animations

    public void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    public void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    public void MovePanel_Out_Right(RectTransform panel, System.Action OnComplete)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f).OnComplete(()=> 
        {
            OnComplete();
        }); //To right position
    }

    public void MovePanel_Out_Left(RectTransform panel, System.Action OnComplete)
    {
        panel.DOAnchorPos(new Vector2(-1300, 0), 0.25f).OnComplete(() =>
        {
            OnComplete();
        }); //To left position
    }

    public void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-1300, 0), 0.25f); //To left position
    }
    #endregion
}
