﻿using DG.Tweening;
using ExitGames.Client.Photon.Chat;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RadChatPhotonManager : MonoBehaviour, IChatClientListener {

    //public string[] ChannelsToJoinOnConnect;
    public List<string> ChannelsToJoinOnConnect;
    public List<string> FriendsList;

    public ChatClient chatClient;
    public int HistoryPhotonChatLengthToFetch;
    public int HistoryNinjapiChatLengthToFetch;

    public RectTransform ChatPanel;

    private readonly Dictionary<string, FriendItem> friendListItemLUT = new Dictionary<string, FriendItem>();

    public List<string> channelsAvailable = new List<string>();

    public TMPro.TextMeshProUGUI chatStatusText;
    public Button connectButton;

    public TMPro.TMP_InputField InputFieldChat;   // set in inspector

    public GameObject chatBoxPrefab;
    public GameObject chatboxesContainer;

    //private string CurrentChannelText;

    [BoxGroup("ChatBox Image Default for Quests")] public Sprite defaultQuestSprite;
    
    //[BoxGroup("ChatBox Placeholders")] public GameObject CHATBOXPLACEHOLDER;
    [BoxGroup("ChatBox Placeholders")] public GameObject WHITECHATBUBBLEPLACEHOLDER;
    [BoxGroup("ChatBox Placeholders")] public GameObject REDCHATBUBBLEPLACEHOLDER;

    [BoxGroup("Coach Links")] public GameObject coachChatBox;
    [BoxGroup("Coach Links")] public Image onlineIcon;
    [BoxGroup("Coach Links")] public Image coachImage;
    [BoxGroup("Coach Links")] public TMPro.TextMeshProUGUI coachChatBoxTitleName;
    [BoxGroup("Coach Links")] public TMPro.TextMeshProUGUI coachChatBoxLastMessage;
    [BoxGroup("Coach Links")] public TMPro.TextMeshProUGUI coachTimeStamp;


    [BoxGroup("OpenChat Links")] public RectTransform openChatRect;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI openChannelName;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI openChannelInfo;
    [BoxGroup("OpenChat Links")] public Image channelImage;
    [BoxGroup("OpenChat Links")] public GameObject openChannelOutputParent;
    [BoxGroup("OpenChat Links")] public TMPro.TextMeshProUGUI selectedChannelText;

    private string usernameChat;
    private bool _appIDpresent;

    private BackendManager backendManager;
    private QuestManagerV2 questManager;
    private bool _connected;

    private string selectedChannelName;
    private string _coachChannel;
    //private int[] arrayChatBoxPos = new int[7] { 60, 69, -320, -697, -1069, -1441, -1821 };
    private List<GameObject> chatBoxesList = new List<GameObject>();
    string coachChannel = "";

    IEnumerator Start()
    {

        yield return new WaitForSeconds(0.5f);

        DontDestroyOnLoad(gameObject);

        backendManager = FindObjectOfType<BackendManager>();
        questManager = FindObjectOfType<QuestManagerV2>();

        connectButton.gameObject.SetActive(false);

        //this.CurrentChannelText = "";

        var _str = BackendManager.Instance.radModel.ninja.has_coaches[0].Split('/');
        int coachID = int.Parse(_str[_str.Length - 2]);
        coachChannel = coachID.ToString() + BackendManager.Instance.radModel.ninja.pk.ToString();
        //Check if the user is Coach or Ninja

        //Subscribe to events
        BackendManager.Instance.GetCoachSuccess += OnGetCoachSuccess;
        BackendManager.Instance.GetCoachFailure += OnGetCoachFailure;

        BackendManager.Instance.GetHistorySuccess += OnGetHistorySuccess;
        BackendManager.Instance.GetHistoryFailure += OnGetHistoryFailure;
        //we define which first channels you have to be subscribed to:
        // - SFN_Global chat
        // - Chat channel per quest started
        // - One on one channel to Coach
        this.CheckChannelsToSubscribe();

        this.CheckFriendsChannelsToSubscribe();



        

    }

    void Update()
    {
        if (this.chatClient != null)
        {
            this.chatClient.Service(); // make sure to call this regularly! it limits effort internally, so calling often is ok!
        }

        if (!_connected)
        {
            chatStatusText.text = "Not connected";
            connectButton.gameObject.SetActive(true);
        }
        else
        {
            connectButton.gameObject.SetActive(false);
            chatStatusText.text = "Connected as " + usernameChat;
        }

        

    }

    /// <summary>
    /// Shows the channel on the FrontEnd
    /// </summary>
    /// <param name="channelName"></param>
    public void ShowChannel(string channelName)
    {

        if (string.IsNullOrEmpty(channelName))
        {
            return;
        }

        if (!channelName.Contains(backendManager.radModel.myCoach.coach_name))
        {
            ChatChannel channel = null;
            bool found = this.chatClient.TryGetChannel(channelName, out channel);
            if (!found)
            {
                Debug.Log("ShowChannel failed to find channel: " + channelName);
                return;
            } 
            
        }
        else
        {
            this.selectedChannelName = channelName;
        }

        //We fill in the data corresponding to the channel
        if (channelName.Contains(coachChannel))
        {
            openChannelName.text = backendManager.radModel.myCoach.coach_name;
            channelImage.sprite = backendManager.DecodeBase64Texture(backendManager.radModel.myCoach.coach_avatar);
        }
        else
        {
            if (channelName.Contains("Groepschat met alle Ninja en Sensei"))
            {
                channelImage.sprite = backendManager.schoolForNinjaLogo;
            }
            else
            {
                channelImage.sprite = QuestManagerV2.Instance.defaultQuestCoverImage[0];
            }
            openChannelName.text = channelName;
        }
        
        
        openChannelInfo.text = GetDescriptionForChannel(channelName);

        GetHistoryforChannel(channelName);
        //print("Getting history for Channels: " + channelName);




    }

    private void SendChatMessage(string inputLine)
    {
        if (string.IsNullOrEmpty(inputLine))
        {
            Debug.Log("inputline is empty");
            return;
        }

        this.chatClient.PublishMessage(this.selectedChannelName, inputLine);

        //History on Ninjapi
        Rad_Model.ChatHistory histo = new Rad_Model.ChatHistory();

        histo.sender = backendManager.radModel.ninja.url;
        histo.channel = this.selectedChannelName;
        histo.content = backendManager.radModel.ninja.ninja_name + ":" + inputLine;

        var jsonToSend = JsonUtility.ToJson(histo);

       backendManager.CreateChatHistory(jsonToSend);
    }




    /// <summary>To avoid that the Editor becomes unresponsive, disconnect all Photon connections in OnApplicationQuit.</summary>
    public void OnApplicationQuit()
    {
        if (this.chatClient != null)
        {
            this.chatClient.Disconnect();
        }
    }





    #region Chat Setup

    public void InitializeChat()
    {
        if (ChatSettings.Instance.AppId == "" | ChatSettings.Instance.AppId == null)
        {
            _appIDpresent = false;
        } else
        {
            _appIDpresent = true;
        }


        if (!_appIDpresent)
        {
            Debug.LogError("You need to set the chat app ID in the PhotonServerSettings file in order to continue.");
            //return;
        } else
        {
            //Connect to the Photon Chat Servers
            this.Connect();



        }
    }


    /// <summary>
    /// Connects to the PhotonChatService
    /// </summary>
    public void Connect()
    {
        usernameChat = backendManager.radModel.ninja.ninja_name;//TODO RADICAL we need to figure out how to make this unique since Photon servers need a unique username. Maybe with a filter not allowing people to use the same ninja name...

        this.chatClient = new ChatClient(this);
        this.chatClient.ChatRegion = "EU";
        this.chatClient.Connect(ChatSettings.Instance.AppId, "0.9", new ExitGames.Client.Photon.Chat.AuthenticationValues(usernameChat));

        Debug.Log("Connection created with: AppId:" + ChatSettings.Instance.AppId + ", AppVersion: 0.9, UserName:" + usernameChat);
    }


    private void CheckChannelsToSubscribe()
    {
        List<Rad_Model.Quest_Status> questStatusListTemp = questManager.GetQuestStatusList();

        var _str = BackendManager.Instance.radModel.ninja.has_coaches[0].Split('/');
        int coachID = int.Parse(_str[_str.Length - 2]);
        string coachChannel = coachID.ToString() + BackendManager.Instance.radModel.ninja.pk.ToString();
        _coachChannel = coachChannel;
        ChannelsToJoinOnConnect.Add(coachChannel);

        for (int i = 0; i < questStatusListTemp.Count; i++)
        {
            //questStatus.belongs_to_quest;

            var str = questStatusListTemp[i].belongs_to_quest.Split('/');

            int QuestID = int.Parse(str[str.Length - 2]);

            //we build the channel name from the QuestID
            string questname = "";

            var questList = QuestManagerV2.Instance.GetQuestsList();
            for (int j = 0; j < questList.Count; j++)
            {
                if(QuestID == questList[j].id)
                {
                    questname = questList[j].quest_title;
                    break;
                }
            }

            string channelName = "Quest " + questname;
            ChannelsToJoinOnConnect.Add(channelName);
        }

        if (questStatusListTemp.Count == 0)
        {
            Debug.Log("No channels from Quests to Subscribe...");
        }

    }

    /// <summary>
    /// We check the friends (COACH) from an user. In this case can only be the Coach/Sensei
    /// </summary>
    private void CheckFriendsChannelsToSubscribe()
    {
        if (backendManager.radModel.ninja.has_coaches.Count == 1)
        {
            backendManager.GetMyCoachName(backendManager.radModel.ninja.has_coaches[0]);

        } else
        {
            Debug.LogError("There's more (or less) than one coach assigned to this user, why???");
            Debug.LogError("Number of coaches assgined " + backendManager.radModel.ninja.has_coaches.Count);
        }
    }


    private void CreateChatBoxforChannel(string channel)
    {

        //We avoid creation of another chatbox for the coach cause we have it already...
        //So we only create the chatbox if the 
        if (!channel.Contains(backendManager.radModel.myCoach.coach_name))
        {
            GameObject chatbox = Instantiate(chatBoxPrefab, chatboxesContainer.transform);
            chatbox.GetComponent<RectTransform>().position = Vector3.zero;
            chatbox.transform.localScale = Vector3.one;
            UIChatPanelController chatBoxPanelCtr = chatbox.GetComponent<UIChatPanelController>();

            if (channel.Contains(_coachChannel))
            {
                chatBoxPanelCtr.chatTitle.text = backendManager.radModel.myCoach.coach_name;
                chatBoxPanelCtr.chatIcon.sprite = backendManager.DecodeBase64Texture(backendManager.radModel.myCoach.coach_avatar);
            }
            else
            {
                chatBoxPanelCtr.chatTitle.text = channel;
                if(channel.Contains("Groepschat met alle Ninja en Sensei"))
                {
                    chatBoxPanelCtr.chatIcon.sprite = backendManager.schoolForNinjaLogo;
                }
                else
                {
                    chatBoxPanelCtr.chatIcon.sprite = QuestManagerV2.Instance.defaultQuestCoverImage[0];
                }
                
            }

            chatbox.GetComponent<Button>().onClick.AddListener(() => { Button_OpenChat(channel); });
            chatBoxesList.Add(chatbox);
        }

    }

    #endregion

    #region UI Input events
    public void OnEnterSend()
    {
        if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
        {
            SendChatMessage(this.InputFieldChat.text);
            this.InputFieldChat.text = "";
        }
    }

    public void OnClickSend()
    {
        if (this.InputFieldChat.text != null)

        {
            Debug.Log("OnClickSend: "+this.InputFieldChat.text);
            SendChatMessage(this.InputFieldChat.text);
            this.InputFieldChat.text = "";
        }
        else
        {
            Debug.Log("InputFieldChat is empty");
        }
    }

    public void Button_OpenChat(string channelName)
    {

        this.selectedChannelText.text = "";

        this.selectedChannelName = channelName;

        //we fill in the data for each channel on the fly
        ShowChannel(channelName);

        CreateBubblesChat(channelName);
        
        //We show the openchat panel
        openChatRect.gameObject.SetActive(true);
        MovePanel_In(openChatRect);
        


    }

    public void Button_OpenCoachChat(string channelName)
    {
        //we fill in the data for each channel on the fly
        // If we are on a private chat (coach) we build the channel name as 
        // the channel name for a private conversation is (on the client!!) always composed of both user's IDs: "this:remote". In this case "this" is coach and "remote" is username

        string privateChannelName = backendManager.radModel.myCoach.coach_name + ":" + backendManager.radModel.ninja.ninja_name;

        print("privatechannelName:"+privateChannelName);

        ShowChannel(privateChannelName);

        //CreateBubblesChat(privateChannelName);

        CreateBubblesChatforPrivate(privateChannelName,null,"");

        this.selectedChannelName = privateChannelName;

        //We show the openchat panel
        openChatRect.gameObject.SetActive(true);
        MovePanel_In(openChatRect);

        this.selectedChannelText.text = "";


    }


    public void Button_BackToChatOverview()
    {
        MovePanel_Out_Right(openChatRect);
        
        //UpdateBubblesChat();
    }

    /// <summary>
    /// Button for when you're not connected to the chat. Sometimes we lose chat connection...
    /// It should drop you to the overview if you're on a chat already...
    /// </summary>
    public void Button_Connect()
    {
        if (openChatRect.gameObject.activeInHierarchy)
        {
            Button_BackToChatOverview();
        }

        Connect();
    }


    #endregion

    #region Photon Chat Interface members

    /// <summary>
    /// New status of another user (you get updates for users set in your friends list).
    /// </summary>
    /// <param name="user">Name of the user.</param>
    /// <param name="status">New status of that user.</param>
    /// <param name="gotMessage">True if the status contains a message you should cache locally. False: This status update does not include a
    /// message (keep any you have).</param>
    /// <param name="message">Message that user set.</param>
    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {

        Debug.LogWarning("status: " + string.Format("{0} is {1}. Msg:{2}", user, status, message));

        if (friendListItemLUT.ContainsKey(user))
        {
            FriendItem _friendItem = friendListItemLUT[user];
            if (_friendItem != null) _friendItem.OnFriendStatusUpdate(status, gotMessage, message);
        }


        //we check if it's your coach

        if (backendManager.radModel.myCoach.coach_name.Equals(user))
        {
            print("Status Update from your coach:" + user);

            string _status = "Unknown";

            switch (status)
            {
                case 1:
                    _status = "Invisible";
                    break;
                case 2:
                    _status = "Online";
                    break;
                case 3:
                    _status = "Away";
                    break;
                case 4:
                    _status = "Do not disturb";
                    break;
                case 5:
                    _status = "Looking For Game/Group";
                    break;
                case 6:
                    _status = "Playing";
                    break;
                default:
                    _status = "Offline";
                    break;
            }

            if (_status.Equals("Offline") || _status.Equals("Invisible"))
            {
                onlineIcon.color = new Color32(199, 197, 197, 255);//Gray color
            } else if (_status.Equals("Away") || _status.Equals("Do not disturb"))
            {
                onlineIcon.color = new Color32(247, 135, 133, 255);// Red Color
            }
            else if (_status.Equals("Online"))
            {
                onlineIcon.color = new Color32(134, 255, 11, 255);//Green Color
            }
            else
            {
                onlineIcon.color = new Color32(0, 0, 0, 255);//Black Color
            }


        }


    }

    public void OnUnsubscribed(string[] channels)
    {
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        for (int i = 0; i < channels.Length; i++)
        {
            this.chatClient.PublishMessage(channels[i], "<color=#379A7F>" + usernameChat + "</color>");

            this.CreateChatBoxforChannel(channels[i]);
        }

        Debug.Log("OnSubscribed: " + string.Join(", ", channels));

    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        // as the ChatClient is buffering the messages for you, this GUI doesn't need to do anything here
        // you also get messages that you sent yourself. in that case, the channelName is determinded by the target of your msg
        string[] splitted = channelName.Split(new char[] { ':' });
        channelName = backendManager.radModel.myCoach.coach_name + ":" + splitted[0];
        coachChatBoxTitleName.text = backendManager.radModel.myCoach.coach_name;
        coachTimeStamp.text = System.DateTime.Now.ToShortTimeString();
        CreateBubblesChatforPrivate(channelName,message,sender);
        coachChatBoxLastMessage.text = sender + ":" + message.ToString();
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        GameObject currentChatBox = null;

        for (int i = 0; i < messages.Length; i++)
        {
            for (int j = 0; j < chatBoxesList.Count; j++)
            {
                if (chatBoxesList[j].GetComponent<UIChatPanelController>().chatTitle.text == channelName ||
                    chatBoxesList[j].GetComponent<UIChatPanelController>().chatTitle.text == backendManager.radModel.myCoach.coach_name)
                {
                    currentChatBox = chatBoxesList[j];
                }
            }
            currentChatBox.GetComponent<UIChatPanelController>().chatLastMessage.text = senders[0] + ":" + messages[i].ToString();
            currentChatBox.GetComponent<UIChatPanelController>().chatLastMessageTimeStamp.text = System.DateTime.Now.ToShortTimeString();
            if (channelName.Contains("Quest"))
            {
                currentChatBox.GetComponent<UIChatPanelController>().chatIcon.sprite = defaultQuestSprite;
            }
            if (selectedChannelName == channelName)
            {
                CreateBubblesChat(channelName);
            }
        }
    }


    public void OnChatStateChange(ChatState state)
    {
        // use OnConnected() and OnDisconnected()
        // this method might become more useful in the future, when more complex states are being used.

        //this.StateText.text = state.ToString();
    }

    public void OnConnected()
    {
        _connected = true;

        if (this.ChannelsToJoinOnConnect != null && this.ChannelsToJoinOnConnect.Count > 0)
        {
            this.chatClient.Subscribe(this.ChannelsToJoinOnConnect.ToArray());
        }
        this.ChatPanel.gameObject.SetActive(false);

        if (FriendsList != null && FriendsList.Count > 0)
        {
            this.chatClient.AddFriends(FriendsList.ToArray()); // Add some users to the server-list to get their status updates
            string[] privateChannelName = new string[1];
                privateChannelName[0] =      
                backendManager.radModel.myCoach.coach_name + ":" + backendManager.radModel.ninja.ninja_name;
        }

        this.chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).
    }

    public void OnDisconnected()
    {
        _connected = false;
        Debug.Log("Disconnected");

        for (int i = 0; i < chatBoxesList.Count; i++)
        {
            chatBoxesList[i].gameObject.SetActive(false);
        }
    }

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
    {
        if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
        {
            UnityEngine.Debug.LogError(message);
        } else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
        {
            UnityEngine.Debug.LogWarning(message);
        } else
        {
            UnityEngine.Debug.Log(message);
        }
    }

    #endregion

    #region Event Callbacks

    void OnGetCoachSuccess(string response)
    {
        FriendsList.Add(backendManager.radModel.myCoach.coach_name);
        //We fill the data for the coachChatBox
        coachChatBoxTitleName.text = backendManager.radModel.myCoach.coach_name;
        coachImage.sprite = backendManager.DecodeBase64Texture(backendManager.radModel.myCoach.coach_avatar);
        coachChatBox.GetComponent<Button>().onClick.AddListener(() => { Button_OpenCoachChat(backendManager.radModel.myCoach.coach_name); });
        //we subscribe to the private chat from the coach
        InitializeChat();
    }
   
    void OnGetCoachFailure(string response)
    {
        Debug.LogError(response);
    }


    void OnGetHistorySuccess(string channelName)
    {
        string history = "";

        for (int i = 0; i < backendManager.radModel.chatHistoryList.Count; i++)
        {
            if (backendManager.radModel.chatHistoryList[i].channel.Equals(channelName))
            {
                history += backendManager.radModel.chatHistoryList[i].content + "\n";
            }
        }

        PrettifyBubblesChat(history,true);
        TurnOffPanelHistory();
    }

    void OnGetHistoryFailure(string response)
    {
        Debug.LogError(response);
    }

    #endregion
    

    #region dotween UI animations
    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(0,-719f), 0.25f); //To center position
    }
    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(1080, -719f), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-1300, 0), 0.25f); //To left position
    }

    #endregion

    private string GetDescriptionForChannel(string channelName)
    {
        string desc = "";

        if (channelName.Equals("Groepschat met alle Ninja en Sensei"))
        {
            desc = "";
        }
        else
        {
            desc = channelName + " Chat";
        }

        

        return desc;
    }

    private void CreateBubblesChatforPrivate(string channelName, object message, string sender)
    {
        if (message != null)
        {
            PrettifyBubblesChatSingle(message.ToString());
        }
        


    }


    private void CreateBubblesChat(string channelName)
    {

        ChatChannel channel = null;
        bool found = this.chatClient.TryGetChannel(channelName, out channel);

        if (!found)
        {
            Debug.Log("CreateBubblesChat failed to find channel: " + channelName);
            return;
        }

        PrettifyBubblesChatSingle(channel.ToStringMessages());
    }


    private void GetHistoryforChannel(string channelName)
    {
        backendManager.GetChatHistory(channelName);
        TurnOnPanelHistory();
    }

    private void TurnOnPanelHistory()
    {

    }


    private void TurnOffPanelHistory()
    {

    }

    private void PrettifyBubblesChatSingle(string messages)
    {

        if (String.IsNullOrEmpty(messages))
        {
            return;
        }

        if (messages.Contains("\n"))
        {
            string[] splitted = messages.Split(new char[] { '\n' });

            for (int i = 0; i < splitted.Length - 1; i++)//We left out the last element since we get an empty line..
            {
                splitted[i] = RecolorRealignChatText(splitted[i]);
                if (i == splitted.Length - 2)
                {
                    this.selectedChannelText.text += '\n' + splitted[i];
                }

            }
        }
        else
        {
            this.selectedChannelText.text += '\n' + messages;
        }

    }

    private void PrettifyBubblesChat(string messages, bool doRecolor)
    {

        if (String.IsNullOrEmpty(messages))
        {
            return;
        }

        if (messages.Contains("\n"))
        {
            string[] splitted = messages.Split(new char[] { '\n' });

            for (int i = 0; i < splitted.Length - 1; i++)//We left out the last element since we get an empty line..
            {
                if (doRecolor)
                {
                    splitted[i] = RecolorRealignChatText(splitted[i]);
                }
                this.selectedChannelText.text += '\n' + splitted[i];
            }
        }
        else
        {
            messages = RecolorRealignChatText(messages);

            this.selectedChannelText.text += '\n' + messages;
        }
    }


    private string RecolorRealignChatText(string message)
    {
        string[] recolorsplit = message.Split(new char[] { ':' });

        recolorsplit[0] = "<color=#EF6866><size=75%>" + recolorsplit[0] + "</size></color>";

        if (recolorsplit.Length > 1)
        {
            if (recolorsplit[0].Contains(backendManager.radModel.ninja.ninja_name))
            {
                return "<align=\"right\">" + recolorsplit[1] + "</align>";
            } else
            {
                if (recolorsplit.Length > 1)
                {
                    return recolorsplit[0] + recolorsplit[1];
                } else
                {
                    return recolorsplit[0];
                }

            }
        }
        else
        {
            return recolorsplit[0];
        }
        

        
    }

    
    void OnDestroy()
    {
        //Subscribe to events
        BackendManager.Instance.GetCoachSuccess -= OnGetCoachSuccess;
        BackendManager.Instance.GetCoachFailure -= OnGetCoachFailure;

        BackendManager.Instance.GetHistorySuccess -= OnGetHistorySuccess;
        BackendManager.Instance.GetHistoryFailure -= OnGetHistoryFailure;
    }
}
