﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class Rad_PostLogsUIManager : MonoBehaviour {


    [BoxGroup("ListingContainers")] public GameObject logsListingContainer;
    [BoxGroup("ListingContainers")] public GameObject singlePostContainer;
    [BoxGroup("ListingContainers")] public GameObject singlePersonalPostContainer;

    [BoxGroup("SinglePostContainer")] public Image[] postImages;
    [BoxGroup("SinglePostContainer")] public TMPro.TextMeshProUGUI[] postLogs;

    [BoxGroup("Prefab")] public GameObject postButtonPrefab;

    public RadProfileUIManager radProfileUIManager;

    private RectTransform logsListingRect;
    private RectTransform singlePostRect;
    private RectTransform singlePersonalPostRect;

    private void OnDisable()
    {
        BackendManager.Instance.GetStoriesSuccess -= AssignQuestStoriesButtons;
    }

    void Start()
    {
        BackendManager.Instance.GetStoriesSuccess += AssignQuestStoriesButtons;

        logsListingRect = logsListingContainer.GetComponent<RectTransform>();
        singlePostRect = singlePostContainer.GetComponent<RectTransform>();
        if(singlePersonalPostContainer)
            singlePersonalPostRect = singlePersonalPostContainer.GetComponent<RectTransform>();
    }

    private void OnGetAllQuestStatuSuccess()
    {
        //TODO review to see if this method is needed
    }

    private void OnGetAllQuestStatuFail()
    {
        //TODO review to see if this method is needed
    }




    #region Buttons



    public void Button_ShowLogsForQuest(int _questId,string _questTitle)
    {
        radProfileUIManager.loadingPage.SetActive(true);

        logsListingContainer.transform.Find("Header").Find("questTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _questTitle;
        var _parent = logsListingContainer.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        for (int i = 0; i < _parent.childCount; i++)
        {
            _parent.GetChild(i).gameObject.SetActive(false);
        }


        BackendManager.Instance.GetStoriesForQuest(_questId);
    }

    public void Button_ShowLogsForNinja(int _ninjaID, string _ninjaName)
    {
        logsListingContainer.transform.Find("Header").Find("questTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _ninjaName;

        if (PostsManager.Instance.GetAllQuestsLogsByQuestID().ContainsKey(_ninjaID))
        {
            List<Rad_Model.Post_Log> _posts = PostsManager.Instance.GetAllQuestsLogsByQuestID()[_ninjaID];
            _posts.Sort((a, b) => b.timestamp.CompareTo(a.timestamp));
            PostsManager.Instance.SetLogListByQuestId(_ninjaID);

            for (int i = 0; i < _posts.Count; i++)
            {
                AssignButtonPostCalls(i, _posts);
            }
        }
    }

    public void AssignQuestStoriesButtons(string pumba)
    {

        List<Rad_Model.Post_Log> _posts = PostsManager.Instance.GetQuestStoriesList();

        for (int i = 0; i < _posts.Count; i++)
        {
            if (RadSaveManager.spriteListData.spriteData.ContainsKey(_posts[i].image1))
            {
                _posts[i].image1 = RadSaveManager.spriteListData.spriteData[_posts[i].image1];
            }
        }
        radProfileUIManager.loadingPage.SetActive(false);
        _posts.Sort((a, b) => b.timestamp.CompareTo(a.timestamp));

        for (int i = 0; i < _posts.Count; i++)
        {
            AssignButtonPostCalls(i, _posts);
        }
    }




    public void AssignButtonPostCalls(int index, List<Rad_Model.Post_Log> _posts)
    {
        Debug.Log("set button call");
        var _parent = logsListingContainer.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        GameObject _button = null;
        Debug.Log(_posts[index].postTitle);
        Debug.Log(_posts[index].has_image1);
        Debug.Log(_posts[index].url);
        if (_parent.childCount > index)
        {
            _button = _parent.GetChild(index).gameObject;
            _button.SetActive(true);
        }
        else
        {
            _button = Instantiate(postButtonPrefab, _parent);
        }

        if (_posts[index].has_image1)
        {

            _button.GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_posts[index].image1);
        }
        else
        {
            _button.GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_posts[index].story_cover_image);
        }

        _button.GetComponent<Button>().onClick.RemoveAllListeners();
        _button.GetComponent<Button>().onClick.AddListener(delegate { StartCoroutine(SetPostContent(_posts[index].id)); });
        _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _posts[index].postTitle;

        _button.GetComponent<Button>().onClick.AddListener(Button_ShowSinglePost);


        Debug.Log("set button call end");
    }

    private IEnumerator SetPostContent(int postId)
    {
        Rad_Model.Post_Log _log = new Rad_Model.Post_Log();
        for (int i = 0; i < PostsManager.Instance.GetQuestStoriesList().Count; i++)
        {
            if (PostsManager.Instance.GetQuestStoriesList()[i].id == postId)
            {
                _log = PostsManager.Instance.GetQuestStoriesList()[i];
                break;
            }
        }
        postLogs[0].transform.parent.Find("TitleText").GetComponent<TMPro.TextMeshProUGUI>().text = _log.postTitle;
        TMPro.TextMeshProUGUI _dateText = postLogs[0].transform.parent.Find("TitleText").Find("DateText").GetComponent<TMPro.TextMeshProUGUI>();
        System.DateTime date;
        System.DateTime.TryParse(_log.timestamp,out date);

        switch (date.Month)
        {
            case 1:
                _dateText.text = "jan ";
                break;
            case 2:
                _dateText.text = "feb ";
                break;
            case 3:
                _dateText.text = "mar ";
                break;
            case 4:
                _dateText.text = "apr ";
                break;
            case 5:
                _dateText.text = "may ";
                break;
            case 6:
                _dateText.text = "jun ";
                break;
            case 7:
                _dateText.text = "jul ";
                break;
            case 8:
                _dateText.text = "aug ";
                break;
            case 9:
                _dateText.text = "sep ";
                break;
            case 10:
                _dateText.text = "oct ";
                break;
            case 11:
                _dateText.text = "nov ";
                break;
            case 12:
                _dateText.text = "dec ";
                break;
        }
        _dateText.text += date.Day + ", " + date.Year;




        if (_log.has_image1)
        {
            postImages[0].sprite = BackendManager.Instance.DecodeBase64Texture(_log.image1);

        }
        else
        {
            postImages[0].sprite = QuestManagerV2.Instance.defaultQuestCoverImage[0];
        }
        postImages[0].preserveAspect = true;

        if (!string.IsNullOrEmpty(_log.postContent1))
        {
            postLogs[0].text = _log.postContent1;
            postLogs[0].gameObject.SetActive(true);
        }

        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate(singlePostContainer.GetComponent<RectTransform>());
    }

    public void Button_EditLog()
    {
        if (PostsManager.Instance != null && QuestManagerV2.Instance != null)
        {
            var _title = singlePersonalPostContainer.transform.Find("Header").Find("postTitle").GetComponent<TMPro.TMP_InputField>();
            var _content = singlePersonalPostContainer.transform.Find("postContent").GetComponentInChildren<TMPro.TMP_InputField>();
            var _privacy = singlePersonalPostContainer.transform.Find("PrivacyText").GetComponentInChildren<TMPro.TMP_Dropdown>();

            PostsManager.Instance.ModifyPostLog(_title.text, _content.text,_privacy.options[_privacy.value].text);
        }
    }

    public void Button_ShowLogsListing()
    {
        logsListingContainer.SetActive(true);
        MovePanel_In(logsListingRect);
        radProfileUIManager.onLogListing = true;
    }


    public void Button_ShowSinglePost()
    {
        singlePostContainer.SetActive(true);
        MovePanel_In(singlePostRect);
        radProfileUIManager.onLogListing = true;
    }

    public void Button_ShowSiglePersonalLog()
    {
        singlePersonalPostContainer.SetActive(true);
        MovePanel_In(singlePersonalPostRect);
        radProfileUIManager.onLogListing = true;
    }

    public void Button_BacktoPostLogs()
    {
        MovePanel_Out_Right(logsListingRect);
        radProfileUIManager.onLogListing = false;
    }


    public void Button_BacktoListing()
    {
        MovePanel_Out_Right(singlePostRect);
        MovePanel_Out_Right(singlePersonalPostRect);
    }

    #endregion
    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(0, panel.transform.localPosition.y), 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, panel.transform.localPosition.y), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, panel.transform.localPosition.y), 0.25f); //To left position
    }

    #endregion ===============================================================================================

}
