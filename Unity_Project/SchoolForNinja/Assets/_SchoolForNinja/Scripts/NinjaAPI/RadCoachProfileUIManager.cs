﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;
using TMPro;

public enum Panel { CoachProfile, Chat, Quests, Quest, Stories, Ninjas }
public class RadCoachProfileUIManager : MonoBehaviour
{
    #region Variables
    [BoxGroup("Containers")] public Canvas mainCanvas;
    [BoxGroup("Containers")] public GameObject ProfileChatLogsScreenContainer;
    [BoxGroup("Containers")] public GameObject profileContainer;
    [BoxGroup("Containers")] public GameObject chatContainer;
    [BoxGroup("Containers")] public GameObject storiesContainer;
    [BoxGroup("Containers")] public GameObject storiesContentContainer;
    [BoxGroup("Containers")] public GameObject questsContainer;
    [BoxGroup("Containers")] public GameObject questsContentContainer;
    [BoxGroup("Containers")] public GameObject questContainer;
    [BoxGroup("Containers")] public GameObject questContentContainer;
    [BoxGroup("Containers")] public GameObject coachNinjasContainer;
    [BoxGroup("Containers")] public GameObject coachNinjasContent;
    [BoxGroup("Quest Content Prefab")] public GameObject questContentPrefab;

    [BoxGroup("SecondaryContainers")] public RectTransform logsListingContainer;
    [BoxGroup("SecondaryContainers")] public RectTransform singlePostContainer;

    [BoxGroup("Profile Data")] public TMPro.TextMeshProUGUI coachName;
    [BoxGroup("Profile Data")] public Image coachAvatar;
    [BoxGroup("Profile Data")] public TMPro.TMP_InputField editNameInputField;
    [BoxGroup("Profile Data")] public TMPro.TMP_InputField editDescriptionInputField;
    [BoxGroup("Profile Data")] public TMPro.TextMeshProUGUI coachDescription;

    [BoxGroup("Button Prefab")] public GameObject buttonQuestPrefab;

    [BoxGroup("Profile Logs Prefab")] public GameObject logPrefab;

    [BoxGroup("Back Button TopMenu")] public string next_scene_name;

    [BoxGroup("Top Menu Buttons")] public Button questsButton;

    [HideInInspector]
    public bool onLogListing = false;

    private Rad_Model radModel;
    public Rad_PostLogsCoachUIManager storiesManager;
    private bool ninjasInstantiated = false;
    private bool questsInstantiated = false;

    private List<Rad_Model.Quest> allQuests = new List<Rad_Model.Quest>();
    private List<Rad_Model.Quest_Page> questPages = new List<Rad_Model.Quest_Page>();
    
    #endregion

    void Start()
    {
        //Set default panel as CoachProfile
        EnablePanel(Panel.CoachProfile);
        questsButton.onClick.AddListener(Button_ShowAllQuests);
        questsInstantiated = false;
        
        //BackendManager.Instance.GetQuests();

        if (BackendManager.Instance != null)
        {
            radModel = BackendManager.Instance.radModel;
            coachName.text = radModel.userCoach.coach_name;   
            //set the avatar size correctly
            coachAvatar.preserveAspect = true;
            coachAvatar.rectTransform.sizeDelta = new Vector2(357, 357);
            coachAvatar.sprite = BackendManager.Instance.DecodeBase64Texture(radModel.userCoach.coach_avatar);
            editDescriptionInputField.text = BackendManager.Instance.radModel.userCoach.about_me;
            coachDescription.text = BackendManager.Instance.radModel.userCoach.about_me;


            InvokeRepeating("RefreshPosts", 0, 60);
        }
        else
        {
            Debug.LogError("No hay BackendManager HERMANOOOO");
        }

    }
    
    private void RefreshPosts()
    {
        PostsManager.Instance.ClearAllPostsLists(true);
        BackendManager.Instance.GetStoriesForCoaches();
        BackendManager.Instance.GetStories(true);
    }

    public void Button_ShowChat()
    {
        EnablePanel(Panel.Chat);
    }

    public void Button_ShowAllQuests()
    {
        Transform _parent = questsContentContainer.transform;
        EnablePanel(Panel.Quests);
        allQuests = QuestManagerV2.Instance.GetQuestsList();

        if (!questsInstantiated)
        {
            //Instantiate all quests prefabs and assign title, picture and button listener
            for (int i = 0; i < allQuests.Count; i++)
            {
                string quest_title = "";
                GameObject _button = null;
                int id = 0;
                _button = Instantiate(buttonQuestPrefab, _parent);
                _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = allQuests[i].quest_title;
                //_button.GetComponent<Button>().GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(allQuests[i].quest_cover_image);
                _button.GetComponent<Button>().onClick.RemoveAllListeners();
                //Assign iteration values (id, quest_title) to pass them on Listener, otherwise we get an error.
                id = allQuests[i].id;
                quest_title = allQuests[i].quest_title;
                _button.GetComponent<Button>().onClick.AddListener(() => { Button_ShowQuestContent(id, quest_title); });
            }
            
        }
        //We instantiated all quests with all the required info, no need to do it anymore.
        questsInstantiated = true; 
    }
    /// <summary>
    /// Given a Quest ID and a Quest Title to gather and display the Quest Content, in this case the pages
    /// that we have to gather from QuestManagerV2 
    /// </summary>
    /// <param name="_questId">The Quest ID as int</param>
    /// <param name="_questTitle">The Quest Title as string</param>
    public void Button_ShowQuestContent(int _questId, string _questTitle)
    {
        EnablePanel(Panel.Quest);
        questContainer.transform.Find("Header").Find("QuestTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _questTitle;
        BackendManager.Instance.GetPages(_questId, GetPagesFailed);
    }
    /// <summary>
    /// Called when Button_ShowQuestContent succeed
    /// </summary>
    public IEnumerator GetPagesSucceed()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)questContentPrefab.transform);
        //Get the Quest Pages
        questPages = QuestManagerV2.Instance.GetQuestPagesList();
        //Instantiate the quest content prefab that will hold the pages information
        //Instantiate(questContentPrefab, questContentContainer.transform);
        QuestContentController questContentController = questContentPrefab.GetComponent<QuestContentController>();
        Rad_Model.Quest_Page _infopage = new Rad_Model.Quest_Page();
        int arrayIndex = 0;
        for (int i = 0; i < questPages.Count; i++)
        {
            if (questPages[i].is_info_page)
            {
                _infopage = questPages[i];
            }
            else
            {
                if (questPages[i].has_image)
                {
                    if (!questContentController.imagesArray[arrayIndex].gameObject.activeInHierarchy)
                    {
                        questContentController.imagesArray[arrayIndex].gameObject.SetActive(true);
                    }
                    if (questContentController.imagesArray[arrayIndex].sprite != null)
                    {
                        questContentController.imagesArray[arrayIndex].sprite = null;
                    }
                    Sprite _sprite = BackendManager.Instance.DecodeBase64Texture(questPages[i].image);
                    questContentController.imagesArray[arrayIndex].sprite = _sprite;
                    questContentController.imagesArray[arrayIndex].preserveAspect = true;
                }
                else if (!questPages[i].has_image)
                {
                    questContentController.imagesArray[arrayIndex].gameObject.SetActive(false);
                }


                questContentController.textsArray[arrayIndex].text = " ";
                questContentController.textsArray[arrayIndex].text = questPages[i].description_text + " \n " + questPages[i].question_text + "\n\n\n";

                arrayIndex++;
            }
        }

        questContentController.textsArray[arrayIndex].text = _infopage.description_text; ;
        yield return null;
        LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)questContentPrefab.transform);
    }
    /// <summary>
    /// Called wehn Button_ShowQuestContent failed
    /// </summary>
    public void GetPagesFailed()
    {
        Debug.LogError("Button_ShowQuestContent didn't gather the quest pages.");
    }

    /// <summary>
    /// Show all the Ninja Stories from a given Quest
    /// </summary>
    /// <param name="_questId">Quest ID</param>
    /// <param name="_questTitle">Quest title</param>
    public void Button_ShowQuestStories(int _questId, string _questTitle)
    {
        EnablePanel(Panel.Stories);
        logsListingContainer.transform.Find("Header").Find("questTitle").GetComponent<TMPro.TextMeshProUGUI>().text = _questTitle;

        if (PostsManager.Instance.GetAllQuestsLogsByQuestID().ContainsKey(_questId))
        {
            List<Rad_Model.Post_Log> _storyList = PostsManager.Instance.GetAllQuestsLogsByQuestID()[_questId];
            PostsManager.Instance.SetLogListByQuestId(_questId);

            Transform _parent = storiesContentContainer.transform;
            GameObject _button = null;
            for (int i = 0; i < _storyList.Count; i++)
            {
                _button = Instantiate(buttonQuestPrefab, _parent);
                _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _storyList[i].postTitle;
                _button.GetComponent<Button>().GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_storyList[i].story_cover_image);
                //_button.GetComponent<Button>().onClick.RemoveAllListeners();
                //AssignButtonPostCalls(i, _posts);
            }
        }
    }


    //public void Button_ShowNinjaStories(int _ninjaId, string _ninjaName)
    //{
    //    storiesContainer.transform.Find("Header").Find("Title").GetComponent<TMPro.TextMeshProUGUI>().text = _ninjaName;
    //    List<Rad_Model.Post_Log> _ninjaLogs = new List<Rad_Model.Post_Log>();
    //    if (PostsManager.Instance.GetAllQuestsLogsByNinjaID().ContainsKey(_ninjaId))
    //    {
    //        _ninjaLogs = PostsManager.Instance.GetAllQuestsLogsByNinjaID()[_ninjaId];
    //    }
    //    GameObject _button = null;
    //    Transform _parent = storiesContentContainer.transform;
    //    int _childCount = 0;
    //    for (int i = 0; i < _ninjaLogs.Count; i++)
    //    {
    //        _button = Instantiate(buttonQuestPrefab, _parent);
    //        _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _ninjaLogs[i].postTitle;
    //        _button.GetComponent<Button>().GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_ninjaLogs[i].story_cover_image);
    //        _button.GetComponent<Button>().onClick.RemoveAllListeners();
    //    }



    //}

    /// <summary>
    /// Region where calls from completing a quest, changing the profile image or NinjaName
    /// are grouped
    /// </summary>
    #region Refresh Profile Calls

    public void RefreshPostLogs()
    {

    }


    public void RefreshCoachName()
    {
        coachName.text = radModel.userCoach.coach_name;
    }


    public void RefreshNinjaAvatar()
    {

    }

    #endregion
    #region Buttons
    public void Button_Profile()
    {

        if (BackendManager.Instance != null)
        {
            if (!BackendManager.Instance.IsLogged())
            {
                Debug.LogError("User NOT LOGGED IN, something is smelly here...");
                return;
            }
        }
        else
        {
            Debug.LogError("No Instance of BackendManager, you should be logged in...");
        }


        //MovePanel_In(profileRect);
        //ninjaProfilePanel.SetActive(true);
        EnablePanel(Panel.CoachProfile);
    }
    /// <summary>
    /// Shows the profile when the Main Panel is already open
    /// </summary>
    public void Button_ProfileShow()
    {
        if (onLogListing)
        {
            CloseLogListing();
        }

        EnablePanel(Panel.CoachProfile);
    }
    //    /// <summary>
    ///// Shows the chat panel when the Main Panel is already open
    ///// </summary>
    //public void Button_ChatShow()
    //{
    //    if (onLogListing)
    //    {
    //        CloseLogListing();
    //    }
    //    EnablePanel(Panel.Chat);

    //    personalLogs = false;
    //    globalLogs = false;
    //}
        /// <summary>
    /// Shows all the assigned Ninjas a Coach have.
    /// </summary>
    public void Button_ShowNinjasFromCoach()
    {
        //Show list of ninjas related to your coach
        if (onLogListing)
        {
            CloseLogListing();
        }
        Transform _parent = coachNinjasContainer.transform.Find("Scroll View").GetComponent<ScrollRect>().content;
        GameObject _button = null;
        EnablePanel(Panel.Ninjas);

        for (int i = 0; i < BackendManager.Instance.radModel.userCoachNinjas.Count; i++)
        {
            if (!ninjasInstantiated)
            {
                Rad_Model.Ninja _ninja = BackendManager.Instance.radModel.userCoachNinjas[i];
                _button = Instantiate(buttonQuestPrefab, _parent);
                _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = BackendManager.Instance.radModel.userCoachNinjas[i].ninja_name;
                _button.GetComponent<Button>().GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(BackendManager.Instance.radModel.userCoachNinjas[i].avatar);
                _button.GetComponent<Button>().onClick.AddListener(delegate { storiesManager.Button_ShowLogsForNinja(_ninja.pk, _ninja.ninja_name); });
                _button.GetComponent<Button>().onClick.AddListener(delegate { EnablePanel(Panel.Stories); });
            }

        }
        ninjasInstantiated = true;
    }
    public void Button_EditCOachName()
    {
        coachName.gameObject.SetActive(false);
        editNameInputField.gameObject.SetActive(true);
    }
    public void Button_EditCoachDescription()
    {
        coachDescription.gameObject.SetActive(false);
        editDescriptionInputField.gameObject.SetActive(true);
    }
    public void Button_OnFinnishChangeName()
    {
        coachName.text = editNameInputField.text;
        coachName.gameObject.SetActive(true);
        editNameInputField.gameObject.SetActive(false);

        BackendManager.Instance.radModel.userCoach.coach_name = coachName.text;
        var json = JsonUtility.ToJson(BackendManager.Instance.radModel.userCoach);
        BackendManager.Instance.ModifyUserCoach(json);
    }
    public void Button_OnFinnishChangeDescription()
    {
        coachDescription.text = editDescriptionInputField.text;
        coachDescription.gameObject.SetActive(true);
        editDescriptionInputField.gameObject.SetActive(false);

        BackendManager.Instance.radModel.userCoach.about_me = editDescriptionInputField.text;
        BackendManager.Instance.radModel.userCoach.coach_name = coachName.text;
        var json = JsonUtility.ToJson(BackendManager.Instance.radModel.userCoach);
        BackendManager.Instance.ModifyUserCoach(json);
    }
    
    #endregion

    private void CloseLogListing()
    {
        singlePostContainer.gameObject.SetActive(false);
        logsListingContainer.gameObject.SetActive(false);
    }
    private void ShowLogListing()
    {
        MovePanel_In(singlePostContainer);
        MovePanel_In(logsListingContainer);
    }
    public void EnablePanel(Panel panel)
    {
        chatContainer.SetActive(false);
        storiesContainer.SetActive(false);
        profileContainer.SetActive(false);
        questsContainer.SetActive(false);
        questContainer.SetActive(false);
        coachNinjasContainer.SetActive(false);
        switch (panel)
        {
            case Panel.CoachProfile:
                profileContainer.SetActive(true);
                break;
            case Panel.Chat:
                chatContainer.SetActive(true);
                break;
            case Panel.Quests:
                questsContainer.SetActive(true);
                break;
            case Panel.Quest:
                questContainer.SetActive(true);
                break;
            case Panel.Stories:
                storiesContainer.SetActive(true);
                break;
            case Panel.Ninjas:
                coachNinjasContainer.SetActive(true);
                break;
            default:
                Debug.Log("Incorrect Panel enum value, you have to assign a valid Panel value when calling this method.");
                break;
        }
    }

    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, 0), 0.25f); //To left position
    }

    #endregion ===============================================================================================


}
