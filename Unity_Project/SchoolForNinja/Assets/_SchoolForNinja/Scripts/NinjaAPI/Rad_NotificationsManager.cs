﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif
#if UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class Rad_NotificationsManager : MonoBehaviour {

    static Rad_NotificationsManager instance;
    public static Rad_NotificationsManager Instance {  get { return instance; } }

    private List<int> _iosNotifications = new List<int>();
    private Dictionary<int, int> _activeNotifications = new Dictionary<int, int>();


    private void Awake()
    {
        if (!Instance)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private IEnumerator Start()
    {
#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllScheduledNotifications();
        CreateNotificationsChannel();
#endif
#if UNITY_IOS
        //the first time the user starts the app permission pop up appears,
        //if the permission is granted never ever again the pop up will show, in theory
        using (var req = new AuthorizationRequest(AuthorizationOption.AuthorizationOptionAlert | AuthorizationOption.AuthorizationOptionBadge, true))
        {
            while (!req.IsFinished)
            {
                yield return null;
            };

            string res = "\n RequestAuthorization: \n";
            res += "\n finished: " + req.IsFinished;
            res += "\n granted :  " + req.Granted;
            res += "\n error:  " + req.Error;
            res += "\n deviceToken:  " + req.DeviceToken;
            Debug.Log(res);
        }
        iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif
        yield return null;
    }
#if UNITY_ANDROID
    private void CreateNotificationsChannel()
    {
        var c = new AndroidNotificationChannel()
        {
            Id = "1",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
    }
#endif
    public void CreateNotification(int _id,string _title, string _description, System.DateTime _firetime)
    {
#if UNITY_ANDROID
        if (!_activeNotifications.ContainsKey(_id))
        {
            AndroidNotification _notification = new AndroidNotification
            {
                ShouldAutoCancel = true,
                Title = _title,
                Text = _description,
                FireTime = _firetime,
                RepeatInterval = new System.TimeSpan(12, 0, 0)
            };

            _activeNotifications.Add(_id, AndroidNotificationCenter.SendNotification(_notification, "1"));
        }
#endif

#if UNITY_IOS
        var notification = new iOSNotification()
        {
            Title = _title,
            Body = _description,
            Subtitle = "",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.NotificationPresentationOptionAlert | PresentationOption.NotificationPresentationOptionSound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1"
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif
    }

    public void CreateNotification(int _id,string _title, string _description,string _largeIcon, System.DateTime _firetime)
    {
#if UNITY_ANDROID
        if (!_activeNotifications.ContainsKey(_id))
        {
            AndroidNotification _notification = new AndroidNotification
            {
                ShouldAutoCancel = true,
                Title = _title,
                Text = _description,
                FireTime = _firetime,
                LargeIcon = _largeIcon,
                RepeatInterval = new System.TimeSpan(12, 0, 0)
            };
            _activeNotifications.Add(_id, AndroidNotificationCenter.SendNotification(_notification, "1"));
        }
#endif

#if UNITY_IOS
        var timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new System.TimeSpan(150,0,0),
            Repeats = false
        };
        var notification = new iOSNotification()
        {
            Title = _title,
            Body = _description,
            Subtitle = "",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.NotificationPresentationOptionAlert | PresentationOption.NotificationPresentationOptionSound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif
    }

    private void WeeklyReminder()
    {
#if UNITY_ANDROID 
            AndroidNotification _notification = new AndroidNotification
            {
                ShouldAutoCancel = true,
                Title = "Ha Ninja, heb je deze week al gespeeld?",
                Text = "Ha Ninja, heb je deze week al gespeeld?",
                FireTime = System.DateTime.Now.AddDays(7),
                LargeIcon = "fase1",
            };
            AndroidNotificationCenter.SendNotification(_notification, "1");
#endif

#if UNITY_IOS
        var timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new System.TimeSpan(150,0,0),
            Repeats = false
        };
        var notification = new iOSNotification()
        {
            Title = "Ha Ninja, heb je deze week al gespeeld?",
            Body = "Ha Ninja, heb je deze week al gespeeld?",
            Subtitle = "",
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.NotificationPresentationOptionAlert | PresentationOption.NotificationPresentationOptionSound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger
        };

        iOSNotificationCenter.ScheduleNotification(notification);
#endif
    }

    public void RemoveNotification(int id)
    {
#if UNITY_ANDROID
        if (_activeNotifications.ContainsKey(id))
        {
            AndroidNotificationCenter.CancelNotification(_activeNotifications[id]);
            _activeNotifications.Remove(id);
        }
#endif
#if UNITY_IOS
                
#endif
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
#if UNITY_ANDROID
            AndroidNotificationCenter.CancelAllScheduledNotifications();
#endif

#if UNITY_IOS
            iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif
        }
        else
        {
            WeeklyReminder();
        }
    }

    private void OnApplicationQuit()
    {
#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllScheduledNotifications();
#endif

#if UNITY_IOS
        iOSNotificationCenter.RemoveAllScheduledNotifications();
#endif

        WeeklyReminder();
    }

}
