﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using DG.Tweening;
using Com.LuisPedroFonseca.ProCamera2D;

public class PathsManagerFase3 : PathsManager
{

    public enum TeleportSpots { BoatWindow, BoatEntry, BoatExit, LandExit}

    [BoxGroup("Colliders")] public GameObject[] colliders;
    [BoxGroup("Colliders")] public GameObject[] boatColliders;

    [BoxGroup("Otter")] public Animator otter;
    [BoxGroup("Otter")] public Transform swimPoint;

    [BoxGroup("Sea orchins")] public Transform[] seaOrchin;
    [BoxGroup("Sea orchins")] public GameObject[] seaOrchinPink;
    [BoxGroup("Sea orchins")] public GameObject greenPlant;
    [BoxGroup("Sea orchins")] public GameObject pinkPlant;

    [BoxGroup("Octopus")] public GameObject octopus;
    [BoxGroup("Octopus")] public Transform octopusTp;

    [BoxGroup("Whale")] public GameObject pinkWhale;
    [BoxGroup("Whale")] public GameObject greenWhale;
    [BoxGroup("Whale")] public GameObject whaleWater;
    [BoxGroup("Whale")] public GameObject waves;
    [BoxGroup("Whale")] public WhaleAnimation wavesAnimation;

    [BoxGroup("Boat Octopus")] public GameObject greenOctopus;
    [BoxGroup("Boat Octopus")] public GameObject pinkOctopus;

    [BoxGroup("Boat rudder")] public GameObject rudder;

    [BoxGroup("crab")] public GameObject greenCrab;
    [BoxGroup("crab")] public GameObject pinkCrab;
    [BoxGroup("crab")] public GameObject greenCannon;
    [BoxGroup("crab")] public GameObject pinkCannon;

    [BoxGroup("Portal")] public SpriteRenderer portal;
    [BoxGroup("Portal")] public GameObject endTrigger;

    [BoxGroup("Teleport Spots")] public Transform boatWindow;
    [BoxGroup("Teleport Spots")] public Transform boatEntry;
    [BoxGroup("Teleport Spots")] public Transform boatExit;

    [BoxGroup("Outside Camera Stats")] public float topBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float botBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float leftBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float rightBoundaryOutside;
    [BoxGroup("Outside Camera Stats")] public float cameraSizeOutside;

    [BoxGroup("Boat Camera Stats")] public float topBoundaryCave;
    [BoxGroup("Boat Camera Stats")] public float botBoundaryCave;
    [BoxGroup("Boat Camera Stats")] public float leftBoundaryCave;
    [BoxGroup("Boat Camera Stats")] public float rightBoundaryCave;
    [BoxGroup("Boat Camera Stats")] public float cameraSizeCave;

    private bool[] questCompleted = new bool[8];

    private int lastQuestCompleted = 0;
    public RadEndPhase _endPhase;
    public override void Start()
    {
        base.Start();
    }

    public override void QuestCompleted(int id)
    {
        switch (id)
        {
            case 1:

                otter.GetComponent<OtterAnimation>().questCompleted = true;
                questCompleted[0] = true;
                EnableQuestSprite(quest[0].gameObject);
                break;
            case 2:
                colliders[0].SetActive(false);
                colliders[1].SetActive(true);
                questCompleted[1] = true;
                greenPlant.SetActive(false);
                pinkPlant.SetActive(true);
                ScaleSeaOrchins();
                colliders[1].SetActive(false);

                if(lastQuestCompleted < id)
                    colliders[2].SetActive(true);

                if(questCompleted[2])
                    colliders[3].SetActive(false);

                if(id > lastQuestCompleted)
                {
                    player.transform.position = quest[1].transform.position;
                    movementController.dummyTarget.position = quest[1].transform.position;
                }
                player.GetComponentInChildren<Animator>().SetBool("Swimming", true);
                EnableQuestSprite(quest[1].gameObject);
                break;
            case 3:
                colliders[0].SetActive(false);
                colliders[1].SetActive(true);
                questCompleted[2] = true;
                colliders[1].SetActive(false);

                if (!questCompleted[1])
                    colliders[3].SetActive(true);

                player.GetComponentInChildren<Animator>().SetBool("Swimming", true);
                StartCoroutine(HideOctopus());
                EnableQuestSprite(quest[2].gameObject);
                break;
            case 4:
                pinkWhale.SetActive(true);
                greenWhale.SetActive(false);
                questCompleted[3] = true;

                whaleWater.SetActive(false);

                if (questCompleted[1])
                    colliders[2].SetActive(false);

                if (questCompleted[2])
                    colliders[3].SetActive(false);


                if (questCompleted[4])
                {
                    colliders[5].SetActive(false);
                    colliders[6].SetActive(true);
                }
                else
                {
                    colliders[4].SetActive(true);
                }

                if (id > lastQuestCompleted)
                {
                    player.transform.position = quest[3].transform.position;
                    movementController.dummyTarget.position = quest[3].transform.position;
                }
                EnableQuestSprite(quest[3].gameObject);
                break;
            case 5:
                waves.SetActive(false);
                questCompleted[4] = true;
                wavesAnimation.doAnimation = false;

                if (questCompleted[1])
                    colliders[2].SetActive(false);

                if (questCompleted[2])
                    colliders[3].SetActive(false);

                if (questCompleted[3])
                {
                    colliders[4].SetActive(false);
                    colliders[6].SetActive(true);
                }
                else
                {
                    colliders[5].SetActive(true);
                }
                if (id > lastQuestCompleted)
                {
                    player.transform.position = quest[4].transform.position;
                    movementController.dummyTarget.position = quest[4].transform.position;
                }
                EnableQuestSprite(quest[4].gameObject);
                break;
            case 6:

                questCompleted[5] = true;
                boatColliders[0].SetActive(false);

                if (questCompleted[6])
                {
                    boatColliders[2].SetActive(false);
                    boatColliders[3].SetActive(true);
                }
                else
                {
                    boatColliders[1].SetActive(true);
                }
                SetBoatCameraSettings();
                greenOctopus.SetActive(false);
                pinkOctopus.SetActive(true);

                rudder.transform.DOLocalRotate(new Vector3(0,0, 1035), 2);
                if (id > lastQuestCompleted)
                {
                    player.transform.position = quest[5].transform.position;
                    movementController.dummyTarget.position = quest[5].transform.position;
                }
                EnableQuestSprite(quest[5].gameObject);
                break;

            case 7:

                questCompleted[6] = true;
                boatColliders[0].SetActive(false);

                if (questCompleted[6])
                {
                    boatColliders[1].SetActive(false);
                    boatColliders[3].SetActive(true);
                }
                else
                
                {
                    boatColliders[2].SetActive(true);
                }

                SetBoatCameraSettings();
                greenCannon.SetActive(false);
                greenCrab.SetActive(false);

                pinkCannon.SetActive(true);
                pinkCrab.SetActive(true);
                if (id > lastQuestCompleted)
                {
                    player.transform.position = quest[6].transform.position;
                    movementController.dummyTarget.position = quest[6].transform.position;
                }
                EnableQuestSprite(quest[6].gameObject);
                break;
            case 8:
                player.GetComponentInChildren<Animator>().SetBool("Swimming", false);
                _endPhase.EnableEndPhase = true;
                SetColliders(7);
                SetOutsideCameraSettings();
                questCompleted[7] = true;
                endTrigger.SetActive(true);
                portal.DOFade(1, 2);
                if (id > lastQuestCompleted)
                {
                    player.transform.position = quest[7].transform.position;
                    movementController.dummyTarget.position = quest[7].transform.position;
                }
                EnableQuestSprite(quest[7].gameObject);
                break;
        }
        if (id > lastQuestCompleted) lastQuestCompleted = id;
        pathScan.Scan();
    }



    private IEnumerator PlayerStartSwim()
    {
        colliders[0].SetActive(false);
        colliders[1].SetActive(true);
        pathScan.Scan();
        player.GetComponentInChildren<SpriteRenderer>().DOFade(0, .25f);
        movementController.dummyTarget.position = swimPoint.position;
        player.transform.position = swimPoint.position;
        yield return new WaitForSeconds(2);

        player.GetComponentInChildren<Animator>().SetBool("Swimming", true);
        player.GetComponentInChildren<SpriteRenderer>().DOFade(1, 1);
    }

    public void MovePlayerTo(Vector3 _pos)
    {
        movementController.dummyTarget.position = _pos;
        player.transform.position = _pos;
    }
    private void ScaleSeaOrchins()
    {
        for (int i = 0; i < seaOrchin.Length; i++)
        {
            seaOrchin[i].DOScale(0, 2);
        }

        for (int i = 0; i < seaOrchinPink.Length; i++)
        {
            seaOrchinPink[i].SetActive(true);
        }
    }

    private IEnumerator HideOctopus()
    {
        octopus.GetComponentInChildren<Animator>().SetTrigger("Hide");
        player.GetComponentInChildren<SpriteRenderer>().DOFade(0, .25f);
        movementController.dummyTarget.position = octopusTp.position;
        player.transform.position = octopusTp.position;
        yield return new WaitForSeconds(0.5f);
        octopus.SetActive(false);

        player.GetComponentInChildren<SpriteRenderer>().DOFade(1, 1);

    }

    public void MovePlayerTo(TeleportSpots _spot)
    {
        switch (_spot)
        {
            case TeleportSpots.BoatWindow:
                SetOutsideCameraSettings();
                player.transform.position = boatWindow.position;
                break;
            case TeleportSpots.BoatEntry:
                SetBoatCameraSettings();
                player.transform.position = boatEntry.position;
                break;
            case TeleportSpots.BoatExit:
                player.GetComponentInChildren<Animator>().SetBool("Swimming", true);
                SetBoatCameraSettings();
                player.transform.position = boatExit.position;
                break;
            case TeleportSpots.LandExit:
                player.GetComponentInChildren<Animator>().SetBool("Swimming", false);
                SetColliders(7);
                SetOutsideCameraSettings();
                player.transform.position = quest[7].transform.position;
                break;
        }
        player.GetComponent<Pathfinding.AIDestinationSetter>().target.position = player.transform.position;
    }


    public void OtterTrigger()
    {
        otter.SetTrigger("Underwater");
        StartCoroutine(PlayerStartSwim());
    }


    private void SetColliders(int j)
    {
        for (int i = 0; i < colliders.Length; i++)
        {
            if (i == j) colliders[i].SetActive(true);
            else colliders[i].SetActive(false);
        }

        pathScan.Scan();
    }

    private void SetBoatCameraSettings()
    {
        Camera.main.orthographicSize = cameraSizeCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().TopBoundary = topBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().RightBoundary = rightBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().LeftBoundary = leftBoundaryCave;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().BottomBoundary = botBoundaryCave;
    }

    private void SetOutsideCameraSettings()
    {
        Camera.main.orthographicSize = cameraSizeOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().TopBoundary = topBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().RightBoundary = rightBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().LeftBoundary = leftBoundaryOutside;
        Camera.main.GetComponent<ProCamera2DNumericBoundaries>().BottomBoundary = botBoundaryOutside;
    }
}
