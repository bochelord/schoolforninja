﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.SceneManagement;

public class RadProfileUIManager : MonoBehaviour {

    [BoxGroup("Containers")] public GameObject ProfileChatLogsScreenContainer;
    [BoxGroup("Containers")] public GameObject profileContainer;
    [BoxGroup("Containers")] public GameObject chatContainer;
    [BoxGroup("Containers")] public GameObject logsContainer;
    [BoxGroup("Containers")] public GameObject timelineContainer;

    [BoxGroup("SecondaryContainers")] public RectTransform logsListingContainer;
    [BoxGroup("SecondaryContainers")] public RectTransform singlePostContainer;
    [BoxGroup("SecondaryContainers")] public RectTransform singlePersonalPostContainer;

    [BoxGroup("SinglePersonalPostContainer")] public Image[] personalpostImages;
    [BoxGroup("SinglePersonalPostContainer")] public TMPro.TextMeshProUGUI[] personalpostLogs;
    [BoxGroup("SinglePersonalPostContainer")] public TMPro.TextMeshProUGUI personalStoryTitle;
    [BoxGroup("SinglePersonalPostContainer")] public TMPro.TMP_Dropdown personalPrivacyDropDown;
    [BoxGroup("SinglePersonalPostContainer")] public TMPro.TMP_InputField editStoryContentInputfield;
    [BoxGroup("SinglePersonalPostContainer")] public TMPro.TMP_InputField editStoryTitleInputfield;


    [BoxGroup("Panels")] public GameObject menuBurgerPanel;
    [BoxGroup("Panels")] public GameObject translucidPanel;

    [BoxGroup("Profile Data")] public TMPro.TextMeshProUGUI ninjaName;
    [BoxGroup("Profile Data")] public Image ninjaAvatar;
    [BoxGroup("Profile Data")] public TMPro.TMP_InputField editNameInputField;
    [BoxGroup("Profile Data")] public GameObject ninjaNameUsedWarning;

    [BoxGroup("No Stories Text")] public GameObject profileNoStoriesText;
    [BoxGroup("No Stories Text")] public GameObject questNoStoriesText;

    [BoxGroup("YoutubePlayer")] public YoutubePlayer coachVidedo;

    [BoxGroup("Button Prefab")] public GameObject buttonQuestPrefab;

    [BoxGroup("Profile Logs Prefab")] public GameObject logPrefab;

    [BoxGroup("Back Button TopMenu")] public string next_scene_name;

    [BoxGroup("Top Buttons")] public Color highlightedColor;
    [BoxGroup("Top Buttons")] public Image chatButtonImage;
    [BoxGroup("Top Buttons")] public Image profileButtonImage;
    [BoxGroup("Top Buttons")] public Image logsButtonImage;

    [BoxGroup("Badges")] public GameObject[] badges;

    [BoxGroup("LoadingPage")] public GameObject loadingPage;

    [HideInInspector]
    public bool onLogListing = false;

    private List<Rad_Model.Action> _actionsList = new List<Rad_Model.Action>();

    private RectTransform profileRect;

    private Rad_Model radModel;

    private PlayerMovementController playerMovementCtr;
    private bool onWorldSpace = false;
    private Rad_PostLogsUIManager logsManager;
    private int firstQuestId = 11; //we need to store this value, since this quest it's about chaning your ninja name and won't have a log
    private bool personalLogs = false;
    private bool globalLogs = false;
    private bool coachVideoOn = false;
    void Start()
    {
        BackendManager.Instance.GetPhaseStatusSuccess += OnGetPhaseStatusSuccess;
        profileRect = ProfileChatLogsScreenContainer.GetComponent<RectTransform>();
        logsManager = logsContainer.GetComponent<Rad_PostLogsUIManager>();
        
        if (BackendManager.Instance != null)
        {
            radModel = BackendManager.Instance.radModel;
            if (radModel.ninja.ninja_name.Contains("ninja_default"))
            {
                ninjaName.text = "Nieuwe Ninja";
            }
            else
            {
                ninjaName.text = radModel.ninja.ninja_name;
            }
            
            //set the avatar size correctly
            ninjaAvatar.preserveAspect = true;
            ninjaAvatar.rectTransform.sizeDelta = new Vector2(357, 357);
            ninjaAvatar.sprite = BackendManager.Instance.DecodeBase64Texture(radModel.ninja.avatar);

        }
        else
        {
            Debug.LogError("No hay BackendManager HERMANOOOO");
        }

        //We look for PlayerMovementController that is the code managing the movement 
        //on the WorldSpace scene (so the map inside a phase)
        //If we have it , we need to disable the player movement whenever the profile overlay is on

        playerMovementCtr = FindObjectOfType<PlayerMovementController>();

        if(playerMovementCtr)
            onWorldSpace = true;


        PersonalLogsShow();
        CheckAchievements();

    }

    /// <summary>
    /// Region where calls from completing a quest, changing the profile image or NinjaName
    /// are grouped
    /// </summary>
    #region Refresh Profile Calls

    public void RefreshPostLogs() //TODO
    {

    }


    public void RefreshNinjaName()
    {
        if (radModel.ninja.ninja_name.Contains("ninja_default"))
        {
            ninjaName.text = "Nieuwe Ninja";
        }
        else
        {
            ninjaName.text = radModel.ninja.ninja_name;
        }
        
    }


    public void RefreshNinjaAvatar() //TODO
    {

    }
    
    #endregion



    #region Buttons

    public void Button_Profile()
    {
        SetHighlightColor(profileButtonImage);

        BackendManager.Instance.GetMyCoach();

        if (onWorldSpace)
        {
            //we block the player movement if the profile overlay is on
            playerMovementCtr.canMove = false;
        }

        menuBurgerPanel.SetActive(false);
        translucidPanel.SetActive(true);

        MovePanel_In(profileRect);
        //ninjaProfilePanel.SetActive(true);
    }

    public void Button_PlayCoachVideo()
    {
        coachVideoOn = true;
        coachVidedo.transform.parent.parent.gameObject.SetActive(true);
    }

    public void Button_CloseCoachVideo()
    {
        coachVideoOn = false;
        coachVidedo.transform.parent.parent.gameObject.SetActive(false);
    }

    public void Button_ClosePanel()
    {

        //ninjaProfilePanel.SetActive(false);
        MovePanel_Out_Right(profileRect);
        menuBurgerPanel.SetActive(true);
        translucidPanel.SetActive(false);
        personalLogs = false;
        globalLogs = false;
        if (onWorldSpace)
        {
            StartCoroutine(Coro_AllowMovement());
        }

    }





    /// <summary>
    /// Shows the profile when the Main Panel is already open
    /// </summary>
    public void Button_ProfileShow()
    {
        SetHighlightColor(profileButtonImage);
        if (onLogListing)
        {
            CloseLogListing();
        }
        if (personalLogs)
        {
            MovePanel_Out_Right(singlePersonalPostContainer);
        }

        chatContainer.gameObject.SetActive(false);
        logsContainer.gameObject.SetActive(false);
        profileContainer.gameObject.SetActive(true);
        
        personalLogs = false;
        globalLogs = false;
    }

    /// <summary>
    /// Shows the chat panel when the Main Panel is already open
    /// </summary>
    public void Button_ChatShow()
    {
        SetHighlightColor(chatButtonImage);
        if (personalLogs)
        {
            MovePanel_Out_Right(singlePersonalPostContainer);
        }
        if (onLogListing)
        {
            CloseLogListing();
        }

        if (coachVideoOn)
        {
            coachVideoOn = false;
            coachVidedo.transform.parent.parent.gameObject.SetActive(false);
        }

        chatContainer.gameObject.SetActive(true);
        logsContainer.gameObject.SetActive(false);
        profileContainer.gameObject.SetActive(false);
        personalLogs = false;
        globalLogs = false;
    }

    /// <summary>
    /// Shows the post Logs 
    /// </summary>
    public void Button_LogsShow()
    {


        SetHighlightColor(logsButtonImage);
        if (personalLogs)
        {
            MovePanel_Out_Right(singlePersonalPostContainer);
        }

        if (coachVideoOn)
        {
            coachVideoOn = false;
            coachVidedo.transform.parent.parent.gameObject.SetActive(false);
        }
        //CloseLogListing();
        if (onLogListing && globalLogs)
        {
            ShowLogListing();
        }
        else
        {
            CloseLogListing();
        }
        int _childs = 0;
        int _count = 0;
        var questStatusList = QuestManagerV2.Instance.GetQuestStatusList();
        for (int i = 0; i < questStatusList.Count; i++)
        {
            var str = questStatusList[i].belongs_to_quest.Split('/');
            int _questId = int.Parse(str[str.Length - 2]);

            if (questStatusList[i].completed && _questId != firstQuestId)
            {
                Transform _parent = logsContainer.transform.Find("LogsContainer").Find("Scroll View").GetComponent<ScrollRect>().content;
                Rad_Model.Quest _quest = QuestManagerV2.Instance.FindQuestWithUrl(questStatusList[i].belongs_to_quest);
                GameObject _button = null;
                if (_parent.childCount > _childs)
                {
                     _button = _parent.GetChild(_childs).gameObject;
                }
                else
                {
                     _button = Instantiate(buttonQuestPrefab, _parent);
                }
                _count++;
                _childs++;
                _button.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = _quest.quest_title;


                _button.GetComponent<Image>().sprite = BackendManager.Instance.DecodeBase64Texture(_quest.quest_cover_image);
                _button.GetComponent<Button>().onClick.RemoveAllListeners();
                _button.GetComponent<Button>().onClick.AddListener(delegate { logsManager.Button_ShowLogsForQuest(_quest.id, _quest.quest_title); });
                _button.GetComponent<Button>().onClick.AddListener(logsManager.Button_ShowLogsListing);
            }
        }

        if(_count == 0)
        {
            questNoStoriesText.SetActive(true);
        }
        else
        {
            questNoStoriesText.SetActive(false);
        }
        chatContainer.SetActive(false);
        logsContainer.SetActive(true);
        profileContainer.SetActive(false);
        globalLogs = true;
        personalLogs = false;
    }


    public void Button_EditNinjaName()
    {
        if (coachVideoOn)
        {
            coachVideoOn = false;
            coachVidedo.transform.parent.parent.gameObject.SetActive(false);
        }
        editNameInputField.text = ninjaName.text;
        ninjaName.gameObject.SetActive(false);
        editNameInputField.gameObject.SetActive(true);
    }


    public void Button_OnFinnishChangeName()
    {
        ninjaName.gameObject.SetActive(true);
        editNameInputField.gameObject.SetActive(false);
        BackendManager.Instance.CheckNinjaName(editNameInputField.text, OnEditNameSuccess,delegate { StartCoroutine(OnEditNameUsed()); }, OnEditNameFail);

    }
    
    private void OnEditNameSuccess()
    {
        ninjaName.text = editNameInputField.text;
        BackendManager.Instance.radModel.ninja.ninja_name = ninjaName.text;
        var json = JsonUtility.ToJson(BackendManager.Instance.radModel.ninja);
        BackendManager.Instance.ModifyNinja(json);
    }
    private IEnumerator OnEditNameUsed()
    {
        ninjaNameUsedWarning.SetActive(true);
        yield return new WaitForSeconds(3);
        ninjaNameUsedWarning.SetActive(false);
    }

    private void OnEditNameFail()
    {
        //TODO show no internet panel ?
    }
    public void PersonalLogsShow()
    {
        if (coachVideoOn)
        {
            coachVideoOn = false;
            coachVidedo.transform.parent.parent.gameObject.SetActive(false);
        }
        int _childs = 0;
        var _list = PostsManager.Instance.GetPersonalLogsList();
        _list.Sort((a, b) => b.timestamp.CompareTo(a.timestamp));

        if(_list.Count > 0)
        {
            profileNoStoriesText.SetActive(false);

            for (int i = 0; i < _list.Count; i++)
            {

                Rad_Model.Post_Log _Log = PostsManager.Instance.GetPersonalLogsList()[i];
                GameObject _button = null;
                if (timelineContainer.transform.childCount > _childs)
                {
                    _button = timelineContainer.transform.GetChild(_childs).gameObject;
                }
                else
                {
                    _button = Instantiate(logPrefab, timelineContainer.transform);
                }

                _childs++;
                _button.GetComponent<LogCard>().CreateCard(_Log);
                _button.GetComponent<Button>().onClick.RemoveAllListeners();
                _button.GetComponent<Button>().onClick.AddListener(delegate { SetPersonalPostContent(_Log.id); });
                _button.GetComponent<Button>().onClick.AddListener(Button_ShowSiglePersonalLog);
            }
        }
        else
        {
            profileNoStoriesText.SetActive(true);
        }
        
    }

    public void UpdateLogCard(Rad_Model.Post_Log post)
    {
        for (int i = 0; i < timelineContainer.transform.childCount; i++)
        {
            if(timelineContainer.transform.GetChild(i).GetComponent<LogCard>().postId == post.id)
            {
                timelineContainer.transform.GetChild(i).GetComponent<LogCard>().CreateCard(post);
                break;
            }
        }
    }

    #endregion


    private IEnumerator Coro_AllowMovement()
    {
        yield return new WaitForSeconds(0.25f);
        if (onWorldSpace)
        {
            playerMovementCtr.canMove = true;
        }
    }

    private void CloseLogListing()
    {
        personalLogs = false;
        globalLogs = false;
        MovePanel_Out_Right(singlePostContainer);
        MovePanel_Out_Right(logsListingContainer);
        MovePanel_Out_Right(singlePostContainer);
    }

    private void ShowLogListing()
    {
        if (personalLogs)
        {
            MovePanel_Out_Right(singlePersonalPostContainer);
        }
        MovePanel_In(singlePostContainer);
        MovePanel_In(logsListingContainer);
        MovePanel_Out_Right(singlePersonalPostContainer);
    }
    public void BUTTON_MediaPicker(int _image)
    {
        NativeGallery.GetImageFromGallery((path) =>
        {
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, -1, false);
                if (texture == null)
                {
                    return;
                }

                Sprite spriteTemp = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                personalpostImages[_image].sprite = spriteTemp;
                personalpostImages[_image].gameObject.SetActive(true);
                personalpostImages[_image].preserveAspect = true;

            }
        }, "Select an image", "image/jpeg",512);

    }

    public void BUTTON_DeleteImage(int _image)
    {
        personalpostImages[_image].sprite = null;
        personalpostImages[_image].gameObject.SetActive(false);
    }


    public void Button_EditLogContent()
    {
        editStoryContentInputfield.text = personalpostLogs[0].text;
        editStoryContentInputfield.gameObject.SetActive(true);
        personalpostLogs[0].gameObject.SetActive(false);
    }
    public void Button_OnEndEditStoryContent()
    {
        personalpostLogs[0].text = editStoryContentInputfield.text;
        editStoryContentInputfield.gameObject.SetActive(false);
        personalpostLogs[0].gameObject.SetActive(true);
    }
    
    public void button_EditStoryTitle()
    {
        editStoryTitleInputfield.text = personalStoryTitle.text;
        editStoryTitleInputfield.gameObject.SetActive(true);
        personalStoryTitle.gameObject.SetActive(false);
    }

    public void Button_OnEndEditStoryTitle()
    {
        personalStoryTitle.text = editStoryTitleInputfield.text;
        editStoryTitleInputfield.gameObject.SetActive(false);
        personalStoryTitle.gameObject.SetActive(true);
    }

    private void SetPersonalPostContent(int _logId)
    {
        Rad_Model.Post_Log _log = new Rad_Model.Post_Log();
        for (int i = 0; i < PostsManager.Instance.GetPersonalLogsList().Count; i++)
        {
            if(PostsManager.Instance.GetPersonalLogsList()[i].id == _logId)
            {
                _log = PostsManager.Instance.GetPersonalLogsList()[i];
                break;
            }
        }
        PostsManager.Instance.SetCurrentPost(_log);
        //title
        personalStoryTitle.text = _log.postTitle;
        TMPro.TextMeshProUGUI _dateText = personalStoryTitle.transform.Find("DateText").GetComponent<TMPro.TextMeshProUGUI>();
        var date = System.DateTime.Parse(_log.timestamp);
        switch (date.Month)
        {
            case 1:
                _dateText.text = "jan ";
                break;
            case 2:
                _dateText.text = "feb ";
                break;
            case 3:
                _dateText.text = "mar ";
                break;
            case 4:
                _dateText.text = "apr ";
                break;
            case 5:
                _dateText.text = "may ";
                break;
            case 6:
                _dateText.text = "jun ";
                break;
            case 7:
                _dateText.text = "jul ";
                break;
            case 8:
                _dateText.text = "aug ";
                break;
            case 9:
                _dateText.text = "sep ";
                break;
            case 10:
                _dateText.text = "oct ";
                break;
            case 11:
                _dateText.text = "nov ";
                break;
            case 12:
                _dateText.text = "dec ";
                break;
        }

        _dateText.text += date.Day + ", " + date.Year;

        if (_log.has_image1)
        {
            personalpostImages[0].sprite = BackendManager.Instance.DecodeBase64Texture(_log.image1);

        }
        else
        {
            personalpostImages[0].sprite = BackendManager.Instance.DecodeBase64Texture(_log.story_cover_image);
        }
        personalpostImages[0].preserveAspect = true;

        if (!string.IsNullOrEmpty(_log.postContent1))
        {
            personalpostLogs[0].text = _log.postContent1;
        }

        if (_log.privacy.Contains("only_me"))
        {
            personalPrivacyDropDown.value = 0;
        }
        else if (_log.privacy.Contains("only_coach"))
        {
            personalPrivacyDropDown.value = 1;
        }
        else if (_log.privacy.Contains("everyone"))
        {
            personalPrivacyDropDown.value = 2;
        }
        else if (_log.privacy.Contains("everyone_outside"))
        {
            personalPrivacyDropDown.value = 3;
        }

        personalLogs = true;
    }

    public void Button_ClosePersonalLog()
    {
        TMPro.TMP_Dropdown _privacy = personalPrivacyDropDown;
        string ninja = BackendManager.Instance.radModel.ninja.url;
        string _title = personalStoryTitle.text;
        var str = PostsManager.Instance.GetCurrentLog().belongs_to_quest.Split('/');
        int questid = int.Parse(str[str.Length - 2]);
        PostsManager.Instance.ModifyPostLog(_title, personalpostLogs[0].text, "", "", ninja, questid,
            _privacy.options[_privacy.value].text , personalpostImages[0].sprite, null, null,false);

        personalLogs = false;
        MovePanel_Out_Right(singlePersonalPostContainer);
        singlePersonalPostContainer.gameObject.SetActive(false);
    }
    public void Button_ShowSiglePersonalLog()
    {
        singlePersonalPostContainer.gameObject.SetActive(true);
        MovePanel_In(singlePersonalPostContainer);
    }

    public void SetActionsList(Rad_Model.Action[] _actions)
    {
        for (int i = 0; i < _actions.Length; i++)
        {
            _actionsList.Add(_actions[i]);
        }

        _actionsList.Sort((s1, s2) => s1.timestamp.CompareTo(s2.timestamp));
    }

    public void AddActionToList(Rad_Model.Action _actions)
    {
        _actionsList.Add(_actions);

        _actionsList.Sort((s1, s2) => s1.timestamp.CompareTo(s2.timestamp));
    }

    private void SetHighlightColor(Image image)
    {
        chatButtonImage.color = Color.white;
        logsButtonImage.color = Color.white;
        profileButtonImage.color = Color.white;

        image.color = highlightedColor;
    }

    public void Button_BackToPhasesMap()
    {
        if (!string.IsNullOrEmpty(next_scene_name))
        {
            LoadingSceneManager.LoadScene(next_scene_name);
        }
    }

    private void OnGetPhaseStatusSuccess(string response)
    {
        CheckAchievements();
    }
    /// <summary>
    /// Method to check if the User has unlocked a badge on the saved on file achievements array.
    /// </summary>
    public void CheckAchievements()
    {
        for (int i = 0; i < badges.Length; i++)
        {
            if (BackendManager.Instance.radModel.phaseStatusList.Count > i && BackendManager.Instance.radModel.phaseStatusList[i].completed == true)
            {
                badges[i].SetActive(true);
            }
            else
            {
                badges[i].SetActive(false);
            }
        }

        //for (int i = 0; i < RadSaveManager.userAchievements.achievements.Length; i++)
        //{
        //    if (RadSaveManager.userAchievements.achievements[i] == true)
        //    {
        //        badges[i].SetActive(true);
        //    }
        //}

    }

    private void OnDestroy()
    {
        BackendManager.Instance.GetPhaseStatusSuccess -= OnGetPhaseStatusSuccess;
    }

    #region DoTween UI animations ==========================================================================

    private void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(0,panel.transform.localPosition.y), 0.25f); //To center position
    }

    private void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, panel.transform.localPosition.y), 0.25f); //To right position
    }

    private void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, panel.transform.localPosition.y), 0.25f); //To left position
    }

    #endregion ===============================================================================================


}
