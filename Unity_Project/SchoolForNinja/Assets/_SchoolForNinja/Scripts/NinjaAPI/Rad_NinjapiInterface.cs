﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using LitJson;
using TMPro;


/// <summary>
/// Ninjapi - Radical Graphics (C)2018
/// Interface to GET, POST all School for Ninja data
/// Parameters:
/// --------------
/// users
/// ninjas
/// coaches
/// organizations
/// quests
/// quest_statuses
/// post_logs
/// --------------
/// </summary>
public class Rad_NinjapiInterface: MonoBehaviour {

    public GameObject signInUIPanel;
    public GameObject ninjasUIPanel;
    public GameObject ninjaInfoRowPrefab;
    public Transform ninjaProfileRowsParent;




    private NinjaProfilePrefabLinks ninjaProfileUILinks;

    public void button_getNinjapiRoot()
    {
        BackendManager.Instance.GetNinjapiRoot();
    }

    public void button_getNinjas()
    {
        BackendManager.Instance.GetNinjas();
    }

    public void button_getQuests()
    {
        BackendManager.Instance.GetQuests();
    }

    /// <summary>
    /// Helper method to Instantiate a prefab holding all the Ninja Data, Avatar, Name, Gender, etc...
    /// </summary>
    /// <param name="ninja">Class Ninja var holding all the current Ninja (User) data</param>
    /// <param name="ninjaInfoRowPrefab">Prefab that will display all the Ninja data</param>
    public void CopyNinjaDataToUnityPrefab(Rad_Model.Ninja ninja)
    {
        GameObject newRow = Instantiate(ninjaInfoRowPrefab, ninjaProfileRowsParent);

        //Get component holding links to gameobjects which will display the Ninja data
        ninjaProfileUILinks = newRow.GetComponent<NinjaProfilePrefabLinks>();

        //Copy all the data
        ninjaProfileUILinks.url.text = ninja.url;
        //StartCoroutine(BackendManager.Instance.GetSprite(ninja.avatarURL, ninjaProfileUILinks.avatar));
        BackendManager.Instance.DecodeBase64Texture(ninja.avatar, ninjaProfileUILinks.avatar.sprite);
        ninjaProfileUILinks.ninjaName.text = ninja.ninja_name;
        ninjaProfileUILinks.gender.text = ninja.gender;
        ninjaProfileUILinks.level.text = ninja.level.ToString();
        ninjaProfileUILinks.domain.text = ninja.domain;
        ninjaProfileUILinks.power.text = ninja.power;
        //ninjaProfileUILinks.profilePrivacy.text = ninja.profilePrivacy;
    }

    public void Button_ChangeUserName()
    {
        BackendManager.Instance.ChangeUserName();
    }

    /// <summary>
    /// Activates the Login Panel - TODO -> This method should be in a kind of GUIManager.
    /// </summary>
    public void Button_EnableLoginPanel()
    {
        if (!signInUIPanel.activeInHierarchy)
        {
            signInUIPanel.SetActive(true);
        }
    }

    /// <summary>
    /// Method called by Scene "X" button on each panel.
    /// </summary>
    public void ClosePanel(GameObject panel)
    {
        if (panel.activeInHierarchy)
        {
            panel.SetActive(false);
        }
    }

    /// <summary>
    /// Method called by Scene Logout button
    /// Sends the logout api url to the backend manager.
    /// </summary>
    public void button_logout()
    {
        BackendManager.Instance.Logout();
    }

    

    public void button_changeUserName()
    {
        BackendManager.Instance.ChangeUserName();
    }



}
