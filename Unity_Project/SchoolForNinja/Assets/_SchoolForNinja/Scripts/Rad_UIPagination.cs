﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using DG.Tweening;

public class Rad_UIPagination : MonoBehaviour {

    public GameObject[] paginationList;
    public GameObject infoPage;
    [BoxGroup("Button Images")] public Sprite buttonBackgroundCurrent;
    [BoxGroup("Button Images")] public Sprite buttonBackgroundCompleted;

    //[BoxGroup("Button Images")] public Sprite buttonBackgroundDisabled;

    private Vector3 startingScale = new Vector3(0.75f, 0.75f, 0.75f);

    private bool[] pageAvailable = new bool[5];
    /// Active panel has button scaled and page number is displayed

    /// <summary>
    /// Change all buttons to the state they should have if User is in a given 'currentStep'
    /// i.e. User 'currentStep' is 3, Step 1 and 2 is in 'completed state',
    /// Step 3 is in 'current state',
    /// step 4 and on are in 'disabled state'
    /// </summary>
    public void CurrentStep(int currentStep)
    {
        Button button = null;
        Transform textChild = null;
        SetInfoPage(false);
        for (int i = 0; i < paginationList.Length; i++)
        {
            if (i < currentStep || (i > currentStep && pageAvailable[i]) == true)
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = true;
                button.transform.localScale = startingScale;
                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(false);
            }
            else if (i == currentStep)
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = true;

                button.transform.DOScale(new Vector3(1.25f,1.25f,1.25f),0.5f);

                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(true);
            }
            else
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = false;
                button.transform.localScale = startingScale;
                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(false);
            }
        }

    }


    public void CurrentStep(int currentStep, bool infoPageActive)
    {
        Button button = null;
        Transform textChild = null;
        SetInfoPage(infoPageActive);
        for (int i = 0; i < paginationList.Length; i++)
        {
            if (i < currentStep || (i > currentStep && pageAvailable[i]) == true)
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = true;
                button.transform.localScale = startingScale;
                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(false);
            }
            else if (i == currentStep)
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = true;

                if (!infoPageActive)
                {
                    button.transform.DOScale(new Vector3(1.25f, 1.25f, 1.25f), 0.5f);
                }
                else
                {
                    button.transform.localScale = startingScale;
                }
                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(true);
            }
            else
            {
                button = paginationList[i].GetComponent<Button>();
                button.interactable = false;
                button.transform.localScale = startingScale;
                textChild = button.gameObject.transform.GetChild(0);
                textChild.gameObject.SetActive(false);
            }
        }

    }


    public void SetInfoPage(bool _value)
    {
        if (_value)
        {
            infoPage.GetComponent<Button>().transform.DOScale(new Vector3(1.25f, 1.25f, 1.25f), 0.5f);
        }
        else
        {
            infoPage.GetComponent<Button>().transform.localScale = startingScale;
        }
    }


    public void SetPageCompleted(int _page, bool _value)
    {
        pageAvailable[_page] = _value;
    }

    public void SetCurrentPaginationButtons(int _amount)
    {
        for (int i = 0; i < paginationList.Length; i++)
        {
            if(i >= _amount)
            {
                paginationList[i].SetActive(false);
            }
            else
            {
                paginationList[i].SetActive(true);
            }
        }
    }

}
