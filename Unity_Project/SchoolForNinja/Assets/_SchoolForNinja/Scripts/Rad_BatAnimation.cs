﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rad_BatAnimation : MonoBehaviour {

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();

        StartCoroutine(BatWings());
    }



    private IEnumerator BatWings()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5,10));
            anim.SetTrigger("Wings");
        }
    }
}
