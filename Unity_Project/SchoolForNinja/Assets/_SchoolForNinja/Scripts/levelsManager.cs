﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine;

public class levelsManager : MonoBehaviour {

   
    public GameObject[] groupArray;

    public Image fader;

    public void Button_StartLevel1()
    {

        StartCoroutine(Launch_StartLevel1());
    }

    public void Button_Quest()
    {
        StartCoroutine(Launch_Quest());
    }
    
    public void Button_Storieslvl1()
    {
        StartCoroutine(Launch_Stories1());
    }

    public void Button_Communities()
    {
        StartCoroutine(Launch_Communities());
    }
    
    public void Button_Stories3()
    {
        StartCoroutine(Launch_3());
    }

    public void Button_Sensationeel()
    {
        StartCoroutine(Launch_Sensationeel());
    }

    public void Button_Write3()
    {
        StartCoroutine(Launch_Write3());
    }

    IEnumerator Launch_StartLevel1()
    {

        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("level");
        //levelGroupObject.SetActive(true);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }


    IEnumerator Launch_Quest()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("quest");
        //questGroupObject.SetActive(true);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Launch_Stories1()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("stories");
        //storiesGroupObjects.SetActive(true);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Launch_Communities()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("community");
        //storiesGroupObjects.SetActive(false);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Launch_3()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("3");
        //storiesGroupObjects.SetActive(false);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Launch_Write3()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("write");
        //storiesGroupObjects.SetActive(false);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator Launch_Sensationeel()
    {
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Switch_OffGroups();
        Switch_OnGroup("sensationeel");
        //storiesGroupObjects.SetActive(false);
        fader.DOFade(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
    }


    private void Switch_OffGroups()
    {
        for (int i = 0; i < groupArray.Length; i++)
        {
            groupArray[i].SetActive(false);
        }
    }

    private void Switch_OnGroup(string name)
    {

        for (int i = 0; i < groupArray.Length; i++)
        {
            if (((groupArray[i].name.Contains("Map")) && name.Contains("map")) || ((groupArray[i].name.Contains("Level1")) && name.Contains("level")) || ((groupArray[i].name.Contains("Quest")) && name.Contains("quest")) || ((groupArray[i].name.Contains("Stories")) && name.Contains("stories")) || ((groupArray[i].name.Contains("3")) && name.Contains("3")) || ((groupArray[i].name.Contains("Community")) && name.Contains("community")) || ((groupArray[i].name.Contains("Write")) && name.Contains("write")) || ((groupArray[i].name.Contains("Sensationeel")) && name.Contains("sensationeel")))


            {
                groupArray[i].SetActive(true);
            }
        }
                    
    }
}
