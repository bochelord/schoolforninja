﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishAnimationsController : MonoBehaviour {

    public GameObject[] fishes;

	// Use this for initialization
	void Start () {

        for (int i = 0; i < fishes.Length; i++)
        {
            StartCoroutine(GoFish(UnityEngine.Random.Range(0.1f, 2.5f), fishes[i], UnityEngine.Random.Range(-20f, 20f)));
        }
	
	}


    IEnumerator GoFish(float delay, GameObject fish, float yOffset)
    {
        yield return new WaitForSeconds(delay);
        fish.transform.position = new Vector3(fish.transform.position.x, fish.transform.position.y + yOffset, 0f);
        fish.SetActive(true);
    }
}
