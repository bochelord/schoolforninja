﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadSortingOrderWalkable : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            collision.GetComponent<SpriteRenderer>().sortingOrder = 5;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<SpriteRenderer>().sortingOrder = 2;
        }
    }

}
