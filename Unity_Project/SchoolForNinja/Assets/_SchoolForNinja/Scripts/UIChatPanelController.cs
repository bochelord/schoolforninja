﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;
public class UIChatPanelController : MonoBehaviour {

    public Image chatIcon;
    public GameObject onlineIcon;
    public bool isOnline;
    public TextMeshProUGUI chatTitle;
    public TextMeshProUGUI chatLastMessage; //It will display the content of the last message in the group;
    public TextMeshProUGUI chatLastMessageTimeStamp;

	void Start () {
		
        if (isOnline)
        {
            onlineIcon.SetActive(true);
        }
        else
        {
            onlineIcon.SetActive(false);
        }

	}
	
}
