﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NinjaProfilePrefabLinks : MonoBehaviour {

    public Text url, ninjaName, gender, level, domain, power, profilePrivacy;
    public Image avatar;

}
