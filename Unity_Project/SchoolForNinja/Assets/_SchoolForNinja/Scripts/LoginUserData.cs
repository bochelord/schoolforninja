﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginUserData : MonoBehaviour {

    public string username;
    public string password;
    public bool rememberMe;

    public void CreateNewLoginUserData(string usernamein, string passwordin)
    {
        username = usernamein;
        password = passwordin;
        rememberMe = false;
    }

}
