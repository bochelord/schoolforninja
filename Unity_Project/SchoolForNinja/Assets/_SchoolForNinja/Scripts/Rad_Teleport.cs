﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Rad_Teleport : MonoBehaviour {


    public bool fase2;
    public bool fase3;

    [ShowIf("fase2")]public PathsManagerFase2.TeleportSpots teleportTo;
    [ShowIf("fase3")]public PathsManagerFase3.TeleportSpots moveTo;
    private PathsManagerFase2 pathManager2;
    private PathsManagerFase3 pathManager3;

    private void Awake()
    {
        pathManager2 = FindObjectOfType<PathsManagerFase2>();
        pathManager3 = FindObjectOfType<PathsManagerFase3>();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (pathManager2)
        {
            if (collision.tag == "Player")
            {
                switch (teleportTo)
                {
                    case PathsManagerFase2.TeleportSpots.CaveEntrance:
                        pathManager2.TeleportPlayerToCaveEntrance();
                        break;
                    case PathsManagerFase2.TeleportSpots.CaveInside:
                        pathManager2.TeleportPlayerInsideCaveStart();
                        break;
                    case PathsManagerFase2.TeleportSpots.CaveExit:
                        pathManager2.TeleportPlayerToCaveExit();
                        break;
                    case PathsManagerFase2.TeleportSpots.CaveInsideExit:
                        pathManager2.TeleportPlayerInsideCaveExit();
                        break;
                }
            }
        }
        else if (pathManager3)
        {
            if (collision.tag == "Player")
            {
                pathManager3.MovePlayerTo(moveTo);
            }
        }
    }
}
