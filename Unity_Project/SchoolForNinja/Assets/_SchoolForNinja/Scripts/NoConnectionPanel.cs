﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class NoConnectionPanel : MonoBehaviour {

    
    private int numberOfTries = 0;

    public void BUTTON_TryAgain()
    {

        if(numberOfTries >= 3)
        {
            SceneManager.LoadScene("Sign_in");
        }
        numberOfTries++;
        
        BackendManager.Instance.CallLastAction();
        this.gameObject.SetActive(false);
    }
}
