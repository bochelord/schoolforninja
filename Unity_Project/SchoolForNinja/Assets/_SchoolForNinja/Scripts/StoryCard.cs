﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryCard : MonoBehaviour {

    public Image storyImage;
    public Text storyOwner;
    public Text storyDate;
    public Text storyContent;
    
    public void SetStoryImage(Image _newImage)
    {
        storyImage = _newImage;
    }    

    public void SetStoryOwner(string _owner)
    {
        storyOwner.text = _owner;
    }

    public void SetStoryDate(string _date)
    {
        storyDate.text = _date;
    }

    public void SetStoryContent(string _content)
    {
        storyContent.text = _content;
    }
}
