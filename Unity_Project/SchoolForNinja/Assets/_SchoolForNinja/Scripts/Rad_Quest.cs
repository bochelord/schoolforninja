﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
public class Rad_Quest : MonoBehaviour {

    public int questId;
    public int questSlot;
    //public string questName;
    //public Transform panelPosition;
    private SFN_GuiManager _guiManager;
    public bool showBubble = false;

    [ShowIf("showBubble")]
    public GameObject einsteinBubble;
    

    private void Awake()
    {
        _guiManager = FindObjectOfType<SFN_GuiManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            _guiManager.ShowStartQuestButton(questId, this.transform.position);
            QuestManagerV2.Instance.SetQuestSlot(questSlot);
            if(showBubble && einsteinBubble)
            {
                einsteinBubble.SetActive(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _guiManager.CloseAskQuestPanel();
            if (showBubble && einsteinBubble)
            {
                einsteinBubble.SetActive(false);
            }
        }
    }
}
