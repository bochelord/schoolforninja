﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.Net.Mail;
using System.Collections;

public class UIManager : MonoBehaviour {

    [Header("UI Panels")]
    public RectTransform signInPanel;
    public RectTransform signUpPanel;
    public RectTransform forgotPasswordPanel;
    public RectTransform resetPasswordPanel;

    [Space(5)]
    [Header("UI Panel - Login")]
    public TMP_InputField loginEmail;
    public TMP_InputField loginPassword;
    public Toggle loginRememberMe;
    public TMP_Text loginErrorsDisplay;
    public TMP_Text placeholderLogin;
    public Image spinner;


    [Header("UI Panel - Register")]
    public TMP_InputField signUpEmail;
    public TMP_InputField signUpPassword;
    public TMP_InputField signUpRePassword;
    public Toggle signUpAggrement;
    public Toggle privacyAggrement;
    public TMP_Text signUpErrorsDisplay;
    public Image spinnerRegistration;
    public TMP_Text signUpErrorsDisplayNinjaAggrement;
    public TMP_Text signUpErrorsDisplayPrivacyAggrement;
    [Header("UI Panel - Forgot password")]
    public TMP_InputField forgotPasswordEmail;
    public TMP_Text forgotPasswordErrorsDisplay;

    [Header("UI Panel - Reset password")]
    public TMP_InputField resetPasswordVerificationCode;
    public TMP_InputField resetPasswordNewPassword;
    public TMP_InputField resetPasswordRepeatNewPassword;
    public TMP_Text resetPasswordErrorsDisplay;

    [Header("Server Status Panel")]
    public RectTransform serverStatusPanel;
    public TMP_Text serverMessage;
    [Space(10)]

    public string next_scene_name;
    public string fromEmail;

    private MailAddress mailAddress;




    //void Awake()
    //{
        
    //}


    #region Events

    void OnLoginFailure(string response)
    {
        spinner.gameObject.SetActive(false);
        loginErrorsDisplay.gameObject.SetActive(true);
        //loginErrorsDisplay.text = "x  Invalid credentials";
        loginErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid credentials");
    }

    void OnLoginSuccess(string response)
    {
        spinner.gameObject.SetActive(false);
        loginErrorsDisplay.gameObject.SetActive(true);
        loginErrorsDisplay.text = "Welcome back!";
        LoadNextScene();
    }

    void OnRegistrationFailure(string response)
    {
        spinnerRegistration.gameObject.SetActive(false);
        signUpErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid credentials");
    }

    void OnRegistrationSuccess(string response)
    {
        spinnerRegistration.gameObject.SetActive(false);
        signUpErrorsDisplay.gameObject.SetActive(false);
        loginEmail.text = "";
        loginPassword.text = "";
        loginRememberMe.isOn = false;
        Button_ToSignIn();
    }

    void OnForgotPasswordSuccess(string response)
    {
        print("OnForgotPasswordSuccess Event has been triggered!");
        spinner.gameObject.SetActive(false); //TODO -> Add own spinner to Reset Password panel
        forgotPasswordErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Reset password email sent");

        //TODO -> Send user to Verification Code panel
    }

    void OnForgotPasswordFailure(string response)
    {
        print("OnForgotPasswordFailure Event has been triggered!");
        spinner.gameObject.SetActive(false); //TODO -> Add own spinner to Reset Password panel
        forgotPasswordErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("reset_password_email_sent");
    }
    #endregion


    // Use this for initialization
    void Start ()
    {
        //We subscribe to Login events
        BackendManager.Instance.LoginFailure += OnLoginFailure;
        BackendManager.Instance.LoginSuccess += OnLoginSuccess;
        BackendManager.Instance.RegistrationFailure += OnRegistrationFailure;
        BackendManager.Instance.RegistrationSuccess += OnRegistrationSuccess;
        BackendManager.Instance.ForgotPasswordSuccess += OnForgotPasswordSuccess;
        BackendManager.Instance.ForgotPasswordFailure += OnForgotPasswordFailure;

        RadSaveManager.LoadData();
        RadSaveManager.LoadLoginUserData();
        RadSaveManager.LoadSpriteListData();

        if (RadSaveManager.loginUserData.rememberMe)
        {
            loginRememberMe.isOn = true;
        }


        MovePanel_In(signInPanel);

        if (!RadSaveManager.loginUserData.username.Equals("") && loginRememberMe.isOn)
        {
            loginEmail.text = RadSaveManager.loginUserData.username;
            loginPassword.text = RadSaveManager.loginUserData.password;
        }

        BackendManager.Instance.GetServerStatus(GetServerStatusSuccess, GetServerStatusFail);   
        //next_scene_name = "PhasesMap";
    }

    private void GetServerStatusSuccess()
    {
        if (BackendManager.Instance.radModel.serverStatus.maintenance)
        {
            serverMessage.text = BackendManager.Instance.radModel.serverStatus.user_message;
            MovePanel_In(serverStatusPanel);
        }
    }

    private void GetServerStatusFail()
    {
        //TODO
    }

    private void LoadNextScene()
    {
        if (BackendManager.Instance.IsCoach())
        {
            LoadingSceneManager.LoadScene("CoachScene");
        }
        else if (BackendManager.Instance.radModel.ninja.first_time_connected)
        {
            LoadingSceneManager.LoadScene("SurveyScene");
        }
        else if (!string.IsNullOrEmpty(next_scene_name) && !BackendManager.Instance.radModel.ninja.first_time_connected)
        {
            LoadingSceneManager.LoadScene(next_scene_name);
        }
        else
        {
            Debug.LogError("RADError - next scene is undefined. Please define it in inspector on ManagerIntro");
        }

    }

    /// <summary>
    /// Method called by Scene Login button
    /// Calls
    /// </summary>
    public void Button_Login()
    {
        if (!IsEmailValid(loginEmail.text))
        {
            if (!loginErrorsDisplay.gameObject.activeInHierarchy)
            {
                loginErrorsDisplay.gameObject.SetActive(true);
                loginErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid email");
                //loginErrorsDisplay.text = "x  Invalid Email";
            }
        }
        else
        {

            spinner.gameObject.SetActive(true);
            if (loginRememberMe.isOn)
            {
                RadSaveManager.loginUserData.username = loginEmail.text;
                RadSaveManager.loginUserData.password = loginPassword.text;
                RadSaveManager.loginUserData.rememberMe = true;
                RadSaveManager.SaveData();
            }
            else
            {
                RadSaveManager.DeleteLoginUserData();
            }

            BackendManager.Instance.TokenLogin(loginEmail.text, loginPassword.text);
            print("CAlled TokenLogin with username:" + loginEmail.text + " and pass: " + loginPassword.text);
            
            //LoadNextScene();
        }
       

        

    }

    /// <summary>
    /// Method called by Sign Up button on "Sign_in" Scene
    /// </summary>
    public void Button_SignUp()
    {
        if (IsEmailValid(signUpEmail.text) && signUpPassword.text != "" && signUpRePassword.text != "" && signUpPassword.text.Equals(signUpRePassword.text) 
            && privacyAggrement.isOn && signUpAggrement.isOn)
        {

            spinnerRegistration.gameObject.SetActive(true);
            BackendManager.Instance.SignUp(signUpEmail.text, signUpPassword.text, signUpRePassword.text, signUpAggrement.isOn);

            RadSaveManager.loginUserData.username = signUpEmail.text;
            RadSaveManager.loginUserData.password = signUpPassword.text;
            RadSaveManager.SaveData();
        }
        else
        {
            

            if (!IsEmailValid(signUpEmail.text))
            {
                StartCoroutine(ShowRegistrationError(signUpErrorsDisplay.gameObject));
                signUpErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid email");
            }
            else if (signUpPassword.text == "")
            {
                StartCoroutine(ShowRegistrationError(signUpErrorsDisplay.gameObject));
                signUpErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid sign up password");
            }
            else if (!signUpPassword.text.Equals(signUpRePassword.text))
            {
                StartCoroutine(ShowRegistrationError(signUpErrorsDisplay.gameObject));
                signUpErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Passwords dont match");
            }

            if (!privacyAggrement.isOn)
            {
                
                StartCoroutine(ShowRegistrationError(signUpErrorsDisplayPrivacyAggrement.gameObject));
            }

            if (!signUpAggrement.isOn)
            {
                StartCoroutine(ShowRegistrationError(signUpErrorsDisplayNinjaAggrement.gameObject));
            }

        }
    }


    IEnumerator ShowRegistrationError(GameObject _error)
    {
        _error.SetActive(true);
        yield return new WaitForSeconds(3);
        _error.SetActive(false);
    }

    public void Button_SendEmailToResetPassword()
    {
        if (IsEmailValid(forgotPasswordEmail.text))
        {
            BackendManager.Instance.ResetPassword(forgotPasswordEmail.text, fromEmail);

            Button_ToResetPassword(); //Trigger animations.
        }
        else
        {
            resetPasswordErrorsDisplay.gameObject.SetActive(true);
            //resetPasswordErrorsDisplay.text = "X Invalid email";

            resetPasswordErrorsDisplay.text = "x " + I2.Loc.LocalizationManager.GetTranslation("Invalid email");
        }
    }

    public void Button_UpdateAccountWithNewPassword()
    {
        print("TODO! -> Call Server to update user account with given new password and check for verification code...");

        Button_ToForgotPassword();
    }

    #region DoTween UI animations ==========================================================================

    public void MovePanel_In(RectTransform panel)
    {
        panel.DOAnchorPos(Vector2.zero, 0.25f); //To center position
    }

    public void MovePanel_Out_Right(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(2000, 0), 0.25f); //To right position
    }

    public void MovePanel_Out_Left(RectTransform panel)
    {
        panel.DOAnchorPos(new Vector2(-2000, 0), 0.25f); //To left position
    }

    #endregion ===============================================================================================

    /// <summary>
    /// Called from Register UI panel
    /// "Back arrow button"
    /// </summary>
    public void Button_ToSignIn()
    {
        MovePanel_Out_Right(signUpPanel);
        MovePanel_Out_Right(forgotPasswordPanel);
        MovePanel_In(signInPanel);
    }

    public void Button_BackToForgotPassword()
    {
        MovePanel_Out_Right(resetPasswordPanel);
        MovePanel_In(forgotPasswordPanel);
    }

    /// <summary>
    /// Called from Login UI panel
    /// "Geen account? Registreer nu!" link
    /// </summary>
    public void Button_ToSignUp()
    {
        MovePanel_Out_Left(signInPanel);
        MovePanel_In(signUpPanel);
    }

    /// <summary>
    /// Called from Login UI panel
    /// </summary>
    public void Button_ToForgotPassword()
    {
        MovePanel_Out_Left(signInPanel);        //Cover use case if user clicks on signIn forgot password link
        MovePanel_Out_Right(resetPasswordPanel);//Cover use case if user clicks on Arrow back button on Reset Password panel
        MovePanel_In(forgotPasswordPanel);      //Wherever Forgot Password panel is, it goes to center
    }

    public void Button_ToResetPassword()
    {
        MovePanel_Out_Left(forgotPasswordPanel);//Cover use case if user clicks on Send Forgot Password button
        MovePanel_In(resetPasswordPanel);       //Moves reset password panel to the center
    }

    public bool IsEmailValid(string emailAddress)
    {
        try
        {
            mailAddress = new MailAddress(emailAddress.ToLower());
        }
        catch (Exception)
        {
            return false;
        }

        return true;
    }


    public void Button_ExitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Event call to remove the placeholder text when the user clicks on the input field...
    /// </summary>
    public void SelectEvent_PlaceholderEmailLogin()
    {

        placeholderLogin.text = "";
    }

    public void DeselectEvent_PlaceholderEmailLogin()
    {
        placeholderLogin.text = "EMAIL";
    }

    //public void Button_ResetPassword()
    //{
    //    BackendManager.Instance.ResetPassword();
    //}
}
