﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestUIManager : MonoBehaviour {

    public GameObject cameraPanel;

    public void Button_EnableCameraPanel()
    {
        if (!cameraPanel.activeInHierarchy)
        {
            cameraPanel.SetActive(true);
        }
    }

    public void Button_DisableCameraPanel()
    {
        if (cameraPanel.activeInHierarchy)
        {
            cameraPanel.SetActive(false);
        }
    }
}
