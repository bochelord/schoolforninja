﻿
public class QuestPage
{
    public enum Type { Explanation, Assignment, Log }
    public Type type;
    public string titleText;
    public string mainText;
    public string questionText;
    public string popupText;

    public QuestPage(Type type, string titleText, string mainText, string questionText, string popupText)
    {
        this.type = type;
        this.titleText = titleText;
        this.mainText = mainText;
        this.questionText = questionText;
        this.popupText = popupText;

        //get images and videos from database
    }
}