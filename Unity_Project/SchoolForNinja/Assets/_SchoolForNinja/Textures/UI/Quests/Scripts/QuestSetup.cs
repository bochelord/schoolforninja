﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class QuestSetup : MonoBehaviour {

    public TextMeshProUGUI titleText;
    public TextMeshProUGUI mainText;
    public TextMeshProUGUI questionText;
    public TextMeshProUGUI popupText;

    //get pageAmount from database
    int pageAmount = 1;
    QuestPage[] qp;

    private void Awake()
    {
        //all pages are setup

        qp = new QuestPage[pageAmount];
        qp[0] = new QuestPage(QuestPage.Type.Assignment, "Whaddup", "Main text goes here", "What is the answer?", "Can't help you, sorry");

    }

    private void Start()
    {
        titleText.SetText(qp[0].titleText);
        mainText.SetText(qp[0].mainText);
        questionText.SetText(qp[0].questionText);
        popupText.SetText(qp[0].popupText);
        
        //last page is loaded
    }

    public void NextPage()
    {
        //next page is determined and loaded
    }

}