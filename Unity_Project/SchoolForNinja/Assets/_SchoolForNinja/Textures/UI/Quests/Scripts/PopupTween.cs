﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PopupTween : MonoBehaviour {

    int moveX = 100;
    int moveY = 100;
    public float tweenDuration = 0.5f;

    public GameObject buttonMore;

    private float xShowPosition;
    private float yShowPosition;

    private float xHidePosition;
    private float yHidePosition;


    private void Awake()
    {
        xHidePosition = transform.position.x + moveX;
        yHidePosition = transform.position.y - moveY;

        xShowPosition = transform.position.x;
        yShowPosition = transform.position.y;
    }

    public void Start()
    {
        Hide();
    }

    public void Show()
    {
        DOTween.Kill(gameObject);
        transform.DOScaleX(1.1f, tweenDuration);
        transform.DOScaleY(1.1f, tweenDuration);
        transform.DORotate(Vector3.zero, tweenDuration);
        transform.DOMoveX(xShowPosition, tweenDuration);
        transform.DOMoveY(yShowPosition, tweenDuration).OnComplete(Scale);
    }

    private void Scale()
    {
        transform.DOScaleX(1.1f, .01f);
        transform.DOScaleY(1.1f, .01f).OnComplete(Overshoot);
    }

    private void Overshoot()
    {
        transform.DOScaleX(1f, .1f);
        transform.DOScaleY(1f, .1f);
    }

    public void Hide()
    {
        DOTween.Kill(gameObject);
        transform.DOScaleX(0, tweenDuration*.7f);
        transform.DOScaleY(0, tweenDuration*.7f);
        transform.DORotate(Vector3.forward * 90, tweenDuration);
        transform.DOMoveX(xHidePosition, tweenDuration);
        transform.DOMoveY(yHidePosition, tweenDuration).OnComplete(ButtonGrow);
        
    }

    private void ButtonGrow()
    {
        buttonMore.transform.DOScaleX(1.1f, .1f);
        buttonMore.transform.DOScaleY(1.1f, .1f).OnComplete(ButtonShrink);
    }

    private void ButtonShrink()
    {
        buttonMore.transform.DOScaleX(1f, .2f);
        buttonMore.transform.DOScaleY(1f, .2f);
    }

}
