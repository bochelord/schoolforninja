﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ProfileTween : MonoBehaviour {
    
    int moveY = 1400;
    int overshootY = 20;
    float tweenDuration = .25f;
    int moveBottomBarY = 300;

    public GameObject barMoving;
    public GameObject bottomBar;
    public GameObject buttonProfile;
    public GameObject buttonExitProfile;

    public void Show () {
        DOTween.KillAll();
        transform.DOScaleX(2.1f, tweenDuration);
        transform.DOScaleY(2.1f, tweenDuration).OnComplete(Overshoot);
        transform.DOMoveY(transform.position.y + moveY + overshootY, tweenDuration);

        barMoving.transform.DOMoveY(transform.position.y + moveY + overshootY, tweenDuration);
    }

    private void Overshoot()
    {
        transform.DOScaleX(2f, .1f);
        transform.DOScaleY(2f, .1f);
        transform.DOMoveY(transform.position.y - overshootY, .1f);

        barMoving.transform.DOMoveY(transform.position.y - overshootY, .1f);
        bottomBar.transform.DOMoveY(bottomBar.transform.position.y + moveBottomBarY, .1f).OnComplete(ExitButtonGrow);
    }

    public void Hide ()
    {
        DOTween.KillAll();
        transform.DOScaleX(1f, tweenDuration);
        transform.DOScaleY(1f, tweenDuration);
        transform.DOMoveY(transform.position.y - moveY, tweenDuration).OnComplete(ButtonGrow);
        buttonExitProfile.transform.DOScale(Vector3.zero, .02f);

        barMoving.transform.DOMoveY(transform.position.y - moveY, tweenDuration);
        bottomBar.transform.DOMoveY(bottomBar.transform.position.y - moveBottomBarY, .1f);
    }

    private void ButtonGrow()
    {
        buttonProfile.transform.DOScaleX(1.1f, .1f);
        buttonProfile.transform.DOScaleY(1.1f, .1f).OnComplete(ButtonShrink);
    }

    private void ButtonShrink()
    {
        buttonProfile.transform.DOScaleX(1f, .2f);
        buttonProfile.transform.DOScaleY(1f, .2f);
    }

    private void ExitButtonGrow()
    {
        buttonExitProfile.transform.DOScaleX(1.1f, .1f);
        buttonExitProfile.transform.DOScaleY(1.1f, .1f).OnComplete(ExitButtonShrink);
    }

    private void ExitButtonShrink()
    {
        buttonExitProfile.transform.DOScaleX(1f, .2f);
        buttonExitProfile.transform.DOScaleY(1f, .2f);
    }


}
