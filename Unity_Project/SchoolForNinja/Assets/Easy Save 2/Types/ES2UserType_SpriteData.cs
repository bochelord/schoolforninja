using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_SpriteData : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		SpriteData data = (SpriteData)obj;
		// Add your writer.Write calls here.
		writer.Write(data.spriteData);

	}
	
	public override object Read(ES2Reader reader)
	{
		SpriteData data = GetOrCreate<SpriteData>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		SpriteData data = (SpriteData)c;
		// Add your reader.Read calls here to read the data into the object.
		data.spriteData = reader.ReadDictionary<System.String, System.String>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_SpriteData():base(typeof(SpriteData)){}
}