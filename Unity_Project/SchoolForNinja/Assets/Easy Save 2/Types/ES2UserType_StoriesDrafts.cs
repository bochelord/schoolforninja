using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_StoriesDrafts : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		StoriesDrafts data = (StoriesDrafts)obj;
		// Add your writer.Write calls here.
		writer.Write(data.storiesData);

	}
	
	public override object Read(ES2Reader reader)
	{
		StoriesDrafts data = GetOrCreate<StoriesDrafts>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		StoriesDrafts data = (StoriesDrafts)c;
		// Add your reader.Read calls here to read the data into the object.
		data.storiesData = reader.ReadDictionary<System.Int32,System.String>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_StoriesDrafts():base(typeof(StoriesDrafts)){}
}