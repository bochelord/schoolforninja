using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_PlayerProfile : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		PlayerProfile data = (PlayerProfile)obj;
		// Add your writer.Write calls here.
		writer.Write(data.name);
		writer.Write(data.progress);
		writer.Write(data.level);
		writer.Write(data.questsDone);
		writer.Write(data.gameVersion);
		writer.Write(data.profileID);
		writer.Write(data.username);
		writer.Write(data.firstTimeInstalled);
		writer.Write(data.language);
		writer.Write(data.profileCreatedTimestamp);
		writer.Write(data.lastSaveTimestamp);

	}
	
	public override object Read(ES2Reader reader)
	{
		PlayerProfile data = new PlayerProfile();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		PlayerProfile data = (PlayerProfile)c;
		// Add your reader.Read calls here to read the data into the object.
		data.name = reader.Read<System.String>();
		data.progress = reader.Read<System.Single>();
		data.level = reader.Read<System.Int32>();
		data.questsDone = reader.ReadList<System.Int32>();
		data.gameVersion = reader.Read<System.String>();
		data.profileID = reader.Read<System.Int32>();
		data.username = reader.Read<System.String>();
		data.firstTimeInstalled = reader.Read<System.Boolean>();
		data.language = reader.Read<System.String>();
		data.profileCreatedTimestamp = reader.Read<System.DateTime>();
		data.lastSaveTimestamp = reader.Read<System.DateTime>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_PlayerProfile():base(typeof(PlayerProfile)){}
}