using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_LoginUserData : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		LoginUserData data = (LoginUserData)obj;
		// Add your writer.Write calls here.
		writer.Write(data.username);
		writer.Write(data.password);
		writer.Write(data.rememberMe);

	}
	
	public override object Read(ES2Reader reader)
	{
		LoginUserData data = GetOrCreate<LoginUserData>();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		LoginUserData data = (LoginUserData)c;
		// Add your reader.Read calls here to read the data into the object.
		data.username = reader.Read<System.String>();
		data.password = reader.Read<System.String>();
		data.rememberMe = reader.Read<System.Boolean>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_LoginUserData():base(typeof(LoginUserData)){}
}