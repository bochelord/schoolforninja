using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ES2UserType_UserAchievements : ES2Type
{
	public override void Write(object obj, ES2Writer writer)
	{
		UserAchievements data = (UserAchievements)obj;
		// Add your writer.Write calls here.
		writer.Write(data.achievements);

	}
	
	public override object Read(ES2Reader reader)
	{
		UserAchievements data = new UserAchievements();
		Read(reader, data);
		return data;
	}

	public override void Read(ES2Reader reader, object c)
	{
		UserAchievements data = (UserAchievements)c;
		// Add your reader.Read calls here to read the data into the object.
		data.achievements = reader.ReadArray<System.Boolean>();

	}
	
	/* ! Don't modify anything below this line ! */
	public ES2UserType_UserAchievements():base(typeof(UserAchievements)){}
}