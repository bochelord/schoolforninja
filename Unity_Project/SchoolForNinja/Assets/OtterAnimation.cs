﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class OtterAnimation : MonoBehaviour
{


    [BoxGroup("Otter")] public GameObject whirlAnimation;
    [BoxGroup("Otter")] public float whirlAnimationTime;



    public bool questCompleted = false;

    public void WhirlAnimation()
    {
        StartCoroutine(WhirlANimation(whirlAnimationTime));
    }

    private IEnumerator WhirlANimation(float _time)
    {
        GetComponent<SpriteRenderer>().enabled = false;
        whirlAnimation.SetActive(true);
        yield return new WaitForSeconds(_time);
        whirlAnimation.SetActive(false);
        GetComponent<SpriteRenderer>().enabled = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("On Trigger Enter" + questCompleted);
        if(collision.tag == "Player" && questCompleted)
        {
            FindObjectOfType<PathsManagerFase3>().OtterTrigger();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log("On Trigger Stay" + questCompleted);
        if (collision.tag == "Player" && questCompleted)
        {
            FindObjectOfType<PathsManagerFase3>().OtterTrigger();
        }
    }
}
