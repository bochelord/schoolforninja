﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Rad_MoveAnim : MonoBehaviour
{
    public Transform moveTo;

    private Vector2 init;

    private void Start()
    {
        init = this.transform.GetChild(0).localPosition;
        this.transform.GetChild(0).DOKill();
        this.transform.GetChild(0).DOMove(moveTo.position, 2).OnComplete(()=> 
        {
            this.transform.GetChild(0).localPosition = init;
        });
    }


}
