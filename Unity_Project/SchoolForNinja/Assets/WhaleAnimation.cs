﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WhaleAnimation : MonoBehaviour
{

    public bool doAnimation = true;
    // Start is called before the first frame update
    void Start()
    {
        Animation();

    }

    private void Animation()
    {
        this.transform.DORotate(new Vector3(0, 0, 3), 3).OnComplete(() =>
        {
            this.transform.DORotate(new Vector3(0, 0, 10), 3).OnComplete(() =>
            {
                if (doAnimation)
                {
                    Animation();
                }

            });
        });
    }

}
